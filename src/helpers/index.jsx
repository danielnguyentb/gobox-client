export Localize from './Localize'
export callApi from './callApi'
export * as Schemas from './Schemas'
export * as cookies from './cookie'
export * as formats from './formats'

export const serializeJSON = (data) => {
    return Object.keys(data).map(function (keyName) {
        return encodeURIComponent(keyName) + '=' + encodeURIComponent(data[keyName])
    }).join('&');
};
export const encode = (code) => encodeURIComponent(`%${typeof code == 'string' ? code.trim() : ''}%`.replace(/\\/g, ``));