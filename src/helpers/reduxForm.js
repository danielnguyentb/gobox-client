/**
 * Created by Tester-Ali on 01-12-2016.
 */
import Localize from '../../resources/Localize'

export const parseErrors = errors => {
    const submitErrors = {};
    if (errors.details) {
        const {messages} = errors.details;
        Object.keys(messages).map(field => {
            submitErrors[field] = (Localize.t(messages[field][0]))
        });
    }
    if (Object.keys(submitErrors).length === 0) {
        submitErrors['_error'] = Localize.t(errors.message)
    }
    return submitErrors;
};
