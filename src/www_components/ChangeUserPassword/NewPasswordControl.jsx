import React, {Component, PropTypes} from 'react'
import * as RBT from 'react-bootstrap'
import ReactPasswordStrength from '../PasswordStrength'

const NewPasswordControl = ({onChange, onFocus, error, autoFocus, scoreFn, validate}) => (
    <RBT.FormGroup controlId='' className="style-strength-pass">
        <RBT.ControlLabel>Mật khẩu mới</RBT.ControlLabel>
        {validate ?
        <ReactPasswordStrength
            minLength={6}
            minScore={2}
            scoreFn={scoreFn}
            scoreWords={['Quá lởm', 'Yếu', 'Đơn giản quá', 'Tốt', 'Rất tốt']}
            changeCallback={(e) => {
                let value = e.password;
                onChange({newPassword: value});
            }}
            inputProps={{className: "form-control", autoFocus, type: 'password', onFocus}}
        />
            :
            <div className="input-required">
                <input
                    type="password" className="form-control"
                    onFocus={onFocus}
                    onChange={e => onChange({newPassword: e.target.value})}/>
                <span className="required-symbol">*</span>
            </div>
        }
        {error &&
        <RBT.HelpBlock className="alert alert-warning">
            {error || 'Tối thiểu 6 ký tự, bao gồm chữ hoa và số'}
        </RBT.HelpBlock>
        }
    </RBT.FormGroup>
);


NewPasswordControl.propTypes = {
    onChange: PropTypes.func.isRequired,
    onFocus: PropTypes.func.isRequired,
    error: PropTypes.string,
    autoFocus: PropTypes.bool.isRequired,
};

export default NewPasswordControl