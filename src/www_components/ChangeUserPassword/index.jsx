import React, {Component, PropTypes} from 'react'
import * as RBT from 'react-bootstrap'
import NewPasswordControl from './NewPasswordControl'
import ConfirmPasswordControl from './ConfirmPasswordControl'
import Localize from '../../resources/Localize'
import zxcvbn from 'zxcvbn'

const scoreFn = (value) => {
    let result = zxcvbn(value);
    if (value.length == 0) {
        result.feedback.warning = 'Mật khẩu không được để trống';
    } else if (value.length < 6 || value.search(/[A-Z]/g) < 0 || value.search(/\d/g) < 0) {
        result.score = 0;
        result.feedback.warning = 'Tối thiểu 6 ký tự, bao gồm chữ hoa và số';
    }
    return result;
};
export default class ChangeUserPassword extends Component {
    static propTypes = {
        onChange: PropTypes.func.isRequired,
        requireCurrentPassword: PropTypes.bool.isRequired,
        validate: PropTypes.bool.isRequired,
    };

    constructor(props) {
        super(props);
        this.state = {
            currentPassword: '',
            newPassword: '',
            confirmPassword: '',
            errors: {},
        }
    }

    handleChange(data) {
        let newState = {
            ...this.state,
            ...data,
        };
        let errors = this.getErrors(newState);
        this.setState({...newState, errors});
        let {newPassword, currentPassword, confirmPassword} = newState;
        let isValid = false === Object.values(errors).some(err => err && err.length > 0);
        this.props.onChange({newPassword, currentPassword, confirmPassword, isValid});
    }

    onValidate() {
        this.setState({errors: this.getErrors(this.state)})
    }

    getErrors({newPassword, currentPassword, confirmPassword}) {
        let errors = {};
        let {validate, requireCurrentPassword} = this.props;
        if (validate) {
            errors['newPassword'] = Localize.t(scoreFn(newPassword).feedback.warning);
        }
        if (!newPassword) {
            errors['newPassword'] = 'Mật khẩu không được để trống'
        }
        if (requireCurrentPassword) {
            if (!currentPassword) {
                errors['currentPassword'] = 'Mật khẩu không được để trống';
            } else if (currentPassword === newPassword) {
                errors['newPassword'] = 'Mật khẩu mới không được trùng với mật khẩu hiện tại'
            }
        }
        if (newPassword != confirmPassword) {
            errors['confirmPassword'] = 'Mật khẩu không khớp'
        }
        return errors;
    }

    render() {
        let {requireCurrentPassword, validate} = this.props;
        let {errors} = this.state;
        return (
            <div>
                {requireCurrentPassword &&
                <RBT.FormGroup>
                    <RBT.ControlLabel>Mật khẩu hiện tại</RBT.ControlLabel>
                    <div className="input-required">
                        <input
                            autoFocus="true"
                            className="form-control" type="password"
                            onChange={e => this.handleChange({currentPassword: e.target.value})}/>
                        <span className="required-symbol">*</span>
                    </div>
                    {errors.currentPassword && <RBT.HelpBlock className="alert alert-warning">
                        {errors.currentPassword}
                    </RBT.HelpBlock>}
                </RBT.FormGroup>
                }
                <NewPasswordControl
                    autoFocus={!requireCurrentPassword}
                    error={errors.newPassword} onFocus={::this.onValidate}
                    scoreFn={scoreFn} validate={validate}
                    onChange={::this.handleChange}/>
                <ConfirmPasswordControl
                    error={errors.confirmPassword} onFocus={::this.onValidate}
                    onChange={::this.handleChange}/>
            </div>
        )
    }
}