import React, {Component, PropTypes} from 'react'
import * as RBT from 'react-bootstrap'
const ConfirmPasswordControl = ({onChange, onFocus, error}) => (
    <RBT.FormGroup controlId=''>
        <RBT.ControlLabel>Xác nhận mật khẩu mới</RBT.ControlLabel>
        <div className="input-required">
            <input
                type="password" className="form-control"
                name='passwordConfirm' onFocus={onFocus}
                onChange={e => onChange({confirmPassword: e.target.value})}/>
            <span className="required-symbol">*</span>
        </div>
        {error &&
        <RBT.HelpBlock className="alert alert-warning">
            {error}
        </RBT.HelpBlock>
        }
    </RBT.FormGroup>
);

ConfirmPasswordControl.propTypes = {
    onChange: PropTypes.func.isRequired,
    onFocus: PropTypes.func.isRequired,
    error: PropTypes.string,
};

export default ConfirmPasswordControl