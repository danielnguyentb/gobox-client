import React from 'react'
import moment from 'moment'
import avatar from './logo.png'
moment.locale('vi');
import Gravatar from 'react-gravatar'

export default ({histories}) => (
    <div className="col-md-12 col-xs-12 pt-15">
        {histories && histories.map(obj =>
            <div className={`${obj.context_type == 'CHAT' ? 'chat-items' : 'log-activity' }`} key={obj.id}>
                {obj.context_type == 'CHAT' &&
                <div className="avatar-chat">
                    {(obj.actionBy && obj.actionBy.username) ?
                        <Gravatar email={obj.actionBy.username}/>
                        :
                        <img src={obj.avatar ? obj.avatar : avatar} alt=""/>
                    }
                </div>
                }
                {obj.context_type == 'CHAT' &&
                <div>
                    <div className="content-chat">
                        <p className="mb-0">
                            <span
                                className={`fw-600 mr-10 ${obj.scope == 'EXTERNAL' ? 'text-primary' : ''}`}>
                                {obj.actionBy ? (obj.actionBy.fullName ? obj.actionBy.fullName + (obj.actionBy.username ? ' /' : '') : '') + (obj.actionBy.username ? ' @' + obj.actionBy.username : '') : ''}
                            </span>
                            <span className="text-grey fs-12">
                                {moment(obj.createdTime).format("H:mm Ngày DD/M/Y")}
                            </span>
                        </p>
                        <span>
                            {obj.context['_attributes'].description}
                        </span>
                    </div>
                    <div className="clearfix"></div>
                </div>
                }

                {(obj.context_type == 'ACTIVITY' || obj.context_type == 'LOG') &&
                <p className="mb-0 text-grey fs-12">
                    <span className="fw-600 mr-5">
                        {obj.actionBy ? (obj.actionBy.fullName ? obj.actionBy.fullName + (obj.actionBy.username ? ' /' : '') : '') + (obj.actionBy.username ? ' @' + obj.actionBy.username : '') : ''}
                    </span>
                    <span className="mr-10">{obj.context['_attributes'].description}</span>
                    <span>{moment(obj.createdTime).format("H:mm Ngày DD/M/Y")}</span>
                </p>
                }

            </div>
        )}
    </div>
);