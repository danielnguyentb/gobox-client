import React, {PropTypes} from 'react'
import {connect} from 'react-redux'
import ChatBoxForm from './ChatBoxForm'
import * as RBT from 'react-bootstrap'
import {reset} from 'redux-form'
import moment from 'moment'
moment.locale('vi');
import LogItem from './LogItem'

class ChatBox extends React.Component {
    constructor(props) {
        super(props);
    }
    static propTypes = {
        postChat: PropTypes.func.isRequired,
        objectId: PropTypes.number.isRequired,
        hasPermissionChatWithCustomer: PropTypes.bool.isRequired,
        histories: PropTypes.array,
        postChatSuccess: PropTypes.bool,
        profile: React.PropTypes.object
    };
    componentWillReceiveProps(nextProps) {
        let {postChatSuccess, reset} = nextProps;
        if(postChatSuccess) {
            reset("OrderChatForm");
        }
    }


    render() {
        let {histories, postChat, hasPermissionChatWithCustomer, profile} = this.props;
        return (
            <div className="panel panel-default no-shadow">
                <div className="row">
                    <LogItem {...this.props} />
                </div>
                {hasPermissionChatWithCustomer &&
                <div className="row">
                    <ChatBoxForm
                        onSubmit={postChat}
                        {...profile}
                    />
                </div>
                }
            </div>
        )
    }
}

const mapStateToProps = (state, props) => ({

});

export default connect(
    mapStateToProps, {reset}
)(ChatBox);