import React from 'react'
import {Field, reduxForm} from 'redux-form'
import * as RBT from 'react-bootstrap'
import avatar from './avatar.png'
import Gravatar from 'react-gravatar'

const ChatBoxForm = ({handleSubmit, submitting, pristine, error, username}) => (
    <form onSubmit={handleSubmit}>
        <div className="col-md-12 col-xs-12">
            <div className="input-chat">
                {/*<img src={avatar} alt="" />*/}
                <Gravatar email={username} />
                <button type="submit" className="btn btn-primary save-comment"
                        disabled={pristine || submitting}>
                    LƯU
                </button>
                <Field component={renderTextArea} name="message"
                    placeholder="Bình luận"
                    rows="2"
                    className="form-control txt-chat" />
                <div className="clearfix"></div>
            </div>
        </div>
    </form>
);

const validate = values => {
    const errors = {};
    if(Object.keys(values).length === 0) return errors;

    if (!values.message || values.message == '' || (values.message).replace(/^\s+|\s+$/gm,'') == 0) {
        errors.message = 'Nội dung chat không được để trống.'
    }
    return errors
};

export default reduxForm({
    form: 'OrderChatForm',
    validate
})(ChatBoxForm);



export const renderTextArea = ({input, className, onChange, placeholder, rows, meta: {touched, error, warning}}) => (
    <RBT.FormGroup validationState={touched && error ? 'error' : 'success'}>
         <textarea {...input}
                   placeholder={placeholder}
                   rows={rows}
                   className={className}>
         </textarea>
    </RBT.FormGroup>
);