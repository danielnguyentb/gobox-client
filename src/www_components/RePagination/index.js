import {RePagination} from './RePagination'
import reducer from './reducer'
const NAME = 'pagination';

export {RePagination, reducer, NAME}
export default RePagination;