import React, {Component, PropTypes} from 'react'
import './inline.scss'

export default class InlineEdit extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showInput: false,
        }
    }

    static propTypes = {
        inputProps: PropTypes.object.isRequired,
    };

    onBlur(e) {
        this.setState({showInput: false});
        if (this.props.inputProps.hasOwnProperty('onBlur')) {
            this.props.inputProps.onBlur(e);
        }
    }

    onEnter(e) {
        if (e.key == 'Enter') {
            this.setState({showInput: false});
            if (this.props.inputProps.hasOwnProperty('onEnter')) {
                this.props.inputProps.onEnter(e);
            }
        }
    }

    render() {
        let {showInput} = this.state;
        let {inputProps} = this.props;
        let {value, tabIndex = 0} = inputProps;
        return (
            <span className="input-edit">
                {showInput
                    ? <input className="editing" type="text" {...inputProps} onBlur={::this.onBlur} onKeyUp={::this.onEnter}/>
                    : <span tabIndex={tabIndex} onClick={e => this.setState({showInput: true})}>{value}</span>
                }
            </span>
        );
    }
}
