import React, {PropTypes} from 'react'
import {DateField, DatePicker} from 'react-date-picker'
import moment from 'moment'

const ReDatePicker = ({onChange, placeholder, className, defaultValue, updateOnDateClick, okButton, onOkClick, cancelButton, clearButton, todayButton}) => {
    defaultValue = defaultValue ? new Date(defaultValue) : null;
    return (
        <DateField
            locale="vi"
            className={className}
            defaultValue={defaultValue ? defaultValue.getTime() : null}
            updateOnDateClick={updateOnDateClick}
            onChange={e => {
                const time = moment(e, "DD/MM/YYYY HH:mm");
                if(e && time.isValid()) {
                    onChange(time.toISOString())
                } else {
                    onChange('')
                }
            }}
            placeholder={placeholder}
            dateFormat="DD/MM/YYYY HH:mm">
            <DatePicker
                dateFormat="DD/MM/YYYY HH:mm"
                okButton={okButton}
                onOkClick={onOkClick}
                okButtonText="Chọn"
                cancelButton={cancelButton}
                cancelButtonText="Thoát"
                clearButton={clearButton}
                clearButtonText="Xóa"
                todayButton={todayButton}
                todayButtonText="Hôm nay"
            />
        </DateField>
    )
};

ReDatePicker.defaultProps = {
    updateOnDateClick: false,
    className: 'form-control',
    onChange: () => {
    },
    placeholder: '',
    todayButton: false,
    clearButton: false
};

ReDatePicker.propTypes = {
    defaultValue: PropTypes.string,
    updateOnDateClick: PropTypes.bool,
    className: PropTypes.string,
    okButton: PropTypes.bool,
    onOkClick: PropTypes.func,
    onChange: PropTypes.func,
    cancelButton: PropTypes.bool,
    clearButton: PropTypes.bool,
    todayButton: PropTypes.bool,
    placeholder: PropTypes.string
};

export default ReDatePicker;