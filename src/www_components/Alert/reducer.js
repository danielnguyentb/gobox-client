/**
 * Created by Tester-Ali on 21-10-2016.
 */
import {combineReducers} from 'redux'

const error = (state, {errors = null}) => errors;

const ajaxType = (state = null, {type, ajax = false}) => ajax ? type : state;

export default combineReducers({ajaxType, error})