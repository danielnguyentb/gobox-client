/**
 * Created by Tester-Ali on 21-10-2016.
 */
import reducer from './reducer'
import * as constants from './constants'
import * as components from './components'

const NAME = 'alert';
export {reducer, NAME}
export {ErrorAlert, ErrorToast, SuccessAlert, SuccessToast} from './components'
export default {reducer, constants, components}