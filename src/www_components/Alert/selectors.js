/**
 * Created by saber on 22/10/2016.
 */
import { createSelector } from 'reselect'
import {NAME} from './constants'

export const alertSelector = state => state[NAME];