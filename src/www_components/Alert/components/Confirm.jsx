import React, {Component, PropTypes} from 'react'
import {toastr} from 'react-redux-toastr'
import {connect} from 'react-redux'
class Confirm extends Component {
    constructor(props){
        super(props);
        this.state
    }

    componentWillReceiveProps({message, onOk, ajaxType, dispatchTypes}) {
        if(message){
            toastr.confirm(message, onOk)
        }
        if (dispatchTypes.includes(ajaxType) && message) {
            toastr.error(message);
        }
    }

    static propTypes = {
        message: PropTypes.string.isRequired,
        dispatchTypes: PropTypes.array.isRequired,
    };

    render() {
        return null;
    }
}

const mapStateToProps = (state) => ({
    message: state.alert.message,
});

export default connect(mapStateToProps)(Confirm);