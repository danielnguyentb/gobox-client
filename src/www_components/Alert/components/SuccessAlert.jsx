import React, {Component, PropTypes} from 'react'
import {connect} from 'react-redux'
import {Alert} from 'react-bootstrap'

class SuccessAlert extends Component {
    constructor(props){
        super(props);
        this.state = {message: ''}
    }

    componentWillReceiveProps({ajaxType, dispatchTypes}) {
        if(ajaxType != this.props.ajaxType) {
            if (Object.keys(dispatchTypes).includes(ajaxType)) {
                this.setState({message: dispatchTypes[ajaxType]})
            }
        }
    }

    render() {
        let {message} = this.state;
        return message ? <Alert bsStyle="success">{message}</Alert> : null;
    }
}

SuccessAlert.propTypes = {
    dispatchTypes: PropTypes.array.isRequired,
};
const mapStateToProps = (state) => ({
    ajaxType: state.alert.ajaxType
});
export default connect(mapStateToProps)(SuccessAlert);