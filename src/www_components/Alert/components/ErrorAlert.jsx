import React, {Component, PropTypes} from 'react'
import {connect} from 'react-redux'
import Localize from '../../../resources/Localize'
import {Alert} from 'react-bootstrap'

class ErrorAlert extends Component {
    constructor(props) {
        super(props);
        this.state = {message: null}
    }

    componentWillReceiveProps({error, ajaxType, dispatchTypes}) {
        if (ajaxType != this.props.ajaxType) {
            if (dispatchTypes.includes(ajaxType) && error) {
                let message = '';
                if (error.details && error.details.messages) {
                    message = Object.values(error.details.messages).map(m => Localize.t(m)).join('<br/>');
                } else {
                    message = Localize.t(error.message);
                }
                this.setState({message})
            }
        }
    }

    render() {
        let {message} = this.state;
        return message ? <Alert bsStyle="danger">
            <span dangerouslySetInnerHTML={{__html: message}}/>
        </Alert> : null;
    }
}

ErrorAlert.propTypes = {
    dispatchTypes: PropTypes.array.isRequired,
};
const mapStateToProps = (state) => ({
    ajaxType: state.alert.ajaxType,
    error: state.alert.error,
});
export default connect(mapStateToProps)(ErrorAlert);