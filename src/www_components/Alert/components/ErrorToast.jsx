import React, {Component, PropTypes} from 'react'
import {connect} from 'react-redux'
import Localize from '../../../resources/Localize'
import {toastr} from 'react-redux-toastr'
import {alertSelector} from '../selectors'

class ErrorToast extends Component {

    componentWillReceiveProps({error, ajaxType, dispatchTypes}) {
        if (ajaxType != this.props.ajaxType) {
            let {code, statusCode, details, message} = error || {};
            if (dispatchTypes.includes(ajaxType) && code != 401 && statusCode != 401) {
                if (details && details.messages) {
                    message = Object.values(details.messages).map(m => Localize.t(m))[0];
                }
                if (message) {
                    message = Localize.t(message);
                    toastr.error(message);
                }
            }
        }
    }

    render() {
        return null;
    }
}

ErrorToast.propTypes = {
    dispatchTypes: PropTypes.array.isRequired,
};
const mapStateToProps = (state) => {
    return {
        ajaxType: alertSelector(state).ajaxType,
        error: alertSelector(state).error,
    }
};
export default connect(mapStateToProps)(ErrorToast);