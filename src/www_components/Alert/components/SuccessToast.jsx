import React, {Component, PropTypes} from 'react'
import {connect} from 'react-redux'
import {toastr} from 'react-redux-toastr'

class SuccessToast extends Component {

    componentWillReceiveProps({ajaxType, dispatchTypes}) {
        if (ajaxType != this.props.ajaxType) {
            if (Object.keys(dispatchTypes).includes(ajaxType)) {
                toastr.success(dispatchTypes[ajaxType]);
            }
        }
    }

    render() {
        return null;
    }
}

SuccessToast.propTypes = {
    dispatchTypes: PropTypes.object.isRequired,
};
const mapStateToProps = (state) => ({
    ajaxType: state.alert.ajaxType
});
export default connect(mapStateToProps)(SuccessToast);