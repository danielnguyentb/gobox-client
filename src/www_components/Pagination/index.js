import React, {PropTypes} from 'react'
import ReactPaginate from 'react-paginate'

const Pagination = ({currentPage, pageCount, onSubmit}) => {
    return (
        <ReactPaginate
            previousLabel="Trước"
            nextLabel="Tiếp"
            breakLabel={<span>...</span>}
            breakClassName="break-me"
            pageNum={pageCount}
            marginPagesDisplayed={2}
            pageRangeDisplayed={5}
            clickCallback={onSubmit}
            forceSelected={currentPage - 1}
            containerClassName="pagination"
            subContainerClassName="pages pagination"
            activeClassName="active"/>
    )
};
Pagination.propTypes = {
    currentPage: PropTypes.number.isRequired,
    pageCount: PropTypes.number.isRequired,
    onSubmit: PropTypes.func.isRequired,
};
export default Pagination;