/**
 * Created by Tester-Ali on 21-10-2016.
 */
import {combineReducers} from 'redux'

const error = (state, {error = null}) => error;

const actionType = (state = null, {type, ajax = false}) => ajax ? type : state;

export default combineReducers({actionType, error})