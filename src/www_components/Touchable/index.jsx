import React, {Component, PropTypes} from 'react'
import {Button} from 'react-bootstrap'
import {connect} from 'react-redux'

class Touchable extends Component {
    constructor(props) {
        super(props);
        this.state = {
            disabled: false,
        }
    }

    componentWillReceiveProps({actionType, actionTypes}) {
        if (actionTypes && actionTypes.includes(actionType)) {
            this.setState({disabled: false})
        }
    }

    handleClick(e) {
        if (this.state.disabled) {
            e.preventDefault()
        } else {
            this.setState({disabled: true});

            if (typeof this.props.onClick !== 'undefined') {
                this.props.onClick(e);
            }
        }
    }

    render() {
        let copy = {...this.props};
        delete copy['actionTypes'];
        delete copy['actionType'];
        delete copy['dispatch'];
        let {className = ''} = copy;
        if (this.state.disabled) {
            className += ' disabled';
        } else {
            className = className.replace('disabled', '');
        }
        return <Button
            {...copy}
            onClick={::this.handleClick}
            className={className}
        />
    }
}

Touchable.propTypes = {
    actionTypes: PropTypes.array.isRequired,
    onClick: PropTypes.func,
};

const mapStateToProps = (state) => ({
    actionType: state.touchable.actionType
});
export * as constants from './constants'
export reducer from './reducer'
export default connect(mapStateToProps)(Touchable);