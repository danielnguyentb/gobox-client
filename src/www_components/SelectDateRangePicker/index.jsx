import React from 'react'
import * as RBT from 'react-bootstrap'
import moment from 'moment'
import DateRangePicker from 'react-bootstrap-daterangepicker'
import 'react-bootstrap-daterangepicker/css/daterangepicker.css'


export default class SelectDateRangePicker extends React.Component {
    static propTypes = {
        onChange: React.PropTypes.func.isRequired,
        startDate: React.PropTypes.string,
        endDate: React.PropTypes.string,
        dateLabel: React.PropTypes.string
    };

    constructor(props) {
        super(props);
        moment.locale('vi');
        let timeFrom = moment().subtract(10, 'days').hour(0).minute(0).second(0);
        let timeTo = moment().hour(23).minute(59).second(59);
        this.state = {
            startDate: timeFrom,
            endDate: timeTo
        };
    }

    componentWillReceiveProps({startDate, endDate}){
        if(startDate != this.props.startDate || endDate!= this.props.endDate){
            this.setState({
                startDate: startDate ? moment(startDate) : this.state.startDate,
                endDate: endDate ? moment(endDate) : this.state.endDate
            })
        }
    }

    handleApply(event, picker) {
        this.setState({
            startDate: picker.startDate,
            endDate: picker.endDate,
        });
        this.props.onChange(picker)
    }

    handleCancel(){
        this.setState({label: ''});
        this.props.onChange({
            startDate: null,
            endDate: null
        })
    }

    render() {
        let {label, startDate, endDate} = this.state;
        let start = startDate.format('HH:mm DD/MM/YYYY');
        let end = this.state.endDate.format('HH:mm DD/MM/YYYY');
        if (this.props.startDate && this.props.endDate) {
            label = start + ' - ' + end;
            if (start === end) {
                label = start;
            }
        } else {
            label = '';
        }
        const {dateLabel} = this.props;

        return (
            <div className="form-group">
                {dateLabel && <label className="text-base fs-15">{dateLabel}</label>}
                <DateRangePicker
                    startDate={startDate}
                    endDate={endDate}
                    onApply={::this.handleApply}
                    onCancel={::this.handleCancel}
                    timePicker={true}
                    timePicker24Hour={true}
                    opens="left"
                    locale={{
                        format: "DD/MM/YYYY",
                        applyLabel: "Chọn",
                        cancelLabel: "Xóa"
                    }}>
                    <div className="input-group">
                        <input
                            onChange={e => {}}
                            type="text" className="form-control" value={label}/>
                        <span className="input-group-btn">
                            <RBT.Button className="default date-range-toggle btn-daterangepicker">
                              <i className="fa fa-calendar"/>
                            </RBT.Button>
                        </span>
                    </div>
                </DateRangePicker>
            </div>
        )
    }

}