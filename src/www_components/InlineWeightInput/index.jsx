import React, {Component, PropTypes} from 'react'
import WeightInput from '../WeightInput'
import {formats} from 'helpers'
import './inline.scss'

export default class InlineWeightInput extends Component {
    static propTypes = {
        onSubmit: PropTypes.func,
        value: PropTypes.number,
        disabled: PropTypes.bool,
        delimiter: PropTypes.string,
        className: PropTypes.string,
    };

    static defaultProps = {
        delimiter: '.',
        className: "form-control",
        disabled: false,
        onSubmit(){
        },
    };

    constructor(props) {
        super(props);
        this.state = {showInput: false}
    }

    handleEnter(value) {
        this.setState({showInput: false});
        const {onSubmit} = this.props;
        onSubmit(value)
    }

    render() {
        const {disabled, value, className} = this.props;
        const {showInput} = this.state;
        return (
            <span className={disabled ? '' : 'input-edit'}>
                {showInput
                    ? (
                    <WeightInput
                        value={value}
                        className={className}
                        autoFocus={true}
                        onEnter={::this.handleEnter}
                    />
                )
                    : (
                    <span
                        disabled={disabled}
                        onClick={e => {
                            this.setState({showInput: true})
                        }}>{value == null ? '--' : formats.formatNumber(value)}</span>
                )}
            </span>
        )
    }
}