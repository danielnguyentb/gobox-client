import React from 'react';
import * as RBT from 'react-bootstrap';
import Typeahead from 'react-bootstrap-typeahead';

class Typehead extends React.Component {
    static propTypes = {
        options: React.PropTypes.array.isRequired,
        onChange: React.PropTypes.func,
        onInputChange: React.PropTypes.func,
        placeholder: React.PropTypes.string,
        className: React.PropTypes.string
    };
    constructor(props) {
        super(props);
    }
    render() {
        let {input, options, type, className, onInputChange, onChange, placeholder, meta: {touched, error, warning}} = this.props;
        return (
            <RBT.FormGroup validationState={touched && error ? 'error' : ''}>
                <Typeahead {...input} type={type} className={className} placeholder={placeholder}
                    options={options}
                    onInputChange={onInputChange}
                    onChange={ (e) => {
                        const value = e[0] ? e[0].value : 0;
                        input.onChange(value);
                    }}
                   onBlur={ (e) => {
                       let value = onChange(e.target.value);
                       input.onBlur(value);
                   }}
                />
                {touched && ((error && <span className="help-block">{error}</span>) || (warning && <span>{warning}</span>))}
            </RBT.FormGroup>
        )
    }
}
export default (Typehead);