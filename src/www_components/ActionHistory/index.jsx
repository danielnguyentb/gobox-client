import React, {PropTypes} from 'react'
import moment from 'moment'
moment.locale('vi');

const SCOPE = {
    INTERNAL: 'INTERNAL',
    EXTERNAL: 'EXTERNAL'
};

class ActionHistory extends React.Component {
    constructor(props) {
        super(props);
    }
    static propTypes = {
        data: PropTypes.array.isRequired
    };
    render() {
        let {data} = this.props;
        if(!data) return null;
        return (
            <section>
                {data.map(obj =>
                    <p key={obj.id}>
                        <span className="mr-5">
                            <i className={`glyphicon glyphicon-globe fs-10 ${SCOPE.EXTERNAL == obj.scope ? '' : 'invisibility'}`} />
                            {moment(obj.createdTime).format("H:mm Ngày DD/M/Y")}
                        </span>
                        <span className="fw-600 mr-5">{obj.createdBy ? obj.actionBy.username : ''}</span>
                        <span>{obj.context['_attributes'].description}</span>
                    </p>
                )}
            </section>
        )
    }
}

export default (ActionHistory);