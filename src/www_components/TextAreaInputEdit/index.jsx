import React, {PropTypes} from 'react'
import './inline.scss'

export default class TextAreaInputEdit extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            disabled: true,
            inputValue: ''
        };
    }

    static propTypes = {
        value: PropTypes.string,
        name: PropTypes.string,
        onKeyUpFunc: PropTypes.func.isRequired
    };

    render() {
        let {value, name, onKeyUpFunc} = this.props;
        return (
            <span className="input-edit">
                {this.state.disabled
                    ?
                    <section>{value} <span onClick={(e) => this.setState({disabled : false})}></span> </section>
                :
                    <section>
                         <textarea
                             name={name}
                             onKeyUp={(e) => this.setState({inputValue: e.target.value})}
                             cols="30" rows="2"
                             defaultValue={value}
                             className="form-control">
                        </textarea>
                        <button className="btn btn-primary" disabled={this.state.disabled}
                                onClick={(e) => {
                                    onKeyUpFunc(this.state.inputValue);
                                    this.setState({disabled: true})
                                }}>
                            Lưu
                        </button>
                    </section>

                }
            </span>
        )
    }
}