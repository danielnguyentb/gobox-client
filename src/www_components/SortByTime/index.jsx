import React from 'react'
import * as RBT from 'react-bootstrap'
import './DateTime.scss'

export default class SortByTime extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            active: 'active',
        }
    }
    static propTypes = {
        onClick: React.PropTypes.func.isRequired,
        defaultValue: React.PropTypes.string
    };
    render() {
        let {onClick, defaultValue} = this.props;
        let {active} = this.state;
        return (
            <div className="btn-group btn-group-toggle sort-time mb-15 mr-15 ml-5">
                <RBT.ControlLabel className={`btn btn-default control-label ${defaultValue == 'asc' ? active : ''}`}>
                    <input value="" type="checkbox"
                        onClick={t => onClick({sort : 'asc', skip: 0})}
                    /> Cũ trước
                </RBT.ControlLabel>
                <RBT.ControlLabel className={`btn btn-default control-label ${defaultValue == 'desc' ? active : ''}`}>
                    <input value="" type="checkbox"
                           onClick={t => onClick({sort : 'desc', skip: 0})}
                    /> Mới trước
                </RBT.ControlLabel>
            </div>
        )
    }
}
