import React, {Component, PropTypes} from 'react'
import {FormGroup, ControlLabel} from 'react-bootstrap'

const renderDateField = ({input, meta: {touched, error, warning}}) => (
    <FormGroup validationState={touched && error ? 'error' : 'success'}>
        <input {...input} type="number" className="form-control" placeholder="Ngày"/>
    </FormGroup>
);
const renderMonthField = ({input, meta: {touched, error, warning}}) => (
    <FormGroup validationState={touched && error ? 'error' : 'success'}>
        <select
            {...input}
            className="form-control">
            <option value="">Tháng</option>
            <option value="1">Tháng 1</option>
            <option value="2">Tháng 2</option>
            <option value="3">Tháng 3</option>
            <option value="4">Tháng 4</option>
            <option value="5">Tháng 5</option>
            <option value="6">Tháng 6</option>
            <option value="7">Tháng 7</option>
            <option value="8">Tháng 8</option>
            <option value="9">Tháng 9</option>
            <option value="10">Tháng 10</option>
            <option value="11">Tháng 11</option>
            <option value="12">Tháng 12</option>
        </select>
    </FormGroup>
);
const renderYearField = ({input, meta: {touched, error, warning}}) => (
    <FormGroup validationState={touched && error ? 'error' : 'success'}>
        <input {...input} type="number" className="form-control" placeholder="Năm"/>
    </FormGroup>
);

export const renderDatetimeSelectField = (fields) => {
    const touched = fields.date.meta.touched && fields.month.meta.touched && fields.year.meta.touched;
    const errors = [fields.date.meta.error, fields.month.meta.error, fields.year.meta.error].filter(t => t);
    return (
        <div className="row">
            <div className="col-sm-4">
                {renderDateField(fields.date)}
            </div>
            <div className="col-sm-4">
                {renderMonthField(fields.month)}
            </div>
            <div className="col-sm-4">
                {renderYearField(fields.year)}
            </div>
            {touched && errors.length > 0 &&
            <div className="col-sm-12 has-error">
                <div className="help-block">
                    {errors.join(', ')}
                </div>
            </div>
            }
        </div>
    )
};


export const DatetimeSelection = ({date, month, year, error, onChange}) => (
    <div className="row">
        <div className="col-sm-4">
            <FormGroup validationState={touched && error ? 'error' : 'success'}>
                <input type="number" className="form-control" placeholder="Ngày" value={date}/>
            </FormGroup>
        </div>
        <div className="col-sm-4">
            <FormGroup validationState={touched && error ? 'error' : 'success'}>
                <select
                    value={month}
                    className="form-control">
                    <option value="">Tháng</option>
                    <option value="1">Tháng 1</option>
                    <option value="2">Tháng 2</option>
                    <option value="3">Tháng 3</option>
                    <option value="4">Tháng 4</option>
                    <option value="5">Tháng 5</option>
                    <option value="6">Tháng 6</option>
                    <option value="7">Tháng 7</option>
                    <option value="8">Tháng 8</option>
                    <option value="9">Tháng 9</option>
                    <option value="10">Tháng 10</option>
                    <option value="11">Tháng 11</option>
                    <option value="12">Tháng 12</option>
                </select>
            </FormGroup>
        </div>
        <div className="col-sm-4">
            <FormGroup validationState={touched && error ? 'error' : 'success'}>
                <input value={year} type="number" className="form-control" placeholder="Năm"/>
            </FormGroup>
        </div>
    </div>
);

export class DatetimeSelectionContainer extends Component {
    static propTypes = {
        value: PropTypes.oneOfType([
            PropTypes.string,
            PropTypes.object
        ]),
        onChange: PropTypes.func
    };

    constructor(props) {
        super(props);
        if (props.value) {
            const time = new Date(props.value);
            this.state = {
                datetime: props.value,
                date: time.getDate(),
                month: time.getMonth(),
                year: time.getFullYear(),
            }
        } else {
            this.state = {
                datetime: new Date(),
                date: null,
                month: null,
                year: null,
            }
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.value) {
            const time = new Date(props.value);
            this.setState({
                date: time.getDate(),
                month: time.getMonth(),
                year: time.getFullYear(),
            });
        }
    }

    handleChange(data) {
        const {onChange} = this.props;
        const {date, month, year, datetime} = {...this.state, ...data};
        if (date && month && year) {
            datetime.setDate(date);
            datetime.setMonth(month);
            datetime.setFullYear(year);
        }
        this.setState(data);
        onChange(datetime)
    }

    render() {
        const {date, month, year} = this.state;
        return (
            <DatetimeSelection
                date={date}
                month={month}
                year={year}
                onChange={::this.handleChange}
            />
        )
    }
}