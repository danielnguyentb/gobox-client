import React, {Component, PropTypes}  from 'react'
import * as RBT from 'react-bootstrap'

export class Confirm extends Component {
    static propTypes = {
        onConfirm: PropTypes.func.isRequired,
        message: PropTypes.string.isRequired,
        onCancel: PropTypes.func,
        title: PropTypes.string,
        buttons: PropTypes.object,
    };

    componentWillMount() {
        this.setState({showModal: true});
    }

    showModal() {
        this.setState({showModal: true});
    }

    closeModal() {
        this.setState({showModal: false});
    }

    render() {
        let {title, message, buttons = {}, onConfirm, onCancel} = this.props;
        let {confirm, cancel} = buttons;
        return (
            <RBT.Modal show={this.state.showModal} onHide={this.closeModal.bind(this)}>
                <RBT.Modal.Header closeButton>
                    <RBT.Modal.Title>{title ? title : ''}</RBT.Modal.Title>
                </RBT.Modal.Header>
                <RBT.Modal.Body>{message}</RBT.Modal.Body>
                <RBT.Modal.Footer>
                    <RBT.Button
                        bsStyle={confirm && confirm.style ? confirm.style : "primary"}
                        onClick={onConfirm}>
                        {confirm && confirm.label ? confirm.label : 'Đồng ý'}
                    </RBT.Button>
                    <RBT.Button
                        bsStyle={cancel && cancel.style ? cancel.style : "default"}
                        onClick={onCancel ? onCancel : this.closeModal.bind(this)}>
                        {cancel && cancel.label ? cancel.label : 'Đóng'}</RBT.Button>
                </RBT.Modal.Footer>
            </RBT.Modal>
        );
    }
}
export class Alert extends Component {
    static propTypes = {
        onCancel: PropTypes.func,
        title: PropTypes.string,
        message: PropTypes.string,
        buttons: PropTypes.object,
    };

    componentWillMount() {
        this.setState({showModal: true});
    }

    showModal() {
        this.setState({showModal: true});
    }

    closeModal() {
        this.setState({showModal: false});
    }

    render() {
        let {title, message, buttons = {}, onCancel} = this.props;
        let {cancel} = buttons;
        return (
            <RBT.Modal show={this.state.showModal} onHide={this.closeModal.bind(this)}>
                {title &&
                <RBT.Modal.Header closeButton>
                    <RBT.Modal.Title>title</RBT.Modal.Title>
                </RBT.Modal.Header>
                }
                <RBT.Modal.Body>{message ? message : 'Có lỗi xảy ra, vui lòng liên hệ kĩ thuật để được xử lý!'}</RBT.Modal.Body>
                <RBT.Modal.Footer>
                    <RBT.Button
                        bsStyle={cancel && cancel.style ? cancel.style : "default"}
                        onClick={onCancel ? onCancel : this.closeModal.bind(this)}>
                        {cancel && cancel.label ? cancel.label : 'Đóng'}</RBT.Button>
                </RBT.Modal.Footer>
            </RBT.Modal>
        );
    }
}
export class Prompt extends Component {
    static propTypes = {
        children: PropTypes.node.isRequired,
        onConfirm: PropTypes.func.isRequired,
        onCancel: PropTypes.func,
        title: PropTypes.string,
        buttons: PropTypes.object,
    };

    componentWillMount() {
        this.setState({showModal: true});
    }

    showModal() {
        this.setState({showModal: true});
    }

    closeModal() {
        this.setState({showModal: false});
    }

    render() {
        let {title, children, buttons = {}, onConfirm, onCancel} = this.props;
        let {confirm, cancel} = buttons;
        return (
            <RBT.Modal show={this.state.showModal} onHide={this.closeModal.bind(this)}>
                <RBT.Modal.Header closeButton>
                    <RBT.Modal.Title>{title ? title : ''}</RBT.Modal.Title>
                </RBT.Modal.Header>
                <RBT.Modal.Body>{children}</RBT.Modal.Body>
                <RBT.Modal.Footer>
                    <RBT.Button
                        bsStyle={confirm && confirm.style ? confirm.style : "primary"}
                        onClick={onConfirm}>
                        {confirm && confirm.label ? confirm.label : 'Đồng ý'}
                    </RBT.Button>
                    <RBT.Button
                        bsStyle={cancel && cancel.style ? cancel.style : "default"}
                        onClick={onCancel ? onCancel : this.closeModal.bind(this)}>
                        {cancel && cancel.label ? cancel.label : 'Đóng'}</RBT.Button>
                </RBT.Modal.Footer>
            </RBT.Modal>
        );
    }
}

export default {
    Confirm,
    Alert,
    Prompt
}