import React from 'react'
import CountDown from './src/CountDown'

export default class CountDownTimer extends React.Component {
    static propTypes = {
        options: React.PropTypes.array.isRequired
    };
    render() {
        let {options} = this.props;
        return (
            <CountDown options={options} />
        )
    }
}