let DateBetween = function(startDate, endDate) {
    let second = 1000;
    let minute = second * 60;
    let hour = minute * 60;
    let day = hour * 24;
    let distance = endDate - startDate;

    if (distance < 0) {
        return "hết thời gian";
    }

    let days = Math.floor(distance / day);
    let hours = Math.floor((distance % day) / hour);
    let minutes = Math.floor((distance % hour) / minute);
    let seconds = Math.floor((distance % minute) / second);

    var day_description;
    var hour_description;
    var min_description;
    var sec_description;

    if(days == 1) {
        day_description = ' ngày ';
    } else {
        day_description = ' ngày ';
    }

    if(hours == 1) {
        hour_description = ' giờ ';
    } else {
        hour_description = ' giờ ';
    }

    if(minutes == 1) {
        min_description = ' phút ';
    } else {
        min_description = ' phút ';
    }

    if(seconds == 1) {
        sec_description = ' giây';
    } else {
        sec_description = ' giây';
    }

    let between = days > 0 ? days + day_description : '';
    between += hours + hour_description;
    between += minutes + min_description;
    // between += seconds + sec_description;

    return between;
};

module.exports = DateBetween;