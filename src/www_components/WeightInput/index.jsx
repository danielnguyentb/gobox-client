import React, {Component, PropTypes} from 'react'

export default class WeightInput extends Component {
    static propTypes = {
        onChange: PropTypes.func,
        onBlur: PropTypes.func,
        onEnter: PropTypes.func,
        value: PropTypes.number,
        autoFocus: PropTypes.bool,
        delimiter: PropTypes.string
    };

    static defaultProps = {
        className: '',
        type: 'text',
        delimiter: ',',
        autoFocus: false,
        disabled: false,
        onBlur(){
        },
        onEnter(){
        },
        onChange(){
        }
    };

    constructor(props) {
        super(props);
        this.state = {mark: this.getFormatValue(props.value)}
    }

    componentDidUpdate(prevProps) {
        const {value} = this.props;
        if (value != prevProps.value) {
            this.setState({mark: this.getFormatValue(value)})
        }
    }

    handleChange(value) {
        const {onChange} = this.props;
        this.setState({mark: this.getMark(value)});
        onChange(this.getRawValue(value));
    }

    handleBlur(value) {
        const {onBlur} = this.props;
        this.setState({mark: this.getMark(value)});
        onBlur(this.getRawValue(value));
    }

    handleEnter(value){
        const {onEnter} = this.props;
        this.setState({mark: this.getMark(value)});
        onEnter(this.getRawValue(value));
    }

    getValues(value){
        const values = value.split(/[.,]/).map(t => t.replace(/\D/g, ''));
        const [first, ...rest] = values;
        return [first, rest.join('').substr(0, 3), rest.length > 0];
    }

    getMark(value){
        const [first, second, decimal] = this.getValues(value);
        const {delimiter} = this.props;
        return first + (decimal ? delimiter + second : second);
    }

    getRawValue(value) {
        const values = this.getValues(value);
        return parseFloat(values[0] + '.' + (values[1] || '0'));
    }

    getFormatValue(number) {
        const {delimiter} = this.props;
        if(number == null) return '';
        return String(number).replace(/[.,]/g, delimiter);
    }

    render() {
        const input = {...this.props};
        delete input['delimiter'];
        delete input['onEnter'];
        const {mark} = this.state;
        return (
            <input
                {...input}
                type={input.type}
                className={input.className}
                value={mark}
                onKeyUp={e => {
                    if(e.key == 'Enter'){
                        this.handleEnter(e.target.value)
                    }
                }}
                onBlur={e => this.handleBlur(e.target.value)}
                onChange={e => this.handleChange(e.target.value)}/>
        )
    }
}