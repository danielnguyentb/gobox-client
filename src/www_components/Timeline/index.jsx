import React, {PropTypes} from 'react'
import moment from 'moment'
moment.locale('vi');

class TimeLine extends React.Component {
    constructor(props) {
        super(props);
    }
    static propTypes = {
        data: PropTypes.array.isRequired
    };
    render() {
        let {data} = this.props;
        if(!data) return null;
        return (
            <div className="col-md-12 col-xs-12">
                <div className="widget no-border">
                    <ul className="activities list-unstyled mb-0">
                        {data.map(obj =>
                            <li className="activity-info" key={obj.label}>
                                <div className="media">
                                    <div className="media-left">
                                        <span>{moment(obj.time).format("H:mm DD/M")}</span>
                                    </div>
                                    <div className="media-body">
                                        <span className="media-heading">{obj.label}</span>
                                    </div>
                                </div>
                            </li>
                        )}
                    </ul>
                </div>
            </div>
        )
    }
}
export default (TimeLine);