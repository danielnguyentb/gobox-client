import React from 'react'
import moment from 'moment'
import {DateField, Calendar, DatePicker} from 'react-date-picker'
 moment.locale("vi");

export default class DatetimepickerFromToDate extends React.Component {
    constructor(props) {
        super(props);
    }
    static propTypes = {
        onSelect: React.PropTypes.func.isRequired,
        fromValue: React.PropTypes.string,
        toValue: React.PropTypes.string,
        dateFormat: React.PropTypes.string
    };
    handleFormatDate(date) {
        if(!date || date == '') return '';
        let {dateFormat} = this.props;
        if(!dateFormat) dateFormat = "DD/MM/YYYY HH:mm";
        return moment(date, dateFormat).toISOString();
    }
    render() {
        let {onSelect, fromValue, toValue, dateFormat} = this.props;
        if(!dateFormat) dateFormat = "DD/MM/YYYY HH:mm";
        let dateFrom = fromValue ? new Date(fromValue) : null;
        let dateTo = toValue ? new Date(toValue) : null;
        return (
            <div>
                <DateField
                    locale="vi"
                    className="small-datetime-picker"
                    defaultValue={dateFrom ? dateFrom.getTime() : null}
                    dateFormat={dateFormat}
                    placeholder="Từ"
                    onChange={t => onSelect({fromTime: this.handleFormatDate(t), skip: 0})}>
                    <DatePicker
                              okButtonText="Chọn"
                              cancelButtonText="Hủy"
                              todayButtonText="Hôm nay"
                              clearButton={false}
                              dateFormat={dateFormat}
                    />
                </DateField>
                <DateField
                    className="small-datetime-picker"
                    dateFormat={dateFormat}
                    defaultValue={dateTo ? dateTo.getTime() : null}
                    placeholder="Tới"
                    onChange={t => onSelect({toTime: this.handleFormatDate(t), skip: 0})}>
                    <DatePicker className="small-datetime-picker"
                              dateFormat={dateFormat}
                              cancelButtonText="Hủy"
                              okButtonText="Chọn"
                              todayButtonText="Hôm nay"
                              clearButton={false}
                    />
                </DateField>
            </div>
        )
    }
}