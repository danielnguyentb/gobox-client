import React from 'react';
import InlineNumericEdit from '../index';
import {shallow} from 'enzyme';
import renderer from 'react-test-renderer';

const defaultProps = {
};

it('renders correctly', () => {
    const tree = renderer.create(
        <InlineNumericEdit {...defaultProps} />
    ).toJSON();
    expect(tree).toMatchSnapshot();
});

it('InlineNumericEdit keep the empty after click', () => {
    // Render a checkbox with label in the document
    const inline = shallow(
        <InlineNumericEdit />
    );

    expect(inline.text()).toEqual('');

    inline.simulate('click');

    expect(inline.find('input')).toBeTruthy();
    expect(inline.find('input').first().text()).toEqual('');
});