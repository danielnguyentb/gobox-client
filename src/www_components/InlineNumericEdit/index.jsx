import React, {Component, PropTypes} from 'react'
import SimpleCurrencyInput from '../SimpleCurrencyInput'
import './inline.scss'

export default class InlineNumericEdit extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showInput: false,
        }
    }

    static propTypes = {
        onSubmit: PropTypes.func,
        onChange: PropTypes.func,
        value: PropTypes.number,
        unit: PropTypes.string,
        disabled: PropTypes.bool,
        tabIndex: PropTypes.number,
        activeClassName: PropTypes.string,
    };

    render() {
        let {showInput} = this.state;
        let {onSubmit, onChange, value, unit = '', tabIndex = 0, disabled = false, activeClassName} = this.props;
        let displayValue = value ? (new Intl.NumberFormat('vi-VN')).format(value) : '';
        if (disabled) return <span>{displayValue}</span>;
        return (
            <span className="input-edit">
                {showInput
                    ?
                    <SimpleCurrencyInput
                        value={value}
                        precision={0}
                        separator=''
                        delimiter='.'
                        unit={unit}
                        onInputChange={onChange}
                        activeClassName={activeClassName}
                        onInputBlur={e => {
                            onSubmit(e);
                            this.setState({showInput: false})
                        }}
                        onInputEnter={e => {
                            onSubmit(e);
                            this.setState({showInput: false})
                        }}
                    />
                    : <span
                    tabIndex={tabIndex}
                    disabled={disabled}
                    onClick={e => this.setState({showInput: true})}>{displayValue}</span>
                }
            </span>
        );
    }
}
