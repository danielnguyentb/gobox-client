import React from 'react'
import lodash from 'lodash'

export default class ChoosePriceRange extends React.Component {
    static propTypes = {
        onBlur: React.PropTypes.func,
        onEnter: React.PropTypes.func,
        onChange: React.PropTypes.func,
        onChangeOperator: React.PropTypes.func,
        onChangeFrom: React.PropTypes.func,
        onChangeTo: React.PropTypes.func,
        operator: React.PropTypes.string,
        object: React.PropTypes.string.isRequired,
        fromValue: React.PropTypes.number,
        toValue: React.PropTypes.number,
        placeholder: React.PropTypes.string
    };

    static defaultProps = {
        onChangeOperator(e){},
        onChangeFrom(){},
        onChangeTo(){},
        onChange(){},
        onBlur(){},
        onChange(){},
    };

    constructor(props) {
        super(props);
        const {operator, fromValue, toValue}= props;
        this.state = {
            selected: operator || 'eq',
            from: fromValue || '',
            to: toValue || '',
        }
    }

    componentWillReceiveProps({operator, fromValue, toValue}) {
        this.setState({
            selected: operator || 'eq',
            from: fromValue || '',
            to: toValue || '',
        })
    }

    handleAction(callback) {
        if (typeof callback !== 'function') return;
        const {selected, from, to} = this.state;
        const {object} = this.props;
        if ((!from && !to) || (from && to)) {
            callback({
                object, from, to,
                operator: selected,
            })
        }
    }

    handleChangeSelect(e) {
        const selected = e.target.value;
        this.setState({selected});
        const {from, to} = this.state;
        const {object, onChange} = this.props;
        if ((!from && !to) || (from && to)) {
            onChange({
                object, from, to,
                operator: selected,
            })
        }
    }


    render() {
        const {
            onBlur, onEnter, object, placeholder, operator,
            onChangeOperator, onChangeFrom, onChangeTo
        } = this.props;
        const {selected, from, to} = this.state;
        return (
            <div>
                <select
                    className="form-control mb-15 pull-left opt-search-time"
                    onChange={e => {
                        onChangeOperator(e);
                        this.handleChangeSelect(e);
                    }}
                    value={operator}>
                    <option value="eq">=</option>
                    <option value="lte">&lt;=</option>
                    <option value="lt">&lt;</option>
                    <option value="gte">&gt;=</option>
                    <option value="gt">&gt;</option>
                    <option value="between">Khoảng</option>
                </select>

                {selected !== 'between' ?
                    <div className="btn-group btn-group-toggle user-status mb-15 mr-5">
                        <input
                            className="form-control input-balance"
                            type="number"
                            value={from}
                            placeholder={placeholder}
                            onChange={e => {
                                this.setState({from: e.target.value, to: e.target.value});
                                onChangeFrom(e);
                            }}
                            onBlur={e => this.handleAction(onBlur)}
                            onKeyUp={e => {
                                if (e.key == 'Enter') {
                                    this.handleAction(onEnter)
                                }
                            }}
                        />
                    </div>
                    :
                    <div>
                        <div className="btn-group btn-group-toggle user-status mb-15 mr-5">
                            <input className="form-control input-balance" placeholder="Từ"
                                   value={from}
                                   onBlur={e => {
                                       this.handleAction(onBlur)
                                   }}
                                   onKeyUp={e => {
                                       if (e.key === 'Enter') {
                                           this.handleAction(onEnter)
                                       }
                                   }}
                                   onChange={e => {
                                       this.setState({from: e.target.value});
                                       onChangeFrom(e)
                                   }}
                            />
                        </div>
                        <div className="btn-group btn-group-toggle user-status mb-15">
                            <input className="form-control input-balance" placeholder="đến"
                                   value={to}
                                   onBlur={e => {
                                       this.handleAction(onBlur)
                                   }}
                                   onKeyUp={e => {
                                       if (e.key === 'Enter') {
                                           this.handleAction(onEnter)
                                       }
                                   }}
                                   onChange={e => {
                                       this.setState({to: e.target.value});
                                       onChangeTo(e);
                                   }}
                            />
                        </div>
                    </div>
                }
            </div>
        )
    }
}
