module.exports = [
    {
        code: "VBSP",
        name: "Ngân hàng Chính sách Xã hội Việt Nam",
        nameEng: "Vietnam Bank for Social Policies",
    },
    {
        code: "VDB",
        name: "Ngân hàng Phát triển Việt Nam",
        nameEng: "The Vietnam Development Bank",
    },
    {
        code: "CB",
        name: "Ngân hàng Xây dựng",
        nameEng: "Construction Bank",
    },
    {
        code: "Oceanbank",
        name: "Ngân hàng Đại Dương",
        nameEng: "Ocean Bank",
    },
    {
        code: "GPBank",
        name: "Ngân hàng Dầu Khí Toàn Cầu",
        nameEng: "Global Petro Bank",
    },
    {
        code: "Agribank",
        name: "Ngân hàng Nông nghiệp và Phát triển Nông thôn VN",
        nameEng: "Vietnam Bank for Agriculture and Rural Development",
    },
    {
        code: "ACB",
        name: "Ngân hàng Á Châu",
        nameEng: "Asia Commercial Joint Stock Bank",
    },
    {
        code: "TPBank",
        name: "Ngân hàng Tiên Phong",
        nameEng: "Tien Phong Bank",
    },
    {
        code: "DAF",
        name: "Ngân hàng Đông Á",
        nameEng: "DongA Bank",
    },
    {
        code: "SeABank",
        name: "Ngân hàng Đông Nam Á",
        nameEng: "Southest Asia Bank",
    },
    {
        code: "ABBANK",
        name: "Ngân hàng An Bình",
        nameEng: "An Binh Bank",
    },
    {
        code: "BacABank",
        name: "Ngân hàng Bắc Á",
        nameEng: "Bac A Bank",
    },
    {
        code: "VietCapitalBank",
        name: "Ngân hàng Bản Việt",
        nameEng: "Viet Capital Bank",
    },
    {
        code: "Maritime Bank, MSB",
        name: "Hàng Hải Việt Nam",
        nameEng: "",
    },
    {
        code: "Techcombank",
        name: "Kỹ Thương Việt Nam",
        nameEng: "",
    },
    {
        code: "KienLongBank",
        name: "Kiên Long",
        nameEng: "",
    },
    {
        code: "Nam A Bank",
        name: "Nam Á",
        nameEng: "",
    },
    {
        code: "National Citizen Bank, NCB",
        name: "Quốc Dân",
        nameEng: "",
    },
    {
        code: "VPBank",
        name: "Việt Nam Thịnh Vượng",
        nameEng: "",
    },
    {
        code: "HDBank",
        name: "Phát triển Thành phố Hồ Chí Minh",
        nameEng: "",
    },
    {
        code: "Orient Commercial Bank, OCB",
        name: "Phương Đông",
        nameEng: "",
    },
    {
        code: "Military Bank, MBB",
        name: "Quân đội",
        nameEng: "",
    },
    {
        code: "PVcom Bank",
        name: "Đại chúng",
        nameEng: "",
    },
    {
        code: "VIBBank, VIB",
        name: "Quốc tế",
        nameEng: "",
    },
    {
        code: "Sài Gòn, SCB",
        name: "Sài Gòn",
        nameEng: "",
    },
    {
        code: "Saigonbank, SGB",
        name: "Sài Gòn Công Thương",
        nameEng: "",
    },
    {
        code: "SHBank, SHB",
        name: "Sài Gòn-Hà Nội",
        nameEng: "",
    },
    {
        code: "Sacombank, STB",
        name: "Sài Gòn Thương Tín",
        nameEng: "",
    },
    {
        code: "VietABank, VAB",
        name: "Việt Á",
        nameEng: "",
    },
    {
        code: "BaoVietBank, BVB",
        name: "Bảo Việt",
        nameEng: "",
    },
    {
        code: "VietBank",
        name: "Việt Nam Thương Tín",
        nameEng: "",
    },
    {
        code: "Petrolimex Group Bank, PG Bank",
        name: "Xăng dầu Petrolimex",
        nameEng: "",
    },
    {
        code: "Eximbank, EIB",
        name: "Xuất Nhập khẩu Việt Nam",
        nameEng: "",
    },
    {
        code: "LienVietPostBank, LPB",
        name: "Bưu điện Liên Việt",
        nameEng: "",
    },
    {
        code: "Ngoại Thương Việt Nam,VCB",
        name: "Ngoại thương Việt Nam",
        nameEng: "",
    },
    {
        code: "Vietinbank, CTG",
        name: "Công Thương Việt Nam",
        nameEng: "",
    },
    {
        code: "BIDV, BID",
        name: "Đầu tư và Phát triển Việt Nam",
        nameEng: "",
    },
    {
        code: "Australia And Newzealand Bank",
        name: "Ngân hàng TNHH một thành viên ANZ (Việt Nam)",
        nameEng: "",
    },
    {
        code: "Deutsche Bank AG, Vietnam",
        name: "Deutsche Bank Việt Nam",
        nameEng: "",
    },
    {
        code: "Citibank",
        name: "Ngân hàng Citibank Việt Nam",
        nameEng: "",
    },
    {
        code: "HSBC",
        name: "Ngân hàng TNHH một thành viên HSBC (Việt Nam)",
        nameEng: "",
    },
    {
        code: "Standard Chartered Bank (Vietnam) Limited, Standard Chartered",
        name: "Standard Chartered",
        nameEng: "",
    },
    {
        code: "Shinhan Vietnam Bank Limited - SHBVN",
        name: "Ngân hàng TNHH MTV Shinhan Việt Nam",
        nameEng: "",
    },
    {
        code: "Hong Leong Bank Vietnam Limited - HLBVN",
        name: "Ngân hàng Hong Leong Việt Nam",
        nameEng: "",
    },
    {
        code: "BIDC",
        name: "Ngân hàng Đầu tư và Phát triển Campuchia",
        nameEng: "",
    },
    {
        code: "PBBVN",
        name: "Public Bank Việt Nam",
        nameEng: "",
    },
    {
        code: "UOB",
        name: "Ngân hàng United Overseas Bank tại Việt Nam",
        nameEng: "",
    },
    {
        code: "IVB",
        name: "Ngân hàng TNHH Indovina",
        nameEng: "",
    },
    {
        code: "VRB",
        name: "Ngân hàng Việt - Nga",
        nameEng: "",
    },
    {
        code: "VSB",
        name: "Ngân hàng Việt - Thái",
        nameEng: "",
    },


];