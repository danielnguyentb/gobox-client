const numberFormat = new Intl.NumberFormat('vi-VN');
import moment from 'moment';
moment.locale('vi');

export const formatTime = (time) => {
    time = time ? moment(time) : null;
    return time && time.isValid() ? time.format('DD/MM/YYYY') : '--/--/--';
};

export const formatFullTime = time => {
    time = time ? moment(time) : null;
    return time && time.isValid() ? time.format('HH:mm DD/MM/YYYY') : '--/--/--';
};

export const formatShortTime = time => {
    time = time ? moment(time) : null;
    return time && time.isValid() ? time.format('HH:mm DD/MM') : '--/--';
};

export const formatCurrency = number => typeof number === 'number' ? numberFormat.format(number) : '';