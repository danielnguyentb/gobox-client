import 'isomorphic-fetch'
import {getToken} from '../helpers'
import {CALL_API} from '../constants'

export default class Client {
    serializeJSON(data) {
        return Object.keys(data).map(function (keyName) {
            return encodeURIComponent(keyName) + '=' + encodeURIComponent(data[keyName])
        }).join('&');
    }

    callApi(config, loading) {
        let {types, endpoint, method = 'GET', headers, body, schema, scope, authenticate = true, form} = config;

        if (typeof config.headers === 'undefined' || config.headers['Content-Type'] !== null ) {
            body = typeof body === 'object' ? JSON.stringify(body) : body;
        }

        let header = {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': authenticate ? this.getToken() : '',
            ...headers,
        };

        if (config.headers  && config.headers['Content-Type'] === null) {
            delete header["Content-Type"];
        }

        return {
            [CALL_API]: {
                types, endpoint, schema, loading, scope, form,
                config: {
                    headers: header,
                    method, body,
                }
            }
        }
    }

    isURL(url) {
        return url.search(/(https?:\/\/(?:www\.|(?!www))[^\s\.]+\.[^\s]{2,}|www\.[^\s]+\.[^\s]{2,})/) >= 0;
    }

    getToken() {
        return getToken();
    }

    getPostFormDataConfig(data, authenticate = false) {
        let body = typeof data === 'string' ? data : this.serializeJSON(data);
        return {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/x-www-form-urlencoded',
                'Authorization': authenticate ? this.getToken() : ''
            },
            body,
        }
    }
}