import React from 'react';
import * as RBT from 'react-bootstrap';
import Select from 'react-select'
import {submit} from 'redux-form'
import Localize from '../../resources/Localize'
import { DateField, DatePicker } from 'react-date-picker'


export const parseErrors = errors => {
    const submitErrors = {};
    if (errors.details) {
        const {messages} = errors.details;
        Object.keys(messages).map(field => {
            submitErrors[field] = (Localize.t(messages[field][0]))
        });
    }
    if (Object.keys(submitErrors).length === 0) {
        submitErrors['_error'] = Localize.t(errors.message)
    }
    return submitErrors;
};

export const renderDateField = ({input, label, type, className, dateFormat, placeholder, meta: {touched, error, warning}}) => (
    <RBT.FormGroup validationState={touched && error ? 'error' : 'success'}>

        <DateField {...input}
            dateFormat={dateFormat} locale="vi" className={className}
            updateOnDateClick={true}
            placeholder={placeholder}
        >
            <DatePicker
                dateFormat={dateFormat}
                cancelButtonText="Hủy"
                okButtonText="Chọn"
                todayButton={false}
                clearButtonText="Xóa"
            />
        </DateField>
        {touched && ((error && <span className="help-block">{error}</span>) || (warning && <span>{warning}</span>))}
    </RBT.FormGroup>
);

export const renderField = ({input, label, type, className, placeholder, meta: {touched, error, warning}}) => (
    <RBT.FormGroup validationState={touched && error ? 'error' : 'success'}>
        <input {...input} type={type} className={className} placeholder={placeholder} autoComplete="off"/>
        {touched && ((error && <span className="help-block">{error}</span>) || (warning && <span>{warning}</span>))}
    </RBT.FormGroup>
);


export const renderFieldSelect = ({input, label, type, className, options, defaultValue, placeholder, meta: {touched, error, warning}}) => (
    <RBT.FormGroup validationState={touched && error ? 'error' : ''}>
        <select type="select" {...input} className={className}>
            {options.map(option =>
                <option key={option.value} value={option.value}>
                    {option.label}
                </option>
            )}
        </select>
        {touched && ((error && <span className="help-block">{error}</span>) || (warning && <span>{warning}</span>))}
    </RBT.FormGroup>
);

export const renderSelectAddress = ({input, className, onChange, placeholder, options, meta: {touched, error, warning}}) => (
    <RBT.FormGroup validationState={touched && error ? 'error' : ''}>
        <Select options={options} {...input} onChange={onChange}
                onBlur={() => {
                    input.onBlur(input.value);
                }}
                placeholder={placeholder}/>
        {touched && ((error && <span className="help-block">{error}</span>) || (warning && <span>{warning}</span>))}
    </RBT.FormGroup>
);

export const renderTextArea = ({input, className, onChange, placeholder, rows, meta: {touched, error, warning}}) => (
    <RBT.FormGroup validationState={touched && error ? 'error' : ''}>
         <textarea {...input}
             placeholder={placeholder}
             rows={rows}
             className={className}>
         </textarea>
        {touched && ((error && <span className="help-block">{error}</span>) || (warning && <span>{warning}</span>))}
    </RBT.FormGroup>
);