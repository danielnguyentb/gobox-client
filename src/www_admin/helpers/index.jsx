import {SESSION_KEY} from '../constants'
export {encode} from 'helpers';

export const saveSession = (session) => {
    localStorage.setItem(SESSION_KEY, JSON.stringify(session));
};

export const clearSession = () => localStorage.removeItem(SESSION_KEY);

export const isAuthenticated = () => {
    return getToken() && typeof getToken() === 'string';
};

export const getSession = () => {
    return JSON.parse(localStorage.getItem(SESSION_KEY)) || {};
};

export const getToken = () => {
    return getSession().id;
};

export const getUserId = () => {
    return parseInt(getSession().userId);
};

export const getPermissions = () => {
    return parseInt(getSession().permissions);
};

export const getNeedChangePass = () => {
    return !!getSession().needChangePass;
};

export const serializeJSON = (data) => {
    return Object.keys(data).map(function (keyName) {
        return encodeURIComponent(keyName) + '=' + encodeURIComponent(data[keyName])
    }).join('&');
};
