import React from 'react';
import {connect} from 'react-redux';
import {push, replace} from 'react-router-redux'
import AccessDenied from '../components/Pages/Error/AccessDenied'
import NotFound from '../components/Pages/Error/NotFound'
import * as User from '../components/user'
import {getToken} from './index'

export function requireAuthentication(Component) {

    class AuthenticatedComponent extends React.Component {

        componentWillMount() {
            this.checkAuth(this.props.isAuthenticated);
            this.redirectToBack();
        }

        redirectToBack() {
            const {location, dispatch} = this.props;
            if (location && location.query && location.query['r']) {
                dispatch(push(location.query['r']));
            }
        }

        componentDidUpdate(){
            const {isAuthenticated, session, dispatch, token} = this.props;
            this.checkNeedChangePass(session);
            this.checkAuth(isAuthenticated);
            if(token != getToken()){
                dispatch(User.actions.logoutSaga());
            }
        }

        checkNeedChangePass(session) {
            const {dispatch, location} = this.props;
            if (session.needChangePass == 1) {
                dispatch(push({
                    pathname: '/change_pass',
                    query: location && location.query
                }));
            }
        }

        checkAuth(isAuthenticated) {
            if (!isAuthenticated) {
                let redirectAfterLogin = this.props.location.pathname;
                this.props.dispatch(replace(`/login?r=${redirectAfterLogin}`));
            }
        }

        render() {
            if (this.props.isAccessDenied) {
                return (<AccessDenied />);
            }
            if (this.props.isNotFound) {
                return (<NotFound />);
            }
            return (this.props.isAuthenticated === true
                ? <Component {...this.props}/>
                : <div/>);
        }
    }

    const mapStateToProps = (state) => ({
        token: User.selectors.tokenSelector(state),
        session: state.user.session,
        isNotFound: state.isNotFound,
        isAccessDenied: state.isAccessDenied,
        isAuthenticated: state.user.isAuthenticated
    });

    return connect(mapStateToProps)(AuthenticatedComponent);

}