import React from 'react';
import Validation from 'react-validation';
import validator from 'validator';

export default Object.assign(Validation.rules, {
    required: {
        rule: (value, component, form) => {
            return value.trim();
        },
        hint: value => {
            return <span className='text-danger is-visible'>Thông tin này không được để trống!</span>
        }
    },
    email: {
        rule: value => {
            return validator.isEmail(value);
        },
        hint: value => {
            return <span className='text-danger is-visible'>{value} không phải định dạng email.</span>
        }
    },
    password: {
        rule: (value, component, form) => {
            let password = form.state.states.password;
            let passwordConfirm = form.state.states.passwordConfirm;

            let isBothUsed = password && passwordConfirm && password.isUsed && passwordConfirm.isUsed;
            let isBothChanged = isBothUsed && password.isChanged && passwordConfirm.isChanged;

            if (!isBothUsed || !isBothChanged) {
                return true;
            }

            return password.value === passwordConfirm.value;
        },
        hint: value => {
            return <span className="text-danger is-visible ">Mật khẩu phải giống nhau</span>
        }
    },
    null: {
        rule: (value, component, form) => {
            return true;
        },
        hint: value => {
            return true;
        }
    }
});