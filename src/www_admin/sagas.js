/**
 * Created by Tester-Ali on 30-11-2016.
 */
import {takeEvery, takeLatest, delay} from 'redux-saga'
import {call, put, fork, select} from 'redux-saga/effects'
import {LOCATION_CHANGE} from './ActionTypes'
import {actions as ReduxToastrActions} from 'react-redux-toastr'
import {actions as ReToastrActions} from 'cores/Toastr2'
import {CALL_API} from './constants'
import fetchApi from './helpers/callApi'
import {parseErrors} from './helpers/redux-form'
import {getToken, getSession} from './helpers'
import {user, role, tasks, taskBatches} from './components'
import {startSubmit, stopSubmit} from 'redux-form'

function* resetToast(action) {
    yield call(delay, 1000);
    yield put(ReduxToastrActions.clean());
    yield put(ReToastrActions.clean());
}

export function* callApi(action) {
    const {types, endpoint, schema, body, method, headers, form, ...rest} = action.payload || {};
    const [ requestType, successType, failureType ] = types;
    const config = {
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            ...headers,
        },
        method, body,
    };
    try {
        const token = yield select(user.selectors.tokenSelector);
        config.headers.Authorization = token || getToken();
        yield put({type: requestType, ajax: true});
        if (form) {
            yield put(startSubmit(form))
        }
        const response = yield call(fetchApi, endpoint, config, schema);
        yield put({type: successType, ajax: false, response, ...rest});
        if (form) {
            yield put(stopSubmit(form))
        }
    } catch (error) {
        yield put({type: failureType, ajax: false, error});
        if (form) {
            yield put(stopSubmit(form, parseErrors(error)))
        }
    }
}

export default function* startApp() {
    const session = getSession();
    if (session && session.id) {
        yield put(user.actions.loginUserSuccess(session));
    }
    yield takeEvery(LOCATION_CHANGE, resetToast);
    yield takeEvery(CALL_API, callApi);
    yield fork(role.saga);
    yield fork(tasks.saga);
    yield fork(taskBatches.saga);
}