import {createStore, applyMiddleware, compose} from 'redux'
import thunk from 'redux-thunk'
import createLogger from 'redux-logger'
import api from './middlewares/api'
import rootReducer from '../reducer'
import {browserHistory, hashHistory} from 'react-router'
import {routerMiddleware} from 'react-router-redux'
import {loadingBarMiddleware} from 'react-redux-loading-bar'
import DevTools from '../../www_components/DevTools'
import reduxRecord from 'redux-test-recorder'
import createSagaMiddleware, { END } from 'redux-saga'

const record = reduxRecord({rootReducer});
const TypeSuffixes = ['REQUEST', 'SUCCESS', 'FAILURE'];

export default function configureStore(preloadedState) {
    const sagaMiddleware = createSagaMiddleware();

    const store = createStore(
        rootReducer,
        preloadedState,
        compose(
            applyMiddleware(thunk, api, record.middleware, createLogger(),
                routerMiddleware(hashHistory),
                sagaMiddleware,
                loadingBarMiddleware({promiseTypeSuffixes: TypeSuffixes})
            ),
            DevTools.instrument(),
        )
    );

    store.runSaga = sagaMiddleware.run;
    store.close = () => store.dispatch(END);

    if (module.hot) {
        // Enable Webpack hot module replacement for reducers
        module.hot.accept('../reducer', () => {
            const nextRootReducer = require('../reducer').default;
            store.replaceReducer(nextRootReducer)
        })
    }
    return store
}
export const recordProps = record.props;