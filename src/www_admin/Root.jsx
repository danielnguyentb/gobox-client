/**
 * Created by Tester-Ali on 25-08-2016.
 */

import React from 'react'
import {Provider} from 'react-redux';
import {browserHistory, hashHistory} from 'react-router'
import {syncHistoryWithStore} from 'react-router-redux'
import configureStore from './stores/configureStore'
import {Router} from 'react-router'
import routes from './Routes'
import rootSaga from './sagas'
import Perf from 'react-addons-perf'

const store = configureStore();
store.runSaga(rootSaga);
const history = syncHistoryWithStore(hashHistory, store);
window.Perf = Perf;

export default () => {
    return (
        <Provider store={store}>
            <Router history={history} routes={routes}/>
        </Provider>
    );
}
