import React from 'react'

import AccessDenied from './components/Pages/Error/AccessDenied'
import NotFound from './components/Pages/Error/NotFound'
import DashBoard from './components/dashboard/components/Dashboard'
import * as components from './components'
import {Route, IndexRoute, Redirect} from 'react-router'
import {requireAuthentication} from './helpers/requireAuthentication';

const {user, customer,role,order,hub, barcode, balance, transportRequests, tasks, taskBatches, layouts} = components;
const {UserChangePassLayout, MainLayout, LoginLayout, RootLayout, ErrorLayout} = layouts;
export default (
    <Route component={RootLayout}>
        <Route path="/" component={requireAuthentication(MainLayout)}>
            <IndexRoute component={DashBoard}/>
            <Route path="users">
                <IndexRoute name="users_manage" component={user.components.Management}/>
                <Route name="users_create" path="create" component={user.components.Form}/>
                <Route name="user_detail" path="detail/:userId" component={user.components.Detail}/>
            </Route>
            <Route path="customers">
                <IndexRoute name="customers_manage" component={customer.components.Management}/>
                <Route name="customers_create" path="create" component={customer.components.Form}/>
                <Route name="customers_detail" path="detail/:customerId" component={customer.components.Detail}/>
            </Route>
            <Route path="roles">
                <IndexRoute name="roles_manage" component={role.components.Management}/>
                <Route name="roles_detail" path="detail/:roleId" component={role.components.Detail}/>
            </Route>
            <Route path="orders">
                <IndexRoute name="orders_manage" component={order.components.Management}/>
                <Route name="orders_create" path="create" component={order.components.Form}/>
                <Route name="orders_detail" path="detail/:orderId" component={order.components.Detail}/>
            </Route>

            <Route path="hubs" component={hub.components.Hub}>
                <Route name="hubs_manage" path=":hubId" component={hub.components.DashBoard}/>
                <Route name="hubs_request" path=":hubId/transport_request" component={transportRequests.components.RequestBoardManagement}/>
                <Route name="hubs_barcode" path=":hubId/barcodes" component={hub.components.Barcode}/>
                <Route path=":hubId/task_batches">
                    <IndexRoute component={taskBatches.components.CreateTaskBatches}/>
                    <Route path="create" component={taskBatches.components.CreateTaskBatches}/>
                    <Route path="detail/:batchId" component={taskBatches.components.Detail}/>
                </Route>
            </Route>

            <Route path="barcodes">
                <IndexRoute name="barcodes" component={barcode.components.Management} />
            </Route>

            <Route path="balances">
                <IndexRoute name="balance_manage" component={balance.components.Management} />
                <Route name="balance_create" path="create" component={balance.components.Form} />
                <Route name="balance_customer" path="customer/:customerId" component={balance.components.Management} />
                <Route name="balance_history" path="history/:customerId" component={balance.components.Management} />
            </Route>

            <Route path="tasks">
                <IndexRoute name="task_manage" component={tasks.components.Management} />
                <Route name="tasks_detail" path="detail/:taskId" component={tasks.components.Detail}/>
            </Route>
            {/*<Route path="*" component={NoMatch}/>*/}
            <Route path="request_list_order">
                <IndexRoute name="request_list_order" component={transportRequests.components.RequestListOrder}/>
            </Route>
        </Route>

        <Route name="print_order" path="orders/print/:orderId" component={requireAuthentication(order.components.PrintBarcode)}/>

        <Route path="/change_pass" component={UserChangePassLayout}>
            <IndexRoute name="change_pass" component={user.components.ChangePassword}/>
        </Route>
        <Route path="/login" component={LoginLayout}>
            <IndexRoute name="login" component={user.components.Login}/>
        </Route>

        <Route path="error" component={ErrorLayout}>
            <Route name="403" path="403" component={AccessDenied}/>
            <Route name="404" path="404" component={NotFound}/>
        </Route>

        <Route path="*" component={NotFound}/>
    </Route>
);