/**
 * Created by ltlam on 25/12/2016.
 */
import {createSelector}  from 'reselect'
export const getLocation = state => state['routing'] && state['routing']['locationBeforeTransitions'];