import {routerReducer as routing} from 'react-router-redux'
import {loadingBarReducer as loadingBar} from 'react-redux-loading-bar'
import {reducer as toastr} from 'react-redux-toastr'
import {reducer as toastr2} from 'cores/Toastr2'
import {combineForms} from 'react-redux-form';
import {combineReducers} from 'redux'
import Alert from 'cores/Alert'
import {constants as touchableConstants, reducer as touchableReducer} from 'cores/Touchable'
import {RESOURCE_NOT_FOUND_CODE, ACCESS_DENIED_CODE} from './constants'
import {LOCATION_CHANGE} from './ActionTypes'
import {reducer as formReducer} from 'redux-form'
import * as fromPagination from 'cores/RePagination'
import {reducer as modalReducer} from 'redux-modal'
import {
    user, role, tasks, transportRequests, order,
    hub, customer, location, balance, taskBatches
} from './components'

const appReducer = {
    [user.constants.NAME]: user.reducer,
    [customer.constants.NAME]: customer.reducer,
    [role.constants.NAME]: role.reducer,
    [order.constants.NAME]: order.reducer,
    [hub.constants.NAME]: hub.reducer,
    [location.constants.NAME]: location.reducer,
    [transportRequests.constants.NAME]: transportRequests.reducer,
    [tasks.constants.NAME]: tasks.reducer,
};

const isAccessDenied = (state = false, {code, type}) => {
    if (code === ACCESS_DENIED_CODE) {
        return true;
    } else if (type === LOCATION_CHANGE) {
        return false;
    }
    return state;
};

const isNotFound = (state = false, {code, type, request}) => {
    if (code === RESOURCE_NOT_FOUND_CODE && request.method === 'GET') {
        return true;
    } else if (type === LOCATION_CHANGE) {
        return false;
    }
    return state;
};

const actionType = (state = null, {type, ajax = false}) => type;
const ajaxType = (state = null, action) => {
    const {type, ajax = false} = action;
    return ajax ? type : state;
};
const ajax = (state, {ajax = false}) => {
    if (typeof ajax === 'boolean') return ajax;
    return state;
};
const isLocationChanged = (state = null, action) => action.type === LOCATION_CHANGE;

export default combineReducers({
    ...appReducer, [Alert.constants.NAME]: Alert.reducer,
    [fromPagination.NAME]: fromPagination.reducer,
    [taskBatches.constants.NAME]: taskBatches.reducer,
    form: formReducer,
    [touchableConstants.NAME]: touchableReducer,
    [balance.constants.NAME]: balance.reducer,
    routing, loadingBar, toastr, toastr2,
    deep: combineForms(appReducer, 'deep'),
    modal: modalReducer,
    isAccessDenied, isNotFound, actionType, ajaxType, isLocationChanged, ajax,
})

let {isAllow, isGod, isMe} = user.fromReducer || {};
export {isAllow, isGod, isMe};