import {combineReducers} from 'redux'
import {
    ROLE_SUCCESS, ROLE_FAILURE,

    ROLES_SUCCESS, ROLES_FAILURE,

    ROLE_CREATE_SUCCESS, ROLE_CREATE_FAILURE,

    ROLE_MEMBER_ADD_SUCCESS, ROLE_MEMBER_ADD_FAILURE,

    ROLE_MEMBER_REMOVE_SUCCESS, ROLE_MEMBER_REMOVE_FAILURE,

    ROLE_MEMBERS_SUCCESS, ROLE_MEMBERS_FAILURE,

    PERMISSION_SUCCESS, PERMISSION_FAILURE,

    PERMISSION_ADD_SUCCESS, PERMISSION_ADD_FAILURE,
    PERMISSION_REMOVE_SUCCESS, PERMISSION_REMOVE_FAILURE,
    ROLE_UPDATE_SUCCESS, ROLE_UPDATE_FAILURE,
    USERS_SUGGEST_SUCCESS, USERS_SUGGEST_FAILURE,
} from './ActionTypes'
import {LOCATION_CHANGE} from '../../ActionTypes'

const permission = (state = {}, action) => {
    switch (action.type) {
        case PERMISSION_ADD_SUCCESS:
            if (state.acl && action.response) {
                state.acl.id = action.response.id;
                return {
                    ...state,
                    assigned: true
                };
            }
            break;
        case PERMISSION_REMOVE_SUCCESS:
            if (state.acl) {
                state.acl.id = null;
                return {
                    ...state,
                    assigned: false
                };
            }
            break;
    }
    return state;
};

const permissions = (state = {}, action) => {
    let {response} = action;
    let copy = {...state};
    switch (action.type) {
        case PERMISSION_SUCCESS:
            return {
                ...state, ...response.permissions
            };

        case PERMISSION_ADD_SUCCESS:
            let {code} = action.response;
            Object.keys(state).map(group => {
                if (copy[group]['permissions'].hasOwnProperty(code)) {
                    copy[group]['permissions'][code] = permission(copy[group]['permissions'][code], action);
                }
            });
            return copy;

        case PERMISSION_REMOVE_SUCCESS:
            var {code} = action.response;
            Object.keys(state).map(group => {
                if (copy[group]['permissions'].hasOwnProperty(code)) {
                    copy[group]['permissions'][code] = permission(copy[group]['permissions'][code], action);
                }
            });
            return copy;
    }
    return state;
};

const receivedUsers = (state = [], action) => {
    switch (action.type) {
        case USERS_SUGGEST_SUCCESS:
            return action.response.result;
    }
    return state;
};

export function getSuggestedUsers(state = []) {
    return state.role.receivedUsers.map(u => {
        u['assigned'] = state.role.members.hasOwnProperty(u.id);
        return u;
    });
}

const members = (state = {}, action) => {
    let {response} = action;
    switch (action.type) {
        case ROLE_MEMBERS_SUCCESS:
            return response.reduce((obj, value) => {
                obj[value.id] = value;
                return obj;
            }, {});
        case ROLE_MEMBER_ADD_SUCCESS:
            let user = action.scope;
            return {
                ...state,
                [user.id]: user
            };
        case ROLE_MEMBER_REMOVE_SUCCESS:
            delete state[action.scope.id];
            return state;
    }
    return state;
};


const byId = (state = {}, action) => {
    const {response} = action;
    switch (action.type) {
        case ROLES_SUCCESS:
            let roles = [];
            response.result.map(value => {
                roles[value.id] = value;
            });
            return {
                ...state, ...roles
            };
        case ROLE_SUCCESS:
        case ROLE_CREATE_SUCCESS:
        case ROLE_UPDATE_SUCCESS:
            let copy = {...state};
            copy[response.id] = {
                ...copy[response.id],
                ...response
            };
            return copy;
    }
    return state
};

function visibleIds(state = [], action) {
    const {response} = action;
    const copy = [...state];
    switch (action.type) {
        case ROLES_SUCCESS:
            response.result.map(role => {
                if (!copy.includes(role.id)) {
                    copy.push(role.id)
                }
            });
            break;
        case ROLE_SUCCESS:
        case ROLE_CREATE_SUCCESS:
        case ROLE_UPDATE_SUCCESS:
            if (!copy.includes(response.id)) {
                copy.push(response.id)
            }
            break;
    }
    return copy;
}

function paginateIds(state = [], action) {
    switch (action.type) {
        case ROLES_SUCCESS:
            return action.response.result.map(role => role.id);
    }
    return state;
}


function metadata(state = {}, action) {
    switch (action.type) {
        case ROLES_SUCCESS:
            return action.response.metadata;
    }
    return state;
}

export default combineReducers({
    byId, metadata, visibleIds, paginateIds, members, permissions, receivedUsers
});