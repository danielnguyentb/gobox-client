import Role from './api'
import {generateQuery} from './helper'
import {CALL_API} from '../../ActionTypes'
import {ROLE_UPDATE_REQUEST, ROLE_UPDATE_SUCCESS, ROLE_UPDATE_FAILURE} from './ActionTypes'

export const addPermission = (permission) => dispatch => {
    delete permission['id'];
    dispatch(Role.addPermission(permission));
};

export const removePermission = (permissionId) => dispatch => dispatch(Role.removePermission(permissionId));

export const getSuggestedUser = (keyword) => dispatch => {
    keyword = encodeURIComponent(`%${keyword}%`);
    let filter = ({
        where: {
            or: [
                {fullName: {like: keyword}},
                {email: {like: keyword}}
            ],
            and: {state: 'ACTIVE'}
        }, limit: 10,
    });
    dispatch(Role.getSuggestedUser(filter))
};

export const getRoles = (filter) => dispatch => dispatch(Role.getRoles(generateQuery(filter)));

export const getMembers = (roleId) => dispatch => dispatch(Role.getMembers(roleId));

export const addMember = (role_id, user) => dispatch => dispatch(Role.addMember(role_id, user));

export const removeMember = (role_id, user) => dispatch => dispatch(Role.removeMember(role_id, user));

export const getPermissions = (roleId) => dispatch => dispatch(Role.getPermissions(roleId));

export const getRoleById = (role_id) => dispatch => dispatch(Role.getRoleById(role_id));

export const createRole = (role) => dispatch => dispatch(Role.create(role));

export const updateRole = (role) => dispatch => dispatch(Role.update(role));

export const updateRoleSaga = (id, role) => {
    return {
        type: CALL_API,
        payload: {
            types: [ROLE_UPDATE_REQUEST, ROLE_UPDATE_SUCCESS, ROLE_UPDATE_FAILURE],
            endpoint: `/roles/${id}`,
            body: JSON.stringify(role),
            method: 'PUT',
            form: 'RoleForm'
        }
    }
};