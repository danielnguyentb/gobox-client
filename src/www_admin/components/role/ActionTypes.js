
//-------------ROLES
export const ROLE_REQUEST = 'ROLE_REQUEST';
export const ROLE_SUCCESS = 'ROLE_SUCCESS';
export const ROLE_FAILURE = 'ROLE_FAILURE';

export const ROLES_REQUEST = 'ROLES_REQUEST';
export const ROLES_SUCCESS = 'ROLES_SUCCESS';
export const ROLES_FAILURE = 'ROLES_FAILURE';

export const ROLE_CREATE_REQUEST = 'ROLE_CREATE_REQUEST';
export const ROLE_CREATE_SUCCESS = 'ROLE_CREATE_SUCCESS';
export const ROLE_CREATE_FAILURE = 'ROLE_CREATE_FAILURE';

export const ROLE_UPDATE_REQUEST = 'ROLE_UPDATE_REQUEST';
export const ROLE_UPDATE_SUCCESS = 'ROLE_UPDATE_SUCCESS';
export const ROLE_UPDATE_FAILURE = 'ROLE_UPDATE_FAILURE';

//--ROLE MEMBER
export const ROLE_MEMBER_ADD_REQUEST = 'ROLE_MEMBER_ADD_REQUEST';
export const ROLE_MEMBER_ADD_SUCCESS = 'ROLE_MEMBER_ADD_SUCCESS';
export const ROLE_MEMBER_ADD_FAILURE = 'ROLE_MEMBER_ADD_FAILURE';

export const ROLE_MEMBERS_REQUEST = 'ROLE_MEMBERS_REQUEST';
export const ROLE_MEMBERS_SUCCESS = 'ROLE_MEMBERS_SUCCESS';
export const ROLE_MEMBERS_FAILURE = 'ROLE_MEMBERS_FAILURE';

export const USERS_SUGGEST_REQUEST = 'USERS_SUGGEST_REQUEST';
export const USERS_SUGGEST_SUCCESS = 'USERS_SUGGEST_SUCCESS';
export const USERS_SUGGEST_FAILURE = 'USERS_SUGGEST_FAILURE';

export const ROLE_MEMBER_REMOVE_REQUEST = 'ROLE_MEMBER_REMOVE_REQUEST';
export const ROLE_MEMBER_REMOVE_SUCCESS = 'ROLE_MEMBER_REMOVE_SUCCESS';
export const ROLE_MEMBER_REMOVE_FAILURE = 'ROLE_MEMBER_REMOVE_FAILURE';

//--ROLE PERMISSION
export const PERMISSION_REQUEST = 'PERMISSION_REQUEST';
export const PERMISSION_SUCCESS = 'PERMISSION_SUCCESS';
export const PERMISSION_FAILURE = 'PERMISSION_FAILURE';

export const PERMISSION_ADD_REQUEST = 'PERMISSION_ADD_REQUEST';
export const PERMISSION_ADD_SUCCESS = 'PERMISSION_ADD_SUCCESS';
export const PERMISSION_ADD_FAILURE = 'PERMISSION_ADD_FAILURE';

export const PERMISSION_REMOVE_REQUEST = 'PERMISSION_REMOVE_REQUEST';
export const PERMISSION_REMOVE_SUCCESS = 'PERMISSION_REMOVE_SUCCESS';
export const PERMISSION_REMOVE_FAILURE = 'PERMISSION_REMOVE_FAILURE';

