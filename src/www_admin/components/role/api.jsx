import Client from '../../helpers/Client'
import * as ActionTypes from './ActionTypes'

class Role extends Client {

    addPermission(permission){
        return this.callApi({
            types: [ActionTypes.PERMISSION_ADD_REQUEST, ActionTypes.PERMISSION_ADD_SUCCESS, ActionTypes.PERMISSION_ADD_FAILURE],
            endpoint: '/acls',
            body: permission,
            method: 'POST',
        }, 'addingPermission');
    }

    removePermission(permissionId){
        return this.callApi({
            types: [ActionTypes.PERMISSION_REMOVE_REQUEST, ActionTypes.PERMISSION_REMOVE_SUCCESS, ActionTypes.PERMISSION_REMOVE_FAILURE],
            endpoint: `/acls/${permissionId}`,
            method: 'DELETE',
        }, 'removingPermission');
    }

    getSuggestedUser(_filter){
        let filter = typeof _filter === 'string' ? _filter : JSON.stringify(_filter);
        return this.callApi({
            types: [ActionTypes.USERS_SUGGEST_REQUEST, ActionTypes.USERS_SUGGEST_SUCCESS, ActionTypes.USERS_SUGGEST_FAILURE],
            endpoint: _filter ? `/users?filter=${filter}` : '/users',
            method: 'GET',
        }, 'suggestingUser');
    }

    getRoles(_filter) {
        let filter = typeof _filter == 'string' ? _filter : JSON.stringify(_filter);
        return this.callApi({
            types: [ActionTypes.ROLES_REQUEST, ActionTypes.ROLES_SUCCESS, ActionTypes.ROLES_FAILURE],
            endpoint: filter ? `/roles?filter=${filter}` : '/roles',
            method: 'GET',
        }, 'fetchingRoles');
    }
    
    getMembers(roleId){
        return this.callApi({
            types: [ActionTypes.ROLE_MEMBERS_REQUEST, ActionTypes.ROLE_MEMBERS_SUCCESS, ActionTypes.ROLE_MEMBERS_FAILURE],
            endpoint: `/roles/${roleId}/members`,
            method: 'GET'
        }, 'fetchingMembers')
    }

    getPermissions(roleId){
        return this.callApi({
            types: [ActionTypes.PERMISSION_REQUEST, ActionTypes.PERMISSION_SUCCESS, ActionTypes.PERMISSION_FAILURE],
            endpoint: `/roles/${roleId}/permissions`,
            method: 'GET',
        }, 'fetchingPermissions');
    }

    getRoleById(roleId) {
        return this.callApi({
            types: [ActionTypes.ROLE_REQUEST, ActionTypes.ROLE_SUCCESS, ActionTypes.ROLE_FAILURE],
            endpoint: `/roles/${roleId}`,
            method: 'GET',
        }, 'fetchingRole');
    }

    addMember(role_id, user){
        return this.callApi({
            types: [ActionTypes.ROLE_MEMBER_ADD_REQUEST, ActionTypes.ROLE_MEMBER_ADD_SUCCESS, ActionTypes.ROLE_MEMBER_ADD_FAILURE],
            endpoint: `/roles/${role_id}/members/${user.id}`,
            method: 'POST',
            scope: user,
        }, 'addingMember');
    }

    removeMember(role_id, user){
        return this.callApi({
            types: [ActionTypes.ROLE_MEMBER_REMOVE_REQUEST, ActionTypes.ROLE_MEMBER_REMOVE_SUCCESS, ActionTypes.ROLE_MEMBER_REMOVE_FAILURE],
            endpoint: `/roles/${role_id}/members/${user.id}`,
            method: 'DELETE',
            scope: user,
        }, 'removingMember');
    }

    create(role) {
        return this.callApi({
            types: [ActionTypes.ROLE_CREATE_REQUEST, ActionTypes.ROLE_CREATE_SUCCESS, ActionTypes.ROLE_CREATE_FAILURE],
            endpoint: '/roles',
            body: role,
            method: 'POST',
            form: 'RoleForm'
        });
    }

    update(role) {
        return this.callApi({
            types: [ActionTypes.ROLE_UPDATE_REQUEST, ActionTypes.ROLE_UPDATE_SUCCESS, ActionTypes.ROLE_UPDATE_FAILURE],
            endpoint: `/roles/${role.id}`,
            body: role,
            method: 'PUT',
            form: 'RoleForm'
        });
    }
}

export default new Role();