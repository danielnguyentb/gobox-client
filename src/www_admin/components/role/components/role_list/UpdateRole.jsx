import React from 'react'
import * as RBT from 'react-bootstrap';
import Validation from 'react-validation';

export default class UpdateRole extends React.Component {
    constructor(props) {
        super(props);
    }

    componentWillMount() {
        this.setState({displayName: '', description: '', showModal: true});
    }

    closeModal() {
        this.setState({showModal: false});
    }

    showModal() {
        this.setState({showModal: true});
    }

    handleChangeState(e) {
        this.setState({stateRole: e.target.value});
    }

    handleDisplayName(e) {
        this.setState({displayName: e.target.value});
    }

    handleDescription(e) {
        this.setState({description: e.target.value});
    }

    submitUpdateRole(role) {
        this.props.updateRole(role);
    }

    render() {
        let {displayName, description, state} = this.props;
        return(
            <RBT.Modal show={this.state.showModal} onHide={this.closeModal.bind(this)}>
                <Validation.components.Form>
                    <RBT.Modal.Header closeButton>
                        <RBT.Modal.Title>Sửa role</RBT.Modal.Title>
                    </RBT.Modal.Header>
                    <RBT.Modal.Body>

                        <RBT.FormGroup controlId=''>
                            <RBT.ControlLabel>Tên role</RBT.ControlLabel>
                            <Validation.components.Input
                                onChange={(e) => {
                                    this.handleDisplayName(e);
                                }}
                                value={this.state.displayName ? this.state.displayName : displayName}

                                className="form-control" name='displayName' validations={['required']}
                            />

                            <RBT.ControlLabel>Mô tả</RBT.ControlLabel>
                            <Validation.components.Input
                                onChange={(e) => {
                                    this.handleDescription(e);
                                }}
                                value={this.state.description ? this.state.description : description}

                                className="form-control" name='description' validations={['required']}
                            />

                            <RBT.FormGroup controlId="formControlsSelect">
                                <RBT.ControlLabel>Trạng thái</RBT.ControlLabel>
                                <RBT.FormControl componentClass="select" placeholder="select"
                                                 value={this.state.stateRole ? this.state.stateRole : state}
                                                 onChange={this.handleChangeState.bind(this)}>
                                    <option value="ACTIVE">Kích hoạt</option>
                                    <option value="INACTIVE">Không kích hoạt</option>
                                </RBT.FormControl>
                            </RBT.FormGroup>
                        </RBT.FormGroup>

                    </RBT.Modal.Body>
                    <RBT.Modal.Footer>
                        <RBT.Button onClick={this.closeModal.bind(this)}>ĐÓNG</RBT.Button>

                        <Validation.components.Button
                            className="btn btn-primary"
                            onClick={(e) => {
                                e.preventDefault();

                                this.submitUpdateRole({
                                    id: this.props.id,
                                    displayName: this.state.displayName ? this.state.displayName : displayName,
                                    description: this.state.description ? this.state.description : description,
                                    state: this.state.stateRole ? this.state.stateRole : state
                                });
                                this.closeModal();
                            }}
                        >
                            LƯU
                        </Validation.components.Button>

                    </RBT.Modal.Footer>
                </Validation.components.Form>
            </RBT.Modal>
        )
    }
}