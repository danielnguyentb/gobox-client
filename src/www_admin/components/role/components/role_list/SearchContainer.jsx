import React from 'react';
import {connect} from 'react-redux'
import SearchBox from './SearchBox'
import qs from 'qs';
import {push} from 'react-router-redux'

class SearchContainer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {keyword: props.location.query.keyword}
    }

    search(data) {
        let params = {
            ...this.props.location.query,
            ...data,
            skip: 0,
        };
        this.props.dispatch(push(`/roles?${qs.stringify(params)}`));
    }

    render() {
        let {location, metadata} = this.props;
        let {keyword} = this.state;
        let params = {...metadata, ...location.query, keyword};
        return (
            <SearchBox onSubmit={::this.search} {...params}/>
        );
    }
}

const mapStateToProps = (state) => ({
    metadata: state.role.metadata,
});

export default connect(mapStateToProps)(SearchContainer)