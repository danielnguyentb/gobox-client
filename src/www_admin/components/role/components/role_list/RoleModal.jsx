import React from 'react';
import { connectModal } from 'redux-modal'
import {Modal} from 'react-bootstrap'
import {ReRoleForm} from './RoleForm'

export const RoleModal = ({show, handleHide, ...rest}) => {
    return (
        <Modal show={show} onHide={handleHide}>
            <ReRoleForm onClose={handleHide} {...rest}/>
        </Modal>
    )
};


export default connectModal({name: 'RoleModal', destroyOnHide: true})(RoleModal)