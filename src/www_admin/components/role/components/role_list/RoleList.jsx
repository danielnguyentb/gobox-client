import React from 'react';
import RoleItem from './RoleItem'

export default ({roles, allow}) => {
    return (
        <tbody>
        {roles.map(role => <RoleItem key={role.id} {...role} allow={allow} />)}
        </tbody>
    )
}
