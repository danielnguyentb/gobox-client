import React from 'react';
import Link from '../../../../../www_components/Common/Link'
import '../../../../../www_components/Forms/Button'
import EditRoleContainer from './EditRoleContainer'
import {INACTIVE} from '../../constants'

export default ({id, description, totalMember, displayName, state, allow}) => (
    <tr>
        <td className="hidden-xs">#{id}</td>
        <td className="fw-600">{state == INACTIVE ? <i className="ti ti-lock"/> : ''}&nbsp;
            {allow.viewDetail ?
                <Link to={`/roles/detail/${id}`}>{displayName}</Link>
                : displayName
            }
        </td>
        <td>{description}</td>
        <td className="hidden-xs text-center">{totalMember}</td>
        <td className="hidden-xstext-center">
            {allow.updateRole &&
            <EditRoleContainer id={id}/>
            }
        </td>
    </tr>
)