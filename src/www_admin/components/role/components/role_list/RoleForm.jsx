import React from 'react';
import * as RBT from 'react-bootstrap'
import {ACTIVE, INACTIVE} from '../../constants'
import {Field, reduxForm} from 'redux-form'

const renderField = ({input, label, type, className, autoFocus, meta: {touched, error, warning}}) => (
    <RBT.FormGroup validationState={touched && error ? 'error' : 'success'}>
        <RBT.ControlLabel>{label}</RBT.ControlLabel>
        <input {...input} type={type} className={className} autoFocus={autoFocus}/>
        {touched && ((error && <span className="help-block">{error}</span>) || (warning && <span>{warning}</span>))}
    </RBT.FormGroup>
);

const renderTextAreaField = ({input, label, type, className, meta: {touched, error, warning}}) => (
    <RBT.FormGroup validationState={touched && error ? 'error' : 'success'}>
        <RBT.ControlLabel>{label}</RBT.ControlLabel>
        <textarea {...input} type={type} className={className}/>
        {touched && ((error && <span className="help-block">{error}</span>) || (warning && <span>{warning}</span>))}
    </RBT.FormGroup>
);

const renderStateField = ({input, label, type, className, meta: {touched, error, warning}}) => (
    <RBT.FormGroup validationState={touched && error ? 'error' : 'success'}>
        <RBT.ControlLabel>{label}</RBT.ControlLabel>
        <select className="form-control" {...input}>
            <option value={ACTIVE}>Kích hoạt</option>
            <option value={INACTIVE}>Không kích hoạt</option>
        </select>
        {touched && ((error && <span className="help-block">{error}</span>) || (warning && <span>{warning}</span>))}
    </RBT.FormGroup>
);

export const RoleForm = ({title, error, onClose, handleSubmit, submitting, pristine}) => {
    return (
        <form onSubmit={handleSubmit}>
            <RBT.Modal.Header closeButton>
                <RBT.Modal.Title>{title}</RBT.Modal.Title>
            </RBT.Modal.Header>
            <RBT.Modal.Body>
                {error && <div className="alert alert-danger">{error}</div>}
                <Field component={renderField} autoFocus={true} label="Tên nhóm" type="text" className="form-control"
                       name="displayName"/>
                <Field component={renderTextAreaField} label="Mô tả" type="text" className="form-control"
                       name="description"/>
                <Field component={renderStateField} label="Trạng thái" type="text" className="form-control"
                       name="state"/>
            </RBT.Modal.Body>
            <RBT.Modal.Footer>
                <button className="btn btn-default" type="button" onClick={onClose}>ĐÓNG</button>
                <button
                    className="btn btn-primary"
                    disabled={submitting || pristine}
                    type="submit">LƯU
                </button>
            </RBT.Modal.Footer>
        </form>
    )
};

export const validate = values => {
    const errors = {};
    if(Object.keys(values).length === 0) return errors;
    if (!values.displayName) {
        errors.displayName = 'Tên nhóm không được để trống'
    }
    return errors;
};

export const ReRoleForm = reduxForm({
    form: 'RoleForm',
    validate
})(RoleForm);