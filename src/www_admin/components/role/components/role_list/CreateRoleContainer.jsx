import React from 'react';
import * as RBT from 'react-bootstrap'
import {createRole} from '../../actions'
import {connect} from 'react-redux'
import {ACTIVE} from '../../constants'
import {show} from 'redux-modal'

class CreateRoleContainer extends React.Component {
    create(role) {
        const {displayName, description, state} = role;
        const {createRole} = this.props;
        createRole({displayName, description, state});
    }

    handleCreateClick() {
        const {dispatch} = this.props;
        dispatch(show('RoleModal', {
            title: 'Tạo nhóm',
            initialValues: {state: ACTIVE},
            onSubmit: ::this.create
        }));
    }

    render() {
        return (
            <div>
                <RBT.Button
                    bsStyle="primary" className="btn fw-600"
                    onClick={::this.handleCreateClick}><i className="fa fa-plus mr-5"/>
                    TẠO NHÓM</RBT.Button>
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
});
export default connect(mapStateToProps, {createRole})(CreateRoleContainer)