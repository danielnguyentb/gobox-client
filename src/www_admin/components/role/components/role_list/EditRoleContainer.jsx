import React, {PropTypes} from 'react';
import * as RBT from 'react-bootstrap'
import {updateRoleSaga} from '../../actions'
import {connect} from 'react-redux'
import {RoleByIdSelector} from '../../selectors'
import {show} from 'redux-modal'

class EditRoleContainer extends React.Component {
    static propTypes = {
        id: PropTypes.number.isRequired
    };

    handleClick() {
        const {dispatch, id, role} = this.props;
        dispatch(show('RoleModal', {
            title: 'Sửa thông tin nhóm',
            initialValues: role,
            onSubmit(data){
                dispatch(updateRoleSaga(id, data))
            }
        }));
    }

    render() {
        return (
            <div>
                <RBT.Button
                    bsStyle="primary" bsSize="xsmall"
                    onClick={::this.handleClick}>Sửa</RBT.Button>
            </div>
        );
    }
}

const mapStateToProps = (state, props) => {
    const getRole = RoleByIdSelector(state);
    return {
        role: getRole(props.id)
    }
};
export default connect(mapStateToProps)(EditRoleContainer)