import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux'
import RoleList from './RoleList'
import {getRoles} from '../../actions'
import {VisibleRolesSelector} from '../../selectors'


class RoleListContainer extends Component {
    componentWillMount() {
        this.props.getRoles(this.props.location.query);
    }

    componentWillReceiveProps(nextProps){
        if(this.props.location.search != nextProps.location.search) {
            nextProps.getRoles(nextProps.location.query);
        }
    }

    render() {
        let {roles} = this.props;
        if(!roles || roles.length == 0){
            return null;
        }
        return (
            <RoleList {...this.props}/>
        );
    }
}

const mapStateToProps = (state) => ({
    roles: VisibleRolesSelector(state)
});

export default connect(
    mapStateToProps, {getRoles}
)(RoleListContainer)