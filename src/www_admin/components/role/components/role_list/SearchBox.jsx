import React from 'react';
import * as RBT from 'react-bootstrap'
import {ACTIVE, INACTIVE} from '../../constants'

function getClassNameByValue(active) {
    return 'btn btn-default ' + (active ? 'active' : '');
}

export default ({state = '', keyword = '', onSubmit}) => {
    const onEnter = code => e => {
        e.preventDefault();
        if (e.key == 'Enter') {
            onSubmit({[code]: e.target.value})
        }
    };
    return (
        <div className="col-md-9 pr-0 col-xs-12">
            <RBT.FormGroup className="has-feedback pull-left mr-10">
                <input
                    className="form-control" placeholder="Tên role"
                    defaultValue={keyword}
                    onKeyUp={onEnter('keyword')}
                    onBlur={e => onSubmit({keyword: e.target.value})}/>
                <span className="ti-search form-control-feedback text-hint"/>
            </RBT.FormGroup>
            <div className="btn-group btn-group-toggle user-status">
                <RBT.ControlLabel className={getClassNameByValue(state == '')}>
                    <input type="radio" onClick={e => onSubmit({state: e.target.value})} value=""/> Tất cả
                </RBT.ControlLabel>
                <RBT.ControlLabel className={getClassNameByValue(state == ACTIVE)}>
                    <input type="radio" onClick={e => onSubmit({state: e.target.value})} value={ACTIVE}/>
                    Hoạt động
                </RBT.ControlLabel>
                <RBT.ControlLabel className={getClassNameByValue(state == INACTIVE)}>
                    <input type="radio" onClick={e => onSubmit({state: e.target.value})} value={INACTIVE}/> Bất hoạt
                </RBT.ControlLabel>
            </div>
        </div>
    )
}