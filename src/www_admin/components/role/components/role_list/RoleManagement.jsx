import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux'
import DocumentTitle from '../../../../../www_components/DocumentTitle'
import RoleListContainer from './RoleListContainer'
import SearchContainer from './SearchContainer'
import {isAllow} from '../../../../reducer'
import {
    PERMISSION_ROLE_CREATE,
    PERMISSION_ROLE_VIEW_LIST,
    PERMISSION_ROLE_VIEW,
    PERMISSION_ROLE_EDIT
} from '../../constants'
import CreateRoleContainer from './CreateRoleContainer'
import Alert from '../../../../../www_components/Alert'
import {
    ROLE_CREATE_SUCCESS, ROLE_UPDATE_SUCCESS,
    ROLE_CREATE_FAILURE, ROLE_UPDATE_FAILURE, ROLES_FAILURE
} from '../../ActionTypes'
import RoleModal from './RoleModal'

const {ErrorToast, SuccessToast} = Alert.components;

const successTypeMessages = {
    [ROLE_CREATE_SUCCESS]: 'Tạo nhóm thành công',
    [ROLE_UPDATE_SUCCESS]: 'Cập nhật nhóm thành công'
};

class RoleManagement extends React.Component {
    render() {
        let {allow} = this.props;
        return (
            <div>
                <DocumentTitle title="Nhóm & phân quyền"/>
                <ErrorToast dispatchTypes={[ROLES_FAILURE, ROLE_CREATE_FAILURE, ROLE_UPDATE_FAILURE]}/>
                <SuccessToast dispatchTypes={successTypeMessages}/>
                <RoleModal/>
                <section className="content-header">
                    <div className="row">
                        <div className="col-md-9 col-xs-12">
                            <h2 className="mt-0">Nhóm & phân quyền</h2>
                        </div>
                        <div className="col-md-3 col-xs-12 text-right">
                            {allow.createRole &&
                            <CreateRoleContainer {...this.props} />
                            }
                        </div>
                    </div>
                </section>
                <section className="content">
                    <div className="box">
                        <div className="row">
                            <SearchContainer {...this.props}/>
                        </div>
                        <div className="row">
                            <table className="table tbl-headblue">
                                <thead>
                                <tr>
                                    <th className="hidden-xs">ID</th>
                                    <th>Tên</th>
                                    <th>Mô tả</th>
                                    <th className="hidden-xs text-center">Thành viên</th>
                                    <th className="hidden-xs text-center">Thao tác</th>
                                </tr>
                                </thead>
                                {allow.viewList ?
                                    <RoleListContainer {...this.props}/>
                                    : (
                                    <tbody>
                                    <tr>
                                        <td colSpan="100%">Bạn không được cấp quyền xem danh sách nhóm người dùng
                                        </td>
                                    </tr>
                                    </tbody>)
                                }
                            </table>
                        </div>
                    </div>
                </section>
            </div>
        );
    }
}

const mapStateToProps = (state, props) => ({
    isAllow: props.isAllow,
    allow: {
        viewDetail: props.isAllow(PERMISSION_ROLE_VIEW),
        viewList: props.isAllow(PERMISSION_ROLE_VIEW_LIST),
        updateRole: props.isAllow(PERMISSION_ROLE_EDIT),
        createRole: props.isAllow(PERMISSION_ROLE_CREATE),
    }
});

export default connect(
    mapStateToProps
)(RoleManagement);