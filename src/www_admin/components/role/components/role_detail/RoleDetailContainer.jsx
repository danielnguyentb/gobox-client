import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux'
import {getRoleById} from '../../actions'
import {RoleByIdSelector} from '../../selectors'
import DocumentTitle from '../../../../../www_components/Common/DocumentTitle'
import {push} from 'react-router-redux'

class RoleDetailContainer extends React.Component {
    componentWillMount() {
        this.props.getRoleById(this.props.params.roleId);
    }

    componentWillReceiveProps(nextProps) {
        if (!nextProps.isAllowRoleView) {
            nextProps.dispatch(push('/error/403'));
        }
    }

    render() {
        let {displayName = '', description = ''} = this.props.role || {};
        return (
            <div>
                <DocumentTitle title={`Chi tiết nhóm ${displayName}`} />
                <h4>{displayName}</h4>
                <h5>{description}</h5>
            </div>
        );
    }
}

const mapStateToProps = (state, props) => {
    const getRole = RoleByIdSelector(state);
    return {
        role: getRole(props.params.roleId),
    }
};


export default connect(
    mapStateToProps, {getRoleById}
)(RoleDetailContainer);