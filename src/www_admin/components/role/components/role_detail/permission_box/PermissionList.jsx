import React from 'react'
import * as RBT from 'react-bootstrap';
import PermissionItem from './PermissionItem'

export default (props) => {
    let {permissions} = props;
    const PermissionPanel = ({label, permissions}) => {
        return (
            <RBT.Panel collapsible defaultExpanded header={label} className="list-role">
                <RBT.ListGroup fill className="box-role-detail">
                    <RBT.ListGroupItem>
                        <table className="table">
                            <tbody>
                            {Object.keys(permissions).map(code => {
                                return <PermissionItem
                                    key={code} code={code}
                                    {...permissions[code]} {...props} />
                            })}
                            </tbody>
                        </table>
                    </RBT.ListGroupItem>
                </RBT.ListGroup>
            </RBT.Panel>
        );
    };
    return (
        <div>
            { Object.keys(permissions).map(name => {
                return <PermissionPanel key={name} {...permissions[name]}/>
            })}
        </div>
    );
}