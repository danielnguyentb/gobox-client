import React from 'react'
import Switch from 'react-bootstrap-switch'

export default ({isAllowRoleManage, code, description, name, acl, assigned = false, onChangeSwitch}) => {
    return (
        <tr>
            <td>
                {name}
                <p className="text-hint">{code}</p>
            </td>
            <td className="hidden-xs">{description}</td>
            <td><Switch
                bsSize="mini" value={assigned}
                disabled={!isAllowRoleManage}
                onChange={(elm, _state) => {
                    onChangeSwitch(elm, _state, {code, acl})
                }}/></td>
        </tr>
    );
}