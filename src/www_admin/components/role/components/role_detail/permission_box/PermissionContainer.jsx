import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux'
import PermissionList from './PermissionList'
import {getPermissions, getRoleById} from '../../../actions'
import {addPermission, removePermission} from '../../../actions'
import {isAllow} from '../../../reducer'
import {toastr} from 'react-redux-toastr'


class PermissionContainer extends React.Component {
    static propTypes = {
        role: PropTypes.object,
        params: PropTypes.object.isRequired,
        permissions: PropTypes.object,
    };

    componentWillMount() {
        this.props.getPermissions(this.props.params.roleId);
    }

    handleChangeSwitch(elm, _state, {code, acl}) {
        if(this.props.role && this.props.role.state === 'INACTIVE'){
            return toastr.warning('Nhóm này đã bị khóa, bạn không thể bật tắt phân quyền!');
        }
        const permission = 'ALLOW';
        if (_state) {
            this.props.addPermission({
                ...acl,
                code,
                permission,
                principalId: this.props.params.roleId
            });
        } else if (acl && acl.id) {
            this.props.removePermission(acl.id);
        }
    }

    render() {
        let {permissions} = this.props;
        if (permissions.length === 0) {
            return null;
        }
        return (
            <PermissionList {...this.props} onChangeSwitch={this.handleChangeSwitch.bind(this)}/>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        permissions: state.role.permissions,
    }
};


export default connect(
    mapStateToProps, {getPermissions, getRoleById, addPermission, removePermission}
)(PermissionContainer);