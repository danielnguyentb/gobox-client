import React from 'react';
import RoleDetailContainer from './RoleDetailContainer'
import MemberContainer from './member_box/MemberBox'
import PermissionContainer from './permission_box/PermissionContainer'
import Link from '../../../../../www_components/Common/Link';
import {connect} from 'react-redux'
import {RoleByIdSelector, roleSelector} from '../../selectors'
import {
    PERMISSION_ROLE_EDIT_PERMISSIONS,
    PERMISSION_ROLE_VIEW
} from '../../constants'
import Alert from '../../../../../www_components/Alert'
import {
    PERMISSION_ADD_FAILURE, PERMISSION_REMOVE_FAILURE, PERMISSION_FAILURE, ROLE_MEMBER_ADD_FAILURE, ROLE_MEMBER_REMOVE_FAILURE,
    ROLE_MEMBER_ADD_SUCCESS, ROLE_MEMBER_REMOVE_SUCCESS, PERMISSION_ADD_SUCCESS, PERMISSION_REMOVE_SUCCESS,
} from '../../ActionTypes'

const {ErrorToast, SuccessToast} = Alert.components;

const successTypeMessages = {
    [ROLE_MEMBER_ADD_SUCCESS]: 'Thêm nhân viên vào nhóm thành công!',
    [ROLE_MEMBER_REMOVE_SUCCESS]: 'Xóa nhân viên khỏi nhóm thành công!',
    [PERMISSION_ADD_SUCCESS]: 'Cập nhật thành công',
    [PERMISSION_REMOVE_SUCCESS]: 'Cập nhật thành công'
};

const RoleDetail = (props) => {
    let {displayName = ''} = props.role || {};
    return (
        <div>
            <SuccessToast dispatchTypes={successTypeMessages} />
            <ErrorToast dispatchTypes={[PERMISSION_ADD_FAILURE, PERMISSION_REMOVE_FAILURE, PERMISSION_FAILURE, ROLE_MEMBER_ADD_FAILURE, ROLE_MEMBER_REMOVE_FAILURE]}/>
            <section className="content-header">
                <h1>Chi tiết nhóm {displayName}</h1>
                <ol className="breadcrumb mb-0">
                    <li><Link to="/roles">Danh sách nhóm</Link></li>
                    <li className="active">Chi tiết nhóm {displayName}</li>
                </ol>
            </section>
            <section className="content">
                <div className="box">
                    <div className="row">
                        <div className="col-md-8 col-xs-12">
                            <h4>Danh sách quyền</h4>
                            {props.isAllow(PERMISSION_ROLE_VIEW) ? <PermissionContainer {...props} />
                                : <div className="alert alert-warning">Bạn không có quyền xem thông tin này</div>
                            }
                        </div>
                        <div className="col-md-4 col-xs-12">
                            <RoleDetailContainer {...props} />
                            <MemberContainer {...props} />
                        </div>
                    </div>
                </div>
            </section>
        </div>
    );
};

export default connect((state, props) => {
    const getRole = RoleByIdSelector(state);
    return {
        isAllow: (resource) => props.isAllow(resource),
        isAllowRoleManage: props.isAllow(PERMISSION_ROLE_EDIT_PERMISSIONS),
        isAllowRoleView: props.isAllow(PERMISSION_ROLE_VIEW),
        role: getRole(props.params.roleId),
    }
})(RoleDetail);