import React from 'react';
import {FormGroup} from 'react-bootstrap';
import img_avatar from './avatar.jpg'
import {connect} from 'react-redux'
import {getSuggestedUser, addMember} from '../../../actions'
import {getSuggestedUsers} from '../../../reducer'
import Autosuggest from 'react-autosuggest'
import {
    PERMISSION_ROLE_ADD_MEMBER
} from '../../../constants'

class AddMemberContainer extends React.Component {
    componentWillMount() {
        this.setState({value: '',
            validationState: 'success',
            roleId: this.props.params && this.props.params.roleId ? this.props.params.roleId : 0})
    }

    onSuggestionsFetchRequested(data) {
        let value = data && data.value ? data.value : '';
        this.props.getSuggestedUser(value);
    }

    onSuggestionsClearRequested() {
        this.setState({suggestions: []})
    }

    getSuggestionValue(suggestion) {
        return suggestion.email;
    }

    shouldRenderSuggestions(value) {
        //return value.trim().length > 2;
        return true;
    }

    onSuggestionSelected(e, {suggestion, suggestionValue, sectionIndex, method}) {
        let {addMember, addingMember, params} = this.props;
        if (!addingMember && !suggestion.assigned) {
            addMember(params.roleId, suggestion);
        }
        this.setState({value: ''});
    }

    onChange(event, {newValue}) {
        this.setState({value: newValue});
    };

    renderSuggestion(suggestion) {
        return (
            <div className="item-d">
                <img width={48} height={48} src={img_avatar}/>
                <p className="fw-600 mb-0">{suggestion.email}</p>
                {suggestion.assigned && <span className="text-hint">Đã là thành viên</span>}
            </div>
        )
    }

    componentWillReceiveProps({suggestions}) {
        if(suggestions.length <= 0){
            this.setState({validationState: 'error'})
        } else {
            this.setState({validationState: 'success'})
        }
    }

    render() {
        const inputProps = {
            placeholder: 'Nhập tên hoặc email',
            className: 'form-control',
            value: this.state.value || '',
            onChange: this.onChange.bind(this),
        };
        let {suggestions, isAllow} = this.props;
        return (
            <div>
                <h5>Thêm thành viên</h5>
                <FormGroup className="has-feedback pull-left mr-10 add-user"
                           validationState={this.state.validationState}>
                    {isAllow(PERMISSION_ROLE_ADD_MEMBER) &&
                    <Autosuggest
                        suggestions={suggestions}
                        shouldRenderSuggestions={this.shouldRenderSuggestions.bind(this)}
                        onSuggestionSelected={this.onSuggestionSelected.bind(this)}
                        onSuggestionsFetchRequested={this.onSuggestionsFetchRequested.bind(this)}
                        onSuggestionsClearRequested={this.onSuggestionsClearRequested.bind(this)}
                        getSuggestionValue={this.getSuggestionValue}
                        renderSuggestion={this.renderSuggestion.bind(this)}
                        inputProps={inputProps}/>
                    }
                    <span className="ti ti-plus form-control-feedback text-hint"/>
                </FormGroup>
            </div>
        );
    }
}

const mapStateToProps = (state, props) => ({
    suggestions: getSuggestedUsers(state),
});

export default connect(mapStateToProps, {getSuggestedUser, addMember})(AddMemberContainer)