import React from 'react';
import Link from '../../../../../../www_components/Common/Link';
import * as RBT from 'react-bootstrap';
import img_avatar from './avatar.jpg'
import {
    PERMISSION_ROLE_REMOVE_MEMBER
} from '../../../constants'

export default class MemberItem extends React.Component {
    render() {
        let {fullName, email, id, onRemoveMember, isAllow} = this.props;
        return (
            <RBT.ListGroupItem>
                <div className="block-avatar-user pull-left">
                    <Link to={`/users/detail/${id}`}><img src={img_avatar} alt="avatar"/></Link>
                </div>
                <div className="block-name-user pull-left">
                    <Link to={`/users/detail/${id}`} className="fw-600">{fullName}</Link>
                    <p>{email}</p>
                </div>
                <div className="pull-right">
                    {isAllow(PERMISSION_ROLE_REMOVE_MEMBER) &&
                    <RBT.Button
                        onClick={() => {
                            onRemoveMember(id)
                        }}
                        bsStyle="danger" bsSize="small" className="btn fw-600"><i
                        className="glyphicon glyphicon-remove"/></RBT.Button>
                    }
                </div>
                <div className="clearfix"></div>
            </RBT.ListGroupItem>
        );
    }
}