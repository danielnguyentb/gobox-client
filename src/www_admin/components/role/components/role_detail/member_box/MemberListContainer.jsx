import React, {PropTypes} from 'react';
import * as RBT from 'react-bootstrap';
import {connect} from 'react-redux'
import MemberItem from './MemberItem'
import {getMembers, removeMember} from '../../../actions'
import {toastr} from '../../../../../../www_components/Toastr2'


class MemberListContainer extends React.Component {
    componentWillMount() {
        this.props.getMembers(this.props.params.roleId);
    }

    handleRemoveMember(id) {
        let member = this.props.members[id];
        const toastrConfirmOptions = {
            onOk: () => {
                this.props.removeMember(this.props.params.roleId, member);
            },
        };
        toastr.confirm(`Bạn có chắc muốn xóa thành viên "${member.email}" khỏi nhóm này không?`, toastrConfirmOptions);
    }

    render() {
        let {members} = this.props;
        if (members.length == 0) {
            return null;
        }
        return (
            <RBT.ListGroup>
                <RBT.ListGroupItem>
                    <h5>
                        <span className="fw-600">THÀNH VIÊN</span>
                        <span className="pull-right">{Object.keys(members).length} thành viên</span>
                    </h5>
                </RBT.ListGroupItem>
                {
                    Object.keys(members).map(id => (
                        <MemberItem
                            key={id} {...members[id]}
                            isAllow={this.props.isAllow}
                            onRemoveMember={this.handleRemoveMember.bind(this)}/>
                    ))
                }
            </RBT.ListGroup>
        );
    }
}

const mapStateToProps = (state) => ({
    members: state.role.members,
});

export default connect(mapStateToProps, {getMembers, removeMember})(MemberListContainer)