import React from 'react';
import MemberList from './MemberListContainer'
import AddMemberContainer from './AddMemberContainer'
import {connect} from 'react-redux'
import {isAllow} from '../../../../../reducer'

class MemberBox extends  React.Component{
    render(){
        return (
            <div>
                <AddMemberContainer {...this.props}/>
                <div className="clearfix"></div>
                <MemberList {...this.props}/>
            </div>
        );
    }
}
const mapStateToProps = (state, props) => ({
    isAllow: (resource) => props.isAllow(resource)
});

export default connect(mapStateToProps)(MemberBox)