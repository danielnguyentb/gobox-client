/**
 * Created by saber on 27/11/2016.
 */
import { takeEvery, takeLatest } from 'redux-saga'
import { call, put } from 'redux-saga/effects'
import {ROLE_CREATE_SUCCESS, ROLE_UPDATE_SUCCESS} from './ActionTypes'
import {reset} from 'redux-form'
import {hide} from 'redux-modal'

function* roleCreated(action) {
    try {
        yield put(reset('RoleForm'));
        yield put(hide('RoleModal'));
    } catch (e) {
        //nothing
    }
}

export default function* () {
    yield takeEvery(ROLE_CREATE_SUCCESS, roleCreated);
    yield takeEvery(ROLE_UPDATE_SUCCESS, roleCreated)
}