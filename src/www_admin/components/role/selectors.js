/**
 * Created by Tester-Ali on 09-11-2016.
 */
import {createSelectorCreator, defaultMemoize, createSelector} from 'reselect'
import lodash from 'lodash'
import {NAME} from './constants'

const createDeepEqualSelector = createSelectorCreator(
    defaultMemoize,
    lodash.isEqual
);

export const roleSelector = state => state[NAME];
export const rolesSelector = createSelector(
    roleSelector,
    role => Object.values(role.byId)
);

export const RoleByIdSelector = createDeepEqualSelector(
    roleSelector,
    role => lodash.memoize(
        id => role.byId[id]
    )
);

export const VisibleRolesSelector = createSelector(
    roleSelector,
    role => role.visibleIds.map(id => role.byId[id]).sort((a, b) => {
        const nameA = a.displayName.toUpperCase();
        const nameB = b.displayName.toUpperCase();
        if (nameA < nameB) {
            return -1;
        }
        if (nameA > nameB) {
            return 1;
        }
        // names must be equal
        return 0;
    })
);