/**
 * Created by Tester-Ali on 14-10-2016.
 */
import {encode} from '../../helpers'
export const generateQuery = (params) => {
    let where = {};
    let {keyword, state, limit = 20, skip = 0, order = 'displayName ASC'} = params;
    keyword = keyword ? keyword : '';

    let arr_where = [];

    if (keyword) {
        let t = encode(keyword);
        arr_where.push({'displayName': {like: t}});
    }
    where.and = [{or: arr_where}];
    if (state) where.and.push({state});
    return {where, order};
};