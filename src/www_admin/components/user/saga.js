/**
 * Created by saber on 27/11/2016.
 */
import { takeEvery, takeLatest } from 'redux-saga'
import { call, put } from 'redux-saga/effects'
import {AUTH_LOGOUT_REQUEST} from './ActionTypes'
import {clearSession} from '../../helpers'
import {replace} from 'react-router-redux'
import {getLocation} from '../../selectors'

function* logout(action) {
    clearSession();
    const location = yield select(getLocation);
    yield put(replace({
        pathname: '/login',
        query: {r: location && location.pathname}
    }));
}

export default function* () {
    yield takeEvery(AUTH_LOGOUT_REQUEST, logout);
}