import {combineReducers} from 'redux'
import {
    USER_SUCCESS, USERS_SUCCESS, USER_CREATE_SUCCESS, USER_UPDATE_SUCCESS, USER_CREATE_FAILURE,
    CHANGE_PASSWORD_SUCCESS, USER_LOGOUT_ALL_SUCCESS, USER_CHANGE_EMAIL_SUCCESS,

    AUTH_LOGIN_REQUEST, AUTH_LOGIN_SUCCESS, AUTH_LOGIN_FAILURE, AUTH_LOAD_SUCCESS,
    APPLICATION_VALIDATION_SUCCESS, AUTH_LOGOUT_SUCCESS,
    AUTH_ACCESS_DENIED,
} from './ActionTypes'
import {LOCATION_CHANGE} from '../../ActionTypes'
import {GET_PICKUP_BELONG_TO_HUB} from '../hub/ActionTypes'
import {TRANSPORT_TASKS_SUCCESS} from '../tasks/ActionTypes'
import {GOD, UNAUTHORIZED_CODE, TYPES} from './constants'
import Localize from '../../../resources/Localize'
import moment from 'moment'
moment.locale('vi');

function user(user, action) {
    let {birthday} = user;
    if (birthday) {
        birthday = new Date(birthday);
        user['date'] = moment(birthday).format("D");
        user['month'] = moment(birthday).format("M");
        user['year'] = moment(birthday).format("Y");
    }
    return user;
}

function byId(state = {}, action) {
    let {response, scope} = action;
    let copy = {...state};
    switch (action.type) {
        case USERS_SUCCESS:
            let users = [];
            response.result.map(t => {
                users[t.id] = user(t, action);
            });
            return {
                ...state, ...users
            };
        case USER_SUCCESS:
        case USER_CREATE_SUCCESS:
        case USER_CHANGE_EMAIL_SUCCESS:
        case USER_UPDATE_SUCCESS:
            copy[response.id] = {
                ...state[response.id],
                ...user(response, action),
            };
            return copy;
        case GET_PICKUP_BELONG_TO_HUB.SUCCESS:
            let pickers = response.reduce((obj, u) => {
                u['hubId'] = scope.hubId;
                u['type'] = TYPES.PICKER;
                obj[u.id] = u;
                return obj;
            }, {});
            return {...state, ...pickers};
        case TRANSPORT_TASKS_SUCCESS:
            response.result.map(t => {
                const assignee = t['assignee'] || {};
                copy[assignee.id] = {
                    ...copy[assignee.id],
                    ...assignee
                };
            });
            return copy;
    }
    return state;
}

function successId(state = null, action) {
    switch (action.type) {
        case USER_CREATE_SUCCESS:
        case USER_CHANGE_EMAIL_SUCCESS:
        case USER_UPDATE_SUCCESS:
            return action.response.id;
        case LOCATION_CHANGE:
            return null;
    }
    return state;
}

function visibleIds(state = [], action) {
    const {response} = action;
    let newIds = [];
    switch (action.type) {
        case USERS_SUCCESS:
            if (!Array.isArray(response.result)) return state;
            newIds = response.result.map(user => user.id).filter(id => !state.includes(id));
            return [...state, ...newIds];
        case USER_SUCCESS:
        case USER_CHANGE_EMAIL_SUCCESS:
        case USER_CREATE_SUCCESS:
        case USER_UPDATE_SUCCESS:
            if (!state.includes(action.response.id)) {
                return [...state, action.response.id];
            }
            break;
        case GET_PICKUP_BELONG_TO_HUB.SUCCESS:
            newIds = response.map(u => u.id).filter(id => !state.includes(id));
            return [...state, ...newIds];
    }
    return state;
}

function paginateIds(state = [], action) {
    const {response} = action;
    if (action.type === USERS_SUCCESS) {
        return response.result.map(u => u.id);
    }
    return state;
}

function metadata(state = {}, action) {
    if (action.type == USERS_SUCCESS && action.response.metadata) {
        return action.response.metadata
    }
    return state;
}


//AUTHENTICATE
const validation = (state = {}, action) => {
    switch (action.type) {
        case APPLICATION_VALIDATION_SUCCESS:
            return {
                ...state, ...action.response.token
            };
    }
    return state;
};

const isAuthenticated = (state = false, action) => {
    if ([AUTH_LOAD_SUCCESS, AUTH_LOGIN_SUCCESS].includes(action.type)) return true;
    else if ([AUTH_LOGOUT_SUCCESS].includes(action.type)) return false;
    else if ([UNAUTHORIZED_CODE].includes(action.code)) return false;
    return state;
};


const session = (state = {}, action) => {
    switch (action.type) {
        case AUTH_LOAD_SUCCESS:
            if (action.session)
                return {
                    ...state, ...action.session
                };
            break;
        case AUTH_LOGIN_SUCCESS:
            if (action.response) {
                return {
                    ...state, ...action.response
                }
            }
            break;
    }
    return state;
};

const permissions = (state = [], action) => {
    switch (action.type) {
        case AUTH_LOAD_SUCCESS:
            if (action.session && action.session.permissions)
                return action.session.permissions;
            break;
        case AUTH_LOGIN_SUCCESS:
            if (action.response && action.response.permissions) {
                return action.response.permissions
            }
            break;
    }
    return state;
};

const updatedUser = (state = false, action) => {
    switch (action.type) {
        case USER_UPDATE_SUCCESS:
            return action.response;
        case LOCATION_CHANGE:
            return false;
    }
    return state;
};
const changedUserEmail = (state = false, action) => {
    switch (action.type) {
        case USER_CHANGE_EMAIL_SUCCESS:
            return action.response;
        case LOCATION_CHANGE:
            return false;
    }
    return state;
};

export default combineReducers({
    permissions, session, isAuthenticated, validation, successId,
    byId, metadata, visibleIds, paginateIds, updatedUser, changedUserEmail,
    loginFailed: (state, {type}) => type == AUTH_LOGIN_FAILURE,
    userIdCreated: (state, action) => action.type == USER_CREATE_SUCCESS && action.response.id ? action.response.id : null,
    changedPassword: (state = false, action) => action.type == CHANGE_PASSWORD_SUCCESS,
    changedPasswordMessage: (state = false, action) => action.type == CHANGE_PASSWORD_SUCCESS ? action.response.message : null,
    error: (state = null, {error}) => error ? Localize.t(error) : null,
});