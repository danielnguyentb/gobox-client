/**
 * Created by Tester-Ali on 09-11-2016.
 */
import {createSelectorCreator, defaultMemoize, createSelector} from 'reselect'
import lodash from 'lodash'
import {NAME, TYPES, GOD} from './constants'

const createDeepEqualSelector = createSelectorCreator(
    defaultMemoize,
    lodash.isEqual
);

export const hydrate = (user) => {
    if(user){
        user['detailPath'] = `/users/detail/${user.id}`;
    }
    return user;
};

export const userSelector = state => state[NAME];
export const tokenSelector = createSelector(
    userSelector,
    user => user.session ? user.session.id : null
);
export const UserByIdSelector = createDeepEqualSelector(
    userSelector,
    user => lodash.memoize(
        id => hydrate(user.byId[id])
    )
);
export const usersSelector = createSelector(
    [userSelector, UserByIdSelector],
    (user, getUser) => Object.keys(user.byId).map(id => getUser(id))
);

export const PaginateUsersSelector = createSelector(
    [userSelector, UserByIdSelector],
    (user, getUser) => user.paginateIds.map(id => getUser(id))
);

export const MetadataSelector = createDeepEqualSelector(
    userSelector,
    user => user.metadata
);

export const PickersByHubIdSelector = createDeepEqualSelector(
    usersSelector,
    users => lodash.memoize(
        hubId => users.filter(t => t.type == TYPES.PICKER && t.hubId == hubId)
    )
);

export const ProfileIdSelector = createSelector(
    userSelector,
    user => user.session ? user.session.userId : 0
);

export const ProfileSelector = createDeepEqualSelector(
    [ProfileIdSelector, UserByIdSelector],
    (profileId, getUser) => getUser(profileId)
);

export const UserSuccessSelector = createDeepEqualSelector(
    userSelector,
    user => user.byId[user.successId]
);

export const isGodSelector = createSelector(
    userSelector,
    user => lodash.memoize(
        userId => {
            if (userId) {
                return parseInt(userId) === GOD;
            } else {
                if (!user.session || !user.session.userId) return false;
                return parseInt(user.session.userId) === GOD;
            }
        }
    )
);

export const isAllowSelector = createDeepEqualSelector(
    [userSelector, isGodSelector],
    (user, isGod) => lodash.memoize(
        resource => {
            if (isGod()) return true;
            return Array.isArray(user.permissions) && user.permissions.includes(resource);
        }
    )
);
export const isMeSelector = createSelector(
    userSelector,
    user => lodash.memoize(
        userId => {
            if (!user.session || !user.session.userId) return false;
            return user.session.userId == userId;
        }
    )
);