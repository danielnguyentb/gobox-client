import React from 'react';
import Link from '../../../../../www_components/Link'
import {MenuItem} from 'react-bootstrap'
import {Logout} from '../../components'
import avatar from './avatar.jpg'

export default ({id, fullName, dispatch}) => {
    return (
        <div className="navbar-custom-menu">
            <ul className="nav navbar-nav">
                <li className="dropdown user user-menu">
                    <Link to="/" className="dropdown-toggle" data-toggle="dropdown">
                        <img src={avatar} className="user-image" alt="User Image"/>
                        <span className="hidden-xs">{fullName}</span>
                    </Link>
                    <ul className="dropdown-menu profile-dropdown" role="menu">
                        <li><Link to={`/users/detail/${id}`}><i className="fa fa-info-circle"/>Trang
                            cá nhân</Link></li>
                        <MenuItem><Logout dispatch={dispatch} /></MenuItem>
                    </ul>
                </li>
            </ul>
        </div>
    )
}
