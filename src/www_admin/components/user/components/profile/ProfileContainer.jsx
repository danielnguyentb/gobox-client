import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux'
import Profile from './Profile'
import {ProfileSelector} from '../../selectors'
import {getUserById} from '../../actions'
import {getUserId} from '../../../../helpers'

class ProfileContainer extends Component {
    componentWillMount() {
        this.props.getUserById(getUserId())
    }

    render() {
        return (
            <Profile {...this.props.profile} dispatch={this.props.dispatch}/>
        );
    }
}


const mapStateToProps = (state) => {
    return {
        profile: ProfileSelector(state),
    }
};


export default connect(
    mapStateToProps, {getUserById}
)(ProfileContainer)