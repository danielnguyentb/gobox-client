import React, {Component} from 'react';
import {connect} from 'react-redux'
import {login, getApplicationValidation} from '../../actions'
import {saveSession} from '../../../../helpers'
import {replace} from 'react-router-redux'
import {bindActionCreators} from 'redux'
import {UserLoginReduxForm} from './UserLoginForm'
import {userSelector} from '../../selectors'

class LoginContainer extends Component {
    constructor(props) {
        super(props);
        this.state = {captcha: ''}
    }

    componentWillMount() {
        const {isAuthenticated, getApplicationValidation} = this.props;
        if (isAuthenticated) {
            this.redirectToBack()
        } else {
            getApplicationValidation();
        }
    }

    redirectToBack() {
        const {location, onRedirect} = this.props;
        onRedirect(location && location.query && location.query['r'] ? location.query['r'] : '/');
    }

    componentDidUpdate() {
        const {
            dispatch, isAuthenticated, session, onRedirect, location,
            getApplicationValidation, needChangePass, loginFailed
        } = this.props;
        if (isAuthenticated) {
            saveSession(session);
            if (needChangePass == 1) {
                onRedirect({
                    pathname: '/change_pass',
                    query: location && location.query
                });
            } else {
                this.redirectToBack();
            }
        } else if (loginFailed) {
            getApplicationValidation();
            if (typeof grecaptcha !== "undefined") {
                grecaptcha.reset();
            }
        }
    }

    handleLogin(credentials) {
        const {email, password} = credentials;
        const {login, validation} = this.props;
        const {captcha} = this.state;
        login({
            ...validation,
            captcha, email, password
        });
    }

    handleChangeCaptcha(value) {
        this.setState({captcha: value});
    }


    render() {
        const {requireCaptcha} = this.props;
        return (
            <UserLoginReduxForm
                onChangeCaptcha={::this.handleChangeCaptcha}
                requireCaptcha={requireCaptcha}
                onSubmit={::this.handleLogin}/>
        );
    }
}

const mapStateToProps = (state) => {
    const user = userSelector(state);
    return {
        requireCaptcha: user.validation && user.validation.captcha,
        validation: user.validation,
        isAuthenticated: user.isAuthenticated,
        session: user.session,
        error: user.error,
        loginFailed: user.loginFailed,
        needChangePass: user.session.needChangePass,
    }
};

const mapDispatchToProps = (dispatch) => ({
    ...bindActionCreators({
        login, getApplicationValidation
    }, dispatch),
    onRedirect: (path) => {
        dispatch(replace(path))
    },
    dispatch
});

export default connect(
    mapStateToProps, mapDispatchToProps
)(LoginContainer);