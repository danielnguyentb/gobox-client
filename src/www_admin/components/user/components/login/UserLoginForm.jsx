import React from 'react';
import './login.scss'
import ReCAPTCHA from 'react-google-recaptcha'
import img_dark from './img/logo.png'
import Config from '../../../../../config.json'

import {FormGroup} from 'react-bootstrap'
import {Field, reduxForm} from 'redux-form'
import {renderField, loginValidate} from '../../helper'
import DocumentTitle from '../../../../../www_components/Common/DocumentTitle'

export class UserLoginForm extends React.Component {
    render() {
        const {requireCaptcha, onChangeCaptcha, handleSubmit, error, pristine, submitting} = this.props;
        return (
            <div className="container page-container">
                <DocumentTitle title="Đăng nhập"/>
                <div className="page-content">
                    <div className="v2">
                        <div className="logo mb-10"><img src={img_dark} alt="image"/></div>

                        <form method="POST" onSubmit={handleSubmit}>
                            <Field component={renderField} type="email" name="email" className="form-control"
                                   placeholder="Tên đăng nhập"/>
                            <Field component={renderField} type="password" name="password" className="form-control"
                                   placeholder="Mật khẩu"/>
                            {requireCaptcha &&
                            <FormGroup>
                                <div className="row">
                                    <div className="col-xs-12 ">
                                        <ReCAPTCHA
                                            ref="recaptcha"
                                            sitekey={Config.reCaptchaKey}
                                            onChange={onChangeCaptcha}
                                        />
                                    </div>
                                </div>
                            </FormGroup>
                            }
                            {error && <div className="alert alert-danger">{error}</div>}
                            <button
                                type="submit"
                                disabled={pristine || submitting}
                                className="btn-lg btn btn-primary btn-rounded btn-block"
                            >Đăng nhập</button>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}

export const UserLoginReduxForm = reduxForm({
    form: 'UserLoginForm',
    validate: loginValidate
})(UserLoginForm);