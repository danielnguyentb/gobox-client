import React, {PropTypes} from 'react'
import moment from 'moment'
import ChangeEmail from './modals/ChangeEmail'
import ChangePassword from './modals/ChangePassword'
import {toastr} from 'react-redux-toastr'
import {
    USER_LOGOUT_ALL_FAILURE,
    USER_LOGOUT_ALL_SUCCESS,
    USER_UPDATE_FAILURE,
    USER_UPDATE_SUCCESS
} from '../../ActionTypes'
import {ErrorAlert} from '../../../../../www_components/Alert'
import Touchable from '../../../../../www_components/Touchable'


moment.locale('vi');

export default ({
    user, allow, logoutAll,
    changeEmail, changeMyPassword, changedPassword,
    changeStaffPassword, changeState, changedUserEmail
}) => {
    let {lastChangedPass, email, state} = user;
    return (
        <table className="table tbl-info-detail">
            <thead>
            <tr>
                <th colSpan="2">BẢO MẬT</th>
                <th className="hidden-xs">&nbsp;</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td className="fw-600">Mật khẩu</td>
                <td>
                    ******* {lastChangedPass ? `(từ ${moment(lastChangedPass).format('LLL')})` : '(Chưa đổi mật khẩu bao giờ)'}</td>
                <td className="hidden-xs">
                    {allow.changeMyPassword ?
                        <ChangePassword
                            changedPassword={!!changedPassword}
                            onSubmit={changeMyPassword}
                            showRequireToChangePassword={false}
                            validate={true}
                            showOldPasswordControl={true}/>
                        : (allow.changeStaffPassword ?
                        <ChangePassword
                            validate={false}
                            changedPassword={!!changedPassword}
                            onSubmit={changeStaffPassword}
                            showOldPasswordControl={false}
                            showRequireToChangePassword={true}/> : '')
                    }
                </td>
            </tr>
            {allow.logoutAll &&
            <tr>
                <td className="fw-600">Đăng nhập</td>
                <td>
                    <Touchable
                        actionTypes={[USER_LOGOUT_ALL_FAILURE, USER_LOGOUT_ALL_SUCCESS]}
                        bsStyle="default"
                        onClick={(e) => logoutAll()}>
                        Đăng xuất tất cả thiết bị
                    </Touchable>
                    <ErrorAlert dispatchTypes={[USER_LOGOUT_ALL_FAILURE]}/>
                </td>
                <td className="hidden-xs">&nbsp;</td>
            </tr>
            }
            <tr>
                <td className="fw-600">Email</td>
                <td>
                    { email }
                </td>
                <td className="hidden-xs">
                    {allow.changeEmail &&
                    <ChangeEmail
                        onSubmit={changeEmail} changedUserEmail={changedUserEmail} />
                    }
                </td>
            </tr>
            <tr>
                <td className="fw-600">Trạng thái</td>
                <td className="fw-600">
                    {state == 'ACTIVE' ? 'Hoạt động' : 'Bất hoạt'}
                </td>
                <td className="hidden-xs">
                    {allow.changeState &&
                    <Touchable
                        actionTypes={[USER_UPDATE_FAILURE, USER_UPDATE_SUCCESS]}
                        type="button"
                        className="btn btn-danger"
                        onClick={e => {
                            toastr.confirm('Bạn có chắc chắn muốn ' + (state == 'ACTIVE' ? 'khóa' : 'mở khóa') +
                                ' tài khoản này?', {
                                onOk: () => changeState({state: state == 'ACTIVE' ? 'INACTIVE' : 'ACTIVE'})
                            });
                        }
                        }
                    >{state == 'ACTIVE' ? 'KHÓA' : 'MỞ KHÓA'}</Touchable>
                    }
                </td>
            </tr>
            </tbody>
        </table>
    )
}