import React from 'react'
import img_avatar from './avatar.jpg'

export default ({fullName = ' -- ', username = '', avatar}) => {
    return (
        <div className="col-md-offset-1 col-md-3 col-xs-12 text-center">
            <div className="row">
                <div className="col-md-12 block-avatar hidden">
                    <img src={img_avatar}/>
                </div>
                <div className="col-md-12 user-infomation">
                    <h4>
                        { fullName}
                    </h4>
                    <p>{username}</p>
                </div>
            </div>
        </div>
    )
}