import React, {Component, PropTypes} from 'react'
import Link from '../../../../../www_components/Common/Link'
import AvatarWidget from './AvatarWidget'
import SecurityWidget from './SecurityWidget'
import {PersonalInfoReduxForm} from './PersonalInfoWidget'
import {
    USER_UPDATE_SUCCESS,
    USER_UPDATE_FAILURE,
    USER_LOGOUT_ALL_SUCCESS,
    CHANGE_PASSWORD_SUCCESS,
    USER_CHANGE_EMAIL_SUCCESS
} from '../../ActionTypes'
import {ErrorToast, SuccessToast} from '../../../../../www_components/Alert'

const successMessages = {
    [USER_UPDATE_SUCCESS]: 'Cập nhật thành công',
    [USER_LOGOUT_ALL_SUCCESS]: 'Cập nhật thành công',
    [CHANGE_PASSWORD_SUCCESS]: 'Cập nhật thành công',
    [USER_CHANGE_EMAIL_SUCCESS]: 'Cập nhật thành công'
};

export default class UserDetail extends Component {
    static propTypes = {
        user: PropTypes.object.isRequired,
        isMe: PropTypes.bool.isRequired,
        allow: PropTypes.object.isRequired,
        logoutAll: PropTypes.func.isRequired,
        onSubmit: PropTypes.func.isRequired,
        changeEmail: PropTypes.func.isRequired,
        changeMyPassword: PropTypes.func.isRequired,
        changeStaffPassword: PropTypes.func.isRequired,
        changeState: PropTypes.func.isRequired,
    };

    render() {
        const {user, onSubmit, allow} = this.props;
        return (
            <div>
                <ErrorToast dispatchTypes={[USER_UPDATE_FAILURE]}/>
                <SuccessToast dispatchTypes={successMessages}/>
                <section className="content-header">
                    <h1>Chi tiết nhân viên</h1>
                    <ol className="breadcrumb mb-0">
                        <li><Link to="/users">Danh sách nhân viên</Link></li>
                        <li className="active">Chi tiết nhân viên</li>
                    </ol>
                </section>
                <section className="content">
                    <div className="box">
                        <div className="row">
                            <AvatarWidget {...this.props.user} />
                            <div className="col-md-6 col-xs-12">
                                <SecurityWidget {...this.props} />
                                {user && user.id &&
                                <PersonalInfoReduxForm
                                    onSubmit={onSubmit}
                                    initialValues={user}
                                    allow={allow}
                                />
                                }
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        )
    }
}