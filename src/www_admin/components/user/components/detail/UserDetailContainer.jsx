import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux'
import UserDetail from './UserDetail'
import {
    updateUser,
    getUserById,
    changeEmail,
    changePassword,
    changePasswordStaff,
    logoutAll
} from '../../actions'
import DocumentTitle from '../../../../../www_components/Common/DocumentTitle'
import {UserByIdSelector} from '../../selectors'
import {
    PERMISSION_USER_EDIT,
    PERMISSION_USER_VIEW,
    PERMISSION_USER_CHANGE_PASSWORD,
    PERMISSION_USER_LOGOUT_ALL
} from '../../constants'
import {CHANGE_PASSWORD_SUCCESS} from '../../ActionTypes'
import moment from 'moment'
moment.locale('vi');


class UserDetailContainer extends React.Component {
    constructor(props) {
        super(props);
    }

    componentWillMount() {
        let userId = this.props.userId;
        if (userId > 0) {
            this.props.getUserById(userId);
        }
    }

    render() {
        return (
            <DocumentTitle title="Chi tiết nhân viên">
                <UserDetail
                    {...this.props}
                    user={this.props.user || {}}
                    onSubmit={this.submit.bind(this)}
                    changeEmail={this.changeEmail.bind(this)}
                    changeStaffPassword={this.changePasswordStaff.bind(this)}
                    changeMyPassword={this.changePassword.bind(this)}
                    logoutAll={this.logoutAll.bind(this)}
                    changeState={this.changeState.bind(this)}
                />
            </DocumentTitle>
        );
    }
    changeState(data) {
        const {updateUser, userId} = this.props;
        updateUser(data, userId);
    }
    submit(data) {
        const {updateUser, user, userId} = this.props;
        const {date, month, year, firstName, lastName, gender} = data;
        if (date && month && year) {
            let birthday = new Date();
            birthday.setDate(date);
            birthday.setMonth(month - 1);
            birthday.setFullYear(year);
            user.birthday = moment(birthday.toISOString()).format("Y-M-D");
        }
        delete user['id'];
        updateUser({...user, firstName, lastName, gender}, userId);
    }

    changeEmail(user) {
        this.props.changeEmail(this.clearId(user), this.props.userId);
    }

    changePasswordStaff(user) {
        this.props.changePasswordStaff(this.clearId(user), this.props.userId);
    }

    changePassword(user) {
        this.props.changePassword(this.clearId(user), this.props.userId);
    }

    logoutAll() {
        this.props.logoutAll(this.props.userId);
    }

    clearId(user) {
        let obj = Object.assign({}, user);
        delete obj['id'];
        return obj;
    }
}

const mapStateToProps = (state, props) => {
    const userId = props.params['userId'];
    const getUser = UserByIdSelector(state);
    const {isMe, isGod, isAllow} = props;
    return {
        allow: {
            viewDetail: isMe(userId) || isAllow(PERMISSION_USER_VIEW),
            changeMyPassword: isMe(userId),
            changeStaffPassword: isAllow(PERMISSION_USER_CHANGE_PASSWORD) && !isGod(userId),
            changeEmail: isMe(userId) || (isAllow(PERMISSION_USER_EDIT) && !isGod(userId)),
            updateInfo: isMe(userId) || (isAllow(PERMISSION_USER_EDIT) && !isGod(userId)),
            logoutAll: isMe(userId) || (isAllow(PERMISSION_USER_LOGOUT_ALL) && !isGod(userId)),
            changeState: !isMe(userId) && (isAllow(PERMISSION_USER_EDIT) && !isGod(userId)),
        },
        isAllow: (resource) => isAllow(resource),
        isMe: isMe(userId),
        user: getUser(userId),
        userId,
        changedPassword: state.actionType === CHANGE_PASSWORD_SUCCESS,
        updateUser: state.user.updateUser,
        changedUserEmail: state.user.changedUserEmail
    }
};


export default connect(
    mapStateToProps, {getUserById, updateUser, changePasswordStaff, changePassword, logoutAll, changeEmail}
)(UserDetailContainer);