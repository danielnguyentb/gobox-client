import React, {PropTypes} from 'react'
import * as RBT from 'react-bootstrap'
import {USER_CHANGE_EMAIL_FAILURE, USER_CHANGE_EMAIL_SUCCESS} from '../../../ActionTypes'
import {ErrorAlert} from '../../../../../../www_components/Alert'
import Touchable from '../../../../../../www_components/Touchable'

export default class ChangeEmail extends React.Component {
    static propTypes = {
        onSubmit: PropTypes.func.isRequired
    };

    constructor(props) {
        super(props);
        this.state = {
            email: '',
            showModal: false,
            errEmail: false,
            disabledEmail: true
        };
    }
    componentWillReceiveProps(nextProps) {
        if(nextProps.changedUserEmail) {
            this.setState({showModal: false})
        }
    }
    handleVerifyEmail(val) {
        if(val.target.value != '') {
            if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(val.target.value)) {
                this.setState({errEmail: false});
                this.setState({disabledEmail: false});
            } else {
                this.setState({errEmail: val.target.value + " không phải là định dạng email"});
            }
        }
    }
    handleDisabledEmailInp(val) {
        this.setState({email: val.target.value});
        if(val.target.value != '') {
            if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(val.target.value)) {
                this.setState({disabledEmail: false});
            } else {
                this.setState({disabledEmail: true});
            }
        }
    }
    onSubmit(e) {
        let {onSubmit} = this.props;
        e.preventDefault();
        onSubmit({email: this.state.email});
    }
    render() {
        let {showModal} = this.state;
        return (
            <div>
                <RBT.Button bsStyle="link" onClick={e=>this.setState({showModal: true})}>Thay đổi</RBT.Button>
                <RBT.Modal show={showModal} onHide={e=>this.setState({showModal: false})}>
                    <form>
                        <RBT.Modal.Header closeButton>
                            <RBT.Modal.Title>Thay đổi email</RBT.Modal.Title>
                        </RBT.Modal.Header>
                        <RBT.Modal.Body>

                            <RBT.FormGroup controlId=''>
                                <RBT.ControlLabel>Email mới</RBT.ControlLabel>
                                <div className="input-required">
                                    <input
                                        autoFocus={true}
                                        type="email"
                                        onChange={(e) => {this.handleDisabledEmailInp(e)}}
                                        onBlur={e => this.handleVerifyEmail(e)}
                                        className="form-control" name='email'/>
                                    <span className="required-symbol">*</span>
                                </div>
                            </RBT.FormGroup>
                            {this.state.errEmail &&
                                <div className="alert alert-danger">
                                    {this.state.errEmail}
                                </div>
                            }
                            <ErrorAlert dispatchTypes={[USER_CHANGE_EMAIL_FAILURE]}/>
                        </RBT.Modal.Body>
                        <RBT.Modal.Footer>
                            <RBT.Button onClick={e=>this.setState({showModal: false})}>ĐÓNG</RBT.Button>
                            <RBT.Button
                                disabled={this.state.disabledEmail}
                                className="btn btn-primary"
                                onClick={(e) => {this.onSubmit(e)}}
                            >
                                LƯU
                            </RBT.Button>

                        </RBT.Modal.Footer>
                    </form>
                </RBT.Modal>
            </div>
        )
    }
}