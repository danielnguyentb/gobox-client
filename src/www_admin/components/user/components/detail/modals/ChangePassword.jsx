import React, {PropTypes} from 'react'
import * as RBT from 'react-bootstrap'
import ObjectForm from '../../../../../helpers/ValidationForm'
import Validation from 'react-validation'
import ChangeUserPassword from '../../../../../../www_components/ChangeUserPassword'
import {CHANGE_PASSWORD_FAILURE, CHANGE_PASSWORD_SUCCESS} from '../../../ActionTypes'
import {ErrorAlert} from '../../../../../../www_components/Alert'
import Touchable from '../../../../../../www_components/Touchable'

Validation.rules = ObjectForm;
export default class ChangePassword extends React.Component {
    static propTypes = {
        validate: PropTypes.bool.isRequired,
        changedPassword: PropTypes.bool.isRequired,
        onSubmit: PropTypes.func.isRequired,
        showRequireToChangePassword: PropTypes.bool.isRequired,
        showOldPasswordControl: PropTypes.bool.isRequired,
    };

    constructor(props) {
        super(props);
        this.state = {
            showModal: false,
            currentPassword: '',
            newPassword: '',
            isValid: false,
            requiredToChange: true,
        };
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.changedPassword) {
            this.setState({showModal: false});
        }
    }

    render() {
        let {
            onSubmit, validate,
            showOldPasswordControl, showRequireToChangePassword
        } = this.props;
        let {currentPassword, requiredToChange, newPassword, showModal, isValid}  = this.state;
        return (
            <div>
                <RBT.Button bsStyle="link" onClick={e=>this.setState({showModal: true})}>Thay đổi</RBT.Button>
                <RBT.Modal show={showModal} onHide={e=>this.setState({showModal: false})}>
                    <Validation.components.Form
                        onSubmit={(e) => {
                            e.preventDefault();
                            onSubmit({oldPassword: currentPassword, newPassword: newPassword, requiredToChange: requiredToChange});
                        }}
                        method="POST">
                        <RBT.Modal.Header closeButton>
                            <RBT.Modal.Title>Thay đổi mật khẩu</RBT.Modal.Title>
                        </RBT.Modal.Header>
                        <RBT.Modal.Body>
                            <ChangeUserPassword
                                requireCurrentPassword={showOldPasswordControl} validate={validate}
                                onChange={data => this.setState(data)}/>

                            {showRequireToChangePassword &&
                            <RBT.Checkbox name="required" checked={this.state.requiredToChange}
                                          onChange={e => this.setState({requiredToChange: !this.state.requiredToChange})}>
                                Yêu cầu thay đổi mật khẩu (khi đăng nhập bằng mật khẩu mới phải đổi mật khẩu).
                            </RBT.Checkbox>
                            }
                            <ErrorAlert dispatchTypes={[CHANGE_PASSWORD_FAILURE]}/>
                        </RBT.Modal.Body>
                        <RBT.Modal.Footer>
                            <RBT.Button onClick={e=>this.setState({showModal: false})}>ĐÓNG</RBT.Button>
                            <Touchable
                                actionTypes={[CHANGE_PASSWORD_FAILURE, CHANGE_PASSWORD_SUCCESS]}
                                type="submit" disabled={!isValid}
                                className="btn btn-primary">LƯU</Touchable>

                        </RBT.Modal.Footer>
                    </Validation.components.Form>
                </RBT.Modal>
            </div>
        )
    }
}