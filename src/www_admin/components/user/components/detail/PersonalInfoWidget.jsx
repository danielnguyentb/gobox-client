import React from 'react'
import {renderDatetimeSelectField} from '../../../../../www_components/DatetimeSelection'
import {Field, Fields, reduxForm} from 'redux-form'

export const PersonalInfoForm = ({handleSubmit, error, submitting, allow, pristine}) => {
    return (
        <form onSubmit={handleSubmit}>
            <table className="table tbl-info-detail">
                <thead>
                <tr>
                    <th colSpan="4">THÔNG TIN CÁ NHÂN</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td className="fw-600">Họ tên</td>
                    <td colSpan="2">
                        <Field name="lastName" component="input" type="text" className="form-control" placeholder="Họ"/>
                    </td>
                    <td>
                        <Field name="firstName" component="input" type="text" className="form-control" placeholder="Tên"/>
                    </td>
                </tr>
                <tr>
                    <td className="fw-600">Ngày sinh</td>
                    <td colSpan="3">
                        <Fields
                            names={['date', 'month', 'year']}
                            component={renderDatetimeSelectField}/>
                    </td>
                </tr>
                <tr>
                    <td className="fw-600">Giới tính</td>
                    <td>
                        <Field component="select" name="gender" className="form-control">
                            <option value="1">Nam</option>
                            <option value="0">Nữ</option>
                        </Field>
                    </td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                {error && <tr><td><div className="alert alert-danger">{error}</div></td></tr>}
                <tr>
                    <td colSpan="4" className="text-right">
                        {allow.updateInfo &&
                        <button
                            disabled={pristine || submitting}
                            className="btn btn-primary"
                            type="submit">THAY ĐỔI</button>
                        }
                    </td>
                </tr>
                </tbody>
            </table>
        </form>
    )
};

export const validate = values => {
    const errors = {};
    if(Object.keys(values).length === 0) return errors;
    if (values.date || values.month || values.year) {
        if (!values.date) {
            errors.date = 'Ngày sinh không được để trống'
        } else if (values.date < 0 || values.date > 31 || /^\d+$/.test(values.date) == false) {
            errors.date = 'Ngày sinh không hợp lệ'
        }
        if (!values.month) {
            errors.month = 'Tháng không được để trống'
        }
        if (!values.year) {
            errors.year = 'Năm sinh không được để trống'
        } else if (values.year <= 1000 || values.year > (new Date).getFullYear() || /^\d+$/.test(values.date) == false) {
            errors.year = 'Năm sinh không hợp lệ'
        }
    }
    return errors
};
export const PersonalInfoReduxForm = reduxForm({
    form: 'PersonalInfoForm',
    validate
})(PersonalInfoForm);