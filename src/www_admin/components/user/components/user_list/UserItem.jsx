import React from 'react';
import Link from '../../../../../www_components/Common/Link';

export default ({id, username, state, fullName, email, emailVerified, allow}) => {
    return (
        <tr>
            <td className="hidden-xs">#{id}</td>
            <td className="fw-600">
                {state == 'INACTIVE' ? <i className="ti ti-lock"/> : ''}&nbsp;
                {allow.view ? <Link to={`/users/detail/${id}`}>{username}</Link> : username }
            </td>
            <td className="hidden-xs">{fullName}</td>
            <td className="email-active">
                {emailVerified ? null : <i className="fa fa-circle text-danger"/>}
                {email}
            </td>
        </tr>
    );
}