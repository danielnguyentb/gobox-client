import React from 'react';
import * as RBT from 'react-bootstrap'
import Validation from 'react-validation';

export default ({onSubmit, state = '', keyword}) => {
    const getClassNameByValue = (isActive) => {
        return 'btn btn-default ' + (isActive ? 'active' : '');
    };
    const onEnter = code => e => {
        e.preventDefault();
        if (e.key == 'Enter') {
            onSubmit({[code]: e.target.value})
        }
        return false;
    };
    return (
        <div className="row">
            <div className="col-md-12 pr-0 col-xs-12">
                <RBT.FormGroup className="has-feedback pull-left mr-10">
                    <RBT.FormControl
                        type="text" defaultValue={keyword}
                        onKeyUp={onEnter('keyword')}
                        onBlur={e=>onSubmit({keyword: e.target.value})}
                        placeholder="Username/Email"/>
                    <span className="ti-search form-control-feedback text-hint"/>
                </RBT.FormGroup>
                <div className="btn-group btn-group-toggle user-status mb-15">
                    <RBT.ControlLabel className={getClassNameByValue(state == '')}>
                        <input onClick={e=>onSubmit({state: e.target.value})} value="" type="checkbox"/> Tất cả
                    </RBT.ControlLabel>
                    <RBT.ControlLabel className={getClassNameByValue(state == 'ACTIVE')}>
                        <input onClick={e=>onSubmit({state: e.target.value})} value="ACTIVE" type="checkbox"/>Hoạt
                        động
                    </RBT.ControlLabel>
                    <RBT.ControlLabel className={getClassNameByValue(state == 'INACTIVE')}>
                        <input onClick={e=>onSubmit({state: e.target.value})} value="INACTIVE" type="checkbox"/> Bất
                        hoạt
                    </RBT.ControlLabel>
                </div>
            </div>
        </div>
    );
}