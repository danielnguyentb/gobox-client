import React from 'react'
import UserItem from './UserItem'

export default ({users, allow}) => (
    <tbody>
    {users.map(user => {
        return <UserItem key={user.id} {...user} allow={allow}/>
    })}
    </tbody>
)