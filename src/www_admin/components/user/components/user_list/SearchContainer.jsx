import React, {Component, PropTypes} from 'react'
import {connect} from 'react-redux'
import SearchBox from './SearchBox'
import {push} from 'react-router-redux'
import qs from 'qs'

class SearchContainer extends Component {

    search(data){
        let params = {
            ...this.props.location.query,
            ...data,
            skip: 0,
        };
        this.props.dispatch(push(`/users?${qs.stringify(params)}`));
    }

    render() {
        let params = {...this.props.metadata, ...this.props.location.query};
        return <SearchBox {...params} onSubmit={::this.search}/>
    }
}

const mapStateToProps = (state) => ({
    metadata: state.customer.metadata,
});

export default connect(mapStateToProps)(SearchContainer)