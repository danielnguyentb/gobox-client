import React from 'react';
import UserList from './UserList';
import {connect} from 'react-redux'
import {getAllUsers} from '../../actions'
import {PaginateUsersSelector, MetadataSelector} from '../../selectors'

class UserListContainer extends React.Component {
    componentWillMount() {
        this.props.getAllUsers(this.props.location.query);
    }

    componentWillReceiveProps({location, getAllUsers}) {
        if (location.search != this.props.location.search) {
            getAllUsers(location.query);
        }
    }

    render() {
        return (
            <UserList {...this.props}/>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        users: PaginateUsersSelector(state),
        metadata: MetadataSelector(state),
    }
};

export default connect(mapStateToProps, {getAllUsers})(UserListContainer)