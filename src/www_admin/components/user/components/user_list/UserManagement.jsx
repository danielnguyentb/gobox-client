import React, {Component, PropTypes} from 'react';
import Link from '../../../../../www_components/Common/Link';
import * as RBT from 'react-bootstrap';
import {connect} from 'react-redux'
import UserListContainer from './UserListContainer'
import SearchContainer from './SearchContainer'
import PaginationContainer from './PaginationContainer'
import * as Permissions from '../../constants'
import DocumentTitle  from '../../../../../www_components/DocumentTitle'

class UserManagementContainer extends React.Component {
    render() {
        let {allow} = this.props;
        return (
            <div>
                <DocumentTitle title="Danh sách nhân viên"/>
                <section className="content-header">
                    <div className="row">
                        <div className="col-md-9 col-xs-12">
                            <h2 className="mt-0">Danh sách nhân viên</h2>
                        </div>
                        <div className="col-md-3 col-xs-12 text-right">
                            <RBT.FormGroup>
                                {allow.create &&
                                <Link to="/users/create">
                                    <RBT.Button bsStyle="primary" className="btn fw-600"><i className="fa fa-plus mr-5"/> TẠO
                                        NHÂN VIÊN</RBT.Button>
                                </Link>
                                }
                            </RBT.FormGroup>
                        </div>
                        <div className="clearfix"></div>
                    </div>
                </section>
                <section className="content">
                    <div className="box">
                        <SearchContainer {...this.props}/>
                        <div className="row">
                            <div className="col-md-12">
                                <table className="table tbl-headblue table-striped table-hover">
                                    <thead>
                                    <tr>
                                        <th className="hidden-xs">ID</th>
                                        <th>Username</th>
                                        <th className="hidden-xs">Họ tên</th>
                                        <th>Email</th>
                                        {/*<th className="text-center">Thao tác</th>*/}
                                    </tr>
                                    </thead>
                                    {allow.view_list &&
                                    <UserListContainer {...this.props}/>
                                    }
                                </table>
                            </div>

                            <div className="col-md-12 text-right">
                                <PaginationContainer {...this.props}/>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        );
    }
}
const mapStateToProps = (state, props) => {
    const {isAllow} = props;
    return {
        isAllow,
        allow: {
            view: isAllow(Permissions.PERMISSION_USER_VIEW),
            view_list: isAllow(Permissions.PERMISSION_USER_VIEW_LIST),
            create: isAllow(Permissions.PERMISSION_USER_CREATE),
        }
    }
};
export default connect(mapStateToProps)(UserManagementContainer);