import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import {getUserById, changePassword} from '../../actions'
import DocumentTitle from '../../../../../www_components/Common/DocumentTitle'
import ChangePassForm from './ChangePassForm';
import {replace} from 'react-router-redux'
import {bindActionCreators} from 'redux'

class UserChangePassContainer extends Component {

    componentWillReceiveProps(nextProps) {
        if(nextProps.changedPassword) {
            nextProps.onRedirect('/login');
        }
    }
    handleChangePassword(data) {
        this.props.changePassword(data);
    }
    render() {
        return (
            <DocumentTitle title="Yêu cầu thay đổi mật khẩu">
                <ChangePassForm {...this.props}
                      onSubmit={this.handleChangePassword.bind(this)}
                />
            </DocumentTitle>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        changedPassword: state.user.changedPassword,
        error: state.user.error,
        message: state.user.changedPasswordMessage
    }
};

const mapDispatchToProps = (dispatch) => ({
    ...bindActionCreators({
        changePassword
    }, dispatch),
    onRedirect: (path) => {
        dispatch(replace(path))
    },
    dispatch
});

export default connect(
    mapStateToProps, mapDispatchToProps
)(UserChangePassContainer);