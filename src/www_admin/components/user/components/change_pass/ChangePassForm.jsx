import React from 'react';
import * as RBT from 'react-bootstrap';
import {renderField, renderFieldSelect} from '../../../../helpers/redux-form'
import {ErrorToast, SuccessToast} from '../../../../../www_components/Alert'
import {CHANGE_PASSWORD_FAILURE, CHANGE_PASSWORD_SUCCESS} from '../../ActionTypes'
import {Field, reduxForm} from 'redux-form'
import {validateUserChangePass} from '../../helper'

const ChangePassForm = ({handleSubmit, pristine, error, reset, submitting, invalid}) => (
    <div>
        <SuccessToast dispatchTypes={{
            [CHANGE_PASSWORD_SUCCESS]: 'Đổi mật khẩu thành công'
        }}/>
        <div className="row">
            <div className="col-md-12 text-center">
                <h4 className="fw-600 mt-30 pt-30">Hệ thống yêu cầu bạn thay đổi mật khẩu để sử dụng trong những lần
                    đăng nhập tiếp theo.</h4>
                <h4 className="fw-600 mb-15">Vui lòng nhập thông tin cần thiết để đổi mật khẩu!</h4>
            </div>
        </div>
        <div className="col-md-4 col-md-offset-4 col-xs-12">
            <section className="content">
                <form onSubmit={handleSubmit}>
                    {error && <div className="alert alert-danger">{error}</div>}
                    <RBT.FormGroup controlId=''>
                        <RBT.ControlLabel className="fw-600">Mật khẩu hiện tại</RBT.ControlLabel>
                        <div className="input-required">
                            <Field component={renderField} name="oldPassword" className="form-control"
                                   type="password"/>
                            <span className="required-symbol">*</span>
                        </div>
                    </RBT.FormGroup>

                    <RBT.FormGroup controlId=''>
                        <RBT.ControlLabel className="fw-600">Mật khẩu mới</RBT.ControlLabel>
                        <div className="input-required">
                            <Field component={renderField} name="newPassword" className="form-control"
                                   type="password"/>
                            <span className="required-symbol">*</span>
                        </div>

                        <RBT.HelpBlock>(Tối thiểu 6 ký tự, bao gồm chữ hoa và số)</RBT.HelpBlock>
                    </RBT.FormGroup>

                    <RBT.FormGroup controlId=''>
                        <RBT.ControlLabel className="fw-600">Xác nhận mật khẩu mới</RBT.ControlLabel>
                        <div className="input-required">
                            <Field component={renderField} name="passwordConfirm" className="form-control"
                                                                             type="password"/>
                            <span className="required-symbol">*</span>
                        </div>
                    </RBT.FormGroup>

                    <RBT.FormGroup controlId='' className="text-right">
                        <button
                            disabled={pristine || submitting}
                            className="btn btn-primary"
                            type="submit">
                            Thay đổi mật khẩu
                        </button>
                    </RBT.FormGroup>
                </form>
                {/* Popup thông báo */}
            </section>
        </div>
    </div>
);


export default reduxForm({
    form: 'UserChangePassForm',
    validate: validateUserChangePass
})(ChangePassForm)