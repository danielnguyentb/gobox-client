import React from 'react';
import {connect} from 'react-redux'
import {logoutSaga} from '../../actions'

class LogoutContainer extends React.Component {
    logout() {
        let {dispatch} = this.props;
        dispatch(logoutSaga())
    }

    render() {
        return (
            <div onClick={::this.logout}>
                <span><i className="ti-power-off mr-10"/>Logout</span>
            </div>
        )
    }
}

export default connect()(LogoutContainer)