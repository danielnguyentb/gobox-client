import React from 'react';
import DocumentTitle from '../../../../../www_components/Common/DocumentTitle'
import UserFormContainer from './UserFormContainer'

export default (props) => (
    <div>
        <DocumentTitle title="Tạo nhân viên"/>
        <section className="content-header">
            <h1>Tạo nhân viên</h1>
        </section>
        <div className="col-md-7 col-md-offset-2">
            <section className="content">
                <UserFormContainer {...props} />
            </section>
        </div>
    </div>
)