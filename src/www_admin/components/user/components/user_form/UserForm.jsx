import React from 'react';
import * as RBT from 'react-bootstrap';
import Alert from '../../../../../www_components/Alert'
import {USER_CREATE_SUCCESS} from '../../ActionTypes'
import {Field, Fields, reduxForm} from 'redux-form'
import {renderField, renderFieldNotLabel, renderPasswordStrengthField, createUserValidate} from '../../helper'
import {renderDatetimeSelectField} from '../../../../../www_components/DatetimeSelection'
import {ACTIVE, INACTIVE} from '../../constants'

const {SuccessToast, ErrorAlert} = Alert.components;

const UserForm = ({handleSubmit, error, pristine, reset, submitting, invalid}) => (
    <form onSubmit={handleSubmit}>
        <SuccessToast dispatchTypes={{[USER_CREATE_SUCCESS]: 'Tạo nhân viên thành công'}}/>
        <div className="col-md-6 info-login">
            <h3>Thông tin đăng nhập</h3>

            <label className="control-label">Tên đăng nhập</label>
            <div className="input-required">
                <Field name="username" component={renderFieldNotLabel} type="text" className="form-control"
                       label="Tên đăng nhập"/>
                <span className="required-symbol">*</span>
            </div>

            <label className="control-label">Email</label>
            <div className="input-required">
                <Field name="email" component={renderFieldNotLabel} type="text" className="form-control" label="Email"/>
                <span className="required-symbol">*</span>
            </div>

            <Field name="password" component={renderPasswordStrengthField} type="password"
                   className="form-control" label="Mật khẩu"/>

            <label className="control-label">Xác nhận mật khẩu</label>
            <div className="input-required">
                <Field name="confirmPassword" component={renderFieldNotLabel} type="password"
                       className="form-control" label="Xác nhận mật khẩu"/>
                <span className="required-symbol">*</span>
            </div>

            <RBT.FormGroup controlId="formControlsSelect">
                <RBT.ControlLabel>Trạng thái</RBT.ControlLabel>
                <Field
                    className="form-control select" component="select"
                    name="state">
                    <option value={ACTIVE}>Kích hoạt</option>
                    <option value={INACTIVE}>Không kích hoạt</option>
                </Field>
            </RBT.FormGroup>

        </div>
        <div className="col-md-6 info-login">
            <h3>Thông tin cá nhân</h3>

            <Field name="firstName" component={renderField} type="text" className="form-control"
                   label="Họ"/>

            <Field name="lastName" component={renderField} type="text" className="form-control"
                   label="Tên"/>

            <RBT.FormGroup controlId="formControlsSelect">
                <RBT.ControlLabel>Giới tính</RBT.ControlLabel>
                <Field
                    className="form-control select"
                    component="select"
                    name="gender">
                    <option value={1}>Nam</option>
                    <option value={0}>Nữ</option>
                </Field>
            </RBT.FormGroup>

            <RBT.FormGroup controlId="birthday">
                <RBT.ControlLabel>Ngày sinh</RBT.ControlLabel>
                <Fields
                    names={['date', 'month', 'year']}
                    component={renderDatetimeSelectField}/>
            </RBT.FormGroup>
        </div>
        <div className="col-md-12">
            {error && <div className="alert alert-danger">{error}</div>}
        </div>
        <div className="col-md-12 text-center">
            <RBT.FormGroup>
                <button
                    className="btn btn-primary" type="submit"
                    disabled={pristine || submitting}>
                    TẠO NHÂN VIÊN
                </button>
            </RBT.FormGroup>
        </div>
    </form>
);

export default reduxForm({
    form: 'userForm',
    validate: createUserValidate,
})(UserForm)