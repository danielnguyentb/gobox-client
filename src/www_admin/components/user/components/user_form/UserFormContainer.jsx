import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import UserForm from './UserForm';
import {createUser} from '../../actions'
import {push} from 'react-router-redux'
import {PERMISSION_USER_CREATE, ACTIVE} from '../../constants'
import {UserSuccessSelector} from '../../selectors'

class UserFormContainer extends Component {
    componentWillMount() {
        let {isAllow, dispatch} = this.props;
        if (!isAllow(PERMISSION_USER_CREATE)) {
            dispatch(push('/error/403'));
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.userCreated && nextProps.userCreated.id) {
            let {id} = nextProps.userCreated;
            nextProps.dispatch(push(`/users/detail/${id}`));
        }
    }

    submit(user) {
        let {username, gender, password, state, email, firstName, lastName, date, month, year} = user;
        const {createUser} = this.props;
        if(date && month && year){
            let birthday = new Date();
            birthday.setDate(date);
            birthday.setMonth(month);
            birthday.setFullYear(year);
            birthday = birthday.toISOString();
            createUser({
                username, password, gender, state, email, firstName, lastName, birthday,
                needChangePass: 0,
                emailVerified: 0,
                realm: ''
            });
        } else {
            createUser({
                username, password, gender, state, email, firstName, lastName,
                needChangePass: 0,
                emailVerified: 0,
                realm: ''
            });
        }
    }

    render() {
        return (
            <UserForm
                initialValues={{
                    gender: 1,
                    needChangePass: 0,
                    emailVerified: 0,
                    state: ACTIVE
                }}
                onSubmit={::this.submit}/>
        );
    }
}

const mapStateToProps = (state, props) => {
    return {
        isAllow: props.isAllow,
        userCreated: UserSuccessSelector(state)
    }
};

export default connect(
    mapStateToProps, {createUser}
)(UserFormContainer);

