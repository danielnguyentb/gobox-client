import React, {PropTypes, Component} from 'react'
import Select from 'react-select'
import {getUsersSaga} from '../../actions'
import {PaginateUsersSelector, UserByIdSelector} from '../../selectors'
import {connect} from 'react-redux'
import {encode} from 'helpers'

export class UserTypeahead extends Component {
    static propTypes = {
        placeholder: PropTypes.string,
        value: PropTypes.number,
        onChange: PropTypes.func,
        onInputChange: PropTypes.func
    };

    static defaultProps = {
        onInputChange(){}
    };

    componentDidMount() {
        this.fetchUsers('');
    }

    fetchUsers(value) {
        const {dispatch} = this.props;
        dispatch(getUsersSaga({
            where: {username: {like: encode(value)}},
            order: 'username ASC'
        }));
    }

    render() {
        const {users, placeholder, value, onChange, onInputChange} = this.props;
        const userOptions = users.map(t => ({
            value: t.id,
            label: t.username
        }));
        return (
            <Select
                placeholder={placeholder}
                value={value}
                autoBlur={true}
                noResultsText="Không tìm thấy"
                onInputChange={value => {
                    this.fetchUsers(value);
                    onInputChange(value);
                }}
                onFocus={e => {
                    onChange('');
                }}
                onChange={e => {
                    const value = e ? e.value : '';
                    onChange(value);
                }}
                options={userOptions}
            />
        )
    }
}

const mapStateToProps = (state, props) => {
    let users = [];
    const paginateUsers = PaginateUsersSelector(state);
    const getUser = UserByIdSelector(state);
    const t = getUser(props.value);
    if (paginateUsers.length == 0 && t) {
        users.push(t);
    } else {
        users = paginateUsers
    }
    return {
        users
    }
};

export default connect(mapStateToProps)(UserTypeahead)