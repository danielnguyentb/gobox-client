import React, {PropTypes, Component} from 'react'
import {connect} from 'react-redux'
import {PickersByHubIdSelector} from '../../selectors'
import {getPickupBelongToHub} from '../../../hub/actions'
import {Field} from 'redux-form'

class PickerSelectContainerBase extends Component {
    componentWillMount() {
        let {pickers, getPickupBelongToHub, hubId} = this.props;
        if (!pickers || pickers.length === 0) {
            getPickupBelongToHub(hubId)
        }
    }

    render() {
        let {pickers, name, label} = this.props;
        return (
            <Field name={name} className="form-control select" component="select">
                <option value="">{label}</option>
                {Object.values(pickers).map(picker =>
                    <option key={picker.id} value={picker.id}>{picker.username}</option>
                )}
            </Field>
        )
    }
}

const mapStateToProps = (state, props) => {
    const getPickers = PickersByHubIdSelector(state);
    return {
        pickers: getPickers(props.hubId)
    }
};

export const PickerSelectFieldContainer = connect(mapStateToProps, {getPickupBelongToHub})(PickerSelectContainerBase);

PickerSelectFieldContainer.propTypes = {
    hubId: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired,
    label: PropTypes.string.isRequired
};