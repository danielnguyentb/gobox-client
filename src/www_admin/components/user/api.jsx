import Client from '../../helpers/Client'
import * as types from './ActionTypes'

class User extends Client {

    getProfile(){
        return this.callApi({
            types: [types.AUTH_PROFILE_REQUEST, types.AUTH_PROFILE_SUCCESS, types.AUTH_PROFILE_FAILURE],
            endpoint: `/users/me`,
            method: 'GET',
        });
    }

    getUsers(_filter) {
        let filter = typeof _filter === 'string' ? _filter : JSON.stringify(_filter);
        return this.callApi({
            types: [types.USERS_REQUEST, types.USERS_SUCCESS, types.USERS_FAILURE],
            endpoint: _filter ? `/users?filter=${filter}` : '/users',
            method: 'GET',
        });
    }

    getApplicationValidation(){
        return this.callApi({
            types: [types.APPLICATION_VALIDATION_REQUEST, types.APPLICATION_VALIDATION_SUCCESS, types.APPLICATION_VALIDATION_FAILURE],
            endpoint: '/applicationValidations',
            method: 'GET',
            authenticate: false,
        });
    }

    getUserById(user_id) {
        return this.callApi({
            types: [types.USER_REQUEST, types.USER_SUCCESS, types.USER_FAILURE],
            endpoint: `/users/${user_id}`,
            method: 'GET',
        });
    }

    create(user) {
        return this.callApi({
            types: [types.USER_CREATE_REQUEST, types.USER_CREATE_SUCCESS, types.USER_CREATE_FAILURE],
            endpoint: '/users',
            method: 'POST',
            form: 'userForm',
            body: user
        });
    }

    update(user, id) {
        return this.callApi({
            types: [types.USER_UPDATE_REQUEST, types.USER_UPDATE_SUCCESS, types.USER_UPDATE_FAILURE],
            endpoint: `/users/${id}`,
            method: 'PUT',
            form: 'PersonalInfoForm',
            body: user
        });
    }

    changeEmail(user, id) {
        return this.callApi({
            types: [types.USER_CHANGE_EMAIL_REQUEST, types.USER_CHANGE_EMAIL_SUCCESS, types.USER_CHANGE_EMAIL_FAILURE],
            endpoint: `/users/${id}`,
            method: 'PUT',
            body: user
        });
    }

    changePassword(body) {
        return this.callApi({
            types: [types.CHANGE_PASSWORD_REQUEST, types.CHANGE_PASSWORD_SUCCESS, types.CHANGE_PASSWORD_FAILURE],
            endpoint: `/users/changePassword`,
            method: 'PUT',
            form: 'UserChangePassForm',
            body
        });
    }

    changePasswordStaff(user, id) {
        return this.callApi({
            types: [types.CHANGE_PASSWORD_REQUEST, types.CHANGE_PASSWORD_SUCCESS, types.CHANGE_PASSWORD_FAILURE],
            endpoint: `/users/${id}/changePassword`,
            method: 'PUT',
            form: 'UserChangePassForm',
            body: user
        });
    }

    login(credentials) {
        return this.callApi({
            types: [types.AUTH_LOGIN_REQUEST, types.AUTH_LOGIN_SUCCESS, types.AUTH_LOGIN_FAILURE],
            endpoint: '/users/login',
            method: 'POST',
            authenticate: false,
            form: 'UserLoginForm',
            body: credentials
        });
    }

    logout() {
        return this.callApi({
            types: [types.AUTH_LOGOUT_REQUEST, types.AUTH_LOGOUT_SUCCESS, types.AUTH_LOGOUT_FAILURE],
            endpoint: '/users/logout',
            method: 'POST',
        });
    }

    logoutAll(user_id) {
        return this.callApi({
            types: [types.USER_LOGOUT_ALL_REQUEST, types.USER_LOGOUT_ALL_SUCCESS, types.USER_LOGOUT_ALL_FAILURE],
            endpoint: `/users/${user_id}/logoutAll`,
            method: 'POST'
        })
    }
}

export default new User();