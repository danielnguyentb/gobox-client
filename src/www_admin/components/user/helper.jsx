/**
 * Created by Tester-Ali on 31-10-2016.
 */
import React from 'react';
import * as RBT from 'react-bootstrap';
import zxcvbn from 'zxcvbn'
import ReactPasswordStrength from '../../../www_components/PasswordStrength'
import Localize from '../../../resources/Localize'
import {encode} from '../../helpers'

export const generateQuery = (params) => {
    let where = {};
    let {keyword, state, limit = 20, skip = 0, order = 'registeredDate DESC'} = params;
    keyword = keyword ? keyword : '';

    let arr_where = [];

    if (keyword) {
        let t = encode(keyword);
        arr_where.push({'email': {like: t}});
        arr_where.push({'username': {like: t}});
    }
    where.and = [{or: arr_where}];
    if (state) where.and.push({state});
    return {where, order, limit, skip};
};

export const scoreFn = (value) => {
    let result = zxcvbn(value);
    if (value.length == 0) {
        result.feedback.warning = 'Mật khẩu không được để trống';
    } else if (value.length < 6 || /[A-Z]/.test(value) == false || /\d/.test(value) == false) {
        result.score = 0;
        result.feedback.warning = 'Tối thiểu 6 ký tự, bao gồm chữ hoa và số';
    } else if (result.score <= 2 && !result.feedback.warning) {
        result.feedback.warning = 'Vui lòng chọn một mật khẩu mạnh hơn'
    }
    return result;
};

export const loginValidate = values => {
    const errors = {};
    if(Object.keys(values).length === 0) return errors;
    if (!values.email) {
        errors.email = 'Tên đăng nhập không được để trống'
    }
    if (!values.password) {
        errors.password = 'Mật khẩu không được để trống';
    }
    return errors;
};

export const createUserValidate = values => {
    const errors = {};
    if(Object.keys(values).length === 0) return errors;
    if (!values.username) {
        errors.username = 'Tên đăng nhập không được để trống'
    }
    if (!values.email) {
        errors.email = 'Email không được để trống'
    } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,63}$/i.test(values.email)) {
        errors.email = 'Email không hợp lệ'
    }
    if (!values.password) {
        errors.password = 'Mật khẩu không được để trống';
    } else if (scoreFn(values.password).score <= 2) {
        errors.password = Localize.t(scoreFn(values.password).feedback.warning);
    }

    if (values.password != values.confirmPassword) {
        errors.confirmPassword = 'Mật khẩu không khớp';
    }

    if (!values.oldPassword) {
        errors.oldPassword = 'Mật khẩu không được để trống'
    } else if (values.oldPassword == values.password) {
        errors.oldPassword = 'Mật khẩu mới không được trùng với mật khẩu hiện tại'
    }

    if (values.date || values.month || values.year) {
        if (!values.date) {
            errors.date = 'Ngày sinh không được để trống'
        } else if (values.date < 0 || values.date > 31 || /^\d+$/.test(values.date) == false) {
            errors.date = 'Ngày sinh không hợp lệ'
        }
        if (!values.month) {
            errors.month = 'Tháng không được để trống'
        }
        if (!values.year) {
            errors.year = 'Năm sinh không được để trống'
        } else if (values.year <= 1000 || values.year > (new Date).getFullYear()) {
            errors.year = 'Năm sinh không hợp lệ'
        }
    }
    return errors
};

export const validateUserChangePass = values => {
    const errors = {};
    if (!values.oldPassword) {
        errors.oldPassword = 'Mật khẩu cũ không được để trống'
    }
    if (!values.newPassword) {
        errors.newPassword = 'Mật khẩu mới không được để trống'
    }
    if (!values.passwordConfirm) {
        errors.passwordConfirm = 'Xác nhận mật khẩu mới không được để trống'
    }
    if (values.newPassword && values.passwordConfirm) {
        if (values.newPassword != values.passwordConfirm) {
            errors.passwordConfirm = 'Mật khẩu xác nhận không giống mật khẩu mới'
        }
    }
    return errors
};

export const renderField = ({input, label, type, className, placeholder, meta: {touched, error, warning}}) => (
    <RBT.FormGroup validationState={touched && error ? 'error' : 'success'}>
        <RBT.ControlLabel>{label}</RBT.ControlLabel>
        <input {...input} type={type} className={className} placeholder={placeholder}/>
        {touched && ((error && <span className="help-block">{error}</span>) || (warning && <span>{warning}</span>))}
    </RBT.FormGroup>
);
export const renderFieldNotLabel = ({input, type, className, placeholder, meta: {touched, error, warning}}) => (
    <RBT.FormGroup validationState={touched && error ? 'error' : 'success'}>
        <input {...input} type={type} className={className} placeholder={placeholder}/>
        {touched && ((error && <span className="help-block">{error}</span>) || (warning && <span>{warning}</span>))}
    </RBT.FormGroup>
);

export const renderPasswordStrengthField = ({input, label, type, className, meta: {touched, error, warning}}) => (
    <RBT.FormGroup className="style-strength-pass" validationState={touched && error ? 'error' : ''}>
        <RBT.ControlLabel>{label}</RBT.ControlLabel>
        <ReactPasswordStrength
            minLength={6}
            minScore={2}
            scoreFn={scoreFn}
            scoreWords={['Quá lởm', 'Yếu', 'Đơn giản quá', 'Tốt', 'Rất tốt']}
            inputProps={{...input, className, type}}
        />
        {touched && ((error && <span className="help-block">{error}</span>) || (warning && <span>{warning}</span>))}
    </RBT.FormGroup>
);