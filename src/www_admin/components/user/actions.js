import User from './api'
import * as t from './ActionTypes'
import {generateQuery} from './helper'
import {CALL_API} from '../../ActionTypes'
import * as hubs from '../hub'

export const getAllUsers = (filter) => dispatch => dispatch(User.getUsers(generateQuery(filter)));

export const getUserById = (user_id) => dispatch => dispatch(User.getUserById(user_id));

export const createUser = (user) => dispatch => dispatch(User.create(user));

export const changePassword = (data) => dispatch => dispatch(User.changePassword(data));

export const updateUser = (user, id) => dispatch => dispatch(User.update(user, id));

export const changeEmail = (user, id) => dispatch => dispatch(User.changeEmail(user, id));

export const changePasswordStaff = (user, id) => dispatch => dispatch(User.changePasswordStaff(user, id));

export const logoutAll = (userId) => dispatch => dispatch(User.logoutAll(userId));

export const getUsersSaga = (filter) => {
    return {
        type: CALL_API,
        payload: {
            types: [t.USERS_REQUEST, t.USERS_SUCCESS, t.USERS_FAILURE],
            endpoint: `/users?filter=${JSON.stringify(filter)}`,
            method: 'GET',
        }
    }
};

//AUTHENTICATE

export const loginUserSuccess = (session) => ({
    type: t.AUTH_LOAD_SUCCESS,
    session
});
export const getProfile = () => dispatch => dispatch(User.getProfile());
export const login = (credentials) => (dispatch, getState) => dispatch(User.login(credentials));
export const getApplicationValidation = () => dispatch => dispatch(User.getApplicationValidation());
export const logout = () => (dispatch, getState) => dispatch(User.logout());

export const logoutSaga = () => {
    return {
        type: CALL_API,
        payload: {
            types: [t.AUTH_LOGOUT_REQUEST, t.AUTH_LOGOUT_SUCCESS, t.AUTH_LOGOUT_FAILURE],
            endpoint: '/users/logout',
            method: 'POST',
        }
    }
};

export const fetchPickupByHubIdSaga = id => {
    return {
        type: CALL_API,
        payload: {
            types: [hubs.ActionTypes.GET_PICKUP_BELONG_TO_HUB.REQUEST, hubs.ActionTypes.GET_PICKUP_BELONG_TO_HUB.SUCCESS, hubs.ActionTypes.GET_PICKUP_BELONG_TO_HUB.FAILURE],
            endpoint: `/LogisticsHubs/${id}/user`,
            method: 'GET',
            scope: {hubId: id}
        }
    }
};