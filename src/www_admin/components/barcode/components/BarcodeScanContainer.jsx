import React from 'react'
import  {Button} from 'react-bootstrap'
import {Control, Form} from 'react-redux-form'

import './barcode_scan.scss'

class BarcodeScanContainer extends React.Component{
    constructor(props) {
        super(props);
        this.state = {'className':'glyphicon glyphicon-volume-up'};
    }

    handleClick(e) {
        if(this.state.className == 'glyphicon glyphicon-volume-off'){
            this.setState({'className':'glyphicon glyphicon-volume-up'});
        }else{
            this.setState({'className':'glyphicon glyphicon-volume-off'});
        }
    }
    render(){
        return(
            <div>
                <section className="content-header">
                    <ol className="breadcrumb mb-0">
                        <li><span className="fs-18 fw-600"><i className="fa fa-map-marker mr-5"/> CG-HN</span></li>
                        <li><span className="fs-16">Quét mã vạch</span></li>
                    </ol>
                </section>
                <section className="content">
                    <div className="box">
                        <div className="row">
                            <div className="col-md-offset-3 col-md-6 col-xs-12">
                                <div className="row">
                                    <table className="table tbl-barcode">
                                        <tbody>
                                        <tr>
                                            <td colSpan="4">
                                                <Form model=".form-barcode">
                                                    <div className="col-md-3 col-xs-6 pl-0">
                                                            <Control.select model=".action" className="form-control">
                                                                <option value="select">Hành động</option>
                                                                <option value="1">Nhập</option>
                                                                <option value="2">Xuất</option>
                                                            </Control.select>
                                                    </div>
                                                    <div className="col-md-8 col-xs-5 pr-0 pl-0">
                                                            <Control.text model=".barcode" className="form-control" placeholder="Nhập mã vạch"/>
                                                    </div>
                                                    <div className="col-md-1 col-xs-1 pl-0">
                                                        <Button bsStyle="link" type="submit" className="fs-20 text-base"><i className={this.state.className} onClick={this.handleClick.bind(this)}/></Button>
                                                    </div>
                                                </Form>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td className="fw-600">2</td>
                                            <td className="fw-600 fs-18"><i className=" ti-import mr-5 text-primary"/> 6010203040 </td>
                                            <td>CG - HN / TX - HN</td>
                                            <td>10:58</td>
                                        </tr>
                                        <tr>
                                            <td className="fw-600">1</td>
                                            <td className="fw-600 fs-18"><i className=" ti-export mr-5 text-danger"/> 6010203040 </td>
                                            <td>CG - HN / TX - HN</td>
                                            <td>10:58</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                    </div>
                </section>
            </div>
        );

    }
}
export default (BarcodeScanContainer);