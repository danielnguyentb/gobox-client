import React from 'react'
import Header from './header/HeaderContainer'
import TaskList from './task_list/TaskListContainer'
import ScanTask from './scan_task/ScanTaskContainer'

class CreateTasks extends React.Component{
    render(){
        return(
            <div>
                <Header />
                <section className="content">
                    <div className="box">
                        <ScanTask />
                        <TaskList />
                    </div>
                </section>
            </div>
        )
    }
}
export default (CreateTasks)