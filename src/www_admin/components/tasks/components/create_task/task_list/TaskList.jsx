import React from 'react'
import Link from './../../../../../../www_components/Link'
import * as RBT from 'react-bootstrap'
import {Field, reduxForm} from 'redux-form'
import {DateField, Calendar} from 'react-date-picker'
import ReactTooltip from 'react-tooltip'
import Select from 'react-select'


class TaskList extends React.Component{
    render(){
        return(
            <div>
                <div className="row">
                    <table className="table tbl-headblue table-striped table-hover tbl-orders">
                        <thead>
                        <tr>
                            <th className="text-center">
                                <input className="checkbox" type="checkbox" />
                            </th>
                            <th>Vận đơn</th>
                            <th>Hub</th>
                            <th>Ghi chú NV</th>
                            <th>Hạn</th>
                            <th>Thao tác</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td className="text-center"><input className="checkbox" type="checkbox" /></td>
                            <td>
                                <Link to="" className="fw-600">6105601748</Link>
                                <p>Đ/C giao: Số 2, Khuất Duy Tiến, Thanh Xuân, Hà Nội</p>
                            </td>
                            <td>TX-HN / CG-HN</td>{/*hub giao/hub nhận*/}
                            <td>
                                <input type="text" className="form-control" placeholder="Ghi chú nhiệm vụ"/>
                            </td>
                            <td>
                                <DateField placeholder="Hạn" dateFormat="DD-MM-YYYY HH:mm:ss" />
                            </td>
                            <td>
                                <RBT.Button
                                    data-tip="Xóa khỏi danh sách" className="btn btn-xs btn-danger"
                                >
                                    <i className="ti-close"></i>
                                </RBT.Button>
                                <ReactTooltip />
                                {/*<i className="glyphicon glyphicon-ok text-success fs-16" />*/}
                                {/*<i className="glyphicon glyphicon-remove text-danger fs-16" />*/}
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div className="row">
                    <div className="col-md-12">
                        <div className="pull-right ml-10">
                            <RBT.Button className="btn btn-primary height-36">
                                TẠO NHIỆM VỤ
                            </RBT.Button>
                        </div>
                        <div className="select-assign pull-right">
                            <Select name="assignees"
                                    value=" "
                                    options={[
                                        { value: 'one', label: 'One' },
                                        { value: 'two', label: 'Two'}
                                    ]}
                                    placeholder="Người nhận nhiệm vụ"
                            />
                        </div>
                        <div className="select-type-task pull-right mr-10">
                            <Select name="assignees"
                                    value=" "
                                    options={[
                                        { value: 'one', label: 'One' },
                                        { value: 'two', label: 'Two'}
                                    ]}
                                    placeholder="Loại nhiệm vụ"
                            />
                        </div>
                        <span className="pull-right order-checked">Với các mã đã chọn: Tạo nhiệm vụ </span>
                    </div>
                </div>
            </div>
        )
    }
}
export default reduxForm({
    form: 'createTaskForm'
}) (TaskList)