import React from 'react'
import DocumentTitle from '../../../../../../www_components/DocumentTitle'

class Header extends React.Component{
    render(){
        return(
            <div>
                <DocumentTitle title={`Tạo nhiệm vụ hàng loạt`}/>
                <section className="content-header">
                    <div className="row">
                        <div className="col-md-12 col-xs-12">
                            <h3 className="mt-0 mb-0">Tạo nhiệm vụ hàng loạt</h3>
                        </div>
                    </div>
                </section>
            </div>
        )
    }
}
export default (Header)