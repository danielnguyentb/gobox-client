import React from 'react'
import * as RBT from 'react-bootstrap'
import Link from './../../../../../../www_components/Link'
import {Field, reduxForm} from 'redux-form'

class ScanTask extends React.Component {
    render() {
        const {handleSubmit, error, submitting, pristine, invalid} = this.props;
        return (
            <div className="row">
                <div className="col-md-offset-3 col-md-6 col-xs-12">
                    <div className="row">
                        <div className="col-md-8 col-xs-12">
                            <form onSubmit={handleSubmit}>
                                <RBT.FormGroup>
                                    <RBT.InputGroup>
                                        <Field
                                            component="input"
                                            type="text"
                                            placeholder="Quét mã"
                                            className="height-36"
                                            onKeyUp={e => {
                                                if (e.key == 'Enter') {
                                                    handleSubmit();
                                                }
                                            }}
                                        />
                                        <span className="input-group-btn">
                                        <RBT.Button
                                            type="submit"
                                            disabled={submitting || pristine || invalid}
                                            className="btn btn-secondary btn-scan">
                                            <i className="fa fa-level-down"/>
                                        </RBT.Button>
                                    </span>
                                    </RBT.InputGroup>
                                </RBT.FormGroup>
                                {error ? <span className="alert alert-danger">{error}</span> : null}
                            </form>
                        </div>
                        <div className="col-md-4 col-xs-12">
                            <Link to="" className="clear-text">Xóa trắng</Link>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

const validate = values => {
    const errors = {};
    if (Object.keys(values).length == 0) return {};
    if (!values.code) {
        errors.code = 'invalid code'
    }
    return errors;
};
export default reduxForm({
    form: 'createTaskForm',
    validate
})(ScanTask)