import React from 'react'
import {connect} from 'react-redux'
import ScanTask from './ScanTask'
import * as orders from '../../../../order'
import {encode} from '../../../../../helpers'

class ScanTaskContainer extends React.Component {
    handleSubmit({code}) {
        const {dispatch} = this.props;
        dispatch(orders.actions.fetchDeliverOrdersSaga({
            where: {barcode: {like: encode(code)}}
        }));
    }

    render() {
        return (
            <ScanTask
                onSubmit={::this.handleSubmit}
            />
        )
    }
}

const mapStateToProps = state => {
    return {
    }
};
export default connect(mapStateToProps)(ScanTaskContainer)