import React from 'react'
import {connect} from 'react-redux'
import TaskDetail from './TaskDetail'
import {getTransportTask, getTransportTaskHistories} from '../../actions'
import {TaskViewByIdSelector, TaskViewHistories} from '../../selectors'

class TaskDetailContainer extends React.Component{
    constructor(props) {
        super(props);
    }
    componentWillMount() {
        let taskId = this.props.params.taskId;
        if(taskId) {
            this.props.getTransportTask(taskId);
            this.props.getTransportTaskHistories(taskId);
        }
    }
    componentWillReceiveProps(nextProps) {
        let taskId = nextProps.params.taskId;
        if(taskId != this.props.params.taskId) {
            this.props.getTransportTask(taskId);
            this.props.getTransportTaskHistories(taskId);
        }
    }
    render(){
        let {task} = this.props;
        if(!task) return null;
        return(
            <TaskDetail
                {...this.props}
            />
        )
    }
}
const mapStateToProps = (state, props) => {
    const getTask = TaskViewByIdSelector(state);
    const getTaskViewHistories = TaskViewHistories(state);
    return {
        task: getTask(props.params.taskId),
        taskHistories: getTaskViewHistories(props.params.taskId)
    }
};

export default connect(
    mapStateToProps, {getTransportTask, getTransportTaskHistories}
)(TaskDetailContainer);