import React from 'react'
import Header from './Header'
import {typeTasksTitle, statusTasksTitle} from '../../../constants'

class HeaderContainer extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            statusTasks : [],
            listUser: []
        }
    }
    componentWillMount() {
        if(statusTasksTitle) {
            let options = [];
            Object.keys(statusTasksTitle).map(key =>  options.push({label: statusTasksTitle[key], value: key}));
            this.setState({statusTasks: options})
        }

        let {task} = this.props;
        if(task.assignee) {
            let listUser = [];
            listUser.push({label: task.assignee.fullName + "("+ task.assignee.email  +")", value: task.assignee.id});
            this.setState({listUser: listUser})
        }
    }
    render(){
        return(
            <Header {...this.props} statusTasks={this.state.statusTasks} listUser={this.state.listUser}/>
        )
    }
}
export default (HeaderContainer)