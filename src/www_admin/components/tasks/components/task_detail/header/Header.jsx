import React from 'react'
import DocumentTitle from './../../../../../../www_components/DocumentTitle'
import Link from './../../../../../../www_components/Link'
import {Field, Fields, reduxForm} from 'redux-form'
import {renderSearchSelect} from './../../../helper'
import {typeTasksTitle, statusTasksTitle} from '../../../constants'
import Select from 'react-select'

export default ({task, statusTasks, listUser}) => (
    <div>
        <DocumentTitle title={`Chi tiết nhiệm vụ - ${task.id}`} />
        <section className="content-header">
            <div className="row">
                <div className="col-md-12 col-xs-12">
                    <h3 className="mt-0 mb-0">
                        <span className="pull-left mt-5">Chi tiết nhiệm vụ </span>
                        <span className="text-primary ml-5 mr-20 pull-left mt-5"> {task.id} </span>
                        <span className="pull-left fs-14 mt-10 mr-15">{typeTasksTitle[task.type]}</span>
                        <span className="pull-left fs-14 mt-10 mr-15">-</span>
                        <span className="pull-left fs-14 mt-10 mr-15">{statusTasksTitle[task.status]}</span>

                        <span className="pull-right fw-600 fs-16">{task.assignee ? task.assignee.username : ''}</span>

                        <div className="status-task hidden">
                            <Select name="status" value={task.status}
                                options={statusTasks}
                                placeholder="Trạng thái"
                            />
                        </div>
                        <div className="assign-task hidden">
                            <Select name="assignees"
                                value={task.assignee ? task.assignee.id : ''}
                                options={listUser}
                                placeholder="Người nhận nhiệm vụ"
                            />
                        </div>
                    </h3>
                </div>
            </div>
            <div className="row">
                <div className="col-md-12">
                    <ol className="breadcrumb mb-0 pt-0">
                        <li><Link to="/tasks">Danh sách nhiệm vụ</Link></li>
                        <li className="active">Chi tiết nhiệm vụ</li>
                    </ol>
                </div>
            </div>
        </section>
    </div>
)