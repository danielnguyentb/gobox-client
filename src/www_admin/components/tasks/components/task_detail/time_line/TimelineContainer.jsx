import React from 'react'
import TimeLine from '../../../../../../www_components/Timeline'

class TimelineContainer extends React.Component{
    constructor(props) {
        super(props);
    }
    render(){
        let {task} = this.props;
        if(!task.timeline) return null;
        return(
            <TimeLine data={task.timeline}/>
        )
    }
}
export default (TimelineContainer)