import React from 'react'
import {ObjectTypesDetail, TYPE_TASKS, PERMISSION_VIEW_ALL_TASKS_ALL_HUB, ObjectTypes} from '../../../constants'
const numberFormat = new Intl.NumberFormat('vi-VN');
import Link from './../../../../../../www_components/Link'
import moment from 'moment'
moment.locale('vi');

export default ({task, isAllow}) => {
    let reasonCodes = task.reasonCode ? JSON.parse(task.reasonCode) : null;
    let {tookanData} = task;

    return (
        <div className="col-md-12 col-xs-12">
            <div className="panel panel-default no-shadow">
                <div className="panel-body tbl-info-order">
                    <div className="row">
                        <div className="col-md-12 col-xs-12">
                            <table>
                                <thead>
                                <tr>
                                    <th colSpan="100%">
                                        <h5 className="mt-0 fw-600">Thông tin nhiệm vụ</h5>
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td className="fw-600 pt-nowrap">Đối tượng:</td>
                                        <td>
                                            <span className="ml-5 text-uppercase">
                                                {task.objectType ? ObjectTypesDetail[task.objectType].mark : ''}
                                                {isAllow(PERMISSION_VIEW_ALL_TASKS_ALL_HUB) ?
                                                    task.objectCode &&
                                                        <Link className="ml-5" to={`${task.objectType == ObjectTypes.CustomerDeliveryOrder ? ('/orders/detail/' + task.objectId) : ''}`}>
                                                        {task.objectCode}
                                                        </Link>
                                                    :
                                                    <span>{task.objectCode ? task.objectCode : ''}</span>
                                                }
                                            </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td className="fw-600 pt-nowrap">Tookan ID:</td>
                                        <td>{task.jobId ? task.jobId : ''}</td>
                                    </tr>
                                    <tr>
                                        <td className="fw-600 pt-nowrap">COD (VNĐ):</td>
                                        <td>
                                            <span>{(task.cod || task.cod == 0 ) ? numberFormat.format(task.cod) : ''}</span>
                                        </td>
                                    </tr>

                                    {/*Neu la task di giao*/}
                                    {(task.type == TYPE_TASKS.DELIVER || task.type == TYPE_TASKS.PICKUP_AND_DELIVER) &&
                                    <tr>
                                        <td className="fw-600 pt-nowrap">COD thực thu (VNĐ):</td>
                                        <td>
                                            <span>{(task.realCod || task.realCod == 0 ) ? numberFormat.format(task.realCod) : ''}</span>

                                        </td>
                                    </tr>
                                    }

                                    <tr>
                                        <td className="fw-600 pt-nowrap">Hạn nhiệm vụ:</td>
                                        <td>
                                            <span>{task.taskDeadline ? moment(task.taskDeadline).format("H:mm DD/MM/YYYY") : ''}</span>
                                        </td>
                                    </tr>

                                    { (tookanData && tookanData.barcodes ) &&
                                    <tr>
                                        <td className="fw-600 pt-nowrap">Barcode:</td>
                                        <td className="wbreak">{tookanData.barcodes.map(value => <span
                                            key={value}>{value}</span>)}</td>
                                    </tr>
                                    }
                                    {(tookanData && tookanData.images) &&
                                    <tr>
                                        <td className="fw-600 pt-nowrap">
                                            <span>Hình ảnh:</span>
                                        </td>
                                        <td>
                                            {tookanData.images.map(img => <div key={img} className="img-style  img-thumbnail"><img
                                                src={img} className="img-responsive" alt={img} />
                                            </div>) }
                                        </td>
                                    </tr>
                                    }

                                    {(tookanData && tookanData.signature) &&
                                    <tr>
                                        <td className="fw-600 pt-nowrap">Chữ ký:</td>
                                        <td>
                                            {tookanData.signature.map(img => <div key={img} className="img-style"><img
                                                src={img} className="img-responsive img-thumbnail p-5" alt={img}
                                                /></div>)}
                                        </td>
                                    </tr>
                                    }

                                    <tr>
                                        <td className="fw-600 pt-nowrap">Chú thích:</td>
                                        <td>
                                            <span>{task.note}</span>
                                        </td>
                                    </tr>

                                    { (tookanData && tookanData.notes) &&
                                    <tr>
                                        <td className="fw-600 pt-nowrap">Chú thích tookan:</td>
                                        <td>{tookanData.notes.map(val => <span key={val}>{val}
                                            <br/></span>)}</td>
                                    </tr>
                                    }
                                    <tr>
                                        <td className="fw-600 pt-nowrap">Mô tả:</td>
                                        <td>
                                            <span>{task.description}</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td className="fw-600 pt-nowrap ">Lý do thất bại:</td>
                                        <td>
                                            <span>{task.reason}</span>
                                            {reasonCodes && reasonCodes.map(obj => (
                                                (obj.check == true || obj.check == "true" )&& <span key={obj.value}>{obj.value}<br/></span>
                                            ))}
                                        </td>
                                    </tr>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}