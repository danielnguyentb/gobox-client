import React from 'react'
import TaskInfo from './TaskInfo'

class TaskInfoContainer extends React.Component{
    constructor(props) {
        super(props)
    }
    render(){
        return(
            <TaskInfo  {...this.props}/>
        )
    }
}
export default (TaskInfoContainer)