import React from 'react'

export default ({deliverContactName, deliverContactPhone, deliverAddressFull}) => (
    <section>
        <table>
            <thead>
            <tr>
                <th colSpan="100%">
                    Địa chỉ giao hàng
                </th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td className="fw-600 pt-nowrap">Họ tên:</td>
                <td>
                    {deliverContactName} / {deliverContactPhone}
                </td>
            </tr>
            <tr>
                <td className="fw-600 pt-nowrap">Địa chỉ lấy:</td>
                <td>{deliverAddressFull}</td>
            </tr>
            {/*<tr>*/}
                {/*<td className="fw-600 pt-nowrap">Giao trước: </td>*/}
                {/*<td>12:00 01/12/2016</td>*/}
            {/*</tr>*/}
            {/*<tr>*/}
                {/*<td className="fw-600 pt-nowrap">Hoàn thành trước: </td>*/}
                {/*<td>12:00 01/12/2016</td>*/}
            {/*</tr>*/}
            </tbody>
        </table>
    </section>
)