import React from 'react'

export default ({pickupContactName, pickupContactPhone, pickupAddressFull, pickupDetail}, task) => (
    <section>
        <table>
            <thead>
            <tr>
                <th colSpan="100%">
                    Địa chỉ lấy hàng
                </th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td className="fw-600 pt-nowrap">Họ tên:</td>
                <td>
                    {pickupContactName} / {pickupContactPhone}
                </td>
            </tr>
            <tr>
                <td className="fw-600 pt-nowrap">Địa chỉ lấy:</td>
                <td>{pickupAddressFull}</td>
            </tr>
            {/*<tr>*/}
                {/*<td className="fw-600 pt-nowrap">Lấy trước: </td>*/}
                {/*<td>12:00 01/12/2016</td>*/}
            {/*</tr>*/}
            {/*<tr>*/}
                {/*<td className="fw-600 pt-nowrap">Hoàn thành trước: </td>*/}
                {/*<td>12:00 01/12/2016</td>*/}
            {/*</tr>*/}
            </tbody>
        </table>
    </section>
)