import React from 'react'
import AddressInfo from './AddressInfo'

import {TYPE_TASKS} from '../../../constants'

class AddressInfoContainer extends React.Component{
    constructor(props) {
        super(props);
    }
    render(){
        return(
            <AddressInfo {...this.props}/>
        )
    }
}
export default (AddressInfoContainer)