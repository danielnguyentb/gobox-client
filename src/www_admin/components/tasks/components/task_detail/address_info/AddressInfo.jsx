import React from 'react'
import AddressPickup from './AddressPickup'
import AddressDeliver from './AddressDeliver'

import {TYPE_TASKS, typeTasksTitle} from '../../../constants'

export default ({task}) => (
    <section>
        {task.type == TYPE_TASKS.PICKUP_AND_DELIVER ?
            <section>
                <div className="col-md-6 col-xs-12">
                    <div className="panel panel-default no-shadow">
                        <div className="panel-body tbl-info-order">
                            <div className="row">
                                <div className="col-md-12 col-xs-12">
                                    <AddressPickup {...task}/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-md-6 col-xs-12">
                    <div className="panel panel-default no-shadow">
                        <div className="panel-body tbl-info-order">
                            <div className="row">
                                <div className="col-md-12 col-xs-12">
                                    <AddressDeliver {...task} />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        :
            <div className="col-md-12 col-xs-12">
                <div className="panel panel-default no-shadow">
                    <div className="panel-body tbl-info-order">
                        <div className="row">
                            <div className="col-md-12 col-xs-12">
                                {task.type == TYPE_TASKS.PICKUP ?
                                    <AddressPickup {...task}/>
                                    :
                                    <AddressDeliver {...task} />
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        }
    </section>
)
