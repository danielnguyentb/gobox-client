import React from 'react'
import HeaderContainer from './header/HeaderContainer'
import TimeLineContainer from './time_line/TimelineContainer'
import LogHistoryContainer from './log_history/LogHistoryContainer'
import AddressInfoContainer from './address_info/AddressInfoContainer'
import TaskInfoContainer from './task_info/TaskInfoContainer'

export default (props) => (
    <section>
        <HeaderContainer {...props}/>
        <section className="content">
            <div className="box">
                <div className="row">
                    <div className="col-md-9 col-xs-12">
                        <div className="row">
                            <div className="col-md-offset-1 col-md-10 col-xs-12">
                                <div className="row">
                                    <AddressInfoContainer {...props}/>
                                </div>
                                <div className="row">
                                    <TaskInfoContainer {...props}/>
                                </div>
                                {/*history*/}
                                <div className="row">
                                    <LogHistoryContainer {...props}/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-3 col-xs-12">
                        <div className="row">
                            <TimeLineContainer {...props}/>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </section>
)