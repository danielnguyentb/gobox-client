import React from 'react'
import LogHistory from './LogHistory'

class LogHistoryContainer extends React.Component{
    constructor(props) {
        super(props);
    }
    render(){
        return(
            <LogHistory {...this.props}/>
        )
    }
}
export default (LogHistoryContainer)