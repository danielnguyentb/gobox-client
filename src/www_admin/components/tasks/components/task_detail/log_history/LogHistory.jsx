import React from 'react'
import ActionHistory from '../../../../../../www_components/ActionHistory'

export default ({taskHistories}) => (
    <div className="col-md-12">
        <div className="row">
            <div className="col-md-12 col-xs-12">
                <h5 className="mt-0 fw-600">Lịch sử hành động</h5>
                {taskHistories &&
                    <ActionHistory data={taskHistories}/>
                }
            </div>
        </div>
    </div>
)

