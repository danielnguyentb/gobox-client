import React, {PropTypes, Component} from 'react'
import {TasksTable} from './TasksTable'
import {connect} from 'react-redux'
import ReactTooltip from 'react-tooltip'
import {TasksViewByOrderIdSelector, TasksViewByRequestIdSelector} from '../../selectors'
import {UN_ASSIGN_PICKUP_FROM_TASK} from '../../ActionTypes'
import {STATUS_TASKS} from '../../constants'
import {unAssignPickupFromTask} from '../../actions'
import {toastr} from '../../../../../www_components/Toastr2'
import {SuccessToast, ErrorToast} from '../../../../../www_components/Alert/components'
const successAlert = {
    [UN_ASSIGN_PICKUP_FROM_TASK.SUCCESS]: 'Thành công'
};
import {isAllowSelector} from '../../../user/selectors'

class TasksContainer extends Component {
    static propTypes = {
        orderId: PropTypes.number,
        requestId: PropTypes.number
    };

    onCancel(id) {
        const {unAssignPickupFromTask} = this.props;
        const toastConfirmOptions = {
            onOk: () => {
                unAssignPickupFromTask(id, STATUS_TASKS.CANCELLED);
            },
        };
        toastr.confirm(`Bạn có chắc muốn hủy nhiệm vụ  này không?`, toastConfirmOptions);
    };

    render() {
        let {tasks, isAllow} = this.props;
        return (
            <div>
                <TasksTable
                    isAllow={isAllow}
                    onCancel={::this.onCancel}
                    tasks={tasks}/>

                <ErrorToast dispatchTypes={[UN_ASSIGN_PICKUP_FROM_TASK.FAILURE]}/>
                <ReactTooltip />
            </div>
        )
    }
}

const mapStateToProps = (state, props) => {
    const getTasksByOrderId = TasksViewByOrderIdSelector(state);
    const getTasksByRequestId = TasksViewByRequestIdSelector(state);
    const tasks = props.orderId ? getTasksByOrderId(props.orderId) :
        (props.requestId ? getTasksByRequestId(props.requestId) : []);
    const isAllow = isAllowSelector(state);
    return {
        tasks, isAllow
    }
};
export const TasksTableContainer = connect(mapStateToProps, {unAssignPickupFromTask, isAllowSelector})(TasksContainer);