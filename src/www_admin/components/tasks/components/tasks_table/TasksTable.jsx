import React from 'react'
import TasksItem from './TasksItem'
import {PERMISSION_VIEW_ALL_TASKS_ALL_HUB} from '../../constants'

export const TasksTable = ({tasks, onCancel, isAllow}) => {
    return (
        <div className="row">
            <h5 className="fw-700 ml-15">Nhiệm vụ</h5>
            <div className="col-md-12">
                {tasks.map(task =>
                    <TasksItem {...task} key={task.id} onCancel={onCancel} isAllow={isAllow} />
                )}
            </div>
        </div>
    )
};