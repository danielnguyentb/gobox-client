import React from 'react'
import {formatFullTime} from '../../../../helpers/format'
import {statusTasksTitle, typeTasksTitle, statusLabel, endingStatus, PERMISSION_VIEW_ALL_TASKS_ALL_HUB} from '../../constants'
import {UN_ASSIGN_PICKUP_FROM_TASK} from '../../ActionTypes'
import Link from '../../../../../www_components/Link'
import Touchable from '../../../../../www_components/Touchable'
import CountDownTimer from '../../../../../www_components/CountDownTimer'
import moment from 'moment'
moment.locale('vi');

export default ({id, createdTime, assignedToName, taskDeadline, type, note, status, reason, reasonCode, onCancel, isAllow}) => {
    let reasonCodes = reasonCode ? JSON.parse(reasonCode) : null;
    let optionCountDown = {};
    let showCountDown = false;
    if(taskDeadline) {
        optionCountDown = { endDate: taskDeadline };
        let now = new Date(), countDownTime = new Date(taskDeadline);
        if(countDownTime.getTime() > now.getTime()) {
            showCountDown = true;
        }
    }
    return (
        <div className="row task-items">
            <div className="col-md-7 col-xs-6">
                <div className="row">
                    <div className="col-md-2 col-xs-5">
                        {isAllow(PERMISSION_VIEW_ALL_TASKS_ALL_HUB) ?
                            <Link to={`/tasks/detail/${id}`} target="_blank">#{id}</Link>
                            :
                            <span>#{id}</span>
                        }
                    </div>
                    <div className="col-md-4 hidden-xs">
                        {formatFullTime(createdTime)}
                    </div>
                    <div className="col-md-3 col-xs-7 fw-600">
                        {assignedToName}
                    </div>
                    <div className="col-md-3">{typeTasksTitle[type]}</div>
                </div>
                <div className="row">
                    <div className="col-md-12 text-base">{note}</div>
                </div>
            </div>
            <div className="col-md-5 col-xs-6">
                <div className="row">
                    <div className="col-md-4">
                        <span className={`lbl-style label label-${statusLabel[status]} fs-13`}>{statusTasksTitle[status]}</span>
                    </div>
                    <div className="col-md-5">
                        {taskDeadline ?
                            <span>
                                {moment(taskDeadline).format("H:mm DD/MM/YYYY")} <br/>
                                            {showCountDown &&
                                            <span className="text-danger"><CountDownTimer options={optionCountDown} /></span>
                                            }
                            </span>
                            :
                            ''
                        }
                    </div>
                    <div className="col-md-3 text-right">
                        {!endingStatus.includes(status) &&
                        <Touchable
                            actionTypes={[UN_ASSIGN_PICKUP_FROM_TASK.SUCCESS, UN_ASSIGN_PICKUP_FROM_TASK.FAILURE]}
                            bsStyle="danger" bsSize="xsmall" className="btn-outline"
                            onClick={e => onCancel(id)}
                            data-tip="Hủy nhiệm vụ"><i className="ti-close"/> </Touchable>
                        }
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-12 text-danger">
                        <section>
                            {reason}
                        </section>
                        {reasonCodes && reasonCodes.map(obj => (
                            <section>
                                {(obj.check == true || obj.check == "true" )&& <span key={obj.value}>{obj.value}</span> }
                            </section>
                        ))}
                    </div>
                </div>
            </div>
        </div>
    )
}