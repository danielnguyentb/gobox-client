import React from 'react'
import {FormGroup} from 'react-bootstrap'
import Select from 'react-select'
import ChoosePriceRange from '../../../../../../www_components/ChoosePriceRange'
import SelectDateRangePicker from '../../../../../../www_components/SelectDateRangePicker'
import UserTypeahead from '../../../../user/components/widgets/UserTypeahead'
import {typeTasksTitle, statusTasksTitle, TimeTitle} from '../../../constants'

const typeOptions = Object.keys(typeTasksTitle).map(type => ({
    value: type,
    label: typeTasksTitle[type]
}));
const statusOptions = Object.keys(statusTasksTitle).map(status => ({
    value: status,
    label: statusTasksTitle[status]
}));
const timeOptions = Object.keys(TimeTitle).map(key => ({
    value: key,
    label: TimeTitle[key]
}));
export const UserTypeAhead = ({value, onSubmit}) => {
    return (
        <FormGroup>
            <UserTypeahead
                placeholder="Nhận nhiệm vụ"
                value={parseInt(value)}
                onChange={val => {
                    onSubmit({assignedTo: val});
                }}
            />
        </FormGroup>
    )
};

export const TypeSelect = ({value, onSubmit}) => {
    return (
        <FormGroup>
            <Select
                options={typeOptions}
                value={value}
                noResultsText="Không tìm thấy"
                onChange={e => {
                    const type = (e ? e.value : '');
                    onSubmit({type});
                }}
                placeholder="Loại nhiệm vụ"/>
        </FormGroup>
    )
};

export const StatusSelect = ({value, onSubmit}) => {
    return (
        <FormGroup>
            <Select
                options={statusOptions}
                value={value}
                noResultsText="Không tìm thấy"
                onChange={e => {
                    const status = (e ? e.value : '');
                    onSubmit({status});
                }}
                placeholder="Trạng thái"/>
        </FormGroup>
    )
};

export const ObjectCodeSelect = ({value, onSubmit}) => {
    return (
        <FormGroup>
            <input
                onBlur={e => {
                    const objectCode = e.target.value;
                    onSubmit({objectCode});
                }}
                onKeyUp={e => {
                    if(e.key == 'Enter'){
                        const objectCode = e.target.value;
                        onSubmit({objectCode});
                    }
                }}
                defaultValue={value}
                type="text"
                placeholder="Đối tượng"
                className="form-control height-36"
            />
        </FormGroup>
    )
};


export const ChooseCodRange = fields => {
    const {fromValue, toValue, operator, onSubmit} = fields;
    return (
        <FormGroup>
            <ChoosePriceRange
                object="cod"
                fromValue={parseInt(fromValue) || 0}
                toValue={parseInt(toValue) || 0}
                operator={operator}
                onChange={data => onSubmit(data)}
            />
        </FormGroup>
    )
};


export const DatetimeRangePicker = fields => {
    const {time, from, to, onSubmit} = fields;
    return (
        <div>
            <div className="col-md-2 col-xs-12 control-select-small">
                <FormGroup>
                    <Select
                        value={time}
                        noResultsText="Không tìm thấy"
                        onChange={e => {
                            const time = (e ? e.value : '');
                            onSubmit({time});
                        }}
                        placeholder="Chọn thời gian"
                        options={timeOptions}
                    />
                </FormGroup>
            </div>
            <div className="col-md-3 col-xs-12 pl-0 control-datetime-small">
                <SelectDateRangePicker
                    startDate={from}
                    endDate={to}
                    onChange={e => {
                        onSubmit({
                            from: e.startDate ? e.startDate.toISOString() : null,
                            to: e.endDate ? e.endDate.toISOString() : null
                        });
                    }}/>
            </div>
        </div>
    )
};