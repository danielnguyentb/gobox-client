import React from 'react'
import {typeTasksTitle, statusTasksTitle, statusLabel, ObjectTypesDetail, endingStatus} from '../../constants'
import {formatCurrency, formatTime} from '../../../../helpers/format'
import Link from '../../../../../www_components/Link'
import ReactTooltip from 'react-tooltip'
import CountDownTimer from '../../../../../www_components/CountDownTimer'
import moment from 'moment'
moment.locale('vi');


export const TaskItem = ({id, objectId, objectType, taskDeadline, objectMark, objectUri, objectCode, assignedToName, status, type, createdTime, cod, realCod}) => {
    let optionCountDown = {};
    let showCountDown = false;
    if(taskDeadline) {
        optionCountDown = { endDate: taskDeadline };
        let now = new Date(), countDownTime = new Date(taskDeadline);
        if(countDownTime.getTime() > now.getTime()) {
            showCountDown = true;
        }
    }
    return (
    <tr>
        <td>
            <Link to={`/tasks/detail/${id}`} className="fw-600 fs-15">#{id}</Link>
        </td>
        <td className="text-uppercase">{objectMark}:&nbsp;<Link to={objectUri}>{objectCode ? objectCode : '__'}</Link></td>
        <td>{typeTasksTitle[type]}</td>
        <td>{assignedToName}</td>
        <td>
            {realCod ?
                <span
                    data-tip="COD thực thu"
                    className={realCod ? 'text-success' : ''}>{formatCurrency(realCod)}</span>
                :
                <span>{formatCurrency(cod)}</span>
            }
        </td>
        <td>{formatTime(createdTime)}</td>
        <td>
            {taskDeadline ?
                <span>
                    {moment(taskDeadline).format("H:mm DD/MM/YYYY")} <br/>
                    {showCountDown &&
                        <span className="text-danger"><CountDownTimer options={optionCountDown} /></span>
                    }
                </span>
                :
                ''
            }
        </td>
        <td>
            <span className={`lbl-style label label-${statusLabel[status]}`}>{statusTasksTitle[status]}</span>
        </td>
    </tr>
)};

export const TaskTable = ({tasks}) => {
    ReactTooltip.rebuild();
    return (
        <div className="row">
            <ReactTooltip />
            <table className="table tbl-headblue table-striped table-hover">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Đối tượng</th>
                    <th>Loại nhiệm vụ</th>
                    <th>Người nhận nhiệm vụ</th>
                    <th>COD (VNĐ)</th>
                    <th>Thời gian</th>
                    <th>Hạn nhiệm vụ</th>
                    <th>Trạng thái</th>
                </tr>
                </thead>
                <tbody>
                {tasks.length > 0 ? tasks.map(task => {
                    return <TaskItem key={task.id} {...task}/>
                }) : <tr>
                    <td colSpan="100%">Không tìm thấy kết quả nào</td>
                </tr>}
                </tbody>
            </table>
        </div>
    )
};
export default (TaskTable);