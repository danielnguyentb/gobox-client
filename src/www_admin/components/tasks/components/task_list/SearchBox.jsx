import React from 'react'
import {
    TypeSelect, DatetimeRangePicker, StatusSelect,
    ObjectCodeSelect, UserTypeAhead
} from './search_fields'
import './../task.scss'
import {connect} from 'react-redux'
import {taskSelector} from '../../selectors'
import {formatCurrency} from '../../../../helpers/format'


export const SearchForm = ({handleSubmit, initialValues}) => {
    return (
        <form onSubmit={handleSubmit}>
            <div className="row mb-15">
                <div className="col-md-2 col-xs-6 pr-0">
                    <TypeSelect value={initialValues.type} onSubmit={handleSubmit}/>
                </div>
                <div className="col-md-2 col-xs-6 pr-0">
                    <ObjectCodeSelect value={initialValues.objectCode} onSubmit={handleSubmit}/>
                </div>
                <div className="col-md-2 col-xs-6 pr-0">
                    <StatusSelect value={initialValues.status} onSubmit={handleSubmit}/>
                </div>
                <div className="col-md-2 col-xs-6">
                    <UserTypeAhead value={initialValues.assignedTo} onSubmit={handleSubmit}/>
                </div>
                {/*<div className="col-md-2 col-xs-6 pl-0 pr-0">
                 <Fields
                 component={renderChoosePriceRange}
                 names={['cod.from', 'cod.to', 'cod.operator']}
                 placeholder="COD"
                 />
                 </div>*/}
            </div>
            <div className="row">
                <div className="col-md-7 col-xs-12">
                    {initialValues.assignedTo > 0 ? <CodBox/> : null}
                </div>
                <DatetimeRangePicker
                    onSubmit={handleSubmit}
                    time={initialValues.time}
                    from={initialValues.from}
                    to={initialValues.to}/>
            </div>

        </form>
    )
};
export default SearchForm;


export const CodBox = connect((state) => {
    const {sumCod, sumRealCod} = taskSelector(state)['countCod'];
    return {
        sumCod, sumRealCod
    }
})(
    ({sumCod, sumRealCod}) => (
        <p className="mb-0 mt-10">
            Tổng COD (VNĐ): <span className="fw-600">{formatCurrency(sumCod)}</span> / thực
            thu: <span className="text-success fw-600">{formatCurrency(sumRealCod)}</span>
        </p>
    )
);