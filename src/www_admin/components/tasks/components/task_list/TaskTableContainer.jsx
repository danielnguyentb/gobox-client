import React from 'react'
import {connect} from 'react-redux'
import {TaskTable} from './TaskTable'
import {fetchTransportTasks} from '../../actions'
import {PaginateTasksSelector} from '../../selectors'
import {isEqual} from 'lodash'
import qs from 'qs'

export class TaskTableContainer extends React.Component {
    componentDidMount() {
        const {fetchTransportTasks, location} = this.props;
        fetchTransportTasks(location.query)
    }

    componentDidUpdate(prevProps) {
        const {fetchTransportTasks, location} = this.props;
        if (!isEqual(location.query, prevProps.location.query)) {
            fetchTransportTasks(location.query)
        }
    }

    render() {
        const {tasks} = this.props;
        return (
            <TaskTable tasks={tasks}/>
        )
    }
}
const mapStateToProps = state => {
    return {
        tasks: PaginateTasksSelector(state),
    }
};
export default connect(mapStateToProps, {fetchTransportTasks})(TaskTableContainer)