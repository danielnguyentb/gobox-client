import React from 'react'
import {TaskList} from './TaskList'
import {push} from 'react-router-redux'
import {connect} from 'react-redux'
import {isEqual} from 'lodash'
import {exportTasksCsv} from '../../actions'
import {LIMIT_REQUEST} from '../../../../constants'

class TaskListContainer extends React.Component {
    handleRedirect(query) {
        const {dispatch, location} = this.props;
        dispatch(push({
            pathname: '/tasks',
            query: {...location.query, ...query, skip: 0}
        }))
    }

    handleExportTasksCsv() {
        let params = this.props.location.query;
        params['limit'] = LIMIT_REQUEST;
        params['skip'] = 0;
        this.props.exportTasksCsv(params);
    }

    render() {
        const {location, isAllow} = this.props;
        return (
            <TaskList
                isAllow={isAllow}
                funcExportTaskCSV={::this.handleExportTasksCsv}
                onSubmit={::this.handleRedirect}
                location={location}/>
        )
    }
}

const mapStateToProps = state => {
    return {
    }
};
export default connect(
    mapStateToProps, {exportTasksCsv}
)(TaskListContainer);