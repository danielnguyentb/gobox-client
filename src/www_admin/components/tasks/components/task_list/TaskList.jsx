import React from 'react'
import SearchBox from './SearchBox'
import TaskTableContainer from './TaskTableContainer'
import DocumentTitle from './../../../../../www_components/DocumentTitle'
import {RePagination} from '../../../../../www_components/RePagination'
import {TRANSPORT_TASKS_FAILURE} from '../../ActionTypes'
import {ErrorToast} from '../../../../../www_components/Alert'
import  {PERMISSION_CSV_EXPORT_TASKS} from '../../constants'

export class TaskList extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        const {location, onSubmit, funcExportTaskCSV, isAllow} = this.props;
        return (
            <div>
                <DocumentTitle title="Danh sách nhiệm vụ"/>
                <ErrorToast dispatchTypes={[TRANSPORT_TASKS_FAILURE]}/>
                <section className="content-header">
                    <div className="row">
                        <div className="col-md-10 col-xs-10">
                            <h2 className="mt-0">Danh sách nhiệm vụ</h2>
                        </div>
                        <div className="col-md-2 text-right">
                            {isAllow(PERMISSION_CSV_EXPORT_TASKS) &&
                            <button className="btn btn-primary" onClick={e => funcExportTaskCSV()} >
                                <b>XUẤT CSV</b>
                            </button>
                            }
                        </div>
                    </div>
                </section>

                <section className="content">
                    <div className="box">
                        <SearchBox
                            handleSubmit={onSubmit}
                            initialValues={location.query}
                        />
                        <TaskTableContainer location={location}/>
                    </div>
                </section>
                <div className="row">
                    <div className="col-md-12 text-right">
                        <RePagination {...location}/>
                    </div>
                </div>
            </div>
        )
    }
}
export default (TaskList);