import React, {Component, PropTypes} from 'react'
import {OrderTaskReduxForm} from './OrderTaskForm'
import {connect} from 'react-redux'
import {assignPickerToOrder} from '../../actions'
import {PickersByHubIdSelector} from '../../../user/selectors'
import {getPickupBelongToHub} from '../../../hub/actions'
import moment from 'moment'
moment.locale('vi');

class OrderTaskFormContainerBase extends Component {
    static propTypes = {
        hubId: PropTypes.number.isRequired,
        label: PropTypes.string.isRequired,
        orderId: PropTypes.number.isRequired
    };

    componentWillMount() {
        const {getPickupBelongToHub, hubId, pickers} = this.props;
        if (!pickers || pickers.length === 0) {
            getPickupBelongToHub(hubId)
        }
    }

    submit(data) {
        let {assignedTo, note, deliverBefore} = data;
        let {assignPickerToOrder, orderId} = this.props;
        assignedTo = parseInt(assignedTo) || 0;
        if(deliverBefore) {
            let datetime = moment(deliverBefore, "DD-MM-YYYY HH:mm").toDate();
            assignPickerToOrder(orderId, {
                assignedTo, note, deliverBefore: datetime
            })
        } else {
            assignPickerToOrder(orderId, {
                assignedTo, note
            })
        }
    }

    render() {
        let {hubId, label, pickers} = this.props;
        return (
            <OrderTaskReduxForm
                label={label}
                pickers={pickers}
                hubId={hubId}
                onSubmit={::this.submit}/>
        )
    }
}
const mapStateToProps = (state, props) => {
    const getPicker = PickersByHubIdSelector(state);
    return {
        pickers: getPicker(props.hubId)
    }
};
export const OrderTaskFormContainer = connect(mapStateToProps, {
    assignPickerToOrder,
    getPickupBelongToHub
})(OrderTaskFormContainerBase);