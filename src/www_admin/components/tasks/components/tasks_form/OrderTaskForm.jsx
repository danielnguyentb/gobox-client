import React, {PropTypes} from 'react'
import * as RBT from 'react-bootstrap'
import {ASSIGN_PICKER_TO_ORDER_SUCCESS, ASSIGN_PICKER_TO_ORDER_FAILURE} from '../../ActionTypes'
import {Field, reduxForm} from 'redux-form'
import {SuccessToast, ErrorToast} from '../../../../../www_components/Alert/components'
import {renderDateField} from '../../../../helpers/redux-form'
import 'react-date-picker/index.css'
import { DateField } from 'react-date-picker'

//TODO: refactor continue here
export const TaskSelect = ({input, meta}) => {
    return (
        <select >
            <option value="">{label}</option>
            {Object.values(pickers).map(picker =>
                <option key={picker.id} value={picker.id}>{picker.username}</option>
            )}
        </select>
    )
};

export const OrderTaskForm = ({handleSubmit, invalid, pristine, hubId, label, pickers, submitting}) => (
    <form onSubmit={handleSubmit} method="POST">
        <ErrorToast dispatchTypes={[ASSIGN_PICKER_TO_ORDER_FAILURE]}/>
        <div className="row pt-15">
            <div className="col-md-2 col-xs-12 pr-0">
                <RBT.FormGroup>
                    <Field name="assignedTo" className="form-control select" component="select">
                        <option value="">{label}</option>
                        {Object.values(pickers).map(picker =>
                            <option key={picker.id} value={picker.id}>{picker.username}</option>
                        )}
                    </Field>
                </RBT.FormGroup>
            </div>
            <div className="col-md-3 col-xs-12">
                <RBT.FormGroup>
                    <Field component={renderDateField} name="deliverBefore"
                           forceValidDate
                           dateFormat="DD-MM-YYYY HH:mm"
                           placeholder="Hạn giao hàng"
                           className="form-control"
                    />
                </RBT.FormGroup>
            </div>
            <div className="col-md-4 col-xs-12 pl-0 pr-0">
                <RBT.FormGroup>
                    <Field
                        component="textarea"
                        name="note"
                        placeholder="Ghi chú" className="request-notes form-control"/>
                </RBT.FormGroup>
            </div>
            <div className="col-md-2 col-xs-12 text-right">
                <RBT.FormGroup>
                    <button
                        type="submit"
                        disabled={invalid || pristine || submitting}
                        className="btn btn-primary btn-create-request"><i
                        className="fa fa-plus mr-5"/> TẠO NHIỆM VỤ</button>
                </RBT.FormGroup>
            </div>
        </div>
    </form>
);

OrderTaskForm.propTypes = {
    onSubmit: PropTypes.func,
    label: PropTypes.string,
    hubId: PropTypes.number,
    pickers: PropTypes.array,
};

export const validate = (values) => {
    let errors = {};
    if(Object.keys(values).length === 0) return errors;
    if (!values.assignedTo) {
        errors.message = 'Người giao hàng không được để trống'
    }
    return errors;
};

export const OrderTaskReduxForm = reduxForm({
    form: 'createTaskForm',
    validate
})(OrderTaskForm);