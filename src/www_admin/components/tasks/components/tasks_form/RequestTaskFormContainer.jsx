import React, {PropTypes, Component} from 'react'
import {connect} from 'react-redux'
import {PickersByHubIdSelector} from '../../../user/selectors'
import {TYPE_TASKS} from '../../constants'
import {getPickupBelongToHub} from '../../../hub/actions'
import {assignPickupToTask} from '../../actions'
import {RequestTaskReduxForm} from './RequestTaskForm'

class RequestTaskFormContainer extends Component {
    componentWillMount() {
        let {pickers, getPickupBelongToHub, hubId} = this.props;
        if (!pickers || pickers.length == 0) {
            getPickupBelongToHub(hubId)
        }
    }

    createTask(task) {
        const {requestId, assignPickupToTask} = this.props;
        const {assignedTo, note} = task;
        assignPickupToTask({
            type: TYPE_TASKS.PICKUP,
            requestId: parseInt(requestId) || 0,
            assignedTo: parseInt(assignedTo) || 0,
            note,
        });
    };

    render() {
        let {pickers, label} = this.props;
        return (
            <RequestTaskReduxForm
                label={label}
                pickers={pickers}
                onSubmit={::this.createTask}
            />
        );
    }
}
RequestTaskFormContainer.propTypes = {
    requestId: PropTypes.number.isRequired,
    hubId: PropTypes.number.isRequired,
    label: PropTypes.string.isRequired,
};
const mapStateToProps = (state, props) => {
    const getPickers = PickersByHubIdSelector(state);
    return {
        pickers: getPickers(props.hubId)
    }
};
export const RequestTaskFormContainerConnected = connect(mapStateToProps, {
    assignPickupToTask,
    getPickupBelongToHub
})(RequestTaskFormContainer);