import React, {PropTypes} from 'react'
import * as RBT from 'react-bootstrap'
import Touchable from '../../../../../www_components/Touchable'
import {ASSIGN_PICKUP_TO_REQUEST} from '../../ActionTypes'
import {Field, reduxForm} from 'redux-form'
import {SuccessToast, ErrorToast} from '../../../../../www_components/Alert/components'

const successAlert = {
    [ASSIGN_PICKUP_TO_REQUEST.SUCCESS]: 'Thành công',
};

export const RequestTaskForm = ({handleSubmit, pickers, invalid, pristine, label = 'Người giao *', submitting}) => (
    <form onSubmit={handleSubmit} method="POST" className="col-md-12 col-xs-12 create-task">
        <ErrorToast dispatchTypes={[ASSIGN_PICKUP_TO_REQUEST.FAILURE]}/>
        <div className="row">
            <div className="col-md-3 col-xs-12 pr-0">
                <RBT.FormGroup>
                    <Field name="assignedTo" className="form-control select" component="select">
                        <option value="">{label}</option>
                        {Object.values(pickers).map(picker =>
                            <option key={picker.id} value={picker.id}>{picker.username}</option>
                        )}
                    </Field>
                </RBT.FormGroup>
            </div>

            <div className="col-md-6 col-xs-12">
                <RBT.FormGroup>
                    <Field
                        component="textarea"
                        name="note"
                        placeholder="Ghi chú" className="request-notes form-control"/>
                </RBT.FormGroup>
            </div>
            <div className="col-md-3 col-xs-12 text-right">
                <RBT.FormGroup>
                    <button
                        type="submit"
                        disabled={invalid || pristine || submitting}
                        className="btn btn-primary btn-create-request"><i
                        className="fa fa-plus mr-5"/> TẠO NHIỆM VỤ</button>
                </RBT.FormGroup>
            </div>
        </div>

    </form>
);

export const validate = (values) => {
    let errors = {};
    if(Object.keys(values).length === 0) return errors;
    if (!values.assignedTo) {
        errors.message = 'Người lấy hàng không được để trống'
    }
    return errors;
};

export const RequestTaskReduxForm = reduxForm({
    form: 'createTaskForm',
    validate
})(RequestTaskForm);

RequestTaskReduxForm.propTypes = {
    onSubmit: PropTypes.func,
    label: PropTypes.string,
    pickers: PropTypes.array
};