import {encode} from 'helpers'
import React from 'react'


export const generateQuery = (params) => {
    let where = {};
    let {type, status, objectCode, assignedTo, cod, time, from, to, limit = 20, skip = 0, order = 'createdTime DESC'} = params;
    let arr_where = [];
    where.and = [{or: arr_where}];
    if (type) where.and.push({type});
    if (status) where.and.push({status});
    if (objectCode) where.and.push({objectCode: {like: encode(objectCode)}});
    if (assignedTo > 0) where.and.push({assignedTo});
    if (cod) {
        const {operator, from, to} = cod;
        switch (operator) {
            case 'between':
                where.and.push({
                    cod: {[operator]: [from, to]}
                });
                break;
            case 'eq':
                where.and.push({
                    cod: from
                });
                break;
            default:
                where.and.push({
                    cod: {[operator]: from}
                });
                break;
        }
    }
    if (time && from && to) {
        where.and.push({
            [time]: {between: [from, to]}
        })
    }
    const include = {"relation": "assignee", "scope": {"fields": {"id": true, fullName: true, username: true}}};
    return {where, order, limit, skip, include, sum: true};
};

export const generateQueryIncludeActionBy = () => ({
    "include": [
        {
            "relation": "actionBy"
        }
    ],
});

export const generateQueryIncludeAssignee = () => ({
    "include": [
        { "relation": "assignee" }
    ],
    includeTookan: true
});