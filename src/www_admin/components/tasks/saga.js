/**
 * Created by Tester-Ali on 30-11-2016.
 */
import {takeEvery, takeLatest, delay} from 'redux-saga'
import {call, put, fork, select} from 'redux-saga/effects'
import {EXPORT_TASKS_CSV} from './ActionTypes'
import moment from 'moment'
moment.locale('vi');

function* downloadFile(action) {
    yield delay(1000);
    const {response} = action;
    const csvContent = "data:text/csv,\ufeff" + response.tasks;
    const encodedUri = encodeURI(csvContent);
    //window.open(encodedUri);
    const link = document.createElement("a");
    link.setAttribute("href", encodedUri);
    link.setAttribute("download", "danhsachnhiemvu" + moment().format('YMDHMSS') + ".csv");
    document.body.appendChild(link); // Required for FF
    link.click();
}

export default function*() {
    yield takeLatest(EXPORT_TASKS_CSV.SUCCESS, downloadFile)
}