import Client from '../../helpers/Client'
import * as types from './ActionTypes'

class Task extends Client {

    getTransportRequestTasks(transportRequestId) {
        return this.callApi({
            types: [types.GET_TRANSPORT_REQUEST_TASKS.REQUEST, types.GET_TRANSPORT_REQUEST_TASKS.SUCCESS, types.GET_TRANSPORT_REQUEST_TASKS.FAILURE],
            endpoint: `/TransportRequests/${transportRequestId}/tasks`,
            method: 'GET'
        })
    }

    getTransportTask(id, filter) {
        filter = typeof filter === 'string' ? filter : JSON.stringify(filter);
        return this.callApi({
            types: [types.TRANSPORT_TASK.REQUEST, types.TRANSPORT_TASK.SUCCESS, types.TRANSPORT_TASK.FAILURE],
            endpoint: `/TransportTasks/${id}?filter=${filter}`,
            method: 'GET'
        })
    }

    getTransportTaskHistories(id, filter) {
        filter = typeof filter === 'string' ? filter : JSON.stringify(filter);
        return this.callApi({
            types: [types.TRANSPORT_TASK_HISTORIES.REQUEST, types.TRANSPORT_TASK_HISTORIES.SUCCESS, types.TRANSPORT_TASK_HISTORIES.FAILURE],
            endpoint: `/TransportTasks/${id}/histories?filter=${filter}`,
            method: 'GET'
        })
    }
    fetchTransportTasks(filter){
        filter = typeof filter === 'string' ? filter : JSON.stringify(filter);
        return this.callApi({
            types: [types.TRANSPORT_TASKS_REQUEST, types.TRANSPORT_TASKS_SUCCESS, types.TRANSPORT_TASKS_FAILURE],
            endpoint: `/TransportTasks?filter=${filter}`,
            method: 'GET'
        })
    }

    fetchDeliveryTasks(orderId){
        return this.callApi({
            types: [types.DELIVERY_TASKS_REQUEST, types.DELIVERY_TASKS_SUCCESS, types.DELIVERY_TASKS_FAILURE],
            endpoint: `/CustomerDeliveryOrders/${orderId}/deliverTasks`,
            method: 'GET'
        })
    }

    //tạo task cho đơn
    assignPickerToOrder(orderId, data){
        return this.callApi({
            types: [types.ASSIGN_PICKER_TO_ORDER_REQUEST, types.ASSIGN_PICKER_TO_ORDER_SUCCESS, types.ASSIGN_PICKER_TO_ORDER_FAILURE],
            endpoint: `/CustomerDeliveryOrders/${orderId}/deliverTasks`,
            method: 'POST',
            form: ['createTaskForm', true],
            body: data
        })
    }

    //assign 1 pickup vào task
    assignPickupToTask(data) {
        return this.callApi({
            types: [types.ASSIGN_PICKUP_TO_REQUEST.REQUEST, types.ASSIGN_PICKUP_TO_REQUEST.SUCCESS, types.ASSIGN_PICKUP_TO_REQUEST.FAILURE],
            endpoint: `/TransportTasks/assignTo`,
            method: 'POST',
            form: ['createTaskForm', true],
            body: data
        });
    };

    //unAssign 1 pickup khoi request
    unAssignPickupFromTask(id, status) {
        return this.callApi({
            types: [types.UN_ASSIGN_PICKUP_FROM_TASK.REQUEST, types.UN_ASSIGN_PICKUP_FROM_TASK.SUCCESS, types.UN_ASSIGN_PICKUP_FROM_TASK.FAILURE],
            endpoint: `/TransportTasks/${id}/changeStatus/${status}`,
            method: 'PATCH',
        });
    };

    exportTasksCsv(filter) {
        filter = typeof filter === 'string' ? filter : JSON.stringify(filter);
        return this.callApi({
            types: [types.EXPORT_TASKS_CSV.REQUEST, types.EXPORT_TASKS_CSV.SUCCESS, types.EXPORT_TASKS_CSV.FAILURE],
            endpoint: `/TransportTasks/csvTasksExport?filter=${filter}`,
            method: 'GET',
        });
    }

}

export default new Task();