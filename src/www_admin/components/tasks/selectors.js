/**
 * Created by Tester-Ali on 09-11-2016.
 */
import {createSelectorCreator, defaultMemoize, createSelector} from 'reselect'
import lodash from 'lodash'
import {NAME, ObjectTypes, ObjectTypesDetail} from './constants'
import {UserByIdSelector} from '../user/selectors'

const createDeepEqualSelector = createSelectorCreator(
    defaultMemoize,
    lodash.isEqual
);

export const taskSelector = state => state[NAME];
export const tasksSelector = createSelector(
    taskSelector,
    task => Object.values(task.byId)
);

const hydrate = (task, getUser) => {
    if(!task) return null;
    const assignee = getUser(task.assignedTo);
    task.assginee = assignee;
    task.assignedToName = assignee ? assignee.username : '';
    task.objectUri = task.objectType == ObjectTypes.CustomerDeliveryOrder ? `/orders/detail/${task.objectId}` : '';
    task.objectMark = ObjectTypesDetail[task.objectType] ? ObjectTypesDetail[task.objectType]['mark'] : '__';
    return task;
};
export const TaskViewByIdSelector = createDeepEqualSelector(
    [taskSelector, UserByIdSelector],
    (task, getUser) => lodash.memoize(
        id => hydrate(task.byId[id], getUser)
    )
);

export const TasksViewByRequestIdSelector = createDeepEqualSelector(
    [tasksSelector, UserByIdSelector],
    (tasks, getUser) => lodash.memoize(
        requestId => tasks.filter(t => t['requestId'] == requestId)
            .map(t => hydrate(t, getUser))
    )
);

export const TasksViewByOrderIdSelector = createDeepEqualSelector(
    [tasksSelector, UserByIdSelector],
    (tasks, getUser) => lodash.memoize(
        orderId => tasks.filter(t => t['objectType'] == ObjectTypes.CustomerDeliveryOrder && t['objectId'] == orderId)
            .map(t => hydrate(t, getUser))
    )
);

export const TaskViewHistories = createDeepEqualSelector(
    [taskSelector],
    (tasks) => lodash.memoize(
        taskId => tasks.taskHistoriesById[taskId] ?  tasks.taskHistoriesById[taskId].map(t => tasks.byId[t]) : null
    )
);
export const PaginateTasksSelector = createSelector(
    [taskSelector, TaskViewByIdSelector],
    (task, getTask) => task.paginateIds.map(id => getTask(id))
);