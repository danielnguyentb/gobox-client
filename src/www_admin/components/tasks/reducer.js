import {combineReducers} from 'redux'
import {
    GET_TRANSPORT_REQUEST_TASKS, ASSIGN_PICKUP_TO_REQUEST, UN_ASSIGN_PICKUP_FROM_TASK, EXPORT_TASKS_CSV,
    ASSIGN_PICKER_TO_ORDER_SUCCESS, TRANSPORT_TASKS_SUCCESS, TRANSPORT_TASK, TRANSPORT_TASK_HISTORIES
} from './ActionTypes'
import {DELIVERY_ORDER_SUCCESS} from '../order/ActionTypes'
import {GET_TRANSPORT_REQUEST_HUB} from '../transport_requests/ActionTypes'
import {GET_TRANSPORT_REQUEST_DETAIL} from '../hub/ActionTypes';

const byId = (state = {}, action) => {
    let {response} = action;
    let copy = {...state};

    switch (action.type) {
        case GET_TRANSPORT_REQUEST_HUB.SUCCESS:
            response.result.map(request => {
                request.tasks.map(t => {
                    copy[t.id] = t;
                });
            });
            return copy;
        case TRANSPORT_TASKS_SUCCESS:
            response.result.map(t => {
                copy[t.id] = t;
            });
            return copy;
        case GET_TRANSPORT_REQUEST_TASKS.SUCCESS:
        case TRANSPORT_TASK_HISTORIES.SUCCESS:
            response.map(t => {
                copy[t.id] = t;
            });
            return copy;
        case TRANSPORT_TASK.SUCCESS:
            copy[response.id] = response;
            return copy;
        case GET_TRANSPORT_REQUEST_DETAIL.SUCCESS:
            response.tasks.map(t => {
                t['requestId'] = t['requestId'] || response.id;
                copy[t.id] = t;
            });
            return copy;
        case DELIVERY_ORDER_SUCCESS:
            if (!response['deliverTasks']) return copy;
            response['deliverTasks'].map(t => {
                copy[t.id] = t;
            });
            return copy;
        case ASSIGN_PICKER_TO_ORDER_SUCCESS:
        case ASSIGN_PICKUP_TO_REQUEST.SUCCESS:
            copy[response.id] = response;
            return copy;
        case UN_ASSIGN_PICKUP_FROM_TASK.SUCCESS:
            copy[response.id] = response;
            return copy;
    }
    return state;
};

const taskHistoriesById = (state = {}, action) => {
    let {response} = action;
    let copy = {...state};

    switch (action.type) {
        case TRANSPORT_TASK_HISTORIES.SUCCESS:
            response.map(obj => {
                if(!copy[obj['objectId']]) {
                    copy[obj['objectId']] = [obj.id]
                } else {
                    copy[obj['objectId']].push(obj.id)
                }
            });
            return copy;
    }
    return state;
};

const countCod = (state = {}, action) => {
    if (action.type === TRANSPORT_TASKS_SUCCESS) {
        const {sumCod, sumRealCod} = action.response;
        return {
            sumCod, sumRealCod
        }
    }
    return state;
};

const paginateIds = (state = [], action) => {
    const {response} = action;
    switch (action.type) {
        case TRANSPORT_TASKS_SUCCESS:
            return response.result.map(t => t.id);
    }
    return state;
};

export default combineReducers({
    byId, paginateIds, countCod, taskHistoriesById
});