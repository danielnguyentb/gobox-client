import Task from './api'
import {generateQuery, generateQueryIncludeActionBy, generateQueryIncludeAssignee} from './helper'
import {CALL_API} from '../../ActionTypes'
import * as types from './ActionTypes'

export const getTransportRequestTasks = (id) => dispatch => dispatch(Task.getTransportRequestTasks(id));

export const assignPickupToTask = (data) => dispatch => dispatch(Task.assignPickupToTask(data));

export const unAssignPickupFromTask = (id, status) => dispatch => dispatch(Task.unAssignPickupFromTask(id, status));

export const assignPickerToOrder = (orderId, data) => dispatch => dispatch(Task.assignPickerToOrder(orderId, data));

export const fetchDeliveryTasks = id => dispatch => dispatch(Task.fetchDeliveryTasks(id));

export const fetchTransportTasks = filter => dispatch => dispatch(Task.fetchTransportTasks(generateQuery(filter)));

export const getTransportTask = (id) => dispatch => dispatch(Task.getTransportTask(id, generateQueryIncludeAssignee()));

export const getTransportTaskHistories = (id) => dispatch => dispatch(Task.getTransportTaskHistories(id, generateQueryIncludeActionBy()));

export const exportTasksCsv = filter => dispatch => dispatch(Task.exportTasksCsv(generateQuery(filter)));