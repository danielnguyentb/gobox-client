export const NAME = 'tasks';

export const STATUS_TASKS = {
    NEW: 'NEW',
    IN_PROCESS: 'IN_PROCESS',
    DONE: 'DONE',
    FAILED: 'FAILED',
    CANCELLED: 'CANCELLED',
    REJECT: 'REJECT'
};

export const TYPE_TASKS = {
    PICKUP: "PICKUP",
    DELIVER: "DELIVER",
    PICKUP_AND_DELIVER: "PICKUP_AND_DELIVER"
};

export const typeTasksTitle = {
    [TYPE_TASKS.PICKUP]: "Đi lấy hàng",
    [TYPE_TASKS.DELIVER]: "Đi giao hàng",
    [TYPE_TASKS.PICKUP_AND_DELIVER]: "Đi trả hàng",
};

export const statusLabel = {
    [STATUS_TASKS.NEW]: 'default',
    [STATUS_TASKS.IN_PROCESS]: 'warning',
    [STATUS_TASKS.DONE]: 'success',
    [STATUS_TASKS.CANCELLED]: 'danger',
    [STATUS_TASKS.FAILED]: 'danger',
    [STATUS_TASKS.REJECT]: 'danger'
};

export const statusTasksTitle = {
    [STATUS_TASKS.NEW]: "Nhiệm vụ mới",
    [STATUS_TASKS.IN_PROCESS]: "Đang thực hiện",
    [STATUS_TASKS.DONE]: "Thành công",
    [STATUS_TASKS.FAILED]: "Thất bại",
    [STATUS_TASKS.CANCELLED]: "Hủy",
    [STATUS_TASKS.REJECT]: "Từ chối",
};

export const ObjectTypes = {
    CustomerDeliveryOrder: 'CustomerDeliveryOrder',
    TransportRequest: 'TransportRequest'
};

export const ObjectTypesDetail = {
    [ObjectTypes.CustomerDeliveryOrder]: {
        title: 'Vận đơn',
        mark: 'VĐ'
    },
    [ObjectTypes.TransportRequest]: {
        title: 'Yêu cầu lấy hàng',
        mark: 'YCL'
    }
};

export const endingStatus = [
    STATUS_TASKS.CANCELLED,
];

export const Times = {
    STARTED_TIME: 'startedTime',
    FAILED_TIME: 'failedTime',
    SUCCESS_TIME: 'successTime'
};

export const TimeTitle = {
    [Times.STARTED_TIME]: 'Thời gian bắt đầu',
    [Times.SUCCESS_TIME]: 'Thời gian thành công',
    [Times.FAILED_TIME]: 'Thời gian lỗi',
};

export const PERMISSION_VIEW_ALL_TASKS_ALL_HUB = 'PERMISSION_VIEW_ALL_TASKS_ALL_HUB';

export const PERMISSION_CSV_EXPORT_TASKS = 'PERMISSION_CSV_EXPORT_TASKS';