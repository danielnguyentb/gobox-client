import React, {Component} from 'react'
import DocumentTitle from '../../../www_components/DocumentTitle'

class UserChangePassLayout extends Component {
    render() {
        return (
            <DocumentTitle title="Yêu cầu thay đổi mật khẩu">
                <div className="   pace-done">{this.props.children}</div>
            </DocumentTitle>
        )
    }
}
export default UserChangePassLayout