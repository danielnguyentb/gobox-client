import React, {Component} from 'react';
import 'bootstrap';
import 'admin-lte';

import './css/font/Source-Sans-Pro/_font-source-san-pro.scss';
import './css/font/themify-icon/_themify-icons.scss';
import './css/font/fontawesome/_font-awesome.scss';
import './css/general/_theme-default.scss';
import './css/general/_theme-admin.scss';
import './css/general/_react-bootstrap-switch.scss';
import './css/general/_datetime-picker.scss'
import './css/general/_default.scss';
import './css/general/_all-skins.scss';
import './css/general/_generals.scss';
import './css/general/_responsive.scss';
import './css/general/_pagination.scss'
import './css/general/_small-control.scss'
import './css/general/_control-plugin.scss'


import Header from './widgets/Header'
import SideBar from './widgets/SideBar'
import Footer from './widgets/Footer'
import SideBarHub from './widgets/SideBarHub'
import Boxchat from './widgets/Boxchat'
import * as user from '../user'

import {connect} from 'react-redux'

class MainLayout extends Component {

    render() {
        const {isAllow, isGod, isMe, params, dispatch} = this.props;
        const hubId = params.hubId;

        return (
            <div className="mainlayout">
                <Header {...this.props} />
                {hubId ?
                    <SideBarHub {...this.props}/>
                    :
                    <SideBar {...this.props}/>
                }
                <Boxchat {...this.props}/>
                <div className="content-wrapper">
                    {this.props.children && React.cloneElement(this.props.children, {
                        isAllow, isGod, isMe, dispatch
                    })}

                    {/*<Footer {...this.props} />*/}
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    const isAllow = user.selectors.isAllowSelector(state);
    const isGod = user.selectors.isGodSelector(state);
    const isMe = user.selectors.isMeSelector(state);
    return {
        isAllow, isGod, isMe
    }
};

export default connect(mapStateToProps)(MainLayout)