import React, {Component} from 'react';
import Link from '../../../../www_components/Link';
import * as user from '../../user'
import * as customer from '../../customer'
import * as order from '../../order'
import * as tasks from '../../tasks'
import * as customerTransactions from '../../customer_transactions'
import * as transportRequests from '../../transport_requests'
export default class SideBar extends Component {
    render() {
        let {isAllow} = this.props;
        return (
            <aside className="main-sidebar">

                <section className="sidebar">

                    <ul className="sidebar-menu">
                        <li>
                            <Link to="/">
                                <i className="ti-home mr-5"></i><span>Dashboard</span>
                            </Link>
                        </li>
                        {isAllow(order.constants.PERMISSION_DELIVER_ORDER_VIEW_LIST) &&
                        <li className="treeview">
                            <Link to="/orders">
                                <i className="ti-package mr-5"></i><span>Vận đơn</span>
                            </Link>
                        </li>
                        }

                        {isAllow(tasks.constants.PERMISSION_VIEW_ALL_TASKS_ALL_HUB ) &&
                        <li className="treeview">
                            <Link to="/tasks" >
                                <i className="fa fa-check-square-o mr-5"></i><span>Nhiệm vụ</span>
                            </Link>
                        </li>
                        }

                        {isAllow(transportRequests.constants.PERMISSION_VIEW_ALL_REQUEST) &&
                        <li className="treeview">
                            <Link to="/request_list_order">
                                <i className="ti-location-pin mr-5"></i><span>Yêu cầu lấy</span>
                            </Link>
                        </li>
                        }

                        {(isAllow(customer.constants.PERMISSION_CUSTOMER_VIEW_LIST) || isAllow(customer.constants.PERMISSION_CUSTOMER_CREATE)) &&
                        <li className="treeview">
                            <Link to="/customer">
                                <i className="glyphicon glyphicon-user mr-5"></i><span>Khách hàng</span>
                                <span className="pull-right-container">
                                  <i className="ti-angle-down pull-right"></i>
                                </span>
                            </Link>
                            <ul className="treeview-menu">
                                {isAllow(customer.constants.PERMISSION_CUSTOMER_VIEW_LIST) &&
                                <li><Link to="/customers">Danh sách khách hàng</Link></li>
                                }
                                {isAllow(customer.constants.PERMISSION_CUSTOMER_CREATE) &&
                                <li><Link to="/customers/create">Tạo khách hàng</Link></li>
                                }
                            </ul>
                        </li>
                        }
                        {isAllow(customerTransactions.constants.PERMISSION_CUSTOMER_TRANSACTION_CREATE) &&
                        <li className="treeview">
                            <Link to="/balances/create" >
                                <i className="fa fa-dollar mr-5"></i><span>Điều chỉnh công nợ</span>
                            </Link>
                        </li>
                        }

                        <li className="treeview">
                            <Link to="/users">
                                <i className="ti-user mr-5"></i><span>Nhân viên</span>
                                <span className="pull-right-container">
                                  <i className="ti-angle-down pull-right"></i>
                                </span>
                            </Link>
                            <ul className="treeview-menu">
                                {isAllow(user.constants.PERMISSION_USER_VIEW_LIST) &&
                                    <li><Link to="/users">Danh sách nhân viên</Link></li>
                                }
                                {isAllow(user.constants.PERMISSION_USER_CREATE) &&
                                    <li><Link to="/users/create">Tạo nhân viên</Link></li>
                                }
                                <li>
                                    <Link to="/roles">Nhóm & phân quyền</Link>
                                </li>
                            </ul>
                        </li>

                    </ul>
                </section>
            </aside>
        )
    }
}