import React, {Component} from 'react';
import {connect} from 'react-redux'
import {hubByIdSelector} from '../../hub/selectors'
import SideBarHubContent from './SideBarHubContent'

class SideBarHub extends Component {
    render() {
        let {hub} = this.props;
        return (
            <SideBarHubContent {...this.props} hub={hub || {}}/>
        )
    }
}
const mapStateToProps = (state, props) => {
    const getHub = hubByIdSelector(state);
    return {
        hub: getHub(props.params.hubId)
    }
};

const mapDispatchToProps = state => ({});

export default connect(
    mapStateToProps, mapDispatchToProps
)(SideBarHub);