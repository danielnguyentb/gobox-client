import React, {Component, PropTypes} from 'react'
import Link from '../../../../www_components/Link';
import avarta from '../images/user1.png';
import * as RBT from 'react-bootstrap';
import * as user from '../../user';
import * as hub from '../../hub';

import logo_mini from './../images/logo-mini.png'
import logo_text from './../images/logo-text.png'

export default class Header extends Component {

    render() {
        return (
            <header className="main-header">
                <Link to="/" className="logo">
                    <span className="logo-mini">
                        <img src={logo_mini} alt="image"/>
                    </span>
                    <span className="logo-lg">
                        <img src={logo_text} alt="image"/>
                    </span>
                </Link>

                <nav className="navbar navbar-static-top">
                    <Link to="/" className="sidebar-toggle" data-toggle="offcanvas" role="button">
                        <span className="sr-only">Toggle navigation</span>
                        <span className="icon-bar"></span>
                        <span className="icon-bar"></span>
                        <span className="icon-bar"></span>
                    </Link>
                    {/*<RBT.Form className="search-form pull-left hidden-xs">
                        <RBT.FormGroup className="has-feedback mb-0">
                            <RBT.FormControl type="text" placeholder="Tìm kiếm..."
                                             className="form-control rounded navbar-search-input"/>
                            <span className="ti-search form-control-feedback"/>
                        </RBT.FormGroup>
                    </RBT.Form>
*/}
                    <div className="navbar-custom-menu">
                        <ul className="nav navbar-nav">
                            <li className="dropdown messages-menu hidden">
                                <Link to="/" className="dropdown-toggle" data-toggle="dropdown">
                                    <i className="fa fa-envelope-o"></i>
                                    <span className="label label-success">4</span>
                                </Link>
                                <ul className="dropdown-menu">
                                    <RBT.MenuItem header>You have 1 messages</RBT.MenuItem>
                                    <li>
                                        <ul className="menu">
                                            <RBT.MenuItem>
                                                <div className="pull-left">
                                                    <img src={avarta} className="img-circle" alt="User Image"/>
                                                </div>
                                                <h4>
                                                    Support Team
                                                    <small><i className="fa fa-clock-o"></i> 5 mins</small>
                                                </h4>
                                                <p>Why not buy a new awesome theme?</p>
                                            </RBT.MenuItem>
                                        </ul>
                                    </li>
                                    <RBT.MenuItem className="footer">See All Messages</RBT.MenuItem>
                                </ul>
                            </li>
                            <li>
                                <div className="choose-hub">
                                    <hub.components.SelectHub {... this.props}/>
                                </div>

                            </li>

                            <user.components.Profile {...this.props} />
                        </ul>
                    </div>
                </nav>
            </header>
        )
    }
}