import React, {Component} from 'react';
import Link from '../../../../www_components/Link';
import {HUB_PERMISSION} from '../../hub/constants'

export default class SideBarHubContent extends Component {
    render() {
        let {isAllow, hub} = this.props;
        return (
            <aside className={hub.code + " main-sidebar sidebar-hub bg-hub_board-tx" }>
                <section className="sidebar">

                    <ul className="sidebar-menu">
                        <li>
                            <Link to="/">
                                <i className="ti-arrow-left mr-5"></i><span>Quay về</span>
                            </Link>
                        </li>
                        <li className="hidden">
                            <Link to="">
                                <i className="ti-package mr-5"></i><span>Danh sách vận đơn</span>
                            </Link>
                        </li>
                        {(isAllow(HUB_PERMISSION.PERMISSION_VIEW_ALL_REQUEST) || isAllow(HUB_PERMISSION.PERMISSION_VIEW_HUB_REQUEST)) &&
                            <li>
                                <Link to={"/hubs/"+ hub.id + "/transport_request"}>
                                    <i className="ti-location-pin mr-5"></i><span>Yêu cầu lấy</span>
                                </Link>
                            </li>
                        }

                        <li>
                            <Link to={"/hubs/"+ hub.id +"/barcodes"}>
                                <i className="fa fa-barcode mr-5"></i><span>Quét mã</span>
                            </Link>
                        </li>
                        {(isAllow(HUB_PERMISSION.PERMISSION_CREATE_DELIVER_TASK_ALL_HUB) || isAllow(HUB_PERMISSION.PERMISSION_CREATE_DELIVER_TASK_HUB)) &&
                        <li>
                            <Link to={"/hubs/" + hub.id + "/task_batches/create"}>
                                <i className="fa fa-check-square-o mr-5"></i><span>Tạo nhiệm vụ</span>
                            </Link>
                        </li>
                        }
                    </ul>
                </section>
            </aside>
        )
    }
}