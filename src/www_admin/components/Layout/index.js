/**
 * Created by Tester-Ali on 13-10-2016.
 */
export LoginLayout from './LoginLayout'
export MainLayout from './MainLayout'
export UserChangePassLayout from './UserChangePassLayout'
export RootLayout from './RootLayout'
export ErrorLayout from './ErrorLayout'