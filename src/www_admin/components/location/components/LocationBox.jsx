import React, {Component, PropTypes} from 'react'
import Select from 'react-select';
import 'react-select/dist/react-select.css';

export default ({
    provinces, districts, options, onChangeProvince, onChangeDistrict, provinceId, districtId,
    provinceErrorComponent, districtErrorComponent
}) => (
    <section>
        <div className="row">
            <div className="col-md-6 col-xs-6">
                <div className="input-required">
                    <Select
                        options={provinces} simpleValue
                        value={provinceId} placeholder="Tỉnh/TP"
                        onChange={onChangeProvince}
                        {...options} />
                    {options.required && <span className="required-symbol">*</span>}
                </div>
                {provinceErrorComponent}
            </div>
            <div className="col-md-6 col-xs-6">
                <div className="input-required">
                    <Select
                        options={districts} simpleValue
                        value={districtId} placeholder="Quận/Huyện"
                        onChange={onChangeDistrict}
                        {...options} />
                    {options.required && <span className="required-symbol">*</span>}
                </div>
                {districtErrorComponent}
            </div>

        </div>
    </section>
)
