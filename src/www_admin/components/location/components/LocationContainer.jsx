import React, {Component, PropTypes} from 'react'
import {connect} from 'react-redux';
import {getProvinces, getDistrictsByProvinceId} from '../actions'
import {getViewProvinces, getViewDistrictsById} from '../reducer'
import LocationBox from './LocationBox'

class SelectAddress extends React.Component {
    static propTypes = {
        provinceErrorComponent: PropTypes.node,
        districtErrorComponent: PropTypes.node,
        defaultProvinceId: PropTypes.number,
        defaultDistrictId: PropTypes.number,
        handleChange: PropTypes.func.isRequired,
        checkDisabled: PropTypes.bool,
        options: PropTypes.object,
    };

    constructor(props) {
        super(props);
        this.state = {
            provinceId: 0,
            districtId: 0,
        }
    }

    componentWillMount() {
        let {getDistrictsByProvinceId, defaultProvinceId, defaultDistrictId, provinces, getDistricts, getProvinces} = this.props;
        this.setState({provinceId: defaultProvinceId, districtId: defaultDistrictId});
        if (!provinces || provinces.length == 0) {
            getProvinces()
        }
        let districts = getDistricts(defaultProvinceId);
        if (defaultProvinceId > 0) {
            getDistrictsByProvinceId(defaultProvinceId);
        }
    }

    onChangeProvince(newValue) {
        let provinceId = parseInt(newValue, 10) || 0;
        this.setState({provinceId});
        if(provinceId > 0){
            this.props.getDistrictsByProvinceId(provinceId);
        }
        this.props.handleChange({provinceId});
    }

    onChangeDistrict(newValue) {
        let districtId = parseInt(newValue, 10);
        this.setState({districtId});
        this.props.handleChange({districtId});
    }

    render() {
        let {provinceErrorComponent, districtErrorComponent, provinces, getDistricts, options} = this.props;
        let {provinceId, districtId} = this.state;
        let districts = getDistricts(provinceId);
        options = options || {required: false};
        return (
            <LocationBox
                districtErrorComponent={districtErrorComponent}
                provinceErrorComponent={provinceErrorComponent}
                provinces={provinces}
                districts={districts}
                options={options}
                provinceId={provinceId}
                districtId={districtId}
                onChangeProvince={::this.onChangeProvince}
                onChangeDistrict={::this.onChangeDistrict}
            />
        );
    }
}
const mapStateToProps = (state, props) => {
    return {
        provinces: getViewProvinces(state, props.checkDisabled),
        getDistricts: (id) => getViewDistrictsById(state, id, props.checkDisabled),
    }
};

export default connect(
    mapStateToProps, {getProvinces, getDistrictsByProvinceId}
)(SelectAddress);