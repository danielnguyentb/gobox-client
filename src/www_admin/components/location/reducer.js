import {
    LOCATIONS_SUCCESS, LOCATION_PROVINCES_SUCCESS,
    LOCATION_DISTRICTS_SUCCESS
} from './ActionTypes'
import moment from 'moment'
import {combineReducers} from 'redux'
import Localize from '../../../resources/Localize'
moment.locale('vi');

const TYPE_PROVINCE = 'PROVINCE';
const TYPE_DISTRICT = 'DISTRICT';

const byId = (state = {}, action) => {
    let copy = {...state};
    switch (action.type) {
        case LOCATION_PROVINCES_SUCCESS:
            action.response.map(p => {
                p['type'] = TYPE_PROVINCE;
                copy[p.id] = p;
            });
            break;
        case LOCATION_DISTRICTS_SUCCESS:
            action.response.map(d => {
                d['type'] = TYPE_DISTRICT;
                copy[d.id] = d;
            });
            break;
    }
    return copy;
};


const metadata = (state = {}, action) => {
    switch (action.type) {
        case LOCATIONS_SUCCESS:
            return {
                ...state, ...action.response.metadata,
            };
        case LOCATION_PROVINCES_SUCCESS:
            return {
                ...state, provinces: action.response
            };
        case LOCATION_DISTRICTS_SUCCESS:
            return {
                ...state, districts: action.response
            }
    }
    return state;
};


const visibleIds = (state = [], action) => {
    let t = [];
    switch (action.type) {
        case LOCATION_PROVINCES_SUCCESS:
            t = action.response.map(p => p.id);
            break;
        case LOCATION_DISTRICTS_SUCCESS:
            t = action.response.map(d => d.id);
            break;
    }
    let all = state.concat(t);
    return all.filter((v, pos) => all.indexOf(v) == pos);
};


export const getLocation = (state, id) => {
    return state.location.byId[id] || {};
};

export const getVisibleLocations = (state) => {
    return state.location.visibleIds.map(id => getLocation(state, id));
};

export const getVisibleProvinces = (state) => getVisibleLocations(state).filter(loc => loc.type === TYPE_PROVINCE);

export const getVisibleDistricts = (state) => getVisibleLocations(state).filter(loc => loc.type === TYPE_DISTRICT);

export const getViewProvinces = (state, checkDisabled = false) => {
    return getVisibleProvinces(state).map(province => {
        const t = {value: province.id, label: province.name};
        if (checkDisabled) {
            const notYet = province.state != 'ENABLED' ? ' (Chưa hỗ trợ)' : '';
            t['disabled'] = province.state != 'ENABLED';
            t['label'] += notYet;
        }
        return t;
    })
};

export const getVisibleDistrictsById = (state, id) => getVisibleDistricts(state).filter(d => d['parentId'] === id);

export const getViewDistrictsById = (state, id, checkDisabled = false) => {
    let districts = getVisibleDistrictsById(state, id);
    return districts.map(district => {
        let t = {value: district.id, label: district.name};
        if (checkDisabled) {
            let notYet = district.state != 'ENABLED' ? ' (Chưa hỗ trợ)' : '';
            t['disabled'] = district.state != 'ENABLED';
            t['label'] += notYet;
        }
        return t;
    })
};


export default combineReducers({
    byId, visibleIds, metadata,
    error: (state = null, {error}) => error ? Localize.t(error) : null,
    requestingProvinces: (state = false, {requestingProvinces}) => typeof requestingProvinces === 'boolean' ? requestingProvinces : state,
})