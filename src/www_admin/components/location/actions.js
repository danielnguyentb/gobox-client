import Location from './api'

export const getDistrictsByProvinceId = (province_id) => dispatch => dispatch(Location.getDistrictsByProvinceId(province_id));

export const getProvinces = () => dispatch => dispatch(Location.getProvinces());