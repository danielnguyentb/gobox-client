import CustomerTransactions from './api'
import {} from './helper'

export const createCustomerTransaction = (data) => dispatch => dispatch(CustomerTransactions.createTransaction(data));
