import Client from '../../helpers/Client'
import * as ActionTypes from './ActionTypes'

class CustomerTransactions extends Client {

    createTransaction(data) {
        return this.callApi({
            types: [ActionTypes.CUSTOMER_TRANSACTION_CREATE.REQUEST, ActionTypes.CUSTOMER_TRANSACTION_CREATE.SUCCESS, ActionTypes.CUSTOMER_TRANSACTION_CREATE.FAILURE],
            endpoint: `/CustomerTransactions`,
            method: 'POST',
            body: data,
            form: "BalanceForm"
        })
    }
}

export default new CustomerTransactions();