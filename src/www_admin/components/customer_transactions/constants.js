export const NAME = 'customer_transactions';

// Quyền cho phép quản trị viên tạo giao dịch điều chỉnh công nợ
export const PERMISSION_CUSTOMER_TRANSACTION_CREATE = 'PERMISSION_CUSTOMER_TRANSACTION_CREATE';