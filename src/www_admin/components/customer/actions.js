import Customer from './api'
import {generateQuery} from './helper'

export const getCustomerById = (customer_id) => dispatch => dispatch(Customer.getCustomerById(customer_id));

export const updateCustomer = (customer) => dispatch => dispatch(Customer.update(customer));

export const changeCustomerPassword = (customer, id) => dispatch => dispatch(Customer.changeCustomerPassword(customer, id));

export const getCustomers = (params) => dispatch => dispatch(Customer.getCustomers(generateQuery(params)));

export const createCustomer = (customer) => dispatch => dispatch(Customer.create(customer));

export const getListCustomerCsv = (params) => dispatch => dispatch(Customer.getListCustomerCsv(generateQuery(params)));