export const NAME = 'customer';
//--------customer permission
export const PERMISSION_CUSTOMER_VIEW_LIST = 'PERMISSION_CUSTOMER_VIEW_LIST';
export const PERMISSION_CUSTOMER_VIEW = 'PERMISSION_CUSTOMER_VIEW';
export const PERMISSION_CUSTOMER_CREATE = 'PERMISSION_CUSTOMER_CREATE';
export const PERMISSION_CUSTOMER_EDIT = 'PERMISSION_CUSTOMER_EDIT';
export const PERMISSION_CUSTOMER_CHANGE_PASSWORD = 'PERMISSION_CUSTOMER_CHANGE_PASSWORD';
export const PERMISSION_CUSTOMER_LOGOUT_ALL = 'PERMISSION_CUSTOMER_LOGOUT_ALL';
export const PERMISSION_CUSTOMER_EXPORT_CSV = 'PERMISSION_CUSTOMER_EXPORT_CSV';
