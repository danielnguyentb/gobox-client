import Client from '../../helpers/Client'
import * as ActionTypes from './ActionTypes'

class Customer extends Client {

    getCustomerById(customer_id) {
        return this.callApi({
            types: [ActionTypes.CUSTOMER_REQUEST, ActionTypes.CUSTOMER_SUCCESS, ActionTypes.CUSTOMER_FAILURE],
            endpoint: `/customers/${customer_id}`,
            method: 'GET',
        });
    }

    create(customer){
        return this.callApi({
            types: [ActionTypes.CUSTOMER_CREATE_REQUEST, ActionTypes.CUSTOMER_CREATE_SUCCESS, ActionTypes.CUSTOMER_CREATE_FAILURE],
            endpoint: `/customers`,
            method: 'POST',
            body: customer,
            form: 'customer_form'
        })
    }

    update(customer) {
        return this.callApi({
            types: [ActionTypes.CUSTOMER_UPDATE_REQUEST, ActionTypes.CUSTOMER_UPDATE_SUCCESS, ActionTypes.CUSTOMER_UPDATE_FAILURE],
            endpoint: `/customers/${customer.id}`,
            method: 'PUT',
            body: customer,
            form: "CustomerFormInfo"
        }, 'updatingCustomer');
    }

    getCustomers(_filter) {
        let filter = typeof _filter === 'string' ? _filter : JSON.stringify(_filter);
        return this.callApi({
            types: [ActionTypes.CUSTOMERS_REQUEST, ActionTypes.CUSTOMERS_SUCCESS, ActionTypes.CUSTOMERS_FAILURE],
            endpoint: _filter ? `/customers?filter=${filter}` : '/customers',
            method: 'GET',
        });
    }

    changeCustomerPassword(customer, id) {
        return this.callApi({
            types: [ActionTypes.CHANGE_PASSWORD, ActionTypes.CHANGE_PASSWORD_SUCCESS, ActionTypes.CHANGE_PASSWORD_FAILURE],
            endpoint: `/customers/${id}/changePassword`,
            method: 'PUT',
            body: customer
        });
    }
    getListCustomerCsv(filter = '') {
        filter = typeof filter == 'string' ? filter : JSON.stringify(filter);
        return this.callApi({
            types: [ActionTypes.CUSTOMER_LIST_EXPORT_CSV.REQUEST, ActionTypes.CUSTOMER_LIST_EXPORT_CSV.SUCCESS, ActionTypes.CUSTOMER_LIST_EXPORT_CSV.FAILURE],
            endpoint: `/customers/csvCustomers?filter=${filter}`,
            method: 'GET',
        })
    }
}

export default new Customer();