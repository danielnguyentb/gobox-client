/**
 * Created by Tester-Ali on 07-11-2016.
 */
import {createSelectorCreator, defaultMemoize, createSelector} from 'reselect'
import lodash from 'lodash'
import {NAME} from './constants'

const createDeepEqualSelector = createSelectorCreator(
    defaultMemoize,
    lodash.isEqual
);

export const customerSelector = state => state[NAME];
export const customersSelector = createDeepEqualSelector(
    customerSelector,
    customer => Object.values(customer.byId)
);

export const CustomerByIdSelector = createDeepEqualSelector(
    customerSelector,
    customer => lodash.memoize(
        id => customer.byId[id]
    )
);

export const PaginateCustomersSelector = createSelector(
    [customerSelector, CustomerByIdSelector],
    (customer, getCustomer) => customer.paginateIds.map(id => getCustomer(id))
);