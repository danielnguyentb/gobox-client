import React from 'react'
import {formatCurrency} from '../../../../helpers/format'
import Link from '../../../../../www_components/Common/Link';
import {Status} from '../../../balance/constants'

export default ({id, code, username, state, email, emailVerified, mobile, registeredDateLocale, allow, projectedBalance, realBalance}) => (
    <tr>
        <td className="hidden-xs">#{id}</td>
        <td>{state == "INACTIVE" && <i className="ti ti-lock"/>}
            {allow.viewDetail ? <Link className="fw-600" to={`/customers/detail/${id}`}> {username}</Link> : username}
        </td>
        <td className={`hidden-xs ${!emailVerified && "email-active"}`}>
            {!emailVerified && <i className="fa fa-circle text-danger"/>}{email}
        </td>

        <td className="hidden-xs">{mobile}</td>
        <td>
            {allow.viewBalance ?
                <Link to={`/balances/customer/${id}?status=${Status.EXPECTED}`}>
                    <span className="fw-600">{formatCurrency(projectedBalance)}</span>
                </Link>
                : <span className="fw-600">{formatCurrency(projectedBalance)}</span>
            }
        </td>
        <td>
            {allow.viewBalance ?
                <Link to={`/balances/customer/${id}?status=${Status.COMPLETED}`}>
                    <span className="fw-600">{formatCurrency(realBalance)}</span>
                </Link>
                : <span className="fw-600">{formatCurrency(realBalance)}</span>
            }
        </td>
        {/*<td><span className="text-danger fw-600">(20.000)</span></td>*/}
        {/*Đối với giá trị âm thêm class là text-danger và đóng mở ngoặc*/}
        <td className="hidden-xs">{registeredDateLocale}</td>
        <td className="hidden-xs">30/08/1991</td>
    </tr>
)