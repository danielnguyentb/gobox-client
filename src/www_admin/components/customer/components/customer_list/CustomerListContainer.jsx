import React, {Component, PropTypes} from 'react'
import {connect} from 'react-redux'
import {getCustomers} from '../../actions'
import {PaginateCustomersSelector, customerSelector} from '../../selectors'
import Loading from 'react-loading'
import CustomerList from './CustomerList'
import {PERMISSION_CUSTOMER_VIEW} from '../../constants'
import {PERMISSION_FINANCIAL_HISTORY_VIEW_LIST} from '../../../balance/constants'

class CustomerListContainer extends Component {
    static propTypes = {
        getCustomers: PropTypes.func.isRequired,
        customers: PropTypes.array.isRequired,
        isAllowViewDetail: PropTypes.bool,
    };

    componentWillMount() {
        this.props.getCustomers(this.props.location.query);
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.location.search != this.props.location.search) {
            nextProps.getCustomers(nextProps.location.query);
        }
    }

    render() {
        let {customers, allow} = this.props;
        return (
            <CustomerList customers={customers} allow={allow}/>
        )
    }
}

const mapStateToProps = (state, props) => ({
    customers: PaginateCustomersSelector(state),
    allow: {
        viewDetail: props.isAllow(PERMISSION_CUSTOMER_VIEW),
        viewBalance: props.isAllow(PERMISSION_FINANCIAL_HISTORY_VIEW_LIST)
    }
});

export default connect(mapStateToProps, {getCustomers})(CustomerListContainer)