import React from 'react'
import {push} from 'react-router-redux'
import {connect} from 'react-redux'
import qs from 'qs'
import {getCustomers} from '../../actions'
import {RePagination} from 'cores/RePagination'

export class PaginationContainer extends React.Component {
    onSubmit(data) {
        data = {...this.props['location']['query'], ...data};
        this.props.dispatch(push(`/customers?${qs.stringify(data)}`));
    }

    render() {
        return (
            <div className="row">
                <div className="col-md-12 text-right">
                    <RePagination
                        onSubmit={::this.onSubmit}/>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {};
};

export default connect(mapStateToProps, {getCustomers})(PaginationContainer)