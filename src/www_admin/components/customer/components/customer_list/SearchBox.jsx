import React, {Component, PropTypes} from 'react'
import {StatusDetail as BalanceStatusDetail} from '../../../balance/constants'
import ChoosePriceRange from '../../../../../www_components/ChoosePriceRange'


export class BalanceFormGroup extends Component {
    static propTypes = {
        onSubmit: React.PropTypes.func,
        operator: React.PropTypes.string,
        object: React.PropTypes.string,
        from: React.PropTypes.number,
        to: React.PropTypes.number,
    };

    constructor(props) {
        super(props);
        this.state = {
            object: props.object, from: props.from, to: props.to, operator: props.operator
        }
    }

    componentDidUpdate(prevProps) {
        const {operator, object, from, to} = this.props;
        if (operator !== prevProps.operator) {
            this.setState({operator})
        }
        if (object !== prevProps.object) {
            this.setState({object})
        }
        if (from !== prevProps.from) {
            this.setState({from})
        }
        if (to !== prevProps.to) {
            this.setState({to})
        }
    }

    submit(data) {
        data = {...this.state, ...data};
        this.setState(data);
        const {object, from, to, operator} = data;
        if (object && operator && from !== '') {
            const {onSubmit} = this.props;
            onSubmit(data);
        }
    }

    render() {
        const {object = '', operator, from, to} = this.state;
        const onSubmit = ::this.submit;
        return (
            <div>
                <div className="col-md-2 col-xs-6 pr-5">
                    <select
                        onChange={e => onSubmit({object: e.target.value})}
                        className="form-control mb-15"
                        defaultValue={object}>
                        <option value="">Loại công nợ</option>
                        {Object.values(BalanceStatusDetail).map(detail => {
                            return <option key={detail['balance']} value={detail['balance']}>{detail['label']}</option>
                        })}
                    </select>
                </div>
                <div className="col-md-5 pl-0">
                    <ChoosePriceRange
                        object={object}
                        placeholder="Tiền công nợ"
                        onChange={onSubmit}
                        onBlur={onSubmit}
                        onEnter={onSubmit}
                        fromValue={parseInt(from) || 0}
                        toValue={parseInt(to) || 0}
                        operator={operator}
                    />
                </div>
            </div>
        )
    }
}
export const SearchBox = ({orderBy, field, identity, mobile, onSubmit, object, from, to, operator}) => {
    const onEnter = code => e => {
        e.preventDefault();
        if (e.key == 'Enter') {
            onSubmit({[code]: e.target.value})
        }
    };
    return (
        <div className="row">
            <div className="col-md-3 col-xs-12">
                <div className="form-group">
                    <input
                        onBlur={e=>onSubmit({identity: e.target.value})}
                        onKeyUp={onEnter('identity')}
                        defaultValue={identity}
                        className="form-control" placeholder="Username/Email"/>
                </div>
            </div>
            <div className="col-md-2 col-xs-12">
                <div className="form-group">
                    <input
                        onBlur={e=>onSubmit({mobile: e.target.value})}
                        onKeyUp={onEnter('mobile')}
                        defaultValue={mobile}
                        className="form-control" placeholder="Số điện thoại"/>
                </div>
            </div>
            <BalanceFormGroup
                operator={operator}
                from={parseInt(from)||0}
                to={parseInt(to)||0}
                onSubmit={onSubmit}
                object={object}/>
        </div>
    )
};