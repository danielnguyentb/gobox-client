import React, {Component, PropTypes} from 'react'
import {connect} from 'react-redux'
import {SearchBox} from './SearchBox'
import {getCustomers} from '../../actions'
import {push} from 'react-router-redux'
import qs from 'qs'

export class SearchContainer extends Component {

    search(data) {
        //let {object, operator, from, to} = data;
        let params = {
            ...this.props.location.query,
            ...data,
            skip: 0,
        };
        this.props.dispatch(push(`/customers?${qs.stringify(params)}`));
    }

    render() {
        return (
            <SearchBox {...this.props.location.query} onSubmit={::this.search}/>
        )
    }
}

const mapStateToProps = (state) => ({});

export default connect(mapStateToProps, {getCustomers})(SearchContainer)