import React, {Component, PropTypes} from 'react';
import Link from '../../../../../www_components/Common/Link';
import * as RBT from 'react-bootstrap';
import {connect} from 'react-redux'
import CustomerListContainer from './CustomerListContainer'
import SearchContainer from './SearchContainer'
import DocumentTitle from '../../../../../www_components/Common/DocumentTitle'
import {replace} from 'react-router-redux'
import {PERMISSION_CUSTOMER_VIEW_LIST, PERMISSION_CUSTOMER_VIEW, PERMISSION_CUSTOMER_EXPORT_CSV} from '../../constants'
import {getListCustomerCsv} from '../../actions'
import {toastr} from '../../../../../www_components/Toastr2'
import {RePagination} from 'cores/RePagination'

class CustomerManagement extends Component {
    componentWillMount(){
        if(!this.props.isAllow(PERMISSION_CUSTOMER_VIEW_LIST)){
            this.props.dispatch(replace('/error/403'))
        }
    }
    handleExportCustomerCsv() {
        let params = this.props.location.query;
        params['limit'] = 9999;
        params['skip'] = 0;
        this.props.getListCustomerCsv(params);
    }

    componentWillReceiveProps(nextProps) {
        if(nextProps.error) {
            toastr.error(nextProps.error);
        }
    }
    render() {
        let {isAllow, location} = this.props;
        return (
            <div>
                <DocumentTitle title="Danh sách khách hàng"/>
                <section className="content-header">
                    <div className="row">
                        <div className="col-md-8 col-xs-12">
                            <h2 className="mt-0 mb-15">Danh sách khách hàng</h2>
                            <span className="alert alert-warning p-10">Công nợ dương (&gt;0) là khách nợ dịch vụ. Công nợ âm (&lt;0) là dịch vụ nợ khách.</span>
                        </div>
                        <div className="col-md-4 text-right col-xs-12">
                        {isAllow(PERMISSION_CUSTOMER_EXPORT_CSV) &&
                            <RBT.Button bsStyle="success" onClick={(e) => this.handleExportCustomerCsv()}>
                                <b><i className="fa fa-file-excel-o mr-5"/> XUẤT CSV</b>
                            </RBT.Button>
                        }
                            <Link to="/customers/create">
                                <RBT.Button bsStyle="primary" className="btn fw-600 ml-10"><i className="fa fa-plus mr-5"/> TẠO
                                    KHÁCH HÀNG</RBT.Button>
                            </Link>
                        </div>
                        <div className="clearfix"></div>
                    </div>
                </section>
                <section className="content">
                    <div className="box">
                        <SearchContainer {...this.props} />
                        <CustomerListContainer {...this.props} isAllowViewDetail={this.props.isAllow(PERMISSION_CUSTOMER_VIEW)}  />
                        <div className="row">
                            <div className="col-md-12 text-right">
                                <RePagination {...location} />
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        );
    }
}

const mapStateToProps = (state, props) => ({
    isAllow: (resource) => props.isAllow(resource),
    error: state.customer.error
});
export default connect(mapStateToProps, {getListCustomerCsv})(CustomerManagement);