import React, {PropTypes} from 'react'
import CustomerItem from './CustomerItem'

export default ({customers, allow}) => (
    <div className="row">
        <div className="col-md-12">
            <table className="table tbl-headblue table-striped table-hover">
                <thead>
                <tr>
                    <th className="hidden-xs">ID</th>
                    <th>Username</th>
                    <th className="hidden-xs">Email</th>
                    <th className="hidden-xs">SĐT</th>
                    <th>Dự kiến (VNĐ)</th>
                    <th>Đối soát(VNĐ)</th>
                    <th className="hidden-xs">Ngày ĐK</th>
                    <th className="hidden-xs">Ngày đặt đơn CC</th>
                </tr>
                </thead>
                <tbody>
                {customers.length > 0 ? customers.map(customer => <CustomerItem key={customer.id} {...customer} allow={allow} />)
                    : <tr><td colSpan="100%">Không tìm thấy kết quả nào</td></tr>
                }
                </tbody>
            </table>
        </div>
    </div>
)