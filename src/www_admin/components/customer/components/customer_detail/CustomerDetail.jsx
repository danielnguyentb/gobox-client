import React from 'react';
import Link from '../../../../../www_components/Common/Link';
import * as RBT from 'react-bootstrap'
import img_avatar from './avatar.jpg';
import './../customer-detail.scss'
import { Control, Form, actions, Field, combineForms } from 'react-redux-form';
import {
    PERMISSION_CUSTOMER_CHANGE_PASSWORD,
    PERMISSION_CUSTOMER_EDIT,
    PERMISSION_CUSTOMER_LOGOUT_ALL
} from '../../constants'
import CustomerFormInfo from './CustomerFormInfoContainer'
import Validation from 'react-validation';
import ObjectForm from '../../../../helpers/ValidationForm';
import {toastr} from 'react-redux-toastr'
import ChangePassword from '../../../user/components/detail/modals/ChangePassword'
import ModalChangeEmail from './ModalChangeEmail'

Validation.rules = ObjectForm;
export default class CustomerDetail extends React.Component  {
    constructor(props){
        super(props);
    }
    componentWillMount() {
        this.setState({
            stateUpdated: false,
            disabled: false,
            showModalState: false,
        });
    }
    componentWillReceiveProps(nextProps){
        let updatedCustomer = nextProps.updatedCustomer;
        if(updatedCustomer) {
            if(updatedCustomer.state == 'INACTIVE') {
                this.setState({stateUpdated: 'INACTIVE'})
            } else if(updatedCustomer.state == 'ACTIVE') {
                this.setState({stateUpdated: 'ACTIVE'})
            }
        }

        if(this.props.updatingCustomer && !nextProps.updatingCustomer && !nextProps.error){
            if(nextProps.loggedOutMessage) {
                toastr.success(nextProps.loggedOutMessage)
            } else {
                toastr.success("Lưu thông tin thành công");
            }
        }

        this.handleUnDisabled();

        // if(nextProps.error) {
        //     toastr.error(nextProps.error);
        // }
    }
    showModalState() {
        this.setState({showModalState: true});
    }
    closeModalState() {
        this.setState({showModalState: false});
    }
    handleDisabled() {
        this.setState({disabled: true});
    }
    handleUnDisabled() {
        this.setState({disabled: false});
    }
    showModal(msg) {
        this.setState({messagePopup: msg});
        this.setState({showModal: true});
    }
    closeModal() {
        this.setState({showModal: false});
    }
    render() {
        let {customer, updateCustomer, isAllow, changeStaffPassword, error, changedPassword} = this.props;
        let stateValue = customer ? (this.state.stateUpdated ? (this.state.stateUpdated == 'ACTIVE' ? 'INACTIVE' : 'ACTIVE') : (customer.state == 'ACTIVE' ? 'INACTIVE' : 'ACTIVE')) : "";
        customer = customer || {};
        return (
            <div>
                <section className="content-header">
                    <h1>Chi tiết khách hàng</h1>
                    <ol className="breadcrumb mb-0">
                        <li><Link to="/customers">Danh sách khách hàng</Link></li>
                        <li className="active">Chi tiết khách hàng</li>
                    </ol>
                </section>
                <section className="content">
                    <div className="box">
                        <div className="row">

                            <div className="col-md-offset-1 col-md-3 col-xs-12 text-center">
                                <div className="row">
                                    <div className="col-md-12 block-avatar hidden">
                                        <img src={customer ? img_avatar : img_avatar}/>
                                    </div>
                                    <div className="col-md-12 user-infomation">
                                        <h4>
                                            { customer ? customer.fullName : ' -- '}
                                        </h4>
                                        <p>{customer ? customer.username : ''}</p>
                                    </div>
                                </div>

                                <div className="row menu-detail-info">
                                    <div className="col-md-12">
                                        <hr/>
                                        <ul>
                                            <li><Link to={`/balances/customer/${customer.id}?status=EXPECTED`}>Danh sách công nợ</Link></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-6 col-xs-12">
                                <table className="table tbl-info-detail tbl-customer-detail">
                                    <thead>
                                        <tr>
                                            <th colSpan="3">BẢO MẬT</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td className="fw-600">
                                                Mật khẩu
                                            </td>
                                            <td>******* </td>
                                            <td>
                                                {isAllow(PERMISSION_CUSTOMER_CHANGE_PASSWORD) &&
                                                <ChangePassword
                                                    validate={false}
                                                    error={error ? error : ''}
                                                    changedPassword={!!changedPassword}
                                                    onSubmit={changeStaffPassword}
                                                    showOldPasswordControl={false}
                                                    showRequireToChangePassword={true}/>
                                                }
                                            </td>
                                        </tr>
                                        {isAllow(PERMISSION_CUSTOMER_LOGOUT_ALL) &&
                                        <tr>
                                            <td className="fw-600">Đăng nhập</td>
                                            <td><RBT.Button bsStyle="default">Đăng xuất tất cả thiết bị</RBT.Button></td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        }

                                        <ModalChangeEmail {...this.props}/>

                                        <tr>
                                            <td className="fw-600">Trạng thái</td>
                                            <td className="fw-600">
                                                {
                                                    customer
                                                        ?
                                                        (this.state.stateUpdated ? (this.state.stateUpdated == 'ACTIVE' ? 'Hoạt động' : 'Bất hoạt') : (customer.state == 'ACTIVE' ? 'Hoạt động' : 'Bất hoạt'))
                                                        :
                                                        ""
                                                }
                                            </td>
                                            <td>
                                                {isAllow(PERMISSION_CUSTOMER_EDIT) &&
                                                <RBT.Button className="btn btn-danger" disabled={this.state.disabled }
                                                    onClick={this.showModalState.bind(this)}>
                                                    {customer
                                                        ?
                                                        (this.state.stateUpdated ? (this.state.stateUpdated == 'ACTIVE' ? 'KHÓA' : 'MỞ KHÓA') : (customer.state == 'ACTIVE' ? 'KHÓA' : 'MỞ KHÓA'))
                                                        :
                                                        ""}
                                                </RBT.Button>
                                                }
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>

                                <CustomerFormInfo {...this.props} />

                            </div>
                        </div>
                    </div>
                </section>

                {/* Popup confirm khóa tài khoản */}
                <RBT.Modal show={this.state.showModalState} onHide={this.closeModalState.bind(this)}>
                    <Validation.components.Form>
                        <RBT.Modal.Header closeButton>
                            <RBT.Modal.Title>Xác nhận</RBT.Modal.Title>
                        </RBT.Modal.Header>
                        <RBT.Modal.Body>
                            Bạn có chắc chắn muốn <b>{(this.state.stateUpdated ? (this.state.stateUpdated == 'ACTIVE' ? 'khóa' : 'mở khóa') : (customer.state == 'ACTIVE' ? 'KHÓA' : 'MỞ KHÓA'))}</b> tài khoản <b>{ customer ? customer.username : ''} không?</b>
                        </RBT.Modal.Body>
                        <RBT.Modal.Footer>
                            <RBT.Button onClick={this.closeModalState.bind(this)}>ĐÓNG</RBT.Button>
                            <Validation.components.Button disabled={this.state.disabled} autoFocus={true}
                                className="btn btn-primary"
                                onClick={(e) => {
                                    e.preventDefault();
                                    updateCustomer({ id: customer.id, state: stateValue });
                                    this.handleDisabled();
                                    this.closeModalState();
                                }}
                            >
                                {
                                    customer
                                        ?
                                        (this.state.stateUpdated ? (this.state.stateUpdated == 'ACTIVE' ? 'KHÓA' : 'MỞ KHÓA') : (customer.state == 'ACTIVE' ? 'KHÓA' : 'MỞ KHÓA'))
                                        :
                                        ""
                                }
                            </Validation.components.Button>

                        </RBT.Modal.Footer>
                    </Validation.components.Form>
                </RBT.Modal>
            </div>
        )
    }
}
