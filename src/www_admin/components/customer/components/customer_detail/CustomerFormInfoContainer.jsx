import React from 'react'
import {connect} from 'react-redux'
import * as RBT from 'react-bootstrap'
import Link from '../../../../../www_components/Common/Link';
import {updateCustomer} from '../../actions'
import {PERMISSION_CUSTOMER_EDIT} from '../../constants'
import InfoForm from './CustomerFormInfo'
import moment from 'moment'
moment.locale('vi');

class CustomerFormInfoContainer extends React.Component {

    constructor(props) {
        super(props)
    }

    onSubmit(customer) {
        let obj = Object.assign({}, customer);
        if (obj.birthday) {
            let birthday = new Date(obj.birthday);
            birthday.setDate(customer['date_birthday']);
            birthday.setMonth(customer['month_birthday'] - 1);
            birthday.setFullYear(customer['year_birthday']);
            obj['birthday'] = moment(birthday.toISOString()).format("Y-M-D");
        } else {
            if(customer['date_birthday'] || customer['year_birthday'] ) {
                let birthday = new Date();
                birthday.setDate(customer['date_birthday']);
                birthday.setMonth(customer['month_birthday'] - 1);
                birthday.setFullYear(customer['year_birthday']);
                obj['birthday'] = moment(birthday.toISOString()).format("Y-M-D");
            } else {
                obj['birthday'] = null;
            }
        }
        delete obj['date_birthday'];
        delete obj['month_birthday'];
        delete obj['year_birthday'];
        this.props.updateCustomer(obj);
    }
    render() {
        return(
            <InfoForm
                {...this.props}
                onSubmit={::this.onSubmit}
                initialValues={this.props.customer}
            />
        )
    }
}
const mapStateToProps = (state) => ({

});

export default connect(
    mapStateToProps, {updateCustomer}
)(CustomerFormInfoContainer);
