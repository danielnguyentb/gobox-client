import React, {Component, PropTypes} from 'react'
import { Control, Form, actions} from 'react-redux-form';
import * as RBT from 'react-bootstrap'
import {PERMISSION_CUSTOMER_EDIT} from '../../constants'
import {Field, reduxForm} from 'redux-form'
import {renderField, renderFieldSelect} from '../../../../helpers/redux-form'
import {validate} from '../../helper'

const CustomerFormInfo = ({handleSubmit, submitting, pristine, isAllow}) => (
    <form onSubmit={handleSubmit}>
        <table className="table tbl-info-detail tbl-customer-detail">
            <thead>
            <tr>
                <th colSpan="4">THÔNG TIN CÁ NHÂN</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td className="fw-600">Họ tên</td>
                <td>
                    <Field component={renderField} name="lastName" className="form-control" placeholder="Họ" />
                </td>
                <td colSpan="2">
                    <Field component={renderField} name="firstName" className="form-control" placeholder="tên" />
                </td>
            </tr>
            <tr>
                <td className="fw-600">Ngày sinh</td>
                <td>
                    <Field component={renderField} name="date_birthday" className="form-control" placeholder="Ngày"/>
                </td>
                <td>
                    <Field component={renderFieldSelect} name="month_birthday" className="form-control"
                           options={[
                               {value: "", label: "Tháng"},
                               {value: 1, label: "Tháng 1"},
                               {value: 2, label: "Tháng 2"},
                               {value: 3, label: "Tháng 3"},
                               {value: 4, label: "Tháng 4"},
                               {value: 5, label: "Tháng 5"},
                               {value: 6, label: "Tháng 6"},
                               {value: 7, label: "Tháng 7"},
                               {value: 8, label: "Tháng 8"},
                               {value: 9, label: "Tháng 9"},
                               {value: 10, label: "Tháng 10"},
                               {value: 11, label: "Tháng 11"},
                               {value: 12, label: "Tháng 12"},
                           ]}
                    />
                </td>
                <td>
                    <Field component={renderField} name="year_birthday" className="form-control" placeholder="Năm"/>
                </td>
            </tr>
            <tr>
                <td className="fw-600">Giới tính</td>
                <td>
                    <Field component="select" name="gender" className="form-control">
                        <option value="1">Nam</option>
                        <option value="0">Nữ</option>
                        <option value="">Khác</option>
                    </Field>
                </td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td className="fw-600">Số ĐT</td>
                <td colSpan="3">
                    <Field component={renderField} name="mobile" className="form-control" />
                </td>
            </tr>
            {/*<tr>*/}
                {/*<td className="fw-600">Địa chỉ</td>*/}
                {/*<td colSpan="3">*/}
                    {/*<Control.text model=".address" className="form-control" placeholder="Số nhà" name=''/>*/}
                {/*</td>*/}
            {/*</tr>*/}
            {/*<tr>*/}
                {/*<td>&nbsp;</td>*/}
                {/*<td colSpan="3">*/}
                    {/*<RBT.FormControl componentClass="select" placeholder="select" className="style-select-50 pull-left mr-5">*/}
                        {/*<option value="select">Tỉnh/TP</option>*/}
                        {/*<option value="">Hà Nội</option>*/}
                        {/*<option value="">HCM</option>*/}
                    {/*</RBT.FormControl>*/}
                    {/*<RBT.FormControl componentClass="select" placeholder="select" className="style-select-50 pull-left">*/}
                        {/*<option value="select">Quận/Huyện</option>*/}
                    {/*</RBT.FormControl>*/}
                {/*</td>*/}
            {/*</tr>*/}
            <tr>
                <td colSpan="4" className="text-right">
                    {isAllow(PERMISSION_CUSTOMER_EDIT) &&
                    <button type="submit" className="btn btn-primary"
                            disabled={pristine || submitting}>
                        THAY ĐỔI
                    </button>
                    }
                </td>

            </tr>
            </tbody>
        </table>
    </form>
);

export default reduxForm({
    form: 'CustomerFormInfo',
    validate
})(CustomerFormInfo);