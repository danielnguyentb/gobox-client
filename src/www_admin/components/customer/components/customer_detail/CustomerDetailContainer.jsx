import React from 'react';
import CustomerDetail from './CustomerDetail'
import DocumentTitle from '../../../../../www_components/Common/DocumentTitle'
import {connect} from 'react-redux'
import {getCustomerById, updateCustomer, changeCustomerPassword} from '../../actions'
import {CustomerByIdSelector} from '../../selectors'
import {isAllow} from '../../../../reducer'
import {toastr} from '../../../../../www_components/Toastr2'

class CustomerDetailContainer extends React.Component {
    componentWillMount(){
        let customerId = this.props.params.customerId;
        if(customerId > 0) {
            this.props.getCustomerById(customerId);
        }
    }

    handleUpdateCustomer(customer) {
        this.props.updateCustomer(customer);
    }
    componentWillReceiveProps(nextProps) {
        if(nextProps.changedPassword && nextProps.changedPasswordMessage) {
            toastr.success(nextProps.changedPasswordMessage);
        }
    }

    changeStaffPassword(customer) {
        this.props.changeCustomerPassword(this.clearId(customer), this.props.params.customerId);
    }

    clearId(customer) {
        let obj = Object.assign({}, customer);
        delete obj['id'];
        return obj;
    }

    render () {
        return (
            <DocumentTitle title="Chi tiết khách hàng">
                <CustomerDetail {...this.props}
                                updateCustomer={this.handleUpdateCustomer.bind(this)}
                                changeStaffPassword={this.changeStaffPassword.bind(this)}
                />
            </DocumentTitle>
        );
    }
}
const mapStateToProps = (state, props) => {
    const getCustomer = CustomerByIdSelector(state);
    return {
        customer: getCustomer(props.params.customerId),
        updatedCustomer: state.customer.updatedCustomer,
        error: state.customer.error,
        isAllow: (resource) => props.isAllow(resource),
        updatingCustomer: state.customer.updatingCustomer,
        changedPassword: state.customer.changedPassword,
        changedPasswordMessage: state.customer.changedPasswordMessage,
    }
};


export default connect(
    mapStateToProps, { getCustomerById, updateCustomer, changeCustomerPassword}
)(CustomerDetailContainer);