import React from 'react'
import {connect} from 'react-redux'
import * as RBT from 'react-bootstrap'
import {
    PERMISSION_CUSTOMER_EDIT,
} from '../../constants'
import {ErrorAlert} from '../../../../../www_components/Alert'
import {CUSTOMER_UPDATE_FAILURE} from '../../ActionTypes'

class ModalChangeEmail extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            email: null,
            showModal: false,
            errEmail: false,
            disabledEmail: true
        }
    }
    componentWillReceiveProps(nextProps) {
        if(JSON.stringify(nextProps.updatedCustomer) != JSON.stringify(this.props.updatedCustomer)) {
            this.closeModal();
        }
    }
    showModal() {
        this.setState({showModal: true});
    }
    closeModal() {
        this.setState({showModal: false});
    }
    handleVerifyEmail(val) {
        if(val.target.value != '') {
            if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(val.target.value)) {
                this.setState({errEmail: false});
                this.setState({disabledEmail: false});
            } else {
                this.setState({errEmail: val.target.value + " không phải là định dạng email"});
            }
        }
    }
    handleDisabledEmailInp(val) {
        this.setState({email: val.target.value});
        if(val.target.value != '') {
            if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(val.target.value)) {
                this.setState({disabledEmail: false});
            } else {
                this.setState({disabledEmail: true});
            }
        }
    }
    onSubmit(e) {
        let {updateCustomer, customer} = this.props;
        e.preventDefault();
        updateCustomer({ id: customer.id, email: this.state.email });
        this.setState({disabledEmail: true});
    }
    render() {
        let {customer, isAllow } = this.props;
        if(!customer) return null;
        return (
            <tr>
                <td className="fw-600">Email</td>
                <td>{ customer.email } </td>
                <td>
                    {isAllow(PERMISSION_CUSTOMER_EDIT) &&
                    <RBT.Button bsStyle="link" onClick={this.showModal.bind(this)}>Thay
                        đổi</RBT.Button>
                    }
                </td>

                <RBT.Modal show={this.state.showModal} onHide={this.closeModal.bind(this)}>

                        <RBT.Modal.Header closeButton>
                            <RBT.Modal.Title>Thay đổi email</RBT.Modal.Title>
                        </RBT.Modal.Header>
                        <RBT.Modal.Body>

                            <RBT.FormGroup controlId=''>
                                <RBT.ControlLabel>Email mới</RBT.ControlLabel>
                                <div className="input-required">
                                    <input autoFocus={true}
                                           onChange={(e) => {this.handleDisabledEmailInp(e)}}
                                           onBlur={e => this.handleVerifyEmail(e)}
                                           className="form-control" name='email'/>
                                    <span className="required-symbol">*</span>
                                </div>
                            </RBT.FormGroup>

                            {this.state.errEmail &&
                                <div className="alert alert-danger">
                                    {this.state.errEmail}
                                </div>
                            }
                            <ErrorAlert dispatchTypes={[CUSTOMER_UPDATE_FAILURE]}/>
                        </RBT.Modal.Body>
                        <RBT.Modal.Footer>
                            <RBT.Button onClick={this.closeModal.bind(this)}>ĐÓNG</RBT.Button>
                            <RBT.Button
                                disabled={this.state.disabledEmail}
                                className="btn btn-primary"
                                onClick={(e) => {this.onSubmit(e)}}>
                                LƯU
                            </RBT.Button>
                        </RBT.Modal.Footer>
                </RBT.Modal>
            </tr>

        )
    }

}

const mapStateToProps = (state, props) => {
    return {

    }
};

export default connect(
    mapStateToProps, { }
)(ModalChangeEmail);