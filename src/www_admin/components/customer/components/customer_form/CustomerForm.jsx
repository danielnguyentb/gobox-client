import React from 'react'
import {Field, reduxForm} from 'redux-form'
import {renderField} from '../../../../helpers/redux-form'
import DocumentTitle from './../../../../../www_components/DocumentTitle'
import {UsernameValidator} from '../../../../helpers/UsernameValidator'

export const CustomerForm = (props) => {
    const {onSubmit} = props;
    return (
        <div>
            <DocumentTitle title="Tạo khách hàng"/>
            <section className="content-header">
                <div className="row">
                    <div className="col-md-12 col-xs-12">
                        <h2 className="mt-0 mb-0">Tạo khách hàng</h2>
                    </div>
                </div>
            </section>
            <section className="content">
                <div className="box">
                    <div className="row">
                        <div className="col-md-offset-4 col-md-4 col-xs-12">
                            <div className="row mb-20 text-center">
                                <h2>Thông tin khách hàng</h2>
                            </div>
                            <ReduxForm onSubmit={onSubmit}/>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    )
};

export const Form = ({handleSubmit, error, pristine, submitting}) => {
    return (
        <form>
            <div className="col-md-12 col-xs-12">
                {error && <div className="row">
                    <div className="col-sm-12">
                        <div className="alert alert-danger">{error}</div>
                    </div>
                </div>}
                <div className="row">
                    <div className="input-required">
                        <Field name="username" component={renderField} type="text" className="form-control"
                               placeholder="Tên đăng nhập"/>
                        <span className="required-symbol">*</span>
                    </div>
                </div>
                <div className="row">
                    <div className="input-required">
                        <Field name="email" component={renderField} type="email" className="form-control"
                               placeholder="Email"/>
                        <span className="required-symbol">*</span>
                    </div>
                </div>
                <div className="row">
                    <div className="input-required">
                        <Field name="password" component={renderField} type="password" className="form-control"
                               placeholder="Mật khẩu"/>
                        <span className="required-symbol">*</span>
                    </div>
                </div>
                <div className="row">
                    <Field name="fullName" component={renderField} type="text" className="form-control"
                           placeholder="Họ tên"/>
                </div>
                <div className="row">
                    <Field name="mobile" component={renderField} type="tel" className="form-control"
                           placeholder="Số điện thoại"/>
                </div>
                <div className="row text-right">
                    <button
                        disabled={pristine || submitting}
                        onClick={handleSubmit}
                        className="btn btn-primary" type="submit">
                        TẠO KHÁCH HÀNG
                    </button>
                </div>
            </div>
        </form>
    )
};

export const validate = values => {
    const errors = {};
    if(Object.keys(values).length === 0) return errors;
    if (!values.email) {
        errors.email = 'Email không được để trống'
    } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
        errors.email = 'Email không hợp lệ'
    }
    if (!values.password) {
        errors.password = 'Mật khẩu không được để trống'
    }
    if(!values.username){
        errors.username = 'Tên đăng nhập không được để trống'
    } else if(values.username.length < 3 && values.username.length > 16){
        errors.username = 'Tên đăng nhập cần 3 đến 16 kí tự'
    } else if (!UsernameValidator.validate(values.username).isValid) {
        errors.username = UsernameValidator.validate(values.username).error
    }
    return errors;
};

export const NAME = 'customer_form';

export const ReduxForm = reduxForm({
    form: NAME,
    validate
})(Form);

