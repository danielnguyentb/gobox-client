import React from 'react'
import {CustomerForm, NAME} from './CustomerForm'
import {createCustomer} from '../../actions'
import {customerSelector, CustomerByIdSelector} from '../../selectors'
import {connect} from 'react-redux'
import {push} from 'react-router-redux'
import {isEqual} from 'lodash'

class CustomerFormContainer extends React.Component {
    handleSubmit(data) {
        const {createCustomer, dispatch} = this.props;
        const {username, password, email, mobile, fullName, gender = 1} = data;
        createCustomer({username, password, email, mobile, fullName, gender});
    }

    componentDidUpdate(prevProps) {
        const {customerIdCreated, dispatch} = this.props;
        if (customerIdCreated > 0) {
            dispatch(push(`/customers/detail/${customerIdCreated}`));
        }
    }

    render() {
        return (
            <CustomerForm
                onSubmit={::this.handleSubmit}
            />
        )
    }
}

const mapStateToProps = state => {
    const getCustomer = CustomerByIdSelector(state);
    const customerIdCreated = customerSelector(state).customerIdCreated;
    return {
        customerIdCreated,
        createdCustomer: getCustomer(customerIdCreated)
    };
};
export default connect(mapStateToProps, {createCustomer})(CustomerFormContainer);

