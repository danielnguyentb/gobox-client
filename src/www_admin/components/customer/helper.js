/**
 * Created by Tester-Ali on 14-10-2016.
 */
import {encode} from '../../helpers'
import {isNumber} from '../../helpers/validators'

export const generateQuery = (params) => {
    let where = {};
    let {identity, state, mobile, order = 'registeredDate DESC', limit = 20, skip = 0, from, to, operator, object} = params;
    let or_where = [];
    if (identity) {
        let t = encode(`%${identity}%`);
        or_where.push({'username': {like: t}});
        or_where.push({email: {like: t}})
    }
    where.and = [{or: or_where}];
    if (mobile) where.and.push({mobile: {like: encode(mobile)}});
    if (state) where.and.push({state});
    if (operator && object && from !== '') {
        switch (operator) {
            case 'between':
                where.and.push({
                    [object]: {[operator]: [from, to]}
                });
                break;
            case 'eq':
                where.and.push({
                    [object]: from
                });
                break;
            default:
                where.and.push({
                    [object]: {[operator]: from}
                });
                break;
        }
    }
    return {where, limit, skip, order};
};

export const validate = values => {
    const errors = {};
    if(Object.keys(values).length === 0) return errors;
    if (values.date_birthday || values.month_birthday || values.year_birthday) {
        if(!values.date_birthday) {
            errors.date_birthday = 'Bạn chưa nhập ngày sinh.'
        } else if (!isNumber(values.date_birthday)) {
            errors.date_birthday = 'Ngày sinh phải là chữ số'
        }
        if(!values.month_birthday || values.month_birthday == "") {
            errors.month_birthday = 'Bạn chưa chọn tháng sinh.'
        }
        if(!values.year_birthday) {
            errors.year_birthday = 'Bạn chưa nhập năm sinh.'
        } else if (!isNumber(values.year_birthday)) {
            errors.year_birthday = 'Năm sinh phải là chữ số'
        }
    }
    if (values.year_birthday) {
        let date = new Date();
        if(values.year_birthday >= date.getFullYear() || values.year_birthday <= 0) {
            errors.year_birthday = 'Năm sinh không hợp lệ'
        }
    }
    if(values.month_birthday) {
        if(values.month_birthday <= 0) {
            errors.month_birthday = 'Tháng sinh không hợp lệ'
        }
    }
    if(values.date_birthday) {
        if(values.date_birthday <= 0 || values.date_birthday > 31) {
            errors.date_birthday = 'Ngày sinh không hợp lệ'
        }
    }
    if(values.mobile) {
        if(!isNumber(values.mobile)) {
            errors.mobile = 'Số diện thoại phải là chữ số'
        }
        if((values.mobile.length < 6) || (values.mobile.length > 12) ) {
            errors.mobile = 'Không phải là định dạng số điện thoại.'
        }
    }
    return errors
};