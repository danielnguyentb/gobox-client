import {
    CUSTOMER_SUCCESS, CUSTOMER_UPDATE_SUCCESS, CUSTOMER_CREATE_SUCCESS,
    CUSTOMERS_SUCCESS, CHANGE_PASSWORD_SUCCESS, CUSTOMER_CREATE_FAILURE, CUSTOMER_LIST_EXPORT_CSV
} from './ActionTypes'
import {combineReducers} from 'redux'
import Localize from '../../../resources/Localize'
import {LOCATION_CHANGE} from 'react-router-redux'
import moment from 'moment'
moment.locale('vi');
import { SubmissionError } from 'redux-form'

let isLoad = false;

function customer(customer, action) {
    let {birthday} = customer;
    if (birthday) {
        birthday = new Date(birthday);
        customer['date_birthday'] = moment(birthday).format("D");
        customer['month_birthday'] = moment(birthday).format("M");
        customer['year_birthday'] = moment(birthday).format("Y");
    }
    return customer;
}

const byId = (state = {}, action) => {
    let {response} = action;
    let copy = {...state};
    switch (action.type) {
        case CUSTOMERS_SUCCESS:
            let t = action.response.result.reduce((obj, value) => {
                value['registeredDateLocale'] = moment(value['registeredDate']).format('L');
                obj[value.id] = customer(value, action);
                return obj;
            }, {});
            return { ...state, ...t};
        case CUSTOMER_SUCCESS:
            copy[response.id] = customer(response);
            return copy;
        case CUSTOMER_CREATE_SUCCESS:
            copy[response.id] = customer(response);
            return copy;
        case CUSTOMER_UPDATE_SUCCESS:
            copy[response.id] = {
                ...state[response.id],
                ...customer(response, action),
            };
            return copy;
        case CUSTOMER_LIST_EXPORT_CSV.SUCCESS:
            if (!isLoad) {
                isLoad = true;
                setTimeout(()=> {
                    var csvContent = "data:text/csv,\ufeff" + response.customers;
                    var encodedUri = encodeURI(csvContent);
                    //window.open(encodedUri);
                    var link = document.createElement("a");
                    link.setAttribute("href", encodedUri);
                    link.setAttribute("download", "danhsachkhach" + moment().format('YMDHMSS') + ".csv");
                    document.body.appendChild(link); // Required for FF
                    link.click();

                    isLoad = false;
                }, 1000);
            }
    }
    return state;
};

const customerIdCreated = (state = null, action) => {
    const {response} = action;
    switch (action.type) {
        case CUSTOMER_CREATE_SUCCESS:
            return response.id;
    }
    return null;
};

const customerError = (state = null, action) => {
    const {errors} = action;
    switch (action.type){
        case CUSTOMER_CREATE_FAILURE:
            if(!errors.details) return state;
            const copy = {};
            const {messages} = errors.details;
            Object.keys(messages).map(field => {
                copy[field] = messages[field][0];
            });
            return new SubmissionError(copy);
    }
    return state;
};


const metadata = (state = {}, action) => {
    switch (action.type) {
        case CUSTOMERS_SUCCESS:
            let metadata = action.response.metadata;
            return {
                ...state, ...metadata,
            };
    }
    return state;
};

const paginateIds = (state = [], action) => {
    switch (action.type) {
        case CUSTOMERS_SUCCESS:
            return action.response.result.map(t => t.id);
    }
    return state;
};

const visibleIds = (state = [], action) => {
    switch (action.type) {
        case CUSTOMERS_SUCCESS:
            return action.response.result.map(t => t.id);
        case CUSTOMER_SUCCESS:
        case CUSTOMER_UPDATE_SUCCESS:
            if (state.includes(action.response.id))
                return [...state, action.response.id];
            break;
    }
    return state;
};
export const changedPasswordMessage = (state = [], action) => {
    switch (action.type) {
        case CHANGE_PASSWORD_SUCCESS:
            return action.response.message;
        case LOCATION_CHANGE:
            return false;
    }
    return state;
};

const updatedCustomer = (state = false, action) => {
    switch (action.type) {
        case CUSTOMER_UPDATE_SUCCESS:
            return action.response;
        case LOCATION_CHANGE:
            return false;
    }
    return state;
};

export default combineReducers({
    byId, visibleIds, metadata, paginateIds, customerIdCreated, customerError, changedPasswordMessage, updatedCustomer,
    updatingCustomer: (state = false, {updatingCustomer}) => typeof updatingCustomer === 'boolean' ? updatingCustomer : state,
    error: (state = null, {error}) => error ? Localize.t(error) : null,
    changedPassword: (state = false, action) => action.type == CHANGE_PASSWORD_SUCCESS,
    // loggedOutMessage: (state, action) => action.type == USER_LOGOUT_ALL_SUCCESS ? action.response.message : null,
})