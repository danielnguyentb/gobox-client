import React from 'react'
import {statusLabel} from '../../constants'
import moment from 'moment'
moment.locale('vi');

export default ({id, addressFull , customer, orderIds, status, tasks, createdTime, warehouseLabel, warehouseId, totalTaskSuccess, overLook}) => (
    <tr>
        <td width={100}>
            <a className="fw-600 fs-15" target="_blank" href={`#/hubs/${warehouseId}/transport_request?id=${id}`}>{id}</a>
        </td>
        <td>
            @{customer.username} <br/>
            {customer.fullName}
        </td>
        <td>
                <span className={`lbl-style label ${statusLabel[status].className}`}>
                        {statusLabel[status].title}
                </span>
        </td>
        <td className="hidden-xs" width={100}>{warehouseLabel}</td>
        <td className="hidden-xs" width={180}>{customer.mobile}</td>
        <td className="hidden-xs">{addressFull}</td>
        <td className="hidden-xs">{orderIds.length}</td>
        <td><span className="pull-left"><i className="fa fa-check-square-o mr-5"/>{totalTaskSuccess}/{tasks.length}</span>
        </td>
        <td className= "hidden-xs text-center">
                { overLook ? <i className="ti-package"/> : ''}
        </td>
        <td><span className="request-time">{moment(createdTime).format("DD/MM/YYYY")}</span></td>
    </tr>
);