import React from 'react'
import {push} from 'react-router-redux'
import qs from 'qs';
import {connect} from 'react-redux'
import DocumentTitle from '../../../../../www_components/DocumentTitle'
import './../../../order/components/order.scss';
import './../request_board/logistics.scss';
import RequestList from './RequestList'
import {getTransportRequests} from '../../actions'
import ReactTooltip from 'react-tooltip'
import {getPaginateRequestOrderView} from '../../selectors'
import {hubsSelector} from '../../../hub/selectors'

class RequestListContainer extends React.Component {
    componentWillMount() {
        let {location, getTransportRequests} = this.props;
        getTransportRequests(location.query);
    }
    search(data){
        let params = {
            ...this.props.location.query,
            ...data,
        };
        this.props.dispatch(push(`/request_list_order?${qs.stringify(params)}`));
    }

    componentDidUpdate(prevProps) {
        const {location, getTransportRequests} = this.props;
        if (location.search != prevProps.location.search) {
            getTransportRequests(location.query);
        }
        ReactTooltip.rebuild();
    }

    render() {
        let {metadata, location} = this.props;
        let params = {...metadata, ...location.query};
        return (
            <DocumentTitle title="Danh sách yêu cầu lấy hàng">
                <RequestList {...this.props}
                    onSearch={::this.search} query={params}
                />
            </DocumentTitle>
        )
    }
}

const mapStateToProps = (state, props) => ({
    requestsOrder: getPaginateRequestOrderView(state),
    hubs: hubsSelector(state)
});

export default connect(
    mapStateToProps, {getTransportRequests}
)(RequestListContainer);
