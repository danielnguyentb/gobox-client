import React from 'react'
import Select from 'react-select'
import ReactTooltip from 'react-tooltip'
import SearchContainer from './search/SearchContainer'
import RequestListItem from './RequestListItem'
import RePagination from '../../../../../www_components/RePagination'
import './../transportrequest.scss'

export default (props) => {
    let {onSearch, query, requestsOrder, hubs} = props;

    return(
    <div>
        <ReactTooltip/>
        <section className="content-header">
            <div className="row">
                <div className="col-md-12 col-xs-12">
                    <h2 className="mt-0">Danh sách yêu cầu lấy hàng</h2>
                </div>
            </div>
        </section>
        <section className="content">
            <div className="box">
                <div className="row">
                    <table className="table tbl-headblue table-striped table-hover tbl-orders">
                        <thead>
                        <tr>
                            <th width={100}>ID</th>
                            <th>Khách hàng</th>
                            <th width={130}>Trạng thái</th>
                            <th className="hidden-xs" with={100}>Hub</th>
                            <th className="hidden-xs" width={180}>SĐT</th>
                            <th className="hidden-xs">Địa chỉ</th>
                            <th className="hidden-xs">Đơn</th>
                            <th width={100}>Nhiệm vụ</th>
                            <th className="hidden-xs text-center">Lưu trữ ?</th>
                            <th>Ngày yêu cầu</th>
                        </tr>
                        </thead>
                        <tbody>

                        <SearchContainer onSearch={onSearch} query={query} hubs={hubs}/>

                        {(requestsOrder && requestsOrder.length > 0 ) ?
                            requestsOrder.map(request => <RequestListItem key={request.id} {...request} />)
                        :
                            <tr>
                                <td colSpan="100%">
                                    <h4 className="text-center">Không có yêu cầu lấy nào thỏa mãn điều kiện tìm kiếm!</h4>
                                </td>
                            </tr>
                        }

                        </tbody>
                    </table>
                </div>
            </div>

            <div className="row">
                <div className="col-md-12 text-right">
                    <RePagination {...props.location}/>
                </div>
            </div>
        </section>
    </div>
)}