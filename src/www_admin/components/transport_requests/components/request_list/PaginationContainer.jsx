import React from 'react'
import {push} from 'react-router-redux'
import {connect} from 'react-redux'
import qs from 'qs'
import Pagination from '../../../../../www_components/Pagination'

class PaginationContainer extends React.Component {
    onSubmit(data) {
        let selected = data.selected;
        let limit = 20;
        let skip = Math.ceil(selected * limit);
        let params = {
            ...this.props.location.query,
            skip, limit,
        };
        this.props.dispatch(push(`/request_list_order?${qs.stringify(params)}`));
    }

    render() {
        let {currentPage, pageCount} = this.props.metadata;
        return (
            <div className="row">
                <div className="col-md-12 text-right">
                    {pageCount > 1 &&
                    <Pagination
                        onSubmit={::this.onSubmit} currentPage={currentPage} pageCount={pageCount}/>
                    }
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    let {metadata} = state.order;
    return {
        metadata
    }
};

export default connect(mapStateToProps)(PaginationContainer)