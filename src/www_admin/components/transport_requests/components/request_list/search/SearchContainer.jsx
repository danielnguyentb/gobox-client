import React, {PropTypes} from 'react'
import SearchTable from './SearchTable'
import {connect} from 'react-redux'

class SearchContainer extends React.Component {

    onBlur = (name, callback) => event => callback({[name]: event.target.value});

    onEnter = (name, callback) => event => {
        event.preventDefault();
        if (event.key == 'Enter') {
            callback({[name]: event.target.value});
        }
    };

    onSearch(data) {
        this.props.onSearch({...data, skip: 0})
    }

    render() {
        let onSearch = ::this.onSearch;
        let {query, hubs} = this.props;
        return (
            <SearchTable
                onBlur={n => this.onBlur(n, onSearch)}
                onEnter={n => this.onEnter(n, onSearch)}
                onSelect={onSearch}
                query={query}
                hubs={hubs}
            />
        );
    }
}

SearchContainer.propTypes = {
    onSearch: PropTypes.func.isRequired,
    query: PropTypes.object.isRequired,
};
const mapStateToProps = (state) => ({

});
export default connect(mapStateToProps)(SearchContainer);