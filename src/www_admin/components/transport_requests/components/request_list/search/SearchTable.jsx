import React, {PropTypes} from 'react'
import Select from 'react-select'
import 'react-select/dist/react-select.css'
import {statusLabel} from '../../../constants'

let optionsStatus = [];
Object.keys(statusLabel).map(key => optionsStatus.push({value: key, label: statusLabel[key].title }));

const SearchTable = ({
    query: {warehouseId, status, id, addressFull, mobile, fullName},
    onBlur, onEnter, onSelect, hubs
}) => {
    let optionsHub = [];
    if(hubs) hubs.map(hub => optionsHub.push({value: hub.id, label: hub.code}));
    return (
        <tr className="hidden-xs">
            <td>
                <div className="form-group has-feedback order-indentity">
                    <input
                        defaultValue={id}
                        onBlur={onBlur('id')}
                        onKeyUp={onEnter('id')}
                        className="form-control" type="text" placeholder="ID"/>
                    <span className="ti-search form-control-feedback text-hint"></span>
                </div>
            </td>
            <td className="hidden-xs">
                <div className="form-group">
                    <input
                        defaultValue={fullName}
                        onBlur={onBlur('fullName')}
                        onKeyUp={onEnter('fullName')}
                        className="form-control" type="text" placeholder="Tên"/>
                </div>
            </td>
            <td width={130}>
                <Select
                    placeholder="Trạng thái"
                    value={status ? status : ''}
                    options={optionsStatus}
                    onChange={t => onSelect({status: t ? t.value : ''})}
                />
            </td>
            <td className="hidden-xs" with={100}>
                <Select
                    placeholder="Chọn Hub"
                    value={parseInt(warehouseId)}
                    options={optionsHub}
                    onChange={t => onSelect({warehouseId: t ? t.value : ''})}
                />
            </td>
            <td className="hidden-xs">
                <div className="form-group">
                    <input
                        defaultValue={mobile}
                        onBlur={onBlur('mobile')}
                        onKeyUp={onEnter('mobile')}
                        className="form-control" type="text" placeholder="Số điện thoại"/>
                </div>
            </td>
            <td className="hidden-xs">
                <div className="form-group">
                    <input
                        defaultValue={addressFull}
                        onBlur={onBlur('addressFull')}
                        onKeyUp={onEnter('addressFull')}
                        className="form-control" type="text" placeholder="Địa chỉ lấy hàng"/>
                </div>
            </td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>

        </tr>
)};

SearchTable.propTypes = {
    onBlur: PropTypes.func.isRequired,
    onEnter: PropTypes.func.isRequired,
    onSelect: PropTypes.func.isRequired,
    query: PropTypes.object.isRequired,
};

export default SearchTable;