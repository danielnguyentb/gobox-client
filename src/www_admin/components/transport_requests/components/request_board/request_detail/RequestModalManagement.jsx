import React from 'react'
import * as RBT from 'react-bootstrap'
import {connect} from 'react-redux'
import {push} from 'react-router-redux'
import RequestDetailContainer from './RequestDetailContainer'

class RequestModalManagement extends React.Component {
    constructor(props) {
        super(props);
        this.state = {showModal: props.requestId > 0}
    }

    componentWillReceiveProps({requestId}){
        if(requestId > 0){
            this.setState({showModal: true})
        }
    }

    closeModal() {
        this.setState({showModal: false});
        this.props.dispatch(push(`/hubs/${this.props.hubId}/transport_request`));
    }

    render() {
        let {showModal} = this.state;
        const {requestId, hubId, isAllow} = this.props;
        if (!showModal) return null;
        return (
            <RBT.Modal className="modal-detail-request" show={showModal} onHide={::this.closeModal} bsSize="large">
                <RequestDetailContainer
                    {...this.props}
                    isAllow={isAllow}
                    requestId={requestId}
                    hubId={hubId}
                    onHide={::this.closeModal}/>
            </RBT.Modal>
        )
    }
}

const mapStateToProps = (state, props) => ({
    requestId: props.location.query.id,
    hubId: props.params.hubId,
});

export default connect(
    mapStateToProps
)(RequestModalManagement);