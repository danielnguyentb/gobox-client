import React from 'react'
import {connect} from 'react-redux'
import RequestDetail from './RequestDetail'
import {RequestViewByIdSelector, requestSelector} from '../../../selectors'
import {getTransportRequestDetail, changeStatus} from '../../../actions'
import {Status as requestStatus} from '../../../constants'
import {toastr} from '../../../../../../www_components/Toastr2'

class TransportModalDetailContainer extends React.Component {
    componentWillMount() {
        let {requestId, getTransportRequestDetail, transportRequest} = this.props;
        if (!transportRequest || !transportRequest.isFull) {
            getTransportRequestDetail(requestId);
        }
    }

    onSubmit(data) {
        const {status} = data;
        const {requestId} = this.props;
        if (status) {
            const {changeStatus} = this.props;
            if (requestStatus.CANCELLED == status) {
                toastr.confirm('Bạn có chắc muốn hủy vận đơn này không?', {
                    onOk: () => {
                        changeStatus(requestId, status);
                    }
                })
            } else {
                changeStatus(requestId, status);
            }
        } else {
            //save something else
        }
    }

    render() {
        const {transportRequest, hubId, ajax, changingStatus, isAllow} = this.props;
        return (
            <RequestDetail
                hubId={hubId}
                ajax={ajax}
                isAllow={isAllow}
                changingStatus={changingStatus}
                transportRequest={transportRequest || {}}
                onSubmit={::this.onSubmit}
            />
        );
    }
}

const mapStateToProps = (state, props) => {
    const getRequest = RequestViewByIdSelector(state);
    return {
        ajax: state.ajax,
        transportRequest: getRequest(props.requestId),
        changingStatus: requestSelector(state).changingStatus
    }
};

export default connect(mapStateToProps, {getTransportRequestDetail, changeStatus})(TransportModalDetailContainer);