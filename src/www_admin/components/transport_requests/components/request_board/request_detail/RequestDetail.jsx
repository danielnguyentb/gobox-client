import React from 'react'
import * as RBT from 'react-bootstrap'
import Alert from '../../../../../../www_components/Alert'
import {formatTime} from '../../../../../helpers/format'
import OrderTableContainer from '../../../../order/components/widgets/OrderTableContainer'
import {TasksTableContainer} from '../../../../tasks/components/tasks_table/TasksTableContainer'
import {RequestTaskFormContainerConnected} from '../../../../tasks/components/tasks_form/RequestTaskFormContainer'
import {TRANSPORT_REQUEST_CHANGE_STATUS_SUCCESS, TRANSPORT_REQUEST_CHANGE_STATUS_FAILURE} from '../../../ActionTypes'
import {statusTitle} from '../../../constants'
import {Status as OrderStatus} from '../../../../order/constants'

const {ErrorToast, SuccessToast} = Alert.components;

let success = {
    [TRANSPORT_REQUEST_CHANGE_STATUS_SUCCESS]: 'Cập nhật thành công'
};

export const ChangeStatus = ({status, handleSubmit, disabled}) => (
    <div>
        <SuccessToast dispatchTypes={success}/>
        <ErrorToast dispatchTypes={[TRANSPORT_REQUEST_CHANGE_STATUS_FAILURE]}/>
        <select
            value={status}
            disabled={disabled}
            onChange={e => handleSubmit({status: e.target.value})}
            className="form-control">
            {Object.keys(statusTitle).map(status => {
                return <option key={status}
                               value={status}>{statusTitle[status]}</option>
            })}
        </select>
    </div>
);

export default ({transportRequest = {}, hubId, onSubmit, ajax, changingStatus, isAllow}) => (
    <div>
        <RBT.Modal.Header
            closeButton={true}>
                <span className="fs-18 fw-600 pull-left mr-30">
                    @{transportRequest.customer ? transportRequest.customer.username : ''}<br/>
                    <span
                        className="fs-14 fw-500">Ngày {transportRequest ? formatTime(transportRequest.lastStatusTime) : ''}</span>
                </span>
            <ChangeStatus
                disabled={changingStatus}
                status={transportRequest.status}
                handleSubmit={onSubmit}/>
        </RBT.Modal.Header>
        <RBT.Modal.Body>
            <p className="mb-5">{transportRequest.addressContactName} / {transportRequest.addressContactPhone}</p>
            <p><span
                className="fw-600">Địa chỉ lấy hàng</span>: {transportRequest ? transportRequest.addressFull : ''}
            </p>

            {transportRequest.id &&
            <div>
                <OrderTableContainer
                    isAllow={isAllow}
                    ignoreStatus={[OrderStatus.COMPLETED]}
                    requestId={transportRequest.id}/>

                <TasksTableContainer
                    requestId={transportRequest.id}
                />

                <table>
                    <tbody>
                    <tr>
                        <td colSpan="5" className="pt-20">
                            <div className="row">
                                {(transportRequest.status != OrderStatus.CANCELLED) &&
                                    <RequestTaskFormContainerConnected
                                        requestId={transportRequest.id}
                                        label="Người lấy *"
                                        hubId={parseInt(hubId)}
                                    />
                                }
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            }
        </RBT.Modal.Body>
    </div>
)