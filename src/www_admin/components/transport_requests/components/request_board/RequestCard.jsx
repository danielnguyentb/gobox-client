import React from 'react';
import moment from 'moment'
moment.locale('vi');

export default (props) => {
    let {
        addressContactName, addressContactPhone, id, addressFull, customer, archived,
        numberOfOrder, numberOfSuccessOrder, isAllow, onClickCard, lastStatusTime, totalTaskSuccess, totalTask
    } = props;

    return (
        <div className="logistic-items list-card" onClick={() => onClickCard(id)}>
            <div className={`list-card-details ${archived ? 'list-card-storage' : ''}`}>
                <p className="fw-600">@{customer.username}</p>
                <p>{addressContactName} / {addressContactPhone}</p>
                <p>{addressFull}</p>
                <p className="mt-15">
                    <span className="pull-left"><i
                        className="fa fa-check-square-o mr-5"/> {totalTaskSuccess}/{totalTask} </span>
                    <span className="pull-right label label-primary"><i
                        className="ti-package mr-5 fs-10"/> {numberOfOrder}</span>
                </p>
                <span className="request-time">
                    <i className="fa fa-calendar mr-5"/> {moment(lastStatusTime).format('D/M')} </span>
            </div>
        </div>
    );
}