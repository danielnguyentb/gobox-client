import React from 'react'
import {connect} from 'react-redux'
import {push} from 'react-router-redux'
import RequestBoard from './RequestBoard'
import {
    getTransportRequestsHub
} from '../../actions'
import {getPickupBelongToHub} from '../../../hub/actions'

import {
    RequestsViewNewAndInProcessByHubIdSelector,
    RequestsViewNotNewAndInProcessByHubIdSelector
} from '../../selectors';

class RequestBoardContainer extends React.Component {
    componentWillMount() {
        let hubId = this.props.hubId;
        if (hubId) {
            this.props.getTransportRequestsHub({
                warehouseId: hubId,
                type: 'PICKUP'
            });
        }
    }

    handleShowDetail(id) {
        let hubId = this.props.hubId;
        if (hubId) {
            this.props.dispatch(push(`/hubs/${hubId}/transport_request?id=${id}`));
        }
    }

    render() {
        return (
            <RequestBoard {...this.props} onClickCard={::this.handleShowDetail}/>
        )
    }
}
const mapStateToProps = (state, props) => {
    const hubId = props.params.hubId;
    const getRequestNotNewAndInProcess = RequestsViewNotNewAndInProcessByHubIdSelector(state);
    const getRequestNewAndInProcess = RequestsViewNewAndInProcessByHubIdSelector(state);
    return {
        hubId: props.params.hubId,
        transportRequests: getRequestNotNewAndInProcess(hubId),
        transportRequestsNew: getRequestNewAndInProcess(hubId),
    }
};

export default connect(
    mapStateToProps, {
        getTransportRequestsHub,
        getPickupBelongToHub
    }
)(RequestBoardContainer);