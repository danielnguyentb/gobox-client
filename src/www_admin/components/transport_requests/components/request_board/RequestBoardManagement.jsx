import React from 'react'
import './logistics.scss'
import {connect} from 'react-redux'
import DocumentTitle from '../../../../../www_components/DocumentTitle'
import HubBoardContainer from './RequestBoardContainer'
import RequestModalManagement from './request_detail/RequestModalManagement'

const RequestBoardManagement = (props) => {
    return (
        <div className="board-wrapper">
            <DocumentTitle title="Chờ lấy hàng"/>
            <div className="board-main-content">
                <div className="board-header">
                    <ol className="breadcrumb">
                        <li><span className="fs-18 fw-600"><i className="fa fa-map-marker mr-5"/> CG-HN</span></li>
                        <li><span className="fs-16">Yêu cầu lấy hàng</span></li>
                    </ol>
                </div>
                <div className="board-canvas">
                    <HubBoardContainer {...props}/>
                </div>
            </div>

            <RequestModalManagement {...props}/>
        </div>
    )
};

export default connect()(RequestBoardManagement)