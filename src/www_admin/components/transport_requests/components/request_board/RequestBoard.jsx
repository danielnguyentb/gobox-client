import React from 'react';
import RequestCard from './RequestCard'
import {statusLabel} from '../../constants'

export default ({transportRequestsNew, transportRequests, onClickCard}) => (
    <div id="board" className="pt-scrollbar">
        {Object.keys(statusLabel).map((key) =>
            <div className={`logistic-list list-wrapper ${statusLabel[key].className}`} key={key}>
                <div className="logistic-item list">
                    <div className="list-header">{statusLabel[key].title}</div>
                    <div className="list-cards pt-scrollbar">
                        {statusLabel[key].sort > 0 ?
                            <section>
                                {Object.values(transportRequestsNew).map(request => (request.status == key &&
                                    <RequestCard
                                        {...request}
                                        key={request.id}
                                        onClickCard={onClickCard}
                                    />
                                ))}
                            </section>
                            :
                            <section>
                                {Object.values(transportRequests).map(request => (request.status == key &&
                                    <RequestCard
                                        {...request}
                                        key={request.id}
                                        onClickCard={onClickCard}
                                    />
                                ))}
                            </section>
                        }
                    </div>
                </div>
            </div>
        )}
    </div>
)