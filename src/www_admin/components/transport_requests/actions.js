import TransportRequestApi from './api'
import {generateQuery, generateListTransportQuery} from './helper'


export const getTransportRequestsHub = (filter) => dispatch => dispatch(TransportRequestApi.getTransportRequestsHub(generateQuery(filter)));

export const getTransportRequests = (filter) => dispatch => dispatch(TransportRequestApi.getTransportRequestsHub(generateListTransportQuery(filter)));

export const getTransportRequestDetail = (id) => dispatch => dispatch(TransportRequestApi.getTransportRequestDetail(id));

export const getTransportRequestTasks = (id) => dispatch => dispatch(TransportRequestApi.getTransportRequestTasks(id));

export const changeStatus = (id, data) => dispatch => dispatch(TransportRequestApi.changeStatus(id, data));