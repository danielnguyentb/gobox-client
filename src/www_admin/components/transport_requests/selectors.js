/**
 * Created by Tester-Ali on 07-11-2016.
 */
import {createSelectorCreator, defaultMemoize, createSelector} from 'reselect'
import lodash from 'lodash'
import {NAME, Status} from './constants'
import {TasksViewByRequestIdSelector} from '../tasks/selectors'
import {hubSelector} from '../hub/selectors'

const createDeepEqualSelector = createSelectorCreator(
    defaultMemoize,
    lodash.isEqual
);

export const requestSelector = state => state[NAME];
export const requestsSelector = createSelector(
    requestSelector,
    request => Object.values(request.byId)
);
const hydrate = (request, getTasksByRequestId) => {
    if (!request) return null;
    let t = request;
    const tasks = getTasksByRequestId(request.id);
    t['tasks'] = tasks;
    t['totalTask'] = tasks.length;
    t['totalTaskSuccess'] = tasks.filter(t => t.status == Status.DONE).length;
    return t;
};

const hydrateRequestOrder = (request, hubs) => {
    if(!request) return null;
    let t  = request;
    if(t.warehouseId) {
        let hub = hubs.byId[t.warehouseId];
        t['warehouseLabel'] = hub ? hub.code : '';
    }
    t['totalTaskSuccess'] = t.tasks.filter(t => t.status == Status.DONE).length;
    return t;
};

export const RequestViewByIdSelector = createDeepEqualSelector(
    [requestSelector, TasksViewByRequestIdSelector],
    (request, getTasks) => lodash.memoize(
        id => hydrate(request.byId[id], getTasks)
    )
);

export const getPaginateRequestOrderView = createDeepEqualSelector(
    [requestSelector, hubSelector],
    (requestsSelector, hubs) =>  requestsSelector.paginateIds.map(id => hydrateRequestOrder(requestsSelector.byId[id], hubs))
);

export const RequestViewByHubIdSelector = createDeepEqualSelector(
    [requestsSelector, TasksViewByRequestIdSelector],
    (requests, getTasks) => lodash.memoize(
        hubId => requests.filter(t => t.warehouseId == hubId).map(t => hydrate(t, getTasks))
    )
);

export const RequestsViewNewAndInProcessByHubIdSelector = createDeepEqualSelector(
    [requestsSelector, TasksViewByRequestIdSelector],
    (requests, getTasks) => lodash.memoize(
        hubId => {
            let data = requests.filter(t => t && (t.status == Status.NEW || t.status == Status.IN_PROCESS));
            data.sort((a, b) => {
                if (a.lastStatusTime == b.lastStatusTime) return 0;
                return a.lastStatusTime < b.lastStatusTime ? -1 : 1;
            });
            return data.filter(t => t.warehouseId == hubId)
                .map(t => hydrate(t, getTasks));
        }
    )
);

export const RequestsViewNotNewAndInProcessByHubIdSelector = createDeepEqualSelector(
    [requestsSelector, TasksViewByRequestIdSelector],
    (requests, getTasks) => lodash.memoize(
        hubId => {
            let data = requests.filter(t => t && (t.status != Status.NEW || t.status != Status.IN_PROCESS));
            data.sort((a, b) => {
                if (a.lastStatusTime == b.lastStatusTime) return 0;
                return a.lastStatusTime < b.lastStatusTime ? 1 : -1;
            });
            return data.filter(t => t.warehouseId == hubId)
                .map(t => hydrate(t, getTasks));
        }
    )
);