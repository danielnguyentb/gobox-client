import {combineReducers} from 'redux'
import {
    GET_TRANSPORT_REQUEST_HUB,
    TRANSPORT_REQUEST_CHANGE_STATUS_SUCCESS,
    TRANSPORT_REQUEST_CHANGE_STATUS_REQUEST,
    TRANSPORT_REQUEST_CHANGE_STATUS_FAILURE,
    GET_TRANSPORT_REQUEST_DETAIL
} from './ActionTypes'

const byId = (state = {}, action) => {
    let {response, scope} = action;
    let copy = {...state};
    switch (action.type) {
        case GET_TRANSPORT_REQUEST_HUB.SUCCESS:
            let requests = response.result.reduce((obj, value) => {
                value['isFull'] = false;
                obj[value.id] = value;
                return obj;
            }, {});
            return requests;
        case TRANSPORT_REQUEST_CHANGE_STATUS_SUCCESS:
            copy[scope.id] = {
                ...copy[scope.id],
                status: scope.status,
            };
            return copy;
        case GET_TRANSPORT_REQUEST_DETAIL.SUCCESS:
            response['isFull'] = true;
            copy[response.id] = response;
            return copy;
    }
    return state;
};

const visibleIds = (state = [], action) => {
    let {response} = action;
    let newIds = [];
    switch (action.type) {
        case GET_TRANSPORT_REQUEST_HUB.SUCCESS:
            newIds = response.result.map(t => t.id).filter(id => !state.includes(id));
            return [...state, ...newIds];
        case GET_TRANSPORT_REQUEST_DETAIL.SUCCESS:
            if (!state.includes(response.id)) {
                return [...state, response.id];
            }
            break;
    }
    return state;
};

const changingStatus = (state = false, action) => {
    switch (action.type) {
        case TRANSPORT_REQUEST_CHANGE_STATUS_REQUEST:
            return true;
        case TRANSPORT_REQUEST_CHANGE_STATUS_SUCCESS:
        case TRANSPORT_REQUEST_CHANGE_STATUS_FAILURE:
            return false;
    }
    return state;
};

const paginateIds = (state = [], action) => {
    const {response} = action;
    if (action.type === GET_TRANSPORT_REQUEST_HUB.SUCCESS && response) {
        return response.result.map(t => t.id);
    }
    return state;
};

export default combineReducers({
    byId, visibleIds, changingStatus, paginateIds
});