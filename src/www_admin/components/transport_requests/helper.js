import {encode} from '../../helpers'

export const generateQuery = (params) => {
    let where = {};
    let {
        warehouseId, type
    } = params;

    let or_where = [];

    where.and = [{
        or: or_where,
    }];
    if (warehouseId) where.and.push({warehouseId});
    if (type) where.and.push({type});
    let include = [
        {
            "relation": "tasks",
            "scope": {"fields": {"id": true, "status": true}}
        },
        {
            "relation": "customer",
            "scope": {"fields": {"id": true, "username": true}}
        }
    ];

    return {where, include};
};

export const generateListTransportQuery = (params) => {
    let where = {};
    let {
        warehouseId, id, status, mobile, addressFull, fullName, limit = 20, skip = 0, order = 'createdTime DESC'
    } = params;

    let or_where = [];
    if(mobile) {
        or_where.push({'customer.mobile': {like: encode(mobile)}});
    }

    if(fullName) {
        or_where.push({'customer.fullName': {like: encode(fullName)}});
        or_where.push({'customer.username': {like: encode(fullName)}});
    }
    where.and = [{
        or: or_where,
    }];
    if (warehouseId) where.and.push({warehouseId});
    if(id) where.and.push({id});
    if (status) where.and.push({status});
    if(addressFull) where.and.push({'addressFull': {like: encode(addressFull)}});

    let include = [
        {
            "relation": "tasks",
            "scope": {"fields": {"id": true, "status": true}}
        },
        {
            "relation": "customer",
            "scope": {"fields": {"id": true, "username": true, "fullName" : true, "lastName": true, "firstName" : true, "mobile": true}}
        }
    ];

    return {where, limit, skip, order, include, overLook: true};
};

export const includeTaskAndOrder = {
    "include": [
        {
            "relation": "orders"
        },
        {
            "relation": "tasks"
        },
        {
            "relation": "customer",
            "scope": {"fields": {"id": true, "username": true}}
        }
    ]
};
