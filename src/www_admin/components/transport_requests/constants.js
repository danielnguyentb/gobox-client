export const NAME = 'transport_requests';

export const Status = {
    NEW: 'NEW',
    IN_PROCESS :'IN_PROCESS',
    DONE : 'DONE',
    FAILED : 'FAILED',
    CANCELLED : 'CANCELLED'
};

export const statusTitle = {
    [Status.NEW]: 'Chờ lấy',
    [Status.IN_PROCESS]: 'Đang lấy',
    [Status.DONE]: 'Thành công',
    [Status.FAILED]: 'Thất bại',
    [Status.CANCELLED]: 'Hủy',
};

export const statusLabel = {
    [Status.NEW]: {
        'title':'Chờ lấy',
        'sort': 1,
        'className' : 'logistic-wait'
    },
    [Status.IN_PROCESS]: {
        'title':'Đang lấy',
        'sort': 1,
        'className' : 'logistic-process'
    },
    [Status.DONE]: {
        'title': 'Thành công',
        'sort': -1,
        'className' : 'logistic-success'
    },
    [Status.FAILED]: {
        'title': 'Thất bại',
        'sort': -1,
        'className' : 'logistic-fail'
    },
    [Status.CANCELLED]: {
        'title': 'Hủy',
        'sort': -1,
        'className' : 'logistic-cancel'
    },
};

const PERMISSION_VIEW_ALL_REQUEST = 'PERMISSION_VIEW_ALL_REQUEST';