import Client from '../../helpers/Client'
import * as types from './ActionTypes'
import {includeTaskAndOrder} from './helper'

class TransportRequestApi extends Client {
    // Lấy tất cả những transport request theo filter
    getTransportRequestsHub(filter = '') {
        filter = typeof filter == 'string' ? filter : JSON.stringify(filter);
        return this.callApi(({
            types: [types.GET_TRANSPORT_REQUEST_HUB.REQUEST, types.GET_TRANSPORT_REQUEST_HUB.SUCCESS, types.GET_TRANSPORT_REQUEST_HUB.FAILURE],
            endpoint: `/TransportRequests?filter=${filter}`,
            method: 'GET'
        }));
    }
    // Lấy ra một transport request theo id
    getTransportRequestDetail(id) {
        let filter = JSON.stringify(includeTaskAndOrder);
        return this.callApi(({
            types: [types.GET_TRANSPORT_REQUEST_DETAIL.REQUEST, types.GET_TRANSPORT_REQUEST_DETAIL.SUCCESS, types.GET_TRANSPORT_REQUEST_DETAIL.FAILURE],
            endpoint: `/TransportRequests/${id}?filter=${filter}`,
            method: 'GET'
        }))
    }

    //Cập nhật trạng thái transport request
    changeStatus(id, status){
        return this.callApi({
            types: [types.TRANSPORT_REQUEST_CHANGE_STATUS_REQUEST, types.TRANSPORT_REQUEST_CHANGE_STATUS_SUCCESS, types.TRANSPORT_REQUEST_CHANGE_STATUS_FAILURE],
            endpoint: `/TransportRequests/${id}/changeStatus/${status}`,
            scope: {id, status},
            method: 'PUT',
        })
    }
}

export default new TransportRequestApi();