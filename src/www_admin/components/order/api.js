import Client from '../../helpers/Client'
import * as types from './ActionTypes'

class CustomerDeliveryOrders extends Client {

    create(data) {
        return this.callApi({
            types: [types.DELIVERY_ORDER_CREATE_REQUEST, types.DELIVERY_ORDER_CREATE_SUCCESS, types.DELIVERY_ORDER_CREATE_FAILURE],
            endpoint: '/CustomerDeliveryOrders',
            method: 'POST',
            body: data,
        })
    }

    update(id, data){
        return this.callApi({
            types: [types.DELIVERY_ORDER_UPDATE_REQUEST, types.DELIVERY_ORDER_UPDATE_SUCCESS, types.DELIVERY_ORDER_UPDATE_FAILURE],
            endpoint: `/CustomerDeliveryOrders/${id}`,
            method: 'PUT',
            scope: {...data, id},
            body: data,
        })
    }

    changeType(orderId, type){
        return this.callApi({
            types: [types.DELIVERY_ORDER_CHANGE_TYPE_REQUEST, types.DELIVERY_ORDER_CHANGE_TYPE_SUCCESS, types.DELIVERY_ORDER_CHANGE_TYPE_FAILURE],
            endpoint: `/CustomerDeliveryOrders/${orderId}/changeType/${type}`,
            method: 'PUT'
        })
    }

    changeFee(orderId, fee){
        return this.callApi({
            types: [types.DELIVERY_ORDER_CHANGE_FEE_REQUEST, types.DELIVERY_ORDER_CHANGE_FEE_SUCCESS, types.DELIVERY_ORDER_CHANGE_FEE_FAILURE],
            endpoint: `/CustomerDeliveryOrders/${orderId}/changeFee/${fee}`,
            method: 'PATCH'
        })
    }

    fetchLogHistory(orderId, filter){
        filter = typeof filter == 'string' ? filter : JSON.stringify(filter);
        return this.callApi({
            types: [types.DELIVERY_ORDER_LOGS_REQUEST, types.DELIVERY_ORDER_LOGS_SUCCESS, types.DELIVERY_ORDER_LOGS_FAILURE],
            endpoint: `/CustomerDeliveryOrders/${orderId}/histories?filter=${filter}`,
            method: 'GET'
        })
    }


    getOrderExportFileExcel(filter = '') {
        filter = typeof filter == 'string' ? filter : JSON.stringify(filter);
        return this.callApi({
            types: [types.DELIVERY_ORDER_EXPORT_EXCEL.REQUEST, types.DELIVERY_ORDER_EXPORT_EXCEL.SUCCESS, types.DELIVERY_ORDER_EXPORT_EXCEL.FAILURE],
            endpoint: `/CustomerDeliveryOrders/csvDeliveryOrderExport?filter=${filter}`,
            method: 'GET',
        }, 'fetchingCsvDeliveryOrderExport')
    }

    changeStatus(id, status){
        return this.callApi({
            types: [types.DELIVERY_ORDER_UPDATE_REQUEST, types.DELIVERY_ORDER_UPDATE_SUCCESS, types.DELIVERY_ORDER_UPDATE_FAILURE],
            endpoint: `/CustomerDeliveryOrders/${id}/changeStatus/${status}`,
            scope: {id, status},
            method: 'PUT'
        })
    }

    getDeliveryOrderById(id, filter) {
        filter = typeof filter == 'string' ? filter : JSON.stringify(filter);
        return this.callApi({
            types: [types.DELIVERY_ORDER_REQUEST, types.DELIVERY_ORDER_SUCCESS, types.DELIVERY_ORDER_FAILURE],
            endpoint: `/CustomerDeliveryOrders/${id}?filter=${filter}`,
            method: 'GET',
        }, 'fetchingDeliveryOrders')
    }

    getDeliveryOrders(filter = '') {
        filter = typeof filter == 'string' ? filter : JSON.stringify(filter);
        return this.callApi({
            types: [types.DELIVERY_ORDERS_REQUEST, types.DELIVERY_ORDERS_SUCCESS, types.DELIVERY_ORDERS_FAILURE],
            endpoint: `/CustomerDeliveryOrders?filter=${filter}`,
            method: 'GET',
        }, 'fetchingDeliveryOrders')
    }

    getCountByStatus() {
        return this.callApi(({
            types: [types.DELIVERY_ORDER_STATUS_REQUEST, types.DELIVERY_ORDER_STATUS_SUCCESS, types.DELIVERY_ORDER_STATUS_FAILURE],
            endpoint: '/CustomerDeliveryOrders/getCountByStatus',
            method: 'GET'
        }), 'fetchingOrderStates')
    }
    createRelatedOrders(id, data) {
        return this.callApi(({
            types: [types.DELIVERY_ORDER_RELATE_CREATE.REQUEST, types.DELIVERY_ORDER_RELATE_CREATE.SUCCESS, types.DELIVERY_ORDER_RELATE_CREATE.FAILURE],
            endpoint: `/CustomerDeliveryOrders/${id}/relatedOrders`,
            method: 'POST',
            body: data,
            form: "OrderReturn"
        }))
    }
    getRelatedOrders(id) {
        return this.callApi(({
            types: [types.DELIVERY_ORDER_RELATES.REQUEST, types.DELIVERY_ORDER_RELATES.SUCCESS, types.DELIVERY_ORDER_RELATES.FAILURE],
            endpoint: `/CustomerDeliveryOrders/${id}/relatedOrders`,
            method: 'GET',
        }))
    }

    uploadExcel(data) {
        return this.callApi({
            types: [types.DELIVERY_ORDER_UPLOAD_EXCEL_REQUEST, types.DELIVERY_ORDER_UPLOAD_EXCEL_SUCCESS, types.DELIVERY_ORDER_UPLOAD_EXCEL_FAILURE],
            endpoint: '/CustomerDeliveryOrders/uploadFee',
            method: 'POST',
            body: data,
            headers: {
                'Content-Type': null
            }
        }, 'uploadExcel')
    }

    postChat(id, message, scope) {
        return this.callApi({
            types: [types.DELIVERY_ORDER_CREATE_CHAT.REQUEST, types.DELIVERY_ORDER_CREATE_CHAT.SUCCESS, types.DELIVERY_ORDER_CREATE_CHAT.FAILURE],
            endpoint: `/CustomerDeliveryOrders/${id}/chat/${scope}`,
            method: 'POST',
            body: message,
            form: 'OrderChatForm'
        })
    }

    getChatHistories(id, filter) {
        filter = typeof filter == 'string' ? filter : JSON.stringify(filter);
        return this.callApi({
            types: [types.DELIVERY_ORDER_CHAT.REQUEST, types.DELIVERY_ORDER_CHAT.SUCCESS, types.DELIVERY_ORDER_CHAT.FAILURE],
            endpoint: `/CustomerDeliveryOrders/${id}/histories?filter=${filter}`,
            method: 'GET',
        })
    }
}
export default new CustomerDeliveryOrders();