export const NAME = 'order';
export const TYPE = 'CustomerDeliveryOrder';
export const PERMISSION_ORDER_UPLOAD_EXCEL = 'PERMISSION_ORDER_UPLOAD_EXCEL';
export const PERMISSION_DELIVER_ORDER_VIEW_LIST = 'PERMISSION_DELIVER_ORDER_VIEW_LIST';
export const PERMISSION_DELIVER_ORDER_EDIT = 'PERMISSION_DELIVER_ORDER_EDIT';
export const PERMISSION_HISTORY_TIMES_VIEW = 'PERMISSION_HISTORY_TIMES_VIEW';
export const PERMISSION_DELIVER_ORDER_CREATE = 'PERMISSION_DELIVER_ORDER_CREATE';
export const PERMISSION_PUBLIC_CHAT = 'PERMISSION_PUBLIC_CHAT';
export const PERMISSION_ORDER_CHANGE_FEATURE = 'PERMISSION_ORDER_CHANGE_FEATURE';
export const PERMISSION_DELIVER_ORDER_CHANGE_STATUS = 'PERMISSION_DELIVER_ORDER_CHANGE_STATUS';

export const types = {
    DELIVERY: 'DELIVERY',
    RETURN: 'RETURN'
};

export const OrderServices = {
    INSURANCE: 'INSURANCE'    // bảo hiểm
};

export const WarehouseStatus = {
    IN: 'IN',
    OUT: 'OUT'
};

export const WarehouseStatusDetail = {
    [WarehouseStatus.IN]: {
        label: 'Trong kho',
        className: 'ti-import mr-5 text-primary'
    },
    [WarehouseStatus.OUT]: {
        label: 'Xuất kho',
        className: "ti-export mr-5 text-danger"
    }
};


export const typesTitle = {
    [types.DELIVERY]: 'Giao đi',
    [types.RETURN]: 'Trả lại'
};

export const Status = {
    WAITING_FOR_PICKUP: 'WAITING_FOR_PICKUP',
    PICKED_UP: 'PICKED_UP',
    IN_TRANSFER: 'IN_TRANSFER',
    WAITING_FOR_DELIVER: 'WAITING_FOR_DELIVER',
    DELIVERING: 'DELIVERING',
    DELIVERED: 'DELIVERED',
    COMPLETED: 'COMPLETED',
    REJECT: 'REJECT',
    CANCELLED: 'CANCELLED',
};

export const statusLabel = {
    [Status.WAITING_FOR_PICKUP]: 'warning',
    [Status.PICKED_UP]: 'purple',
    [Status.IN_TRANSFER]: 'info',
    [Status.WAITING_FOR_DELIVER]: 'warning',
    [Status.DELIVERING]: 'info',
    [Status.DELIVERED]: 'success',
    [Status.COMPLETED]: 'success',
    [Status.REJECT]: 'danger',
    [Status.CANCELLED]: 'danger',
};
export const statusTitle = {
    [Status.WAITING_FOR_PICKUP]: 'Chờ lấy',
    [Status.PICKED_UP]: 'Đã lấy',
    [Status.IN_TRANSFER]: 'Trung chuyển',
    [Status.WAITING_FOR_DELIVER]: 'Chờ giao',
    [Status.DELIVERING]: 'Đang giao',
    [Status.DELIVERED]: 'Đã giao',
    [Status.COMPLETED]: 'Đối soát',
    [Status.REJECT]: 'Từ chối',
    [Status.CANCELLED]: 'Đã hủy',
};

export const endingStatus = [
    Status.COMPLETED,
    Status.REJECT,
    Status.CANCELLED
];

export const LOG_OBJECT = {
    DeliveryOrder: 'DeliveryOrder',
    CustomerDeliveryOrder: 'CustomerDeliveryOrder'
};

export const SCOPES = {
    INTERNAL: 'INTERNAL',
    EXTERNAL: 'EXTERNAL'
};

export const ContextTypes = {
    ACTIVITY: 'ACTIVITY',
    LOG: 'LOG'
};

export const ORDER_TIMES = {
    WAITING_FOR_PICKUP: 'createdTime',
    PICKED_UP: 'pickedUp',
    IN_TRANSFER: 'inTransfer',
    WAITING_FOR_DELIVER: 'waitingForDeliver',
    DELIVERING: 'beginDelivering',
    DELIVERED: 'delivered',
    COMPLETED: 'completed',
    CANCELLED: 'cancelled',
    REJECT: 'rejectedTime'
};

export const ORDER_TIME_BY_STATUS = {
    [Status.WAITING_FOR_PICKUP] : ORDER_TIMES.WAITING_FOR_PICKUP,
    [Status.WAITING_FOR_DELIVER] : ORDER_TIMES.WAITING_FOR_DELIVER,
    [Status.PICKED_UP] : ORDER_TIMES.PICKED_UP,
    [Status.IN_TRANSFER] : ORDER_TIMES.IN_TRANSFER,
    [Status.DELIVERED] : ORDER_TIMES.DELIVERED,
    [Status.DELIVERING] : ORDER_TIMES.DELIVERING,
    [Status.COMPLETED] : ORDER_TIMES.COMPLETED,
    [Status.CANCELLED] : ORDER_TIMES.CANCELLED,
    [Status.REJECT] : ORDER_TIMES.REJECT,
};


export const ORDER_TIMES_TITLE = {
    [ORDER_TIMES.WAITING_FOR_PICKUP] : 'Tạo vận đơn',
    [ORDER_TIMES.PICKED_UP]: 'Đã lấy',
    [ORDER_TIMES.IN_TRANSFER]: 'Trung chuyển',
    [ORDER_TIMES.WAITING_FOR_DELIVER]: 'Chờ giao',
    [ORDER_TIMES.DELIVERING]: 'Đang giao',
    [ORDER_TIMES.DELIVERED]: 'Đã giao',
    [ORDER_TIMES.COMPLETED]: 'Đối soát',
    [ORDER_TIMES.CANCELLED]: 'Đã hủy',
    [ORDER_TIMES.REJECT]: 'Từ chối',
};