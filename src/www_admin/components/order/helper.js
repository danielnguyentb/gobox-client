/**
 * Created by Tester-Ali on 14-10-2016.
 */

const numeric = new Intl.NumberFormat('vi-VN');
const dateTimeFormat = new Intl.DateTimeFormat('vi-VN');
import {typesTitle} from './constants'
import {encode} from '../../helpers'
import {required, isNumber, rangeNumber} from '../../helpers/validators'

export const hydrate = (order) => {
    let {owner} = order;
    let {fullName = '', username = '', mobile = ''} = owner || {};
    const owner_detail = (username) + (mobile ? ' - ' + mobile : '');
    const typeTitle = typesTitle.hasOwnProperty(order.type) ? typesTitle[order.type] : order.type;
    const tipFrom = `${owner_detail}<br/>${order.deliverFromFullAddress || ''}`;
    const tipTo = `${order.deliverToContact} - ${order.deliverToPhone}<br/>${order.deliverToFullAddress || ''}`;
    const deliveredLocalize = order.delivered ? dateTimeFormat.format(new Date(order.delivered)) : '--/--/--';
    return {
        tipTo, tipFrom, typeTitle, deliveredLocalize,
        codLocalize: numeric.format(order.cod),
        ...order
    }
};
export const generateQueryOrderIncludeOwner = () => ({
    "include": [
        {
            relation: "owner",
        }, {
            relation: "originOrder"
        }, {
            relation: 'deliverTasks'
        }, {
            relation: 'features'
        }
    ],
});

export const generateQuery = (params) => {
    let where = {};
    let {
        barcode = '', type = '', deliverTo = '', owner = '', privateNote = '', currentWarehouseStatus, currentWarehouseId,
        status, limit = 20, skip = 0, orderTime = '', sort = '', toTime, fromTime, pickUpWarehouseId, deliverWarehouseId, id, deliverTries
    } = params;

    let or_where = [];
    if (owner) {
        let t = encode(owner);
        or_where.push({'owner.fullName': {like: t}});
        or_where.push({'owner.username': {like: t}});
        or_where.push({'owner.mobile': {like: t}});
        or_where.push({'deliverFromFullAddress': {like: t}});
        or_where.push({'type': {like: t}});
    }
    if (deliverTo) {
        let t = encode(deliverTo);
        or_where.push({'deliverToContact': {like: t}});
        or_where.push({'deliverToPhone': {like: t}});
        or_where.push({'deliverToFullAddress': {like: t}});
    }
    where.and = [{
        or: or_where,
    }];
    if (pickUpWarehouseId = parseInt(pickUpWarehouseId)) where.and.push({pickUpWarehouseId});
    if (deliverWarehouseId = parseInt(deliverWarehouseId)) where.and.push({deliverWarehouseId});
    if (barcode) where.and.push({barcode: {like: encode(barcode)}});
    if (privateNote) where.and.push({privateNote: {like: encode(privateNote)}});
    if (id) where.and.push({"id": {inq: id}});
    if (type) where.and.push({type: {like: encode(type)}});
    if (currentWarehouseStatus) where.and.push({currentWarehouseStatus});
    if (currentWarehouseId > 0) where.and.push({currentWarehouseId});
    if (status && status.length > 0) {
        status = Array.isArray(status) ? status : status.split(',');
        where.and.push({status: {inq: status}})
    }
    if(deliverTries && deliverTries == "gt5") {
        where.and.push({deliverTries: {"gt": 5}})
    } else if(deliverTries) {
        where.and.push({deliverTries});
    }
    if(!sort) sort = 'desc';
    if(!orderTime) orderTime = `createdTime`;

    where.and.push({[orderTime] : {neq: null}});

    if(toTime && fromTime) {
        if(fromTime <= toTime) {
            where.and.push({ [orderTime] : {between: [fromTime, toTime]}})
        } else {
            where.and.push({ [orderTime] : {between: [toTime, fromTime]}})
        }
    } else if(fromTime) {
        where.and.push({ [orderTime] : {gte:fromTime }})
    } else if (toTime) {
        where.and.push({ [orderTime] : {lte: toTime}})
    }

    const include = generateQueryOrderIncludeOwner();
    return {where, limit, skip, order: orderTime + ' ' + sort, ...include};
};

export const validate = values => {
    const errors = {};
    if(Object.keys(values).length === 0) return errors;
    if (!values.type || values.type == "") {
        errors.type = 'Bạn chưa chọn loại vận đơn'
    }
    if (!values.weight) {
        errors.weight = 'Bạn chưa nhập số cân nặng trả lại'
    } else if(!isNumber(values.weight)) {
        errors.weight = 'Cân nặng phải là chữ số'
    }

    return errors
};