import React, {PropTypes, Component} from 'react'
import {connect} from 'react-redux'
import OrderTable from './OrderTable'
import {DeliveryOrderViewByRequestIdSelector, orderSelector} from '../../selectors'
import {changeStatus} from '../../actions'
import {DELIVERY_ORDER_UPDATE_FAILURE, DELIVERY_ORDER_UPDATE_SUCCESS} from '../../ActionTypes'
import {SuccessToast, ErrorToast} from '../../../../../www_components/Alert/components'

class OrderTableContainer extends Component {
    static propTypes = {
        requestId: PropTypes.number.isRequired,
        isAllow: PropTypes.func.isRequired,
        ignoreStatus: PropTypes.any,
        disabled: PropTypes.bool
    };

    onChange(id, status) {
        this.props.changeStatus(id, status);
    }

    render() {
        const {disabled, orders, ignoreStatus, isAllow} = this.props;
        return (
            <div>
                <SuccessToast dispatchTypes={{[DELIVERY_ORDER_UPDATE_SUCCESS]: 'Cập nhật trạng thái thành công'}}/>
                <ErrorToast dispatchTypes={[DELIVERY_ORDER_UPDATE_FAILURE]}/>
                <OrderTable
                    disabled={disabled}
                    isAllow={isAllow}
                    orders={orders}
                    ignoreStatus={ignoreStatus}
                    onChange={::this.onChange}/>
            </div>
        )
    }
}

const mapStateToProps = (state, props) => {
    const getDeliveryOrderView = DeliveryOrderViewByRequestIdSelector(state);
    return {
        orders: getDeliveryOrderView(props.requestId),
        disabled: orderSelector(state).updatingOrder
    }
};
export default connect(mapStateToProps, {changeStatus})(OrderTableContainer)