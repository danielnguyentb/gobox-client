import React, {PropTypes} from 'react'
import Link from '../../../../../www_components/Link'
import {ChangeStatus} from './ChangeStatus'
import {Status as OrderStatus, endingStatus} from '../../constants'
import {PERMISSION_DELIVER_ORDER_CHANGE_STATUS} from '../../constants'

export const OrderRow = ({order, onChange, ignoreStatus, disabled, isAllow}) => {
    const {id, barcode, note, deliverToContact, deliverToFullAddress, status} = order || {};
    return (
        <tr className={status == OrderStatus.CANCELLED ? 'order-disable' : ''}>
            <td className="fw-600">
                <Link to={`/orders/detail/${id}`} target="_blank">{barcode}</Link><br/>
                <span className="fw-500 text-grey">{note}</span>
            </td>
            <td>
                <span>{deliverToContact}</span><br/>
                <span className="text-grey">{deliverToFullAddress}</span>
            </td>
            <td>
                <ChangeStatus
                    options={{
                        notify: false,
                        disabled: endingStatus.includes(status) || disabled || !isAllow(PERMISSION_DELIVER_ORDER_CHANGE_STATUS),
                        confirmBeforeChangeToStatus: endingStatus,
                        ignoreStatus
                    }}
                    handleSubmit={({status}) => onChange(id, status)}
                    status={status}/>
            </td>
            <td className="text-center" width={60}>
                <Link target='_blank' to={`/orders/print/${id}`}
                      className="btn btn-default btn-xs" data-tip="In tem"><i
                    className="ti-printer"/></Link></td>
        </tr>
    )
};

const OrderTable = ({orders, onChange, disabled, ignoreStatus, isAllow}) => (
    <table>
        <thead>
        <tr>
            <th colSpan="4">VẬN ĐƠN</th>
        </tr>
        </thead>
        <tbody>
        {orders.map(order => {
            return (
                <OrderRow
                    key={order.id}
                    order={order}
                    isAllow={isAllow}
                    onChange={onChange}
                    disabled={disabled}
                    ignoreStatus={ignoreStatus}
                />
            )
        })}
        </tbody>
    </table>
);

OrderTable.propTypes = {
    orders: PropTypes.array.isRequired,
    isAllow: PropTypes.func.isRequired,
    ignoreStatus: PropTypes.any,
    onChange: PropTypes.func,
};

export default OrderTable;