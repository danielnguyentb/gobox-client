import React, {PropTypes} from 'react'
import {FormControl} from 'react-bootstrap'
import {statusTitle, Status as OrderStatus} from '../../constants'
import {SuccessToast, ErrorToast} from '../../../../../www_components/Alert/components'
import {DELIVERY_ORDER_UPDATE_FAILURE, DELIVERY_ORDER_UPDATE_SUCCESS} from '../../ActionTypes'
import {toastr} from '../../../../../www_components/Toastr2'

export const ChangeStatus = ({handleSubmit, status, options}) => {
    let {
        disabled = false, notify = false,
        confirmBeforeChangeToStatus, ignoreStatus
    } = options || {};
    confirmBeforeChangeToStatus = confirmBeforeChangeToStatus ? (
        Array.isArray(confirmBeforeChangeToStatus) ? confirmBeforeChangeToStatus : [confirmBeforeChangeToStatus]) : null;
    ignoreStatus = ignoreStatus ? (
        Array.isArray(ignoreStatus) ? ignoreStatus : [ignoreStatus]
    ) : [];
    return (
        <div>
            {notify &&
            <div>
                <SuccessToast dispatchTypes={{[DELIVERY_ORDER_UPDATE_SUCCESS]: 'Cập nhật trạng thái thành công'}}/>
                <ErrorToast dispatchTypes={[DELIVERY_ORDER_UPDATE_FAILURE]}/>
            </div>
            }
            <FormControl
                onChange={e => {
                    let status = e.target.value;
                    if (confirmBeforeChangeToStatus && confirmBeforeChangeToStatus.includes(status)) {
                        toastr.confirm(`Bạn có chắc muốn chuyển trạng thái vận đơn sang ${statusTitle[status]} không?`, {
                            onOk(){
                                handleSubmit({status});
                            }
                        });
                    } else {
                        handleSubmit({status});
                    }
                }}
                disabled={disabled}
                componentClass="select" placeholder="select" value={status} className="auto-with pull-left">
                {Object.keys(statusTitle)
                    .map(key => <option key={key} disabled={ignoreStatus.includes(key)} value={key}>{statusTitle[key]}</option>)}
            </FormControl>
        </div>
    )
};
ChangeStatus.propTypes = {
    status: PropTypes.string,
    handleSubmit: PropTypes.func,
    options: PropTypes.shape({
        disabled: PropTypes.bool,
        notify: PropTypes.bool,
        confirmBeforeChangeToStatus: PropTypes.oneOfType([
            PropTypes.array,
            PropTypes.string,
        ]),
        ignoreStatus: PropTypes.oneOfType([
            PropTypes.array,
            PropTypes.string,
        ]),
    }),
};