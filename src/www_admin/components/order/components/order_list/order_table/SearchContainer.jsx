import React, {PropTypes} from 'react'
import SearchTable from './SearchTable'
import {hubOptionsSelector} from '../../../../hub/selectors'
import {getHubs} from '../../../../hub/actions'
import {connect} from 'react-redux'

class SearchContainer extends React.Component {
    componentWillMount() {
        let {getHubs,} = this.props;
        getHubs();
    }

    onBlur = (name, callback) => event => callback({[name]: event.target.value});

    onEnter = (name, callback) => event => {
        event.preventDefault();
        if (event.key == 'Enter') {
            callback({[name]: event.target.value});
        }
    };

    onSearch(data) {
        this.props.onSearch({...data, skip: 0})
    }

    render() {
        let onSearch = ::this.onSearch;
        let {query, logisticsHubs} = this.props;
        return (
            <SearchTable
                onBlur={n => this.onBlur(n, onSearch)}
                onEnter={n => this.onEnter(n, onSearch)}
                onSelect={onSearch}
                query={query} logisticsHubs={logisticsHubs}/>
        );
    }
}

SearchContainer.propTypes = {
    onSearch: PropTypes.func.isRequired,
    query: PropTypes.object.isRequired,
};
const mapStateToProps = (state) => ({
    logisticsHubs: hubOptionsSelector(state)
});
export default connect(mapStateToProps, {getHubs})(SearchContainer);