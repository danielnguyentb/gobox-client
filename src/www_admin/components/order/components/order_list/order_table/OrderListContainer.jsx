import React, {PropTypes} from 'react';
import {connect} from 'react-redux';
import OrderList from './OrderList'
import ReactTooltip from 'react-tooltip'
import {getDeliveryOrders, changeFee} from '../../../actions'
import {getPaginateDeliveryOrdersView, metadataSelector} from '../../../selectors'
import Loading from 'react-loading'

import '../../order.scss';

class OrderListContainer extends React.Component {
    componentWillMount() {
        let {getDeliveryOrders, location,} = this.props;
        getDeliveryOrders(location.query);
    }

    changeFee(orderId, fee) {
        const {changeFee} = this.props;
        changeFee(orderId, fee);
    }

    componentDidUpdate(prevProps) {
        const {location, getDeliveryOrders} = this.props;
        if (location.search != prevProps.location.search) {
            getDeliveryOrders(location.query);
        }
        ReactTooltip.rebuild();
    }

    render() {
        let {metadata, location, orders, onSearch, count, ...rest} = this.props;
        let params = {...metadata, ...location.query};
        if (typeof count === 'undefined') return <Loading type="spin" color='#e3e3e3'/>;
        return (
            <OrderList
                orders={orders}
                query={params}
                onSearch={onSearch}
                onChangeFee={::this.changeFee}
                count={count}
                {...rest} />
        );
    }
}

const mapStateToProps = (state) => ({
    orders: getPaginateDeliveryOrdersView(state),
    count: metadataSelector(state).count
});
OrderListContainer.propTypes = {
    onSearch: PropTypes.func.isRequired,
    orders: PropTypes.array.isRequired,
};
export default connect(mapStateToProps, {getDeliveryOrders, changeFee})(OrderListContainer);