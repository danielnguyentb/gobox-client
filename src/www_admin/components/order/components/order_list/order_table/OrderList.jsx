import React from 'react'
import * as RBT from 'react-bootstrap'
import Select from 'react-select'
import 'react-bootstrap-daterangepicker/css/daterangepicker.css'
import OrderItem from './OrderItem'
import SearchContainer from './SearchContainer'
import ReactTooltip from 'react-tooltip'
import {DELIVERY_ORDER_CHANGE_FEE_FAILURE, DELIVERY_ORDER_CHANGE_FEE_SUCCESS} from '../../../ActionTypes'
import {ErrorToast, SuccessToast} from '../../../../../../www_components/Alert/components'

import DatetimepickerFromToDate from '../../../../../../www_components/DatetimepickerFromToDate'
import SortByTime from '../../../../../../www_components/SortByTime'

const success = {[DELIVERY_ORDER_CHANGE_FEE_SUCCESS]: 'Thành công'};
import {ORDER_TIMES_TITLE, ORDER_TIMES} from '../../../constants'

let optionsTime = [];
Object.keys(ORDER_TIMES_TITLE).map(key => optionsTime.push({value: key, label: ORDER_TIMES_TITLE[key]}));

export default ({orders, onSearch, onChangeFee, query,  count, ...args}) => {
    let {orderTime, fromTime, toTime, sort} = query;
    return (
        <div className="row">
            <ReactTooltip/>
            <ErrorToast dispatchTypes={[DELIVERY_ORDER_CHANGE_FEE_FAILURE]}/>
            <SuccessToast dispatchTypes={success}/>
            <div className="total">
                <div className="pull-left">
                    <p className="ml-15 mt-5">Tìm thấy <b>{count || 0}</b> kết quả</p>
                </div>
                <div className="pull-right">
                    <SortByTime onClick={onSearch} defaultValue={sort ? sort : 'desc'} />
                </div>
                <div className="pull-right">
                    <DatetimepickerFromToDate
                        onSelect={onSearch}
                        fromValue={fromTime}
                        toValue={toTime}
                    />
                </div>
                <div className="pull-right">
                    <Select className="small-select"
                        placeholder="Chọn thời gian"
                        value={orderTime ? orderTime : ORDER_TIMES.WAITING_FOR_PICKUP}
                        options={optionsTime}
                        onChange={t => onSearch({orderTime: t ? t.value : '', skip: 0})}
                    />
                </div>
            </div>



            <table className="table tbl-headblue table-striped table-hover tbl-orders">
                <thead>
                <tr>
                    <th>Mã vận đơn</th>
                    <th className="hidden-xs" width={110}>Loại VĐ</th>
                    <th>Trạng thái</th>
                    <th className="hidden-xs">Khách hàng</th>
                    <th className="hidden-xs">Người nhận</th>
                    <th>Tình trạng</th>
                    <th className="hidden-xs">Hub nhận</th>
                    <th className="hidden-xs">Hub giao</th>
                    <th className="hidden-xs">COD (VNĐ)</th>
                    <th width={100}>Phí (VNĐ)</th>
                    <th className="hidden-xs">Thời gian</th>
                    <th className="hidden-xs text-center" width={100}>Thao tác</th>
                </tr>
                </thead>
                <tbody>
                <SearchContainer onSearch={onSearch} query={query}/>
                {orders.length > 0 ? orders.map(order => <OrderItem key={order.id} order={order} onChangeFee={onChangeFee}/>)
                    :
                    <tr>
                        <td colSpan="100%">
                            <h4 style={{'textAlign': 'center'}}>Không có đơn nào thỏa mãn điều kiện tìm kiếm!</h4>
                        </td>
                    </tr>}
                </tbody>
            </table>
        </div>
    )
}