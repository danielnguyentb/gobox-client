import React from 'react'
import Link from '../../../../../../www_components/Link'
import InlineNumericEdit from '../../../../../../www_components/InlineNumericEdit'
import {formatCurrency} from '../../../../../helpers/format'
import {typesTitle, ORDER_TIME_BY_STATUS} from '../../../constants'
import moment from 'moment'
moment.locale('vi');

export default ({order, onChangeFee}) => {
    const {
        id, statusDetail, barcode, deliveredLocalize, currentWarehouse,
        deliverToContact, tipTo, deliverToDetail, totalFee, status,
        tipFrom, deliverFromDetail, inEndingStatus, cod, realCod,
        pickUpWarehouseCode, deliverWarehouseCode, type, owner:{username} = {},
    } = order || {};
    let timeByStatus = ORDER_TIME_BY_STATUS[status];
    return (
        <tr data-id={id}>
            <td><Link to={`/orders/detail/${id}`} target="_blank" className="fw-600 fs-15">{barcode}</Link></td>
            <td className="hidden-xs">{typesTitle[type]}</td>
            <td><span
                className={`lbl-style label label-${statusDetail.label}`}>{statusDetail.title}</span></td>
            <td className="hidden-xs">
                <span data-tip={tipFrom} data-html={true}>{username}</span>
                <br/>
                <span className="text-grey">{deliverFromDetail}</span>
            </td>
            <td className="hidden-xs">
                <span data-tip={tipTo} data-html={true}>{deliverToContact}</span>
                <br/>
                <span className="text-grey">{deliverToDetail}</span>
            </td>
            <td width={100}>
                {currentWarehouse &&
                <span><i className={currentWarehouse['className']}
                         data-tip={currentWarehouse.tooltip}/>{currentWarehouse.code || ''}</span>
                }
            </td>
            <td className="hidden-xs" width={100}>{pickUpWarehouseCode}</td>
            <td className="hidden-xs" width={100}>{deliverWarehouseCode}</td>
            <td className="hidden-xs" width={100}>
                {realCod ?
                    <span
                        data-tip="COD thực thu"
                        className={realCod ? 'text-success' : ''}>{formatCurrency(realCod)}</span>
                    :
                    <span>{formatCurrency(cod)}</span>
                }
            </td>
            <td className="edit-fee" width={100}>
                <InlineNumericEdit
                    value={totalFee || 0}
                    onSubmit={(value)=> {
                        if (totalFee != value) {
                            onChangeFee(id, value)
                        }
                    }}
                    disabled={inEndingStatus}
                    activeClassName="form-control"
                />
            </td>
            <td>
                {order[timeByStatus] ? moment(order[timeByStatus]).format("H:mm DD/MM/YYYY") : ''}
            </td>
            <td width={100} className="hidden-xs text-center">
                <Link target='_blank' to={`/orders/print/${id}`} className="btn btn-default btn-xs mr-5"
                      data-tip="In tem">
                    <i className="ti-printer"/>
                </Link>
            </td>
        </tr>
    )
}