import React, {PropTypes} from 'react'
import Select from 'react-select'
import {WarehouseStatus, typesTitle, types} from '../../../constants'
import 'react-select/dist/react-select.css'

const SearchTable = ({
    query: {barcode, type, deliverTo, owner, pickUpWarehouseId = 0,  deliverWarehouseId = 0, currentWarehouseStatus = '', currentWarehouseId},
    onBlur, onEnter, onSelect, logisticsHubs,
}) => (
    <tr className="hidden-xs">
        <td>
            <div className="form-group has-feedback order-indentity">
                <input
                    defaultValue={barcode}
                    onBlur={onBlur('barcode')}
                    onKeyUp={onEnter('barcode')}
                    className="form-control" placeholder="Mã đơn" type="text"/>
                <span className="ti-search form-control-feedback text-hint"/>
            </div>
        </td>
        <td className="hidden-xs">
            <Select
                placeholder="Loại VĐ"
                value={type ? type : ''}
                options={[
                    {label: 'Tất cả', value: ''},
                    {label: typesTitle[types.DELIVERY], value: types.DELIVERY},
                    {label: typesTitle[types.RETURN], value: types.RETURN}
                ]}
                onChange={t => onSelect({type: t ? t.value : ''})}
            />
        </td>
        <td> </td>
        <td className="hidden-xs">
            <div className="form-group">
                <input
                    defaultValue={owner}
                    onBlur={onBlur('owner')}
                    onKeyUp={onEnter('owner')}
                    className="form-control" placeholder="Tên / SĐT / địa chỉ" type="text"/>
            </div>
        </td>
        <td>
            <div className="form-group">
                <input
                    defaultValue={deliverTo}
                    onBlur={onBlur('deliverTo')}
                    onKeyUp={onEnter('deliverTo')}
                    className="form-control" placeholder="Tên / SĐT / địa chỉ" type="text"/>
            </div>
        </td>
        <td>
            <Select
                placeholder="Chọn hub" className='mb-5'
                value={parseInt(currentWarehouseId)}
                noResultsText="Không tìm thấy hub"
                options={logisticsHubs}
                onChange={t => onSelect({currentWarehouseId: t ? t.value : 0})}
            />
            <Select
                placeholder="Tình trạng"
                value={currentWarehouseStatus}
                options={[
                    {label: 'Tất cả', value: ''},
                    {label: 'Nhập', value: WarehouseStatus.IN},
                    {label: 'Xuất', value: WarehouseStatus.OUT}
                ]}
                onChange={t => onSelect({currentWarehouseStatus: t ? t.value : ''})}
            />
        </td>
        <td className="hidden-xs">
            <Select
                placeholder="Hub nhận"
                noResultsText="Không tìm thấy hub"
                value={parseInt(pickUpWarehouseId)}
                options={logisticsHubs}
                onChange={t => onSelect({pickUpWarehouseId: t ? t.value : 0})}
            />
        </td>
        <td className="hidden-xs">
            <Select
                placeholder="Hub giao"
                noResultsText="Không tìm thấy hub"
                value={parseInt(deliverWarehouseId)}
                options={logisticsHubs}
                onChange={t => onSelect({deliverWarehouseId: t ? t.value : 0})}
            />
        </td>
        <td className="hidden-xs">&nbsp;</td>
        <td className="hidden-xs">&nbsp;</td>
        <td className="hidden-xs">&nbsp;</td>
        <td className="hidden-xs">&nbsp;</td>
    </tr>
);

SearchTable.propTypes = {
    onBlur: PropTypes.func.isRequired,
    onEnter: PropTypes.func.isRequired,
    onSelect: PropTypes.func.isRequired,
    query: PropTypes.object.isRequired,
    logisticsHubs: PropTypes.array,
};

export default SearchTable;