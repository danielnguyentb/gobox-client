import React from 'react';
import {connect} from 'react-redux';
import * as RBT from 'react-bootstrap';
import {getOrderExportFileExcel, uploadExcel} from '../../../actions'
import FileInput from 'react-file-input'
import * as types from "../../../ActionTypes";
import {SuccessAlert} from '../../../../../../www_components/Alert'
import Touchable from '../../../../../../www_components/Touchable'
import {LIMIT_REQUEST} from '../../../../../constants'

class OrderExportCSV extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showModal: false,
            excelFile: {},
            showUploadMessage: false
        };
    }

    handleOrderExportExcel() {
        let params = this.props.location.query;
        params['limit'] = LIMIT_REQUEST;
        params['skip'] = 0;
        this.props.getOrderExportFileExcel(params);
    }

    handleUploadExcel() {
        let formData = new FormData();
        formData.append("file", this.state.excelFile);

        this.props.uploadExcel(formData);
    }

    close() { this.setState({showModal: false}); }

    open() { this.setState({showModal: true}); }

    handleChange(event) {
        this.setState({excelFile: event.target.files[0]});
    }

    render() {
        return (
            <div>
                <RBT.FormGroup>
                    <RBT.Button bsStyle="primary" onClick={e => this.handleOrderExportExcel() }>
                        <b>XUẤT CSV</b>
                    </RBT.Button>

                    <RBT.Button bsStyle="success" className="ml-10" onClick={::this.open}>
                        <b>UPLOAD EXCEL</b>
                    </RBT.Button>
                </RBT.FormGroup>

                <RBT.Modal show={this.state.showModal} onHide={::this.close}>
                    <RBT.Modal.Header closeButton>
                        <RBT.Modal.Title>Upload file excel tính phí</RBT.Modal.Title>
                    </RBT.Modal.Header>
                    <RBT.Modal.Body>
                        <div className="row">
                            <div className="col-md-12">
                                <form>
                                    <RBT.FormGroup>
                                        <div className="myinput">
                                            <FileInput name="file"
                                               accept=".xlsx"
                                               placeholder="Chọn file upload"
                                               className="form-control pt-upload-file"
                                               onChange={::this.handleChange} />
                                        </div>
                                    </RBT.FormGroup>
                                    <SuccessAlert dispatchTypes={{
                                        [types.DELIVERY_ORDER_UPLOAD_EXCEL_SUCCESS]: 'Upload file thành công. Hệ thống sẽ gửi thông báo kết quả phân tích vào email của bạn'
                                    }}/>
                                </form>
                            </div>
                        </div>
                    </RBT.Modal.Body>
                    <RBT.Modal.Footer>
                        <RBT.Button onClick={::this.close}>HỦY</RBT.Button>
                        <Touchable
                            actionTypes={[types.DELIVERY_ORDER_UPLOAD_EXCEL_SUCCESS, types.DELIVERY_ORDER_UPLOAD_EXCEL_FAILURE]}
                            onClick={::this.handleUploadExcel} bsStyle="primary">UPLOAD</Touchable>
                    </RBT.Modal.Footer>
                </RBT.Modal>
            </div>
        )
    }
}
const mapStateToProps = (state) => ({
});
export default connect(
    mapStateToProps, {getOrderExportFileExcel, uploadExcel}
)(OrderExportCSV);