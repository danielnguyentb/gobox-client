import React from 'react';
import {connect} from 'react-redux';
import {push} from 'react-router-redux'
import qs from 'qs';
import './../order.scss';
import DocumentTitle from '../../../../../www_components/DocumentTitle'
import OrderListContainer from './order_table/OrderListContainer'
import SearchContainer from './header/SearchContainer'
import PaginationContainer from './footer/PaginationContainer'
import Alert from '../../../../../www_components/Alert'
const {ErrorToast, SuccessToast} = Alert.components;
import {DELIVERY_ORDERS_FAILURE, DELIVERY_ORDER_STATUS_FAILURE} from '../../ActionTypes'
import OrderExportCSV from './order_table/OrderExportCSV'
import {PERMISSION_ORDER_UPLOAD_EXCEL} from '../../constants'

class OrderManagement extends React.Component {
    search(data){
        let params = {
            ...this.props.location.query,
            ...data,
        };
        this.props.dispatch(push(`/orders?${qs.stringify(params)}`));
    }

    render() {
        const {isAllow} = this.props;
        return (
            <div>
                <DocumentTitle title="Danh sách vận đơn"/>
                <ErrorToast dispatchTypes={[DELIVERY_ORDERS_FAILURE, DELIVERY_ORDER_STATUS_FAILURE]}/>
                <section className="content-header">
                    <div className="row">
                        <div className="col-md-8 col-xs-12">
                            <h2 className="mt-0">Danh sách vận đơn</h2>
                        </div>
                        <div className="col-md-4 col-xs-12 text-right">
                            {isAllow(PERMISSION_ORDER_UPLOAD_EXCEL) &&
                            <OrderExportCSV {...this.props} />
                            }
                        </div>
                    </div>
                </section>
                <section className="content">
                    <div className="box">
                        <SearchContainer {...this.props} onSearch={::this.search}/>

                        <OrderListContainer {...this.props}  onSearch={::this.search}/>
                        <div className="row">
                            <div className="col-md-12 text-right">
                                <PaginationContainer {...this.props} onSearch={::this.search}/>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        )
    }
}
const mapStateToProps = (state) => ({
    count: state.order.metadata.count || 0
});
export default connect(mapStateToProps)(OrderManagement);