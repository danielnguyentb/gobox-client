import React from 'react'
export default ({label = 'default', code, title, activated, isLoading = false, onSearch, count = 0}) => {
    let clName = (activated ? 'active' : '') + ' ' + (isLoading ? 'disabled' : '');
    return (
        <div onClick={e => {
            if (isLoading) return false;
            onSearch({status: code, skip: 0});
        }} className={clName.trim()}>
            <span className="text-status">{title}</span>
            <span className={`label label-${label}`}>{count}</span>
        </div>
    )
};