import React, {PropTypes} from 'react'
import StatusButton from './StatusButton'
import Select from 'react-select'

const StatusList = ({status, query: {deliverTries} , isLoading, onSearch, onSelect}) => {
    return (
        <div>
            <ul className="orders-status">
                {status.map(obj =>
                    <li key={obj.code}>
                        <StatusButton
                            isLoading={isLoading}
                            onSearch={onSearch}
                            {...obj}/>
                    </li>)}
            </ul>
            <div className="row">
                <div className="col-md-11 col-xs-8 pr-0 text-right">
                    <label className="control-label mt-10 mr-10">Số lần lấy/giao thất bại:</label>
                </div>
                <div className="col-md-1 col-xs-4 mb-15 pl-0">
                    <Select
                        placeholder="Số lần giao thất bại"
                        value={deliverTries ? deliverTries : ''}
                        onChange={t => onSelect({deliverTries: t ? t.value : '', skip : 0})}
                        options={[
                            {value: '', label: 'Tất cả'},
                            {value: '0', label: '0'},
                            {value: '1', label: '1'},
                            {value: '2', label: '2'},
                            {value: '3', label: '3'},
                            {value: '4', label: '4'},
                            {value: '5', label: '5'},
                            {value: "gt5", label: ">5"},
                        ]}
                    />
                </div>
            </div>
        </div>

    )
};
StatusList.propTypes = {
    status: PropTypes.array.isRequired,
    isLoading: PropTypes.bool,
    onSearch: PropTypes.func.isRequired,
};
export default StatusList;