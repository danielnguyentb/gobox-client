import React, {PropTypes, Component} from 'react'
import SearchBox from './SearchBox'
import {connect} from 'react-redux'
import {getOrderStatus} from '../../../actions'
import {countStatusSelector, orderSelector} from '../../../selectors'
import {DELIVERY_ORDERS_REQUEST, DELIVERY_ORDER_STATUS_REQUEST} from '../../../ActionTypes'

class SearchContainer extends Component {
    componentWillMount() {
        this.props.getOrderStatus();
    }

    search(data) {
        let {status, onSearch} = this.props;
        let newStatus = data.status;
        if (newStatus) {
            let statusActivated = [];
            status.map(item => {
                if (item.activated) {
                    statusActivated.push(item.code);
                }
            });
            if (statusActivated.includes(newStatus)) {
                statusActivated = statusActivated.filter(t => t !== newStatus);
            } else {
                statusActivated.push(newStatus);
            }
            data.status = statusActivated.join(',');
        }
        onSearch({...data, skip: 0});
    }

    render() {
        let {isLoading, status, onSearch, location: {query}} = this.props;
        return (
            <SearchBox
                isLoading={isLoading}
                status={status}
                query={query}
                onSearch={::this.search}
                onSelect={onSearch}
            />
        )
    }
}

const mapStateToProps = (state, props) => {
    const getStatus = countStatusSelector(state);
    const activatedStatus = props.location.query && props.location.query.status ? props.location.query.status.split(',') : [];
    return {
        metadata: orderSelector(state).metadata,
        status: getStatus(activatedStatus),
        isLoading: [DELIVERY_ORDERS_REQUEST, DELIVERY_ORDER_STATUS_REQUEST].includes(state.ajaxType),
    }
};
SearchContainer.propTypes = {
    onSearch: PropTypes.func.isRequired,
    status: PropTypes.array.isRequired,
};
export default connect(mapStateToProps, {getOrderStatus})(SearchContainer)