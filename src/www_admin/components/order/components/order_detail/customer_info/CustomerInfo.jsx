import React from 'react'
import AddressContainer from '../address/AddressContainer'
import {types, PERMISSION_DELIVER_ORDER_EDIT} from '../../../constants'

export default ({
    deliverFromFullAddress, pickUpWarehouseCode, deliverFromDetail, deliverFromDistrictId, deliverFromProvinceId,
    id, type, onUpdate, owner:{fullName, username, mobile} = {}, inEndingStatus, isAllow, deliverFromContact, deliverFromPhone,
}) => (
    <div className="panel panel-default no-shadow">
        <div className="panel-body tbl-info-order">
            <div className="hub-address"><i className="fa fa-map-marker mr-5"/> {pickUpWarehouseCode}</div>
            <table>
                <thead>
                <tr>
                    <th colSpan="100%">
                        {types[type] == types.RETURN ? "ĐỊA CHỈ GIAO CŨ" : "KHÁCH HÀNG @" + username}

                    </th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td className="fw-600 pt-nowrap">Liên hệ:</td>
                    <td>{deliverFromContact} / {deliverFromPhone}</td>
                </tr>
                <tr>

                    <td className="fw-600 pt-nowrap">
                        {types[type] == types.RETURN ? "Địa chỉ giao" : "Địa chỉ lấy:"}
                    </td>
                    <td>
                        {deliverFromFullAddress}
                        {id && <AddressContainer
                            orderId={id}
                            title={"Sửa địa lấy hàng"}
                            placeholder={"Địa chỉ lấy hàng"}
                            onUpdate={({provinceId, districtId, detail}) => {
                                onUpdate({
                                    deliverFromProvinceId: provinceId,
                                    deliverFromDistrictId: districtId,
                                    deliverFromDetail: detail
                                })
                            }}
                            checkDisabled={true}
                            provinceId={deliverFromProvinceId}
                            districtId={deliverFromDistrictId}
                            detail={deliverFromDetail}
                            notAllowEdit={(type == types.RETURN || inEndingStatus || !isAllow(PERMISSION_DELIVER_ORDER_EDIT))}
                        />
                        }
                    </td>
                </tr>
                <tr className="hidden">
                    <td className="fw-600 pt-nowrap">Lấy trước:</td>
                    <td>12h</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
)