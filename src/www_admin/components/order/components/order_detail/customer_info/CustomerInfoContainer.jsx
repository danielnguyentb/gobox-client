import React, {PropTypes, Component} from 'react'
import CustomerInfo from './CustomerInfo'
import {connect} from 'react-redux'
import {updateDeliveryOrders} from '../../../actions'
import {hubByIdSelector} from '../../../..//hub/selectors'
class CustomerInfoContainer extends Component {
    render() {
        let {order = {}, pickUpWarehouse = {}, onSave, isAllow} = this.props;
        return <CustomerInfo
            onUpdate={onSave} {...order}
            isAllow={isAllow}
            pickUpWarehouseCode={pickUpWarehouse.code}/>
    }
}
const mapStateToProps = (state, {order}) => {
    const getHub = hubByIdSelector(state);
    return {
        pickUpWarehouse: getHub(order.pickUpWarehouseId)
    }
};
export default connect(mapStateToProps, {updateDeliveryOrders})(CustomerInfoContainer);