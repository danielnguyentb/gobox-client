import React, {PropTypes, Component} from 'react'
import ReceiverInfo from './ReceiverInfo'
import {connect} from 'react-redux'
import {updateDeliveryOrders} from '../../../actions'
import {hubByIdSelector} from '../../../../hub/selectors'
import {getHub} from '../../../../hub/reducer'

class ReceiverInfoContainer extends Component {
    render() {
        let {order = {}, pickUpWarehouse = {}, onSave, isAllow, orderCurrentHub = {} } = this.props;
        return (
            <ReceiverInfo
                {...order}
                isAllow={isAllow}
                onUpdate={onSave}
                orderCurrentHub={orderCurrentHub}
                pickUpWarehouseCode={pickUpWarehouse.code}/>
        )
    }
}
const mapStateToProps = (state, {order}) => {
    const getHub = hubByIdSelector(state);
    return {
        pickUpWarehouse: getHub(order.pickUpWarehouseId),
        orderCurrentHub: getHub(order.currentWarehouseId)
    }
};
export default connect(mapStateToProps, {updateDeliveryOrders})(ReceiverInfoContainer);