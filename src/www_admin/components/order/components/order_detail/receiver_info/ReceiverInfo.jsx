import React from 'react'
import AddressContainer from '../address/AddressContainer'
import {types, PERMISSION_DELIVER_ORDER_EDIT} from '../../../constants'
import {BARCODE_ACTION} from '../../../../hub/constants'
export default ({
    deliverToContact, deliverToPhone, deliverToFullAddress, deliverWarehouseCode, deliverToDetail, totalFee, isAllow,
    deliverToDistrictId, deliverToProvinceId, id, onUpdate, isEndingStatus, note, cod, realCod, inEndingStatus, type,
    currentWarehouseId, currentWarehouseStatus, owner:{fullName, username, mobile} = {}, orderCurrentHub
}) => (

    <div className="panel panel-default no-shadow">
        <div className="panel-body tbl-info-order">
            <div className="hub-address">
                <i className={`${currentWarehouseStatus ? (currentWarehouseStatus == BARCODE_ACTION.BARCODE_ACTION_IN ? 'ti-import  text-primary' : 'ti-export text-danger') : ''} mr-5`}/>
                {orderCurrentHub && orderCurrentHub.code  ? orderCurrentHub.code + ' / ' : ''}
                <i className="fa fa-map-marker mr-5"/> {deliverWarehouseCode}
            </div>

            <table>
                <thead>
                <tr>
                    <td className="fw-600 pt-nowrap" colSpan="100%">
                        {types[type] == types.RETURN ? "ĐỊA CHỈ TRẢ LẠI @" + username : "NGƯỜI NHẬN"}
                    </td>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td className="fw-600 pt-nowrap">Họ tên:</td>
                    <td>{deliverToContact} / {deliverToPhone}</td>
                </tr>
                <tr>
                    <td className="fw-600 pt-nowrap">Địa chỉ giao:</td>
                    <td>
                        {deliverToFullAddress}
                        {id && <AddressContainer
                            onUpdate={({provinceId, districtId, detail}) => {
                                onUpdate({
                                    deliverToProvinceId: provinceId,
                                    deliverToDistrictId: districtId,
                                    deliverToDetail: detail
                                })
                            }}
                            notAllowEdit={isEndingStatus || !isAllow(PERMISSION_DELIVER_ORDER_EDIT)}
                            checkDisabled={false}
                            orderId={id}
                            districtId={deliverToDistrictId}
                            provinceId={deliverToProvinceId}
                            detail={deliverToDetail}
                            title={(type == types.RETURN) ? "Sửa địa chỉ trả lại" : "Sửa địa chỉ giao"}
                            placeholder={(type == types.RETURN) ? "Địa chỉ trả lại" : "Địa chỉ giao"}
                        />
                        }
                    </td>
                </tr>
                <tr className="hidden">
                    <td className="fw-600 pt-nowrap">Giao trước:</td>
                    <td>12h</td>
                </tr>
                <tr>
                    <td className="fw-600 pt-nowrap">Ghi chú:</td>
                    <td>{note}</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
)