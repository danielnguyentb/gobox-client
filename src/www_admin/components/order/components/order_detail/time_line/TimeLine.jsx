import React from 'react';
import {formatShortTime} from '../../../../../helpers/format'

export default ({timelines}) =>  (
    <div className="col-md-12 col-xs-12">
        <div className="widget no-border">
            <ul className="activities list-unstyled mb-0">
                {timelines.filter(t => t.scope == 'INTERNAL').map(timeline =>
                    <li className="activity-info" key={timeline.time}>
                        <div className="media">
                            <div className="media-left">
                                <span>
                                    {formatShortTime(timeline.time)}
                                </span>
                            </div>
                            <div className="media-body">
                                <span className="media-heading">{timeline.label}</span>
                            </div>
                        </div>
                    </li>
                )}

            </ul>
        </div>
    </div>
);