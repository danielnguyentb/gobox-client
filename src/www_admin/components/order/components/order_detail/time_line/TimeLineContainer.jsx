import React, {PropTypes, Component} from 'react'
import TimeLine from './TimeLine'
import {connect} from 'react-redux'

class TimeLineContainer extends Component {
    render(){
        return <TimeLine {...this.props} />
    }
}
export default connect()(TimeLineContainer)