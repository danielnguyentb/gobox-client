import React from 'react'
import * as RBT from 'react-bootstrap'
import Validation from 'react-validation'
import location from '../../../../location'
import ObjectForm from '../../../../../helpers/ValidationForm';
import Touchable from '../../../../../../www_components/Touchable'

import {DELIVERY_ORDER_UPDATE_SUCCESS, DELIVERY_ORDER_UPDATE_FAILURE} from '../../../ActionTypes'

Validation.rules = ObjectForm;
const {LocationSelect} = location.components;

export default ({show, onHide, onChange, onClose, onUpdate, provinceId, districtId, detail, placeholder, title = '', checkDisabled}) => (
    <RBT.Modal className="modal-detail-request" show={show} onHide={onHide}>
        <Validation.components.Form>
            <RBT.Modal.Header closeButton>
                <RBT.Modal.Title>{title}</RBT.Modal.Title>
            </RBT.Modal.Header>
            <RBT.Modal.Body>
                <RBT.FormGroup>
                    <RBT.FormControl
                        defaultValue={detail} onBlur={e => onChange({detail: e.target.value})}
                        type="text" placeholder={placeholder}/>
                </RBT.FormGroup>
                <RBT.FormGroup>
                    <LocationSelect
                        defaultProvinceId={provinceId}
                        defaultDistrictId={districtId}
                        checkDisabled={checkDisabled}
                        handleChange={onChange}/>
                </RBT.FormGroup>
                </RBT.Modal.Body>
                <RBT.Modal.Footer>
                    <RBT.Button onClick={onHide}>ĐÓNG</RBT.Button>
                    <Touchable
                        onClick={onUpdate}
                        actionTypes={[DELIVERY_ORDER_UPDATE_SUCCESS, DELIVERY_ORDER_UPDATE_FAILURE]}
                        bsStyle="primary">LƯU THAY ĐỔI</Touchable>
                </RBT.Modal.Footer>
         </Validation.components.Form>
    </RBT.Modal>
)