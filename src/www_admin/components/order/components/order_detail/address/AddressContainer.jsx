import React, {Component, PropTypes} from 'react'
import AddressModal from './AddressModal'
import {connect} from 'react-redux'
import {DELIVERY_ORDER_UPDATE_SUCCESS} from '../../../ActionTypes'

class AddressContainer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showModal: false,
            districtId: props.districtId,
            provinceId: props.provinceId,
            detail: props.detail,
        }
    }

    static propTypes = {
        onUpdate: PropTypes.func.isRequired,
        orderId: PropTypes.number.isRequired,
        provinceId: PropTypes.number,
        districtId: PropTypes.number,
        detail: PropTypes.string,
        title: PropTypes.string.isRequired,
        notAllowEdit: PropTypes.bool,
        placeholder: PropTypes.string,
        checkDisabled: PropTypes.bool,
    };

    componentDidUpdate(prevProps) {
        const {updateSuccess} = this.props;
        if (updateSuccess && updateSuccess != prevProps.updateSuccess) {
            this.setState({showModal: false})
        }
    }

    componentWillReceiveProps(nextProps) {
        let {districtId, provinceId, detail} = nextProps;

        if(districtId || provinceId || detail) {
            this.setState({
                districtId: districtId,
                provinceId: provinceId,
                detail: detail,
            })
        }
    }

    onChange(data) {
        this.setState(data);
    }

    onUpdate() {
        let {provinceId, districtId, detail} = this.state;
        this.props.onUpdate({provinceId, districtId, detail})
    }


    render() {
        let {districtId, provinceId, detail, showModal} = this.state;
        let {title, placeholder, checkDisabled, notAllowEdit} = this.props;
        return (
            <span>
                {!notAllowEdit &&
                    <span className="ml-15 btn btn-link text-base pt-0 pb-0" onClick={e=>this.setState({showModal: true})}>
                        <i className="ti-pencil ml-15"/>
                    </span>
                }
                {showModal &&
                <AddressModal
                    show={showModal}
                    onHide={e => this.setState({showModal: false})}
                    onUpdate={::this.onUpdate}
                    onChange={::this.onChange}
                    districtId={districtId}
                    provinceId={provinceId}
                    checkDisabled={checkDisabled}
                    detail={detail}
                    title={title}
                    placeholder={placeholder}
                />
                }
            </span>
        )
    }
}
const mapStateToProps = (state) => ({
});
export default connect(mapStateToProps)(AddressContainer);