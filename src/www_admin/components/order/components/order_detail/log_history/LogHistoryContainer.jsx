import React, {PropTypes} from 'react'
import {LogHistoryList} from './LogHistoryList'
import {fetchLogHistory} from '../../../actions'
import {LogsByIdSelector, DeliveryOrderByIdSelector} from '../../../selectors'
import {connect} from 'react-redux'
import {isEqual} from 'lodash'

class LogHistoryContainer extends React.Component {
    static propTypes = {
        orderId: PropTypes.number.isRequired
    };

    componentDidMount() {
        const {logs, fetchLogHistory, orderId} = this.props;
        if (!logs || logs.length == 0) {
            fetchLogHistory(orderId)
        }
    }
    
    componentDidUpdate(prevProps) {
        const {fetchLogHistory, orderId, order} = this.props;
        if (!isEqual(order, prevProps.order)) {
            fetchLogHistory(orderId)
        }
    }

    render() {
        const {logs} = this.props;
        //if(!logs || logs.length === 0) return null;
        return (
            <LogHistoryList
                logs={logs}/>
        )
    }
}

const mapStateToProps = (state, props) => {
    const getLogs = LogsByIdSelector(state);
    const getOrder = DeliveryOrderByIdSelector(state);
    return {
        logs: getLogs(props.orderId),
        order: getOrder(props.orderId)
    }
};
export default connect(mapStateToProps, {fetchLogHistory})(LogHistoryContainer);