import React from 'react'
import {formatFullTime} from '../../../../../helpers/format'
import {SCOPES} from '../../../constants'
import Link from '../../../../../../www_components/Link'

export const LogHistoryList = ({logs}) => {
    return (
        <div className="row">
            <div className="col-md-12 col-xs-12">
                <h5 className="mt-0 fw-600">Lịch sử hành động</h5>
                {logs.map(log => {
                    return <LogHistoryItem key={log.id} {...log}/>
                })}
            </div>
        </div>
    )
};

export const LogHistoryItem = ({scope, actionBy, createdTime, context}) => {
    const {fullName, username, realm, id} = actionBy || {};
    const detailPath = realm == 'customer' ? '/customers/detail/' + id : '/users/detail/' + id;

    return (
        <p>
            <span className="mr-5">
                <i className={`glyphicon glyphicon-globe fs-10 ${scope == SCOPES.INTERNAL ? 'invisibility' : ''}`}/>
                &nbsp;{formatFullTime(createdTime)}:
            </span>
            <span className="fw-600 mr-5">
                <Link to={detailPath} target="_blank">{username}</Link>
            </span>
            <span>{context['_attributes']['description']}</span>
        </p>
    )
};