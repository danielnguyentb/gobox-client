import React from 'react'
import {Field, reduxForm} from 'redux-form'
import {renderField, renderFieldSelect} from '../../../../../helpers/redux-form'
import * as RBT from 'react-bootstrap'
import {DELIVERY_ORDER_RELATE_CREATE} from '../../../ActionTypes'
import Alert from '../../../../../../www_components/Alert'
const {ErrorToast, SuccessToast} = Alert.components;
import Touchable from '../../../../../../www_components/Touchable'
import {validate} from '../../../helper'

const OrderReturn = ({handleSubmit, pristine, reset, submitting, error, invalid, close}) => (
    <form onSubmit={handleSubmit}>
        <RBT.Modal.Header closeButton>
            <RBT.Modal.Title>Thông tin vận đơn trả lại</RBT.Modal.Title>
        </RBT.Modal.Header>
        <RBT.Modal.Body>

            <div className="field">
                <label>Cân nặng</label>
                <div className="input-required">
                    <Field component={renderField} name="weight" className="form-control" placeholder="Nhập cân nặng"/>
                    <span className="required-symbol">*</span>
                </div>
            </div>
            <div className="field">
                <label>Loại vận đơn</label>
                <div className="input-required">
                    <Field  name="type"
                        component={renderFieldSelect} className="form-control"
                        options={[
                            {label: "Chọn loại vận đơn", value: ""},
                            {label: "Trả lại", value: "RETURN"},
                        ]}
                        defaultValue="RETURN"
                    />
                    <span className="required-symbol">*</span>
                </div>
            </div>
        </RBT.Modal.Body>
        <RBT.Modal.Footer>
            <ErrorToast
                dispatchTypes={[DELIVERY_ORDER_RELATE_CREATE.FAILURE]}/>

            <RBT.FormGroup>
                <Touchable actionTypes={[]}
                           className="btn btn-default" type="button"
                           onClick={close}>
                    ĐÓNG
                </Touchable>

                <button className="btn btn-primary" type="submit" disabled={pristine || submitting}>
                    TẠO
                </button>
            </RBT.FormGroup>
        </RBT.Modal.Footer>
    </form>
);
export default reduxForm({
    form: 'OrderReturn',
    validate
})(OrderReturn)