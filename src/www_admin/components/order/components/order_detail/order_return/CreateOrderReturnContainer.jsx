import React from 'react'
import {connect} from 'react-redux';
import * as RBT from 'react-bootstrap'
import {push} from 'react-router-redux'
import {bindActionCreators} from 'redux'
import Touchable from '../../../../../../www_components/Touchable'
import {createRelatedOrders} from '../../../actions'
import {required, isNumber, rangeNumber} from './../../../../../helpers/validators'
import Alert from '../../../../../../www_components/Alert'
const {ErrorToast, SuccessToast} = Alert.components;
import {DELIVERY_ORDER_RELATE_CREATE} from '../../../ActionTypes'
import {Field, reduxForm} from 'redux-form'
import OrderReturn from './OrderReturn'

class CreateOrderReturnContainer extends React.Component{
    static propTypes = {
        orderId: React.PropTypes.number,
    };
    constructor(props) {
        super(props);
        this.state = {
            showModal: false,
        };
    }


    componentWillReceiveProps(nextProps) {
        if(nextProps.createdOrderRelateSuccess) {
            this.close();
            nextProps.onRedirect('/orders/detail/'+ nextProps.createdOrderRelateSuccess.id );
            this.props.dispatch(actions.reset("deep.order.byId[0]"))
        }
    }

    close() { this.setState({showModal: false}); }

    open() { this.setState({showModal: true}); }

    submit(data) {
        this.props.createRelatedOrders(this.props.orderId, data);
    }
    
    render(){
        let {error} = this.props;
        return(
            <div>
                <RBT.FormGroup className="pull-right">
                    <RBT.Button bsStyle="primary" onClick={::this.open}>TẠO VẬN ĐƠN TRẢ LẠI</RBT.Button>
                </RBT.FormGroup>

                <RBT.Modal show={this.state.showModal} onHide={::this.close} bsSize="small">
                    <OrderReturn {...this.props} onSubmit={::this.submit} close={::this.close}/>
                </RBT.Modal>
            </div>
        );
    }
}
const mapStateToProps = (state, props) => ({
    createdOrderRelateSuccess: state.order.createdOrderRelateSuccess,
});
const mapDispatchToProps = (dispatch) => ({
    ...bindActionCreators({
        createRelatedOrders
    }, dispatch),
    onRedirect: (path) => {
        dispatch(push(path))
    },
    dispatch
});

export default connect(
    mapStateToProps, mapDispatchToProps
)(CreateOrderReturnContainer);