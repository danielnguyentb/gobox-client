import React, {Component, PropTypes} from 'react';
import TimeLineContainer from './time_line/TimeLineContainer'
import ReceiverInfoContainer from './receiver_info/ReceiverInfoContainer'
import HeaderContainer from './header/HeaderContainer'
import FeeInfoContainer from './fee/FeeInfoContainer'
import CustomerInfoContainer from './customer_info/CustomerInfoContainer'
import {OrderTaskFormContainer} from '../../../tasks/components/tasks_form/OrderTaskFormContainer'
import {TasksTableContainer} from '../../../tasks/components/tasks_table/TasksTableContainer'
import OtherOrderContainer from './other_order/OtherOrderContainer'
import LogHistoryContainer from './log_history/LogHistoryContainer'
import BoxchatContainer from './boxchat/BoxchatContainer'
import Alert from '../../../../../www_components/Alert'
import {
    DELIVERY_ORDER_FAILURE, DELIVERY_ORDER_CREATE_FAILURE, DELIVERY_ORDER_UPDATE_FAILURE,
    DELIVERY_ORDER_CREATE_SUCCESS, DELIVERY_ORDER_UPDATE_SUCCESS,
} from '../../ActionTypes'

const {ErrorToast, SuccessToast} = Alert.components;
const successTypeMessages = {
    [DELIVERY_ORDER_CREATE_SUCCESS]: 'Tạo vận đơn thành công',
    [DELIVERY_ORDER_UPDATE_SUCCESS]: 'Cập nhật vận đơn thành công'
};
import {types, PERMISSION_HISTORY_TIMES_VIEW} from '../../constants'

export default (props) => {
    const {order, orderId, isAllow} = props;
    return (
        <div>
            <ErrorToast
                dispatchTypes={[DELIVERY_ORDER_FAILURE, DELIVERY_ORDER_CREATE_FAILURE, DELIVERY_ORDER_UPDATE_FAILURE]}/>
            <SuccessToast dispatchTypes={successTypeMessages}/>
            <HeaderContainer {...props} isAllow={isAllow}/>
            <section className="content">
                <div className="box">
                    <div className="row">
                        <div className="col-md-9 col-xs-12">
                            <div className="row">
                                {order &&
                                <section>
                                    <div
                                        className={`col-md-6 col-xs-12 ${order.type == types.RETURN ? "color-gray" : ''}`}>
                                        <CustomerInfoContainer
                                            {... props}
                                            isAllow={isAllow}
                                        />
                                    </div>
                                    <div
                                        className={`col-md-6 col-xs-12 ${order.type == types.RETURN ? 'color-red' : ''}`}>
                                        <ReceiverInfoContainer
                                            {... props}
                                            isAllow={isAllow}
                                        />
                                    </div>
                                </section>
                                }
                            </div>

                            {/*fee*/}
                            <div className="row">
                                <div className="col-md-12">
                                    <FeeInfoContainer
                                        {... props}
                                        isAllow={isAllow}
                                    />
                                </div>
                            </div>

                            {/*task*/}
                            <div className="row">
                                <div className="col-md-12">
                                    <div className="panel panel-default no-shadow">
                                        <div className="panel-body panel-task">
                                            <div className="row">
                                                <div className="col-md-12 col-xs-12">
                                                    {order && order.id &&
                                                    <TasksTableContainer
                                                        orderId={order.id}
                                                    />
                                                    }
                                                </div>
                                                <div className="col-md-12 col-xs-12">
                                                    {order && order.id && order.currentWarehouseId &&
                                                    <OrderTaskFormContainer
                                                        label="Người giao"
                                                        hubId={order.currentWarehouseId}
                                                        orderId={order.id}/>
                                                    }
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            {/*Other-order*/}
                            <div className="row">
                                {order && order.id &&
                                <OtherOrderContainer order={order}/>
                                }
                            </div>

                            <div className="row">
                                <div className="col-md-12 col-xs-12">
                                    {order && order.id &&
                                    <BoxchatContainer {...props} />
                                    }
                                </div>
                            </div>

                            {/*/!*history*!/*/}
                            {/*<div className="row">*/}
                                {/*<div className="col-md-12">*/}
                                    {/*{orderId > 0 && <LogHistoryContainer orderId={orderId} />}*/}
                                {/*</div>*/}
                            {/*</div>*/}
                        </div>

                        {/*timeline*/}
                        <div className="col-md-3 col-xs-12">
                            <div className="row">
                                {order && order['historyTimes'] && isAllow(PERMISSION_HISTORY_TIMES_VIEW) &&
                                <TimeLineContainer timelines={order['historyTimes']}/>
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    );
};
