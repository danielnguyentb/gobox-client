import React from 'react'
import Link from './../../../../../../www_components/Common/Link'
import {typesTitle, statusTitle, statusLabel} from '../../../constants'

class OtherOrder extends React.Component{
    render(){
        let {order, relatedOrders, isOrigin} = this.props;
        let originOrder = isOrigin && order ? order.originOrder : order;

        return(
            <li className={order.id == originOrder.id ? 'active' : ''}>
                <span>
                    <Link to={`/orders/detail/${originOrder.id}`} target="_blank" className="fw-600 fs-16 mr-5">
                        {originOrder.barcode}
                    </Link>
                    ({typesTitle[originOrder.type]})
                </span>

                <span className={`lbl-style label label-${statusLabel[originOrder.status]} ml-15 fs-12`}>
                    {statusTitle[originOrder.status]}
                </span>

                <ul>
                    {relatedOrders && relatedOrders.map(relatedOrder =>
                        <li className={order.id == relatedOrder.id ? 'active' : ''} key={relatedOrder.id}>
                            <span>
                                {order.id == relatedOrder.id ?
                                    <Link to='/' className="fw-600 mr-5">{relatedOrder.barcode}</Link>
                                    :
                                    <Link to={`/orders/detail/${relatedOrder.id}`} target="_blank" className="fw-600 mr-5">{relatedOrder.barcode}</Link>
                                }
                                ({typesTitle[relatedOrder.type]})
                            </span>
                            <span className={`lbl-style label label-${statusLabel[relatedOrder.status]} ml-15 fs-12`}>
                                {statusTitle[relatedOrder.status]}
                            </span>
                        </li>
                    ) }

                </ul>
            </li>

        );


    }
}
export default (OtherOrder)