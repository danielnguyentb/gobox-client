import React from 'react'
import {ErrorToast, SuccessToast} from '../../../../../../www_components/Alert/components'
import {DELIVERY_ORDER_CHANGE_FEE_SUCCESS, DELIVERY_ORDER_CHANGE_FEE_FAILURE} from '../../../ActionTypes'
import {PERMISSION_DELIVER_ORDER_EDIT, PERMISSION_ORDER_CHANGE_FEATURE} from '../../../constants'
import {InsuranceModalContainer} from './ChangeInsuranceModal'
import {InlineWeightInput, InlineNumericEdit} from 'cores'

const successTypeMessages = {
    [DELIVERY_ORDER_CHANGE_FEE_SUCCESS]: 'Cập nhật vận đơn thành công'
};

export default ({onUpdate, order, isAllow}) => {
    const {id, cod, isEndingStatus, realCod, totalFee, insurance, weightKg} = order || {};
    const insuranceService = !!insurance;
    const insuranceValue = insuranceService ? +insurance['customField1'] : null;
    return (
        <div className="panel panel-default no-shadow">
            <ErrorToast dispatchTypes={[DELIVERY_ORDER_CHANGE_FEE_FAILURE]}/>
            <SuccessToast dispatchTypes={successTypeMessages}/>
            <div className="panel-body tbl-info-order">
                <div className="row">
                    <div className="col-md-12 col-xs-12">
                        <table>
                            <thead>
                            <tr>
                                <th colSpan="100%">Thông tin phí</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td className="fw-600 pt-nowrap">Cân nặng (Kg):</td>
                                <td width={150}>
                                    <InlineWeightInput
                                        onSubmit={weightKg => onUpdate({weightKg})}
                                        disabled={isEndingStatus || !isAllow(PERMISSION_DELIVER_ORDER_EDIT)}
                                        value={weightKg}
                                    />
                                </td>
                                <td className="fw-600 pt-nowrap">Tiền bảo hiểm (VNĐ):</td>
                                <td width="150">
                                    <InsuranceModalContainer
                                        onSubmit={onUpdate}
                                        disabled={isEndingStatus || !isAllow(PERMISSION_ORDER_CHANGE_FEATURE)}
                                        insuranceValue={insuranceValue}
                                        insuranceService={insuranceService}/>
                                </td>
                                <td className="fw-600 pt-nowrap">COD (VNĐ):</td>
                                <td width={150}>
                                    <InlineNumericEdit
                                        value={cod || 0}
                                        disabled={isEndingStatus || !isAllow(PERMISSION_DELIVER_ORDER_EDIT)}
                                        onSubmit={value => {
                                            if (cod != value) {
                                                onUpdate({cod: value})
                                            }
                                        }}
                                    />
                                </td>
                                <td className="fw-600 pt-nowrap">COD thực thu (VNĐ):</td>
                                <td width={150}>
                                    <InlineNumericEdit
                                        value={realCod || 0}
                                        disabled={isEndingStatus || !isAllow(PERMISSION_DELIVER_ORDER_EDIT)}
                                        onSubmit={value => {
                                            if (realCod != value) {
                                                onUpdate({realCod: value})
                                            }
                                        }}
                                    />
                                </td>
                            </tr>
                            <tr>
                                <td className="fw-600 pt-nowrap">Tổng phí (VNĐ):</td>
                                <td width={150}>
                                    <InlineNumericEdit
                                        value={totalFee || 0}
                                        disabled={isEndingStatus || !isAllow(PERMISSION_DELIVER_ORDER_EDIT)}
                                        onSubmit={fee => {
                                            if (fee != totalFee) {
                                                onUpdate({totalFee: fee})
                                            }
                                        }}
                                    />
                                </td>
                                <td colSpan="4">&nbsp;</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    )
}