import React, {PropTypes, Component} from 'react'
import ReceiverInfo from './FeeInfo'
class FeeInfoContainer extends Component {
    render() {
        let {order = {}, onSave, isAllow} = this.props;
        return (
            <ReceiverInfo
                order={order}
                isAllow={isAllow}
                onUpdate={onSave} />
        )
    }
}
export default(FeeInfoContainer);
