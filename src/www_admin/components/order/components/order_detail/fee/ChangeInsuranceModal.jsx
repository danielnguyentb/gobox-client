import React, {PropTypes} from 'react'
import * as RBT from 'react-bootstrap'
import {formatCurrency} from '../../../../../helpers/format'
import {toastr} from '../../../../../../www_components/Toastr2'
import {connect} from 'react-redux'
import {CHANGE_ORDER_FEATURE_SUCCESS} from '../../../ActionTypes'
import {ReInsuranceForm} from './ChangeInsuranceForm'

export class InsuranceModal extends React.Component {
    static propTypes = {
        insuranceValue: PropTypes.number,
        insuranceService: PropTypes.bool,
        onSubmit: PropTypes.func,
        disabled: PropTypes.bool,
        changeSuccess: PropTypes.bool,
    };
    constructor(props) {
        super(props);
        this.state = {showModal: false}
    }

    closeModal() {
        this.setState({showModal: false});
    }

    componentDidUpdate(prevProps) {
        const {changeSuccess} = this.props;
        if (changeSuccess && changeSuccess != prevProps.changeSuccess) {
            this.setState({showModal: false});
            toastr.success('Cập nhật thành công');
        }
    }

    render() {
        const {onSubmit, insuranceService, insuranceValue, disabled} = this.props;
        const {showModal} = this.state;
        const insuranceFee = insuranceService ? formatCurrency(insuranceValue) : '--';
        return (
            <div>
                {disabled ? <span>{insuranceFee}</span>
                    :
                    <div className="input-edit" onClick={e => this.setState({showModal: true})}>
                        <span>{insuranceFee}</span>
                    </div>
                }
                <RBT.Modal show={showModal} bsSize="small"  onHide={e => this.setState({showModal: false})}>
                    <RBT.Modal.Header closeButton>
                        <RBT.Modal.Title>Sửa thông tin bảo hiểm</RBT.Modal.Title>
                    </RBT.Modal.Header>
                    <RBT.Modal.Body>
                        <ReInsuranceForm
                            close={::this.closeModal}
                            onSubmit={onSubmit}
                            initialValues={{
                                insuranceValue, insuranceService
                            }}/>
                    </RBT.Modal.Body>
                </RBT.Modal>
            </div>
        )
    }
}

export const InsuranceModalContainer = connect(state => {
    return {
        changeSuccess: state.actionType === CHANGE_ORDER_FEATURE_SUCCESS
    }
})(InsuranceModal);