import React, {PropTypes} from 'react'
import {Fields, reduxForm} from 'redux-form'
import {ReInsuranceService} from '../../services_component/ReInsuranceService'

export const InsuranceForm = ({handleSubmit, error, submitting, pristine, invalid, close}) => {
    return (
        <form onSubmit={handleSubmit}>
            <Fields names={['insuranceService', 'insuranceValue']} component={ReInsuranceService}/>
            {error && <div className="alert alert-danger">{error}</div>}
            <div className="row">
                <div className="col-md-12 text-right">
                    <button
                        className="btn btn-default mr-5" type="button" onClick={close}
                    >HỦY
                    </button>
                    <button
                        disabled={pristine || submitting || invalid}
                        className="btn btn-primary" type="submit"
                    >LƯU
                    </button>
                </div>
            </div>
        </form>
    )
};

InsuranceForm.propTypes = {
    handleSubmit: PropTypes.func,
    insuranceService: PropTypes.bool,
    insuranceValue: PropTypes.number
};
export const validate = values => {
    const errors = {};
    if(Object.keys(values).length === 0) return errors;
    if (values.insuranceService) {
        if (!values.insuranceValue) {
            errors.insuranceValue = 'Giá trị hàng không được để trống'
        } else if (typeof (+values.insuranceValue) !== 'number') {
            errors.insuranceValue = 'Giá trị hàng phải là số'
        } else if (values.insuranceValue < 0) {
            errors.insuranceValue = 'Giá trị hàng không được nhỏ hơn 0'
        }
    }
    return errors;
};
export const ReInsuranceForm = reduxForm({
    form: 'ChangeInsuranceForm',
    validate
})(InsuranceForm);