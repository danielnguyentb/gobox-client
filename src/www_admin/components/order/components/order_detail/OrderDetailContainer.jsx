import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux'
import {toastr} from '../../../../../www_components/Toastr2'
import {DeliveryOrderByIdSelector} from '../../selectors'
import {hubOptionsSelector} from '../../../hub/selectors'
import {getHubs} from '../../../hub/actions'
import OrderDetail from './OrderDetail'
import {typesTitle, OrderServices} from '../../constants'
import {
    getDeliveryOrderById, updateDeliveryOrders,
    changeStatus, changeType, changeFee, fetchChangeFeaturesSaga
} from '../../actions'

class OrderDetailContainer extends React.Component {
    componentWillMount() {
        let {orderId, getDeliveryOrderById} = this.props;
        getDeliveryOrderById(orderId);
    }

    componentDidUpdate(prevProps) {
        const {orderId, getDeliveryOrderById} = this.props;
        if (orderId && orderId != prevProps.orderId) {
            getDeliveryOrderById(orderId)
        }
    }
    componentWillReceiveProps(nextProps) {
        let {error} = nextProps;
        if(error && error.length > 0) {
            toastr.error(error);
        }
    }
    onSave(data) {
        const {status, cod, type, weightKg, totalFee, insuranceService, insuranceValue} = data;
        let {orderId, updateDeliveryOrders, changeStatus, order, changeType, changeFee, dispatch} = this.props;

        if (typeof status !== 'undefined') {
            changeStatus(orderId, status);
        } else if (typeof type !== 'undefined') {
            toastr.confirm(`Bạn có chắc muốn chuyển loại vận đơn sang "${typesTitle[type]}" không?`, {
                onOk(){
                    changeType(orderId, type);
                }
            })
        } else if (typeof totalFee !== 'undefined') {
            changeFee(orderId, totalFee)
        } else if (typeof insuranceService !== 'undefined') {
            dispatch(fetchChangeFeaturesSaga(orderId, OrderServices.INSURANCE, {
                customField1: +insuranceValue,
                isDeleted: insuranceService ? 0 : 1
            }))
        } else if(typeof weightKg !== 'undefined') {
            data.weight = weightKg * 1000;
            updateDeliveryOrders(orderId, data);
        } else {
            updateDeliveryOrders(orderId, data);
        }
    }

    render() {
        let {order, hubs, orderId, isAllow} = this.props;
        return (
            <OrderDetail
                order={order || {}}
                hubs={hubs}
                onSave={::this.onSave}
                orderId={orderId}
                isAllow={isAllow}
            />
        );
    }
}
const mapStateToProps = (state, props) => {
    const orderId = parseInt(props.params.orderId) || 0;
    const getDeliveryOrderView = DeliveryOrderByIdSelector(state);
    return {
        orderId,
        order: getDeliveryOrderView(orderId),
        hubs: hubOptionsSelector(state),
        error: state.order.error
    }
};
export default connect(mapStateToProps, {
    getDeliveryOrderById, getHubs, updateDeliveryOrders, changeStatus, changeType, changeFee,
})(OrderDetailContainer)
