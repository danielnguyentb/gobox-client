import React from 'react'
import {connect} from 'react-redux'
import ChatBox from '../../../../../../www_components/ChatBox'
import {postChat, getChatHistories, fetchLogHistory} from '../../../actions'
import {PERMISSION_PUBLIC_CHAT} from '../../../constants'
import {LogsByIdSelector, DeliveryOrderViewByIdSelector} from '../../../selectors'
import {isEqual} from 'lodash'
import {ProfileSelector} from '../../../../user/selectors'

class BoxchatContainer extends React.Component{
    constructor(props) {
        super(props);
    }
    componentDidMount() {
        const {logs, fetchLogHistory, order} = this.props;
        if (!logs || logs.length == 0) {
            fetchLogHistory(order.id)
        }
    }
    componentDidUpdate(prevProps) {
        const {fetchLogHistory, order} = this.props;
        if (!isEqual(order, prevProps.order)) {
            fetchLogHistory(order.id)
        }
    }
    componentWillReceiveProps(nextProps) {
        let {fetchLogHistory, order, postChatSuccess} = this.props;
        if (postChatSuccess) {
            fetchLogHistory(order.id)
        }
    }
    handlePostChat(message) {
        let {postChat, order} = this.props;
        postChat(order.id, message, "EXTERNAL");
    }

    render(){
        let {order, postChatSuccess, logs, isAllow, profile} = this.props;
        if(!order) return null;
        return(
            <ChatBox
                postChat={::this.handlePostChat} objectId={order.id} postChatSuccess={postChatSuccess} histories={logs}
                profile={profile}
                hasPermissionChatWithCustomer={isAllow(PERMISSION_PUBLIC_CHAT)}
            />
        )
    }
}
const mapStateToProps = (state, props) => {
    const getLogs = LogsByIdSelector(state);
    return {
        logs: getLogs(props.order.id),
        postChatSuccess: state.order.postChatSuccess,
        profile: ProfileSelector(state)
    }
};
export default connect(
    mapStateToProps, {postChat, fetchLogHistory}
)(BoxchatContainer);