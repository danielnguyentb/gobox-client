import React, {PropTypes, Component} from 'react'
import Header from './Header'
import {connect} from 'react-redux'

class HeaderContainer extends Component {
    render() {
        let {order, onSave, isAllow} = this.props;
        return (
            <Header
                order={order || {}}
                isAllow={isAllow}
                onSave={onSave}
            />
        )
    }
}
const mapStateToProps = (state, props) => ({});
export default connect(mapStateToProps)(HeaderContainer);