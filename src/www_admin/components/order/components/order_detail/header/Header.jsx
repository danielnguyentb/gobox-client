import React from 'react'
import Link from '../../../../../../www_components/Link'
import {FormControl} from 'react-bootstrap'
import DocumentTitle from '../../../../../../www_components/DocumentTitle'
import {ChangeStatus} from '../../widgets/ChangeStatus'
import {Status as OrderStatus, typesTitle, endingStatus, PERMISSION_DELIVER_ORDER_CHANGE_STATUS} from '../../../constants'
import {DELIVERY_ORDER_CHANGE_TYPE_SUCCESS, DELIVERY_ORDER_CHANGE_TYPE_FAILURE} from '../../../ActionTypes'
import CreateOrderReturnContainer from '../order_return/CreateOrderReturnContainer'
import {ErrorToast, SuccessToast} from '../../../../../../www_components/Alert/components'

const success = {
    [DELIVERY_ORDER_CHANGE_TYPE_SUCCESS]: 'Thành công'
};

export default ({order, onSave, isAllow}) => {
    const {barcode, id, type, statusDetail: {code, label, title} = {}, typeTitle, inEndingStatus, deliverTries,} = order || {};
    return (
        <div>
            <DocumentTitle title={`Chi tiết vận đơn`}/>
            <ErrorToast dispatchTypes={[DELIVERY_ORDER_CHANGE_TYPE_FAILURE]}/>
            <SuccessToast dispatchTypes={success}/>
            <section className="content-header">
                <div className="row">
                    <div className="col-md-9 col-xs-12">
                        <h3 className="mt-0 mb-0">
                            <span className="pull-left">Chi tiết vận đơn </span>
                            <span className="text-primary ml-5 mr-15 pull-left"> {barcode} </span>
                            <FormControl
                                disabled={inEndingStatus}
                                value={type}
                                onChange={e => onSave({type: e.target.value})}
                                componentClass="select" placeholder="select" name=""
                                className="auto-with pull-left mr-5">
                                {Object.keys(typesTitle).map(key => {
                                    return <option key={key} value={key}>{typesTitle[key]}</option>
                                })}
                            </FormControl>

                            <ChangeStatus options={{
                                notify: false,
                                disabled: inEndingStatus || !isAllow(PERMISSION_DELIVER_ORDER_CHANGE_STATUS),
                                confirmBeforeChangeToStatus: endingStatus
                            }} handleSubmit={onSave} status={code}
                            />
                            <span
                                className="ml-5 fs-14">{deliverTries > 0 ? `(Giao lại lần ${deliverTries})` : ''}</span>
                        </h3>
                    </div>
                    <div className="col-md-3 col-xs-12 text-right">
                        <CreateOrderReturnContainer orderId={id}/>
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-12">
                        <ol className="breadcrumb mb-0 pt-0">
                            <li><Link to="/orders">Danh sách vận đơn</Link></li>
                            <li className="active">Chi tiết vận đơn</li>
                        </ol>
                    </div>
                </div>
            </section>
        </div>
    )
}