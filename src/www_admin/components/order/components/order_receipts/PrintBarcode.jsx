import React from 'react'
import {hubsSelector, hubByIdSelector} from '../../..//hub/selectors'
import {getHubs} from '../../..//hub/actions'
import {connect} from 'react-redux'
import BarcodeRender from 'react-barcode'
import {getDeliveryOrderById} from '../../actions'
import {DeliveryOrderByIdSelector} from '../../selectors'
import './print.scss'
import logo from './logo.png'
import {
    DELIVERY_ORDER_FAILURE, DELIVERY_ORDER_CREATE_FAILURE, DELIVERY_ORDER_UPDATE_FAILURE,
    DELIVERY_ORDER_CREATE_SUCCESS, DELIVERY_ORDER_UPDATE_SUCCESS,
} from '../../ActionTypes'
import Alert from '../../../../../www_components/Alert'
const {ErrorToast, SuccessToast} = Alert.components;

class PrintContainer extends React.Component {
    componentWillMount() {
        let {getHubs, getDeliveryOrderById, params} = this.props;
        getDeliveryOrderById(params.orderId);
        getHubs();
    }

    renderSuccess = false;
    isPrinted = false;

    componentDidUpdate() {
        let {order, hubs} = this.props;
        this.renderSuccess = !!(order && order.owner && hubs && hubs.length > 0);
        if (this.renderSuccess && !this.isPrinted) {
            setTimeout(()=> {
                window.requestAnimationFrame(function () {
                    window.print();
                });
            }, 1500);
            this.isPrinted = true;
        }
    }

    render() {
        let props = this.props;
        if (!props.order) return null;

        let owner = this.props.order.id ?
            (this.props.order.owner ? (this.props.order.owner.fullName || this.props.order.owner.username) : '') : '';
        let pickupWarehouse = this.props.getWarehouse(props.order.pickUpWarehouseId);
        let pickupHubCode = pickupWarehouse ? pickupWarehouse.code : '';
        let deliverWarehouse = this.props.getWarehouse(props.order.deliverWarehouseId);
        let deliverHubCode = deliverWarehouse ? deliverWarehouse.code : '';

        this.renderSuccess = !!(this.props.order.owner && pickupHubCode && deliverHubCode);

        return (
            <div className="container">
                <ErrorToast
                    dispatchTypes={[DELIVERY_ORDER_FAILURE, DELIVERY_ORDER_CREATE_FAILURE, DELIVERY_ORDER_UPDATE_FAILURE]}/>

                {/*Lien 2*/}
                <div className="row mb-10">
                    <div className="col-xs-12">
                        <div className="row">
                            <div className="col-xs-4 pl-0 pr-0">
                                <img src={logo} alt="" />
                                <p className="mb-0 text-center">Liên 2</p>
                            </div>
                            <div className="col-xs-8 pl-0 text-right">
                                <BarcodeRender value={props.order.barcode || 'loading'}/>
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-xs-12 pl-0 pr-0">
                                <p className="mb-0"><span className="fw-600">Tên người gửi:</span> {owner} </p>
                                <p><span className="fw-600">Địa chỉ:</span> {props.order.deliverToFullAddress} </p>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="row mt-30">
                    <div className="col-xs-12 mt-30">
                        <div className="row">
                            <div className="col-xs-4 pl-0 pr-0">
                                <img src={logo} alt="" />
                                <p className="mb-0 text-center">Liên 1</p>
                                <h5 className="mt-0 fw-600 text-center">{pickupHubCode}/ {deliverHubCode}</h5>
                            </div>
                            <div className="col-xs-8 pr-0 text-right">
                                <BarcodeRender value={props.order.barcode || 'loading'}/>
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-xs-12 pl-0 pr-0">
                                <p className="mb-0"><span className="fw-600">Tên khách hàng:</span> {owner} </p>
                                <p className="mb-0"><span
                                    className="fw-600">Tên người nhận:</span> {props.order.deliverToContact} </p>
                                <p><span className="fw-600">Địa chỉ:</span> {props.order.deliverToFullAddress} </p>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-xs-6 text-center pl-0">
                                <p className="fw-600 mt-10">Thực thu</p>
                                <p className="text-center mt-30">----------------------</p>
                            </div>
                            <div className="col-xs-6 text-center pr-0">
                                <p className="fw-600 mt-10">Chữ ký người nhận</p>
                                <p className="text-center mt-30">----------------------</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state, props) => {
    const orderId = props.params.orderId;
    const getDeliveryOrderView = DeliveryOrderByIdSelector(state);
    const getHub = hubByIdSelector(state);
    return {
        orderId,
        order: getDeliveryOrderView(orderId),
        hubs: hubsSelector(state),
        getWarehouse: id => getHub(id)
    }
};
export default connect(mapStateToProps, {getHubs, getDeliveryOrderById})(PrintContainer);