import React from 'react';
import Validation from 'react-validation';
import * as RBT from 'react-bootstrap';
import ObjectForm from '../../../../helpers/ValidationForm.jsx';
import ReactPasswordStrength from 'react-password-strength'
import Localize from '../../../../../resources/Localize'
import zxcvbn from 'zxcvbn'

Validation.rules = ObjectForm;


export default class Form extends React.Component {
    constructor(props) {
        super(props);
    }

    componentWillMount() {
        this.setState({
            stateValue: 'ACTIVE',
            genderValue: 0,
            showModal: false,
            messagePopup: '',
            isValid: false,
            warning: null,
            username: '',
            password: '',
            email: '',
            firstName: '',
            lastName: ''
        });
    }

    componentWillReceiveProps(nextProp) {
        let errors = nextProp.error;
        let details = nextProp.details;
        if(details) {
            let msgError = '';
            if(details['codes']['email']) {
                msgError += Localize.t('email');
            } else if (details['codes']['username']) {
                msgError += Localize.t('username');
            }
            this.showModal(msgError);
        } else if (nextProp.error) {
            this.showModal(nextProp.error);
        }
    }

    showModal(msg) {
        this.setState({messagePopup: msg});
        this.setState({showModal: true});
    }

    closeModal() {
        this.setState({showModal: false});
    }

    handleChangeState(e) {
        this.setState({stateValue: e.target.value});
    }

    handeChangeGender(e) {
        this.setState({genderValue: e.target.value});
    }

    handleSetStateFormUsername(e) {
        this.setState({username: e.target.value});
    }
    handleSetStateFormEmail(e) {
        this.setState({email: e.target.value});
    }
    handleSetStateFormPassword(value) {
        this.setState({password: value});
    }
    handleSetStateFormLastName(e) {
        this.setState({lastName: e.target.value});
    }
    handleSetStateFormFirstName(e) {
        this.setState({firstName: e.target.value});
    }
    render() {
        let {submit, creatingUser} = this.props;
        let {isValid, warning} = this.state;
        let username = '', password = '', email = '', firstName = '', lastName = '', birthday = null;
        let state = this.state.stateValue, gender = this.state.genderValue;

        return (
            <div>
                <section className="content-header">
                    <h1>Tạo vận đơn</h1>
                </section>
                <div className="col-md-7 col-md-offset-2">

                    <section className="content">
                        <Validation.components.Form>
                            <div className="col-md-6">
                                <h3>Thông tin đăng nhập</h3>

                                <RBT.FormGroup controlId=''>
                                    <RBT.ControlLabel>Tên đăng nhập</RBT.ControlLabel>
                                    <Validation.components.Input
                                        onChange={(e) => {
                                            {/*username = e.target.value;*/}
                                            this.handleSetStateFormUsername(e);
                                        }}
                                        className="form-control" name='username' validations={['required']}/>
                                </RBT.FormGroup>

                                <RBT.FormGroup controlId=''>
                                    <RBT.ControlLabel>Email</RBT.ControlLabel>
                                    <Validation.components.Input
                                        onChange={(e) => {
                                            {/*email = e.target.value*/}
                                            this.handleSetStateFormEmail(e);
                                        }}
                                        className="form-control" name='email' validations={['required', 'email']}/>
                                </RBT.FormGroup>

                                <RBT.FormGroup controlId='' className="style-strength-pass">
                                    <RBT.ControlLabel>Mật khẩu</RBT.ControlLabel>
                                    <ReactPasswordStrength
                                        minLength={6}
                                        minScore={2}
                                        scoreWords={['Quá lởm', 'Đơn giản quá', 'Tạm được', 'Tốt', 'Rất tốt']}
                                        changeCallback={(e) => {
                                            this.handleSetStateFormPassword(e.password);
                                            this.setState({
                                                isValid: e.isValid,
                                                warning: zxcvbn(this.state.password).feedback.warning
                                            })
                                        }}
                                        inputProps={{className: "form-control", autoComplete: "off", type: 'password'}}
                                    />
                                    {isValid ? '' :
                                    <RBT.HelpBlock className="alert alert-warning">
                                        {warning
                                            ? Localize.t(warning)
                                            : 'Tối thiểu 6 ký tự, bao gồm chữ hoa và số'}
                                    </RBT.HelpBlock>
                                    }
                                </RBT.FormGroup>

                                <RBT.FormGroup controlId=''>
                                    <RBT.ControlLabel>Nhập lại mật khẩu</RBT.ControlLabel>
                                    <Validation.components.Input
                                        type="password" className="form-control" value=''
                                        name='passwordConfirm'
                                        validations={['required', 'password']}/>
                                </RBT.FormGroup>

                                <RBT.FormGroup controlId="formControlsSelect">
                                    <RBT.ControlLabel>Trạng thái</RBT.ControlLabel>
                                    <RBT.FormControl
                                        componentClass="select" placeholder="select"
                                        name="state"
                                        value={this.state.stateValue}
                                        onChange={this.handleChangeState.bind(this)}>
                                        <option value="ACTIVE">Kích hoạt</option>
                                        <option value="INACTIVE">Không kích hoạt</option>
                                    </RBT.FormControl>
                                </RBT.FormGroup>

                            </div>
                            <div className="col-md-6">
                                <h3>Thông tin cá nhân</h3>
                                <RBT.FormGroup controlId=''>
                                    <RBT.ControlLabel>Họ</RBT.ControlLabel>
                                    <Validation.components.Input
                                        className="form-control"
                                        name="firstName"
                                        onChange={(e) => {
                                            {/*firstName = e.target.value;*/}
                                            this.handleSetStateFormFirstName(e);
                                        }}
                                        validations={['null']}/>
                                </RBT.FormGroup>
                                <RBT.FormGroup controlId=''>
                                    <RBT.ControlLabel>Tên</RBT.ControlLabel>
                                    <Validation.components.Input
                                        className="form-control"
                                        name="lastName"
                                        onChange={(e) => {
                                            {/*lastName = e.target.value;*/}
                                            this.handleSetStateFormLastName(e);
                                        }}
                                        validations={['null']}/>
                                </RBT.FormGroup>
                                <RBT.FormGroup controlId="formControlsSelect">
                                    <RBT.ControlLabel>Giới tính</RBT.ControlLabel>
                                    <RBT.FormControl
                                        componentClass="select" placeholder="select"
                                        name="gender"
                                        value={this.state.genderValue}
                                        onChange={this.handeChangeGender.bind(this)}>
                                        <option value="0">Nam</option>
                                        <option value="1">Nữ</option>
                                    </RBT.FormControl>
                                </RBT.FormGroup>
                                <RBT.FormGroup controlId=''>
                                    <RBT.ControlLabel>Ngày sinh</RBT.ControlLabel>
                                    <Validation.components.Input
                                        type="date" className="form-control"
                                        name="birthday"
                                        onChange={(e) => {
                                            birthday = e.target.value
                                        }}
                                        validations={['null']}/>
                                </RBT.FormGroup>
                            </div>
                            <div className="col-md-12 text-center">
                                <RBT.FormGroup>
                                    <Validation.components.Button
                                        disabled={creatingUser}
                                        className="btn btn-primary"
                                        onClick={(e) => {
                                            e.preventDefault();
                                            submit({
                                                username: this.state.username,
                                                password: this.state.password,
                                                isValid,
                                                email: this.state.email,
                                                firstName: this.state.firstName,
                                                lastName: this.state.lastName,
                                                state,
                                                birthday, needChangePass: 0, emailVerified: 0, gender, realm: ''
                                            })
                                        }}
                                    >
                                       TẠO NHÂN VIÊN
                                    </Validation.components.Button>
                                </RBT.FormGroup>
                            </div>
                        </Validation.components.Form>
                        {/* Popup thông báo */}
                    </section>
                    <RBT.Modal show={this.state.showModal} onHide={this.closeModal.bind(this)}>
                        <RBT.Modal.Header closeButton>
                            <RBT.Modal.Title>Thông báo</RBT.Modal.Title>
                        </RBT.Modal.Header>
                        <RBT.Modal.Body>
                            {this.state.messagePopup}
                        </RBT.Modal.Body>
                        <RBT.Modal.Footer>
                            <RBT.Button onClick={this.closeModal.bind(this)}>ĐÓNG</RBT.Button>
                        </RBT.Modal.Footer>
                    </RBT.Modal>
                </div>
            </div>

        );
    }
}