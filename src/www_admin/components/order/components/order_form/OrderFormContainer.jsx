import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import Form from './OrderForm';
import {createUser, getUserById} from '../../actions'
import {push} from 'react-router-redux'
import {bindActionCreators} from 'redux'
import DocumentTitle from '../../../../../www_components/Common/DocumentTitle'
import {toastr} from 'react-redux-toastr'
import {isAllow} from '../../reducer111'
import {PERMISSION_USER_CREATE} from '../../constants'

class OrderFormContainer extends Component {
    constructor(props) {
        super(props);
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.userIdCreated) {
            nextProps.onRedirect(`/users/detail/${nextProps.userIdCreated}`);
        }
    }


    render() {
        return (
            <DocumentTitle title="Tạo nhân viên">
                <Form {...this.props}
                      submit={this.submit.bind(this)}
                />
            </DocumentTitle>
        );
    }

    submit(user) {
        if (!user.isValid) {
            toastr.warning('Mật khẩu quá lởm, vui lòng thử một mật khẩu khác mạnh hơn');
        } else {
            delete user['isValid'];
            this.props.createUser(user);
        }
    }
}

const mapStateToProps = (state, props) => {
    return {
        isAllow: (resource) => props.isAllow(resource),
        error: state.user.error,
    }
};

const mapDispatchToProps = (dispatch) => ({
    ...bindActionCreators({
        createUser, getUserById
    }, dispatch),
    onRedirect: (path) => {
        dispatch(push(path))
    },
    dispatch
});

export default connect(
    mapStateToProps, mapDispatchToProps
)(OrderFormContainer);

