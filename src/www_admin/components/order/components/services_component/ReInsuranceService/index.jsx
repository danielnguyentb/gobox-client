import React, {PropTypes} from 'react'
import * as RBT from 'react-bootstrap'

export class ReInsuranceService extends React.Component {
    render() {
        let {insuranceService, insuranceValue} = this.props;
        return (
            <div className="row">
                <div className="col-md-4 pr-0">
                    <RBT.Checkbox
                        {...insuranceService.input}
                        checked={insuranceService.input.value}
                    >Bảo hiểm?</RBT.Checkbox>
                    {insuranceService.meta.touched && insuranceService.meta.error &&
                    <span className="error">{insuranceService.meta.error}</span>}
                </div>
                <div className="col-md-8">
                    <div className="pt-input-notes width-100">
                        <RBT.FormGroup
                            validationState={insuranceValue.meta.touched && insuranceValue.meta.error ? 'error' : 'success'}>
                            <input
                                {...insuranceValue.input}
                                type="number" className="form-control"
                                placeholder="Giá trị hàng"
                                value={insuranceService.input.value === false ? '' : insuranceValue.input.value}
                                disabled={insuranceService.input.value === false}
                            />
                            {insuranceValue.meta.touched && ((insuranceValue.meta.error &&
                            <span
                                className="help-block">{insuranceValue.meta.error}</span>) || (insuranceValue.meta.warning &&
                            <span>{insuranceValue.meta.warning}</span>))}
                        </RBT.FormGroup>
                        <span className="currency-symbol">VNĐ</span>
                    </div>
                </div>
            </div>
        )
    }
}

