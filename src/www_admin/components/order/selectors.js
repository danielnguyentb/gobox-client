/**
 * Created by Tester-Ali on 07-11-2016.
 */
import {createSelectorCreator, defaultMemoize, createSelector} from 'reselect'
import lodash from 'lodash'
import {
    NAME,
    Status as OrderStatus,
    WarehouseStatusDetail,
    endingStatus,
    statusLabel,
    statusTitle,
    LOG_OBJECT,
    ContextTypes
} from './constants'
import {hydrate} from './helper'
import {hubSelector} from '../hub/selectors'

const createDeepEqualSelector = createSelectorCreator(
    defaultMemoize,
    lodash.isEqual
);

export const orderSelector = state => state[NAME];


/*******FEATURE************/

export const featuresSelector = createSelector(
    orderSelector,
    order => Object.values(order.featureById)
);

export const FeaturesByOrderIdSelector = createDeepEqualSelector(
    featuresSelector,
    features => lodash.memoize(
        orderId => features.filter(t => t['orderId'] == orderId)
    )
);

export const insurancesSelector = createSelector(
    featuresSelector,
    features => features.filter(t => t['featureId'] == 1 && !t['isDeleted'])
);

export const InsuranceByOrderIdSelector = createDeepEqualSelector(
    insurancesSelector,
    insurances => lodash.memoize(
        orderId => insurances.filter(t => t['orderId'] == orderId).pop()
    )
);


export const statusSelector = createDeepEqualSelector(
    orderSelector,
    order => order.status
);

/*******DeliveryOrder******/

const hydrateDeliveryOrderView = (order, hubs, status, getFeatures, getInsurance) => {
    if (!order) return null;
    order = hydrate(order);
    order['detailPath'] = `/orders/detail/${order.id}`;
    order.pickUpWarehouse = hubs.byId[order.pickUpWarehouseId];
    order.deliverWarehouse = hubs.byId[order.deliverWarehouseId];
    order.pickUpWarehouseCode = order.pickUpWarehouse ? order.pickUpWarehouse['code'] : '--';
    order.deliverWarehouseCode = order.deliverWarehouse ? order.deliverWarehouse['code'] : '--';
    order.features = getFeatures(order.id);
    order.insurance =  getInsurance(order.id);
    if (order.currentWarehouseStatus && WarehouseStatusDetail[order.currentWarehouseStatus]) {
        let metadata = WarehouseStatusDetail[order.currentWarehouseStatus];
        order.currentWarehouse = {
            ...hubs.byId[order['currentWarehouseId']],
            label: metadata['label'],
            tooltip: metadata['label'],
            className: metadata['className']
        };
    }
    const code = order.status;
    order.statusDetail = {
        code,
        count: 0,
        label: statusLabel.hasOwnProperty(code) ? statusLabel[code] : '',
        title: statusTitle.hasOwnProperty(code) ? statusTitle[code] : code,
        ...status.byId[order.status],
    };
    order['weightKg'] = order['weight'] != null ? parseInt(order['weight']) / 1000 : null;
    if (order.deliverTries > 0) {
        order.statusDetail.title += ` (+${order.deliverTries})`
    }
    order.isEndingStatus = endingStatus.includes(order.status);
    order.inEndingStatus = endingStatus.includes(order.status);
    return order;
};

export const DeliveryOrderByIdSelector = createDeepEqualSelector(
    [orderSelector, hubSelector, statusSelector, InsuranceByOrderIdSelector, FeaturesByOrderIdSelector],
    (order, hubs, status, getInsurance, getFeatures) => lodash.memoize(
        id => hydrateDeliveryOrderView(order.byId[id], hubs, status, getFeatures, getInsurance)
    )
);

export const ordersSelector = createDeepEqualSelector(
    [orderSelector, DeliveryOrderByIdSelector],
    (order, getOrder) => Object.keys(order.byId).map(id => getOrder(id))
);

export const DeliveryOrderByCodeSelector = createDeepEqualSelector(
    ordersSelector,
    orders => lodash.memoize(
        code => orders.filter(t => t.barcode == code).pop()
    )
);

export const metadataSelector = createDeepEqualSelector(
    orderSelector,
    order => order.metadata
);

export const getPaginateDeliveryOrders = createSelector(
    orderSelector,
    order => order.paginateIds.map(id => order.byId[id])
);

export const DeliveryOrdersByIdsSelector = createDeepEqualSelector(
    DeliveryOrderByIdSelector,
    (getOrder) => lodash.memoize(
        ids => ids.map(id => getOrder(id))
    )
);

export const DeliveryOrderViewByRequestIdSelector = createDeepEqualSelector(
    ordersSelector,
    orders => lodash.memoize(
        requestId => orders.filter(t => t.requestId == requestId)
    )
);

export const getPaginateDeliveryOrdersView = createSelector(
    [getPaginateDeliveryOrders, hubSelector, statusSelector, InsuranceByOrderIdSelector, FeaturesByOrderIdSelector],
    (orders, hubs, status, getFeatures, getInsurance) => orders.map(order => hydrateDeliveryOrderView(order, hubs, status, getFeatures, getInsurance))
);

/********LOGS***********/

export const logsSelector = createSelector(
    orderSelector,
    order => Object.values(order.logById)
);

export const LogsByIdSelector = createSelector(
    logsSelector,
    logs => lodash.memoize(
        orderId => logs.filter(log =>
            [LOG_OBJECT.DeliveryOrder, LOG_OBJECT.CustomerDeliveryOrder].includes(log.object)
            &&  log.objectId == orderId
        )
    )
);

export const logsChatSelector = createSelector(
    orderSelector,
    order => Object.values(order.logsChatById)
);

export const LogsChatByIdSelector = createSelector(
    logsChatSelector,
    logs => lodash.memoize(
        orderId => logs.filter(log => log )
    )
);


/******STATUS********/
const statusHydrate = (code, value, statusActivated = []) => {
    return {
        count: 0,
        ...value,
        code,
        label: statusLabel.hasOwnProperty(code) ? statusLabel[code] : '',
        title: statusTitle.hasOwnProperty(code) ? statusTitle[code] : code,
        activated: statusActivated.includes(code)
    }
};
export const countStatusSelector = createDeepEqualSelector(
    orderSelector,
    order => lodash.memoize(
        statusActivated => {
            const byId = order.status.byId;
            const all = Object.keys(OrderStatus).map(code => statusHydrate(code, byId[code], statusActivated));
            return [
                {
                    code: '',
                    activated: statusActivated.length === 0,
                    label: 'primary',
                    title: 'Tất cả',
                    count: all.reduce((p, s) => p + s.count, 0)
                },
                ...all
            ];
        }
    )
);