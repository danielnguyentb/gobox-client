import {
    DELIVERY_ORDER_STATUS_SUCCESS,
} from '../ActionTypes'

import {combineReducers} from 'redux'

const byId = (state = {}, action) => {
    const {response} = action;
    let copy = {...state};
    if (action.type == DELIVERY_ORDER_STATUS_SUCCESS) {
        Object.keys(response).map(key => {
            copy[key] = {
                count: response[key]
            }
        });
        return copy;
    }
    return state;
};

export default combineReducers({
    byId,
});