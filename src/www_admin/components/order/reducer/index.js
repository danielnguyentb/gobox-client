import {
    DELIVERY_ORDER_SUCCESS,
    DELIVERY_ORDERS_SUCCESS,
    DELIVERY_ORDER_CREATE_SUCCESS,
    DELIVERY_ORDER_UPDATE_REQUEST,
    DELIVERY_ORDER_UPDATE_SUCCESS,
    DELIVERY_ORDER_UPDATE_FAILURE,
    DELIVERY_ORDER_STATUS_SUCCESS,
    DELIVERY_ORDER_EXPORT_EXCEL,
    DELIVERY_ORDER_CHANGE_TYPE_SUCCESS,
    DELIVERY_ORDER_CHANGE_FEE_SUCCESS,
    DELIVERY_ORDER_RELATE_CREATE,
    DELIVERY_ORDER_RELATES,
    DELIVERY_ORDER_LOGS_SUCCESS,
    CHANGE_ORDER_FEATURE_SUCCESS,
    DELIVERY_ORDER_CREATE_CHAT,
    DELIVERY_ORDER_CHAT
} from '../ActionTypes'

import {combineReducers} from 'redux'
import status from './status'
import {GET_TRANSPORT_REQUEST_DETAIL} from '../../hub/ActionTypes';
import {TASK_BATCHES_SUCCESS} from '../../task_batches/ActionTypes'
import * as constants from '../constants'
import Localize from '../../../../resources/Localize'

import moment from 'moment'
moment.locale('vi');

export * as fromStatus from './status'

let isLoad = false;

const order = order => {
    delete order['features'];
    return order;
};

const byId = (state = {}, action) => {
    let {response, scope} = action;
    let copy = {...state};
    switch (action.type) {
        case DELIVERY_ORDERS_SUCCESS:
            response.result.map(value => {
                copy[value.id] = order(value);
            });
            return copy;
        case DELIVERY_ORDER_SUCCESS:
        case DELIVERY_ORDER_CREATE_SUCCESS:
        case DELIVERY_ORDER_UPDATE_SUCCESS:
        case DELIVERY_ORDER_STATUS_SUCCESS:
            //merge response & scope & state
            if (response && response.id) copy[response.id] = order({...copy[response.id], ...response});
            if (scope && scope.id) copy[scope.id] = order({...copy[scope.id], ...scope});
            return copy;
        case DELIVERY_ORDER_CHANGE_TYPE_SUCCESS:
        case DELIVERY_ORDER_CHANGE_FEE_SUCCESS:
            copy[response.id] = order({...copy[response.id], ...response});
            return copy;
        case GET_TRANSPORT_REQUEST_DETAIL.SUCCESS:
            response.orders.map(t => {
                t['requestId'] = response.id;
                copy[t.id] = order(t);
            });
            return copy;
        case TASK_BATCHES_SUCCESS:
            const {batchData} = response;
            batchData.map(t => {
                if (t.objectType == constants.TYPE) {
                    copy[t.objectId] = {
                        ...copy[t.objectId],
                        ...t['object']
                    }
                }
            });
            return copy;
        case DELIVERY_ORDER_EXPORT_EXCEL.SUCCESS:
            if (!isLoad) {
                isLoad = true;
                setTimeout(()=> {
                    var csvContent = "data:text/csv,\ufeff" + response.orders;
                    var encodedUri = encodeURI(csvContent);
                    //window.open(encodedUri);
                    var link = document.createElement("a");
                    link.setAttribute("href", encodedUri);
                    link.setAttribute("download", "vandon" + moment().format('YMDHMSS') + ".csv");
                    document.body.appendChild(link); // Required for FF
                    link.click();

                    isLoad = false;
                }, 1000);
            }

    }
    return state;
};

const featureById = (state = {}, action) => {
    const {response} = action;
    const copy = {...state};
    switch (action.type) {
        case DELIVERY_ORDERS_SUCCESS:
            response.result.map(t => {
                if (t['features']) {
                    t['features'].map(v => {
                        copy[v.id] = v;
                    });
                }
            });
            break;
        case DELIVERY_ORDER_SUCCESS:
            if (response['features']) {
                response['features'].map(v => {
                    copy[v.id] = v;
                });
            }
            break;
        case CHANGE_ORDER_FEATURE_SUCCESS:
            copy[response.id] = response;
            break;
    }
    return copy;
};

const logsChatById = (state = {}, action) => {
    const {response} = action;
    const copy = {...state};
    switch (action.type) {
        case DELIVERY_ORDER_CHAT.SUCCESS:
            response.map(t => {
                copy[t.id] = t;
            });
            return copy;
    }
    return state;
};

const metadata = (state = {}, action) => {
    switch (action.type) {
        case DELIVERY_ORDERS_SUCCESS:
            return action.response.metadata;
    }
    return state;
};

const logById = (state = {}, action) => {
    const {response} = action;
    const copy = {...state};
    switch (action.type) {
        case DELIVERY_ORDER_LOGS_SUCCESS:
            response.map(t => {
                copy[t.id] = t;
            });
            return copy;
    }
    return state;
};

const paginateIds = (state = [], action) => {
    const {response} = action;
    if (action.type === DELIVERY_ORDERS_SUCCESS && response) {
        return response.result.map(t => t.id);
    }
    return state;
};

const relatedOrders = (state = [], action) => {
    const {response} = action;
    if (action.type === DELIVERY_ORDER_RELATES.SUCCESS) {
        return response;
    }
    return state;
};

const updatingOrder = (state = false, action) => {
    switch (action.type){
        case DELIVERY_ORDER_UPDATE_REQUEST:
            return true;
        case DELIVERY_ORDER_UPDATE_SUCCESS:
        case DELIVERY_ORDER_UPDATE_FAILURE:
            return false;
    }
    return state;
};

export const getRelatedOrdersArr = (state) => {
    return state.order.relatedOrders.length ? state.order.relatedOrders : null;
};

export default combineReducers({
    byId, metadata, status, paginateIds, relatedOrders, logById, featureById, updatingOrder, logsChatById,
    error: (state = null, {error}) => error ? Localize.t(error) : null,
    createdOrderRelateSuccess: (state, action) => action.type == DELIVERY_ORDER_RELATE_CREATE.SUCCESS ? action.response : null,
    postChatSuccess: (state, {type}) => type == DELIVERY_ORDER_CREATE_CHAT.SUCCESS
});
