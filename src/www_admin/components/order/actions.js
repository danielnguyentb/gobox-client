import CustomerDeliveryOrders from './api'
import {generateQuery, generateQueryOrderIncludeOwner} from './helper'
import * as ActionTypes from './ActionTypes'
import {CALL_API} from '../../constants'

export const createDeliveryOrders = (data) => dispatch => dispatch(CustomerDeliveryOrders.create(data));

export const updateDeliveryOrders = (id, data) => dispatch => dispatch(CustomerDeliveryOrders.update(id, data));

export const getDeliveryOrderById = (id) => dispatch => dispatch(CustomerDeliveryOrders.getDeliveryOrderById(id, generateQueryOrderIncludeOwner()));

export const getDeliveryOrders = (filter) => dispatch => dispatch(CustomerDeliveryOrders.getDeliveryOrders(generateQuery(filter)));

export const getOrderStatus = () => dispatch => dispatch(CustomerDeliveryOrders.getCountByStatus());

export const changeStatus = (id, status) => dispatch => dispatch(CustomerDeliveryOrders.changeStatus(id, status));

export const changeType = (id, type) => dispatch => dispatch(CustomerDeliveryOrders.changeType(id, type));

export const createRelatedOrders = (id, data) => dispatch => dispatch(CustomerDeliveryOrders.createRelatedOrders(id, data));

export const getRelatedOrders = (id) => dispatch => dispatch(CustomerDeliveryOrders.getRelatedOrders(id));

export const changeFee = (id, fee) => dispatch => dispatch(CustomerDeliveryOrders.changeFee(id, fee));

export const getOrderExportFileExcel = (filter) => dispatch => dispatch(CustomerDeliveryOrders.getOrderExportFileExcel(generateQuery(filter)));

export const uploadExcel = (data) => dispatch => dispatch(CustomerDeliveryOrders.uploadExcel(data));

export const postChat = (id, message, scope) => dispatch => dispatch(CustomerDeliveryOrders.postChat(id, message, scope));

export const getChatHistories = (id) => dispatch => dispatch(CustomerDeliveryOrders.getChatHistories(id, {
    include: {relation: 'actionBy'}
}));

export const fetchLogHistory = (orderId) => dispatch => dispatch(CustomerDeliveryOrders.fetchLogHistory(orderId, {
    include: {relation: 'actionBy'}
}));


export const fetchChangeFeaturesSaga = (orderId, code, data) => {
    return {
        type: CALL_API,
        payload: {
            types: [ActionTypes.CHANGE_ORDER_FEATURE_REQUEST, ActionTypes.CHANGE_ORDER_FEATURE_SUCCESS, ActionTypes.CHANGE_ORDER_FEATURE_FAILURE],
            endpoint: `/CustomerDeliveryOrders/${orderId}/features/${code}`,
            body: JSON.stringify(data),
            form: 'ChangeInsuranceForm',
            method: 'POST'
        }
    }
};

export const fetchDeliverOrdersSaga = filter => {
    return {
        type: CALL_API,
        payload: {
            types: [ActionTypes.DELIVERY_ORDERS_REQUEST, ActionTypes.DELIVERY_ORDERS_SUCCESS, ActionTypes.DELIVERY_ORDERS_FAILURE],
            endpoint: `/CustomerDeliveryOrders?filter=${JSON.stringify(generateQuery(filter))}`,
            method: 'GET',
        }
    }
};
