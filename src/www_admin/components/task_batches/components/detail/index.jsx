import React from 'react'
import {connect} from 'react-redux'
import Detail from './Detail'

export class DetailContainer extends React.Component {
    render() {
        const {batchId} = this.props.params || {};
        return (
            <Detail batchId={+batchId}/>
        )
    }
}

export default connect()(DetailContainer)
