import React from 'react'
import DocumentTitle from '../../../../../../www_components/DocumentTitle'

class Header extends React.Component{
    render(){
        return(
            <div>
                <DocumentTitle title="Chi tiết nhóm nhiệm vụ"/>
                <section className="content-header">
                    <div className="row">
                        <div className="col-md-12 col-xs-12">
                            <h3 className="mt-0 mb-0">Chi tiết nhóm nhiệm vụ</h3>
                        </div>
                    </div>
                </section>
            </div>
        )
    }
}
export default (Header)