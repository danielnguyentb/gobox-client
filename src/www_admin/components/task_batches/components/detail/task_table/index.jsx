import React, {PropTypes} from 'react'
import {connect} from 'react-redux'
import TaskTable from './TaskTable'
import * as selectors from '../../../selectors'
import * as actions from '../../../actions'


export class TaskTableContainer extends React.Component {
    static propTypes = {
        batchId: PropTypes.number.isRequired
    };

    componentDidMount() {
        const {dispatch, batchId} = this.props;
        dispatch(actions.fetchTaskBatches(batchId))
    }


    render() {
        const {taskBatches} = this.props;
        return (
            <TaskTable
                taskBatches={taskBatches}
            />
        )
    }
}

const mapStateToProp = (state, props) => {
    const getBatch = selectors.TaskBatchByIdSelector(state);
    return {
        taskBatches: getBatch(props.batchId),
    }
};

export default connect(mapStateToProp)(TaskTableContainer)