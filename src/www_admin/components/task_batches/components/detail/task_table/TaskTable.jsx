import React from 'react'
import {Link} from '../../../../../../www_components'
import {formatFullTime} from '../../../../../helpers/format'
import {connect} from 'react-redux'
import * as userSelectors from '../../../../user/selectors'

export const TaskRow = ({order, objectCode, deadline, note, type, status, errors}) => {
    const {detailPath, barcode, deliverToFullAddress, statusDetail = {}, pickUpWarehouse = {}, deliverWarehouse = {}} = order || {};
    return (
        <div className="row task-batches-items">
            <div className="col-md-6 col-xs-6 pl-5">
                <div className="row">
                    <div className="col-md-3 col-xs-6">
                        {detailPath ? <Link to={detailPath} className="fw-600">{barcode || '--'}</Link> : objectCode}<br/>
                        <span className={`lbl-style label label-${statusDetail.label}`}>{statusDetail.title || '--'}</span>
                    </div>
                    <div className="col-md-6 hidden-xs">
                        {deliverToFullAddress || '--'}
                    </div>
                    <div className="col-md-3 col-xs-6">
                        {pickUpWarehouse.code || '--'} / {deliverWarehouse.code || '--'}
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-12 col-xs-12 pt-5">
                        <span className="text-danger">{errors}</span>
                    </div>
                </div>
            </div>
            <div className="col-md-6 col-xs-6">
                <div className="row">
                    <div className="col-md-5 hidden-xs">{note}</div>
                    <div className="col-md-4 col-xs-8">{formatFullTime(deadline)}</div>
                    <div className="col-md-3 text-center col-xs-4">
                        { errors ? <i className="glyphicon glyphicon-remove fs-16"/>
                            : <i className="glyphicon glyphicon-ok fs-16"/> }
                    </div>
                </div>
            </div>
        </div>
    )
};

export const PickerContainer = connect((state, props) => {
    const getPicker = userSelectors.UserByIdSelector(state);
    return {
        picker: getPicker(props.assignedTo) || {}
    }
})(({picker: {fullName, username, detailPath}}) => {
    if(!username) return <span>Người nhận nhiệm vụ: ...</span>;
    return (
        <span>
            Người nhận nhiệm vụ: {fullName || '...'}&nbsp;/&nbsp;
            {detailPath ? <Link to={detailPath}>@{username}</Link>
                : '@' + username}
        </span>
    )
});

export default ({taskBatches}) => {
    const {batchData} = taskBatches || {};
    const {assignedTo, typeTitle} = Array.isArray(batchData) ? batchData[0] : {};
    return (
        <div className="row">
            <div className="col-md-12">
                <div className="row task-batches-title">
                    <div className="col-md-6 col-xs-6 pl-5">
                        <div className="row">
                            <div className="col-md-3 col-xs-6">Vận đơn</div>
                            <div className="col-md-6 hidden-xs">Đ/C giao</div>
                            <div className="col-md-3 col-xs-6">Hub</div>
                        </div>
                    </div>
                    <div className="col-md-6 col-xs-6">
                        <div className="row">
                            <div className="col-md-5 hidden-xs">Ghi chú NV</div>
                            <div className="col-md-4 col-xs-8">Hạn</div>
                            <div className="col-md-3 col-xs-4 text-center">Kết quả</div>
                        </div>
                    </div>
                </div>
                {Array.isArray(batchData) ? batchData.map(t => {
                    return <TaskRow key={t.objectCode} {...t}/>
                }) : null}
                <div className="row">
                    <div className="col-md-12 col-xs-12 text-right pt-10">
                        <PickerContainer assignedTo={assignedTo}/> - {typeTitle || '...'}
                    </div>
                </div>
            </div>
        </div>
    )
}