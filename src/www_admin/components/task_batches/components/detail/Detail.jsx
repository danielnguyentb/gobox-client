import React from 'react'
import Header from './header'
import TaskTable from './task_table'

export default (props) => (
    <div>
        <Header />
        <section className="content">
            <div className="box">
                <div className="row">
                    <div className="col-md-offset-1 col-md-10 col-xs-12">
                        <TaskTable {...props} />
                    </div>
                </div>
            </div>
        </section>
    </div>
)