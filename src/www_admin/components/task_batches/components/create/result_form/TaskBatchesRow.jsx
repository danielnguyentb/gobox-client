import React from 'react'
import {Field, Fields} from 'redux-form'
import {connect} from 'react-redux'
import * as orders from '../../../../order'
import {FormGroup} from 'react-bootstrap'
import {Link, ReDatePicker} from '../../../../../../www_components'
import Loading from 'react-loading'
import {formatFullTime} from '../../../../../helpers/format'

export const renderDateField = ({input, meta: {touched, error}, disabled}) => {
    return (
        <FormGroup validationState={touched && error ? 'error' : 'success'}>
            {disabled ? formatFullTime(input.value) :
                <ReDatePicker
                    defaultValue={input.value}
                    placeholder="Hạn"
                    onChange={e => {
                        input.onChange(e)
                    }}/>
            }
            {touched && ((error && <span className="help-block">{error}</span>))}
        </FormGroup>
    )
};

export const renderAction = ({input: {value}, onRemove}) => {
    if (value === 'success') {
        return <i className="glyphicon glyphicon-ok fs-16"/>
    } else if (value === 'failure') {
        return <i className="glyphicon glyphicon-remove fs-16"/>
    } else if (value === 'pending') {
        return (
            <div style={{margin: '0 auto', width: 20}}>
                <Loading type="spinningBubbles" color="#000" height={20} width={20} delay={100}/>
            </div>
        );
    }
    return (
        <button
            onClick={onRemove}
            type="button"
            className="btn btn-xs btn-danger mr-5"
            data-tip="Xóa khỏi danh sách">
            <i className="ti-close"></i>
        </button>
    )
};

export const renderErrorMessage = ({input: {value}}) => {
    if (!value) return null;
    return (
        <span className="text-danger">{value}</span>
    )
};

const TaskBatchesRow = ({order, task, onRemove, disabled}) => {
    const {id, barcode, statusDetail = {}, deliverToFullAddress, pickUpWarehouse = {}, deliverWarehouse = {}} = order || {};
    return (
        <tr>
            <td className="text-center">
                <Field name={`${task}.objectId`} component="input" type="number" value={id} hidden={true}/>
                <Field name={`${task}.checked`} component="input" className="checkbox" type="checkbox"/>
            </td>
            <td>
                <Link to={`orders/detail/${id}`} className="fw-600">{barcode}</Link><br/>
                <span className={`lbl-style label label-${statusDetail.label}`}>{statusDetail.title}</span>
            </td>
            <td>
                <p>Đ/C giao: {deliverToFullAddress || '--'}</p>
                <Field name={`${task}.error`} component={renderErrorMessage}/>
            </td>
            <td>{`${pickUpWarehouse.code || '--'} / ${deliverWarehouse.code || '--'}`}</td>
            {/*hub giao/hub nhận*/}
            <td>
                <Field
                    name={`${task}.note`} component="input" disabled={disabled} type="text"
                    className="form-control note-task"
                    placeholder="Ghi chú NV"/>
            </td>
            <td className="date-picker-right">
                <Field name={`${task}.deliverBefore`} disabled={disabled} component={renderDateField}
                       placeholder="Hạn"/>
            </td>
            <td className="text-center">
                <Field name={`${task}.state`} onRemove={onRemove} component={renderAction}/>
            </td>
        </tr>
    )
};
const mapStateToProps = (state, props) => {
    const order = orders.selectors.DeliveryOrderByCodeSelector(state)(props['objectCode']);
    return {
        order
    }
};
export default connect(mapStateToProps)(TaskBatchesRow);
