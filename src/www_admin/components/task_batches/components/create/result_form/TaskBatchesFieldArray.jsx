import React from 'react'
import TaskBatchesRow from './TaskBatchesRow'
import ReactTooltip from 'react-tooltip'
import {connect} from 'react-redux'
import * as t from '../../../ActionTypes'
import './../create-tasks.scss'

class TaskBatchesFieldArray extends React.Component {
    componentDidUpdate() {
        ReactTooltip.rebuild()
    }

    onToggleTasks(e) {
        const {checked} = e.target;
        const {dispatch} = this.props;
        dispatch({type: t.TOGGLE_ALL_TASKS, payload: checked});
    }

    render() {
        const {fields, meta: {error}, formValues, disabled} = this.props;
        const checkedAll = formValues && Array.isArray(formValues.tasks) ? formValues.tasks.every(t => t.checked) : false;
        return (
            <div className="col-md-offset-1 col-md-10 col-xs-12">
                <ReactTooltip />
                <table className="table table-hover tbl-create-tasks">
                    <thead>
                    <tr>
                        <th className="text-center">
                            <input className="checkbox" onChange={::this.onToggleTasks} checked={checkedAll}
                                   type="checkbox"/>
                        </th>
                        <th className="width-10">Vận đơn</th>
                        <th>Đ/C giao</th>
                        <th className="width-10">Hub</th>
                        <th className="width-20">Ghi chú NV</th>
                        <th className="width-20">Hạn</th>
                        <th className="width-10">Thao tác</th>
                    </tr>
                    </thead>
                    <tbody>
                    {fields.map((task, idx) => {
                        const {objectCode} = fields.get(idx);
                        return <TaskBatchesRow
                            index={idx}
                            disabled={disabled}
                            onRemove={e => fields.remove(idx)}
                            key={objectCode}
                            objectCode={objectCode}
                            task={task}/>;
                    })}
                    </tbody>
                </table>
                {error && <div className="alert alert-danger">{error}</div>}
            </div>
        )
    }
}

export default connect()(TaskBatchesFieldArray)