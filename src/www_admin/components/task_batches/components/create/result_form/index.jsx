import React from 'react'
import CreateTaskBatchesForm from './TaskBatchesForm'
import {connect} from 'react-redux'
import * as users from '../../../../user'
import {getFormValues, isDirty} from 'redux-form'
import {isEqual} from 'lodash'
import * as constants from '../../../constants'
import * as t from '../../../ActionTypes'
import {toastr} from '../../../../../../www_components/Toastr2'

class TaskBatchesFormContainer extends React.Component {
    componentDidMount() {
        const {dispatch} = this.props;
        dispatch({type: t.INIT});
    }

    componentDidUpdate() {
        const {formValues, isDirty} = this.props;
        try {
            const tasks = formValues['tasks'];
            const cache = JSON.parse(localStorage.getItem(constants.TASK_BATCHES_KEY));
            if (!isEqual(cache, tasks) && isDirty) {
                localStorage.setItem(constants.TASK_BATCHES_KEY, JSON.stringify(tasks));
            }
        } catch (e) {
            console.error(e);
        }
    }

    handleSubmit(payload) {
        const {dispatch} = this.props;
        if (!payload.tasks || payload.tasks.length == 0) {
            toastr.error('Bạn chưa quét tạo nhiệm vụ.');
            return;
        } else {
            const checkedTasks = payload.tasks.filter(t => t.checked);
            if (checkedTasks.length === 0) {
                toastr.error('Vui lòng chọn ít nhất một vận đơn để có thể tạo nhiệm vụ');
                return;
            }
        }
        dispatch({
            type: t.CREATE_TASK_BATCHES_REQUEST,
            payload
        });
    }

    render() {
        const {pickers, formValues} = this.props;
        return (
            <CreateTaskBatchesForm
                onSubmit={::this.handleSubmit}
                formValues={formValues}
                pickers={pickers}
            />
        )
    }
}
const mapStateToProps = (state, props) => {
    const getPickers = users.selectors.PickersByHubIdSelector(state);
    return {
        pickers: getPickers(props.hubId),
        formValues: getFormValues(constants.CreateTaskBatchesForm)(state),
        isDirty: isDirty(constants.CreateTaskBatchesForm)(state)
    }
};
export default connect(mapStateToProps)(TaskBatchesFormContainer)