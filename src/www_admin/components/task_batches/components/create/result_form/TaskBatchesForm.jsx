import React from 'react'
import {Field, FieldArray, reduxForm} from 'redux-form'
import {DateField, Calendar} from 'react-date-picker'
import TaskBatchesFieldArray from './TaskBatchesFieldArray'
import * as tasks from '../../../../tasks'
import {FormGroup} from 'react-bootstrap'
import * as constants from '../../../constants'

export const renderTypeField = ({input, meta : {touched, error}}) => {
    return (
        <FormGroup validationState={touched && error ? 'error' : 'success'}>
            <select {...input} className="form-control select">
                <option value="">Loại nhiệm vụ</option>
                <option value={tasks.constants.TYPE_TASKS.DELIVER}>
                    {tasks.constants.typeTasksTitle[tasks.constants.TYPE_TASKS.DELIVER]}
                </option>
            </select>
            {touched && ((error && <span className="help-block">{error}</span>))}
        </FormGroup>
    )
};

export const renderPickupField = ({input, meta : {touched, error}, pickers}) => {
    return (
        <FormGroup validationState={touched && error ? 'error' : 'success'}>
            <select {...input} className="form-control select">
                <option value="">Người nhận nhiệm vụ</option>
                {Object.values(pickers).map(picker =>
                    <option key={picker.id} value={picker.id}>{picker.username}</option>
                )}
            </select>
            {touched && ((error && <span className="help-block">{error}</span>))}
        </FormGroup>
    )
};

class TaskBatchesForm extends React.Component {
    render() {
        const {pickers, handleSubmit, pristine, error, submitting, formValues} = this.props;
        return (
            <form onSubmit={handleSubmit}>
                <div className="row">
                    <FieldArray name="tasks" disabled={submitting} component={TaskBatchesFieldArray} formValues={formValues}/>
                </div>
                <div className="row">
                    <div className="col-md-offset-1 col-md-10 col-xs-12 pt-5">
                        <div className="pull-right ml-10">
                            <button
                                disabled={submitting}
                                type="button"
                                onClick={handleSubmit}
                                className="btn btn-primary">
                                {submitting ? 'ĐANG TẠO NHIỆM VỤ' : 'TẠO NHIỆM VỤ'}
                            </button>
                        </div>
                        <div className="select-assign pull-right">
                            <Field name="assignedTo" component={renderPickupField} pickers={pickers}/>
                        </div>
                        <div className="select-type-task pull-right mr-10">
                            <Field name="type" component={renderTypeField}/>
                        </div>
                        <span className="pull-right order-checked">Tạo nhiệm vụ với các mã đã chọn: </span>
                    </div>
                    <div className="col-md-offset-2 col-md-8 col-xs-12">
                        {error && <div className="alert alert-danger">{error}</div>}
                    </div>
                </div>
            </form>
        )
    }
}

const validate = values => {
    const errors = {};
    if (Object.keys(values).length == 0) return {};
    if (!values.type) {
        errors.type = 'Loại nhiệm vụ không được để trống.'
    }
    if (!values.assignedTo) {
        errors.assignedTo = 'Người nhận nhiệm vụ không được để trống.'
    }
    return errors;
};
export default reduxForm({
    form: constants.CreateTaskBatchesForm,
    fields: ['tasks[].checked', 'tasks[].objectCode'],
    validate
})(TaskBatchesForm)