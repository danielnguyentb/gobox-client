import React from 'react'
import Header from './header/HeaderContainer'
import TaskBatchesFormContainer from './result_form'
import ScanOrderContainer from './scan_order/ScanOrderContainer'

class CreateTaskBatches extends React.Component {
    render() {
        const {hubId}= this.props;
        return (
            <div>
                <Header />
                <section className="content">
                    <div className="box">
                        <ScanOrderContainer hubId={hubId}/>
                        <TaskBatchesFormContainer hubId={hubId}/>
                    </div>
                </section>
            </div>
        )
    }
}
export default (CreateTaskBatches);