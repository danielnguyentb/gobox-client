import React from 'react'
import {connect} from 'react-redux'
import ScanOrder from './ScanOrder'
import {ADD_CODE, CLEAR_TASK_BATCHES} from '../../../ActionTypes'

class ScanOrderContainer extends React.Component {
    handleSubmit({code}) {
        const {dispatch} = this.props;
        if(code){
            dispatch({
                type: ADD_CODE,
                payload: code.trim()
            });
        }
    }

    resetForm(e){
        e.preventDefault();
        const {dispatch} = this.props;
        dispatch({type: CLEAR_TASK_BATCHES});
    }

    render() {
        return (
            <ScanOrder
                onReset={::this.resetForm}
                onSubmit={::this.handleSubmit}
            />
        )
    }
}

const mapStateToProps = state => {
    return {}
};
export default connect(mapStateToProps)(ScanOrderContainer)