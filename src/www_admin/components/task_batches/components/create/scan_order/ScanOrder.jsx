import React from 'react'
import * as RBT from 'react-bootstrap'
import * as constants from '../../../constants'
import {Field, reduxForm} from 'redux-form'

export const renderScanField = ({input}) => (
    <input
        {...input}
        type="text"
        autoFocus={true}
        placeholder="Quét mã"
        className="form-control scan-orderid"
    />
);

class ScanOrderForm extends React.Component {
    render() {
        const {handleSubmit, onReset, error, submitting, pristine, invalid} = this.props;
        return (
            <div className="row">
                <div className="col-md-offset-1 col-md-10 col-xs-12">
                    <div className="row">
                        <div className="col-md-10 col-xs-12 pr-0 mb-20">
                            <form onSubmit={handleSubmit}>
                                <RBT.FormGroup>
                                    <RBT.InputGroup>
                                        <Field name="code" component={renderScanField}/>
                                        <span className="input-group-btn">
                                        <RBT.Button
                                            type="submit"
                                            disabled={submitting || pristine || invalid}
                                            className="btn-scan">
                                            <i className="fa fa-level-down"/>
                                        </RBT.Button>
                                    </span>
                                    </RBT.InputGroup>
                                </RBT.FormGroup>
                                {error ? <span className="col-sm-12 alert alert-warning">{error}</span> : null}
                            </form>
                        </div>
                        <div className="col-md-2 col-xs-12">
                            <button onClick={onReset} className="btn btn-link clear-text">XÓA TRẮNG</button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

const validate = values => {
    const errors = {};
    if (Object.keys(values).length == 0) return {};
    if (!values.code) {
        errors.code = 'invalid code'
    }
    return errors;
};
export default reduxForm({
    form: constants.ScanOrderForm,
    validate
})(ScanOrderForm)