import React from 'react'
import CreateTaskBatches from './CreateTaskBatches'
import {HUB_PERMISSION} from '../../../hub/constants'
import {connect} from 'react-redux'
import {replace} from 'react-router-redux'

class CreateTaskBatchesContainer extends React.Component {
    componentDidMount(){
        const {isAllow, dispatch} = this.props;
        if(!isAllow(HUB_PERMISSION.PERMISSION_CREATE_DELIVER_TASK_ALL_HUB) && !isAllow(HUB_PERMISSION.PERMISSION_CREATE_DELIVER_TASK_HUB)){
            dispatch(replace('/error/403'))
        }
    }

    render() {
        const {hubId} = this.props.params;
        return (
            <CreateTaskBatches
                hubId={hubId}
            />
        )
    }
}
export default connect()(CreateTaskBatchesContainer)