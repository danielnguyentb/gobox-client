/**
 * Created by ltlam on 19/12/2016.
 */
export const NAME = 'task_batches';
export const CreateTaskBatchesForm = 'CreateTaskBatchesForm';
export const ScanOrderForm = 'ScanOrderForm';
export const TASK_BATCHES_KEY = 'TASK_BATCHES';