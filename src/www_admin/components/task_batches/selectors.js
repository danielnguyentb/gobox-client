/**
 * Created by ltlam on 19/12/2016.
 */
import {createSelectorCreator, defaultMemoize, createSelector} from 'reselect'
import lodash from 'lodash'
import {NAME} from './constants'
import {DeliveryOrderByIdSelector} from '../order/selectors'
import * as orderConstants from '../order/constants'
import * as Task from '../tasks'

const createDeepEqualSelector = createSelectorCreator(
    defaultMemoize,
    lodash.isEqual
);

export const TaskBatchesSelector  = state => state[NAME];

export const hydrate = (taskBatches, getOrder) => {
    if(!taskBatches) return taskBatches;
    const {batchData = []} = taskBatches;
    taskBatches['batchData'] = batchData.map(t => {
        t.order = t['objectType'] == orderConstants.TYPE ? getOrder(t['objectId']) : null;
        t.typeTitle = Task.constants.typeTasksTitle[t.type];
        return t;
    });
    return taskBatches;
};

export const TaskBatchByIdSelector = createDeepEqualSelector(
    [TaskBatchesSelector, DeliveryOrderByIdSelector],
    (batch, getOrder) => lodash.memoize(
        id => hydrate(batch.byId[id], getOrder)
    )
);

export const getPaginateBatchesSelector = createSelector(
    [TaskBatchesSelector, TaskBatchByIdSelector],
    (batches, getBatch) => batches.paginateIds.map(id => getBatch(id))
);