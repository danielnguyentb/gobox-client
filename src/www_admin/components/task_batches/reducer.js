/**
 * Created by ltlam on 19/12/2016.
 */
import * as t from './ActionTypes'
import {combineReducers} from 'redux'
const byId = (state = {}, action) => {
    let copy = {...state};
    const {type, response} = action;
    switch (type) {
        case t.TASK_BATCHES_SUCCESS:
            copy[response.id] = response;
            break;
    }
    return copy;
};

const paginateIds = (state = [], action) => {
    let copy = [...state];
    const {type, response} = action;
    switch (type) {
        case t.TASK_BATCHES_SUCCESS:
            if(!copy.includes(response.id)){
                copy.push(response.id);
            }
            break;
    }
    return copy;
};

export default combineReducers({
    byId, paginateIds
})