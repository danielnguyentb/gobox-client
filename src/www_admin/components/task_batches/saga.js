/**
 * Created by ltlam on 16/12/2016.
 */

import {takeEvery, takeLatest, delay} from 'redux-saga'
import {call, put, fork, select} from 'redux-saga/effects'
import * as t from './ActionTypes'
import {
    startSubmit, stopSubmit, reset, arrayPush, change,
    getFormValues, initialize, arrayInsert, arrayRemoveAll
} from 'redux-form'
import {parseErrors} from '../../helpers/redux-form'
import fetchApi from '../../helpers/callApi'
import * as Order from '../order'
import * as Hubs from '../hub'
import * as Tasks from '../tasks'
import {toastr} from '../../../www_components/Toastr2'
import Localize from '../../../resources/Localize'
import {CreateTaskBatchesForm, ScanOrderForm, TASK_BATCHES_KEY} from './constants'
import {push} from 'react-router-redux'
import {pickBy} from 'lodash'

function* addCode(action) {
    const code = action.payload;
    yield put(reset(ScanOrderForm));
    const formValues = yield select(getFormValues(CreateTaskBatchesForm));
    const duplicate = formValues && formValues.tasks && formValues.tasks.some(t => t.objectCode == code);
    if (duplicate) {
        yield put(stopSubmit(ScanOrderForm, {_error: `Mã "${code}" đã có trong danh sách`}));
    } else {
        yield put(startSubmit(ScanOrderForm));
        const result = yield call(fetchOrdersByCodes, code);
        if (result.ok) {
            yield call(addCodeSuccess, code);
        } else {
            yield put(stopSubmit(ScanOrderForm, parseErrors(result.error)));
        }
    }
}

function* fetchOrdersByCodes(codes) {
    const filter = Array.isArray(codes) ? {where: {barcode: {inq: codes}}} : {where: {barcode: codes}};
    const endpoint = `/CustomerDeliveryOrders?filter=${JSON.stringify(filter)}`;
    const [ requestType, successType, failureType ] = [Order.ActionsTypes.DELIVERY_ORDERS_REQUEST,
        Order.ActionsTypes.DELIVERY_ORDERS_SUCCESS, Order.ActionsTypes.DELIVERY_ORDERS_FAILURE];
    yield put({type: requestType, ajax: true});
    try {
        const response = yield call(fetchApi, endpoint);
        yield put({type: successType, ajax: false, response, payload: codes});
        if (response.result.length === 0) {
            throw new Error(`Vận đơn "${codes}" không tồn tại`)
        }
        return {ok: true};
    } catch (error) {
        yield put({type: failureType, ajax: false, error});
        return {ok: false, error};
    }
}

function* addCodeSuccess(code) {
    yield put(arrayInsert(CreateTaskBatchesForm, 'tasks', 0, {
        objectCode: code,
        checked: true,
    }));
    yield put(stopSubmit(ScanOrderForm));
}

function* createTask(task) {
    let {deliverBefore, note, objectCode, idx, assignedTo, type, batchId} = task;
    try {
        assignedTo = parseInt(assignedTo) || null;
        yield put(change(CreateTaskBatchesForm, `tasks[${idx}].state`, 'pending'));
        yield put({type: Tasks.ActionTypes.ASSIGN_PICKER_TO_ORDER_REQUEST, ajax: true});
        const response = yield call(fetchApi, `/CustomerDeliveryOrders/${objectCode}/deliverTasks`, {
            method: 'POST',
            body: JSON.stringify(pickBy({
                deliverBefore, note, assignedTo, type, batchId
            }))
        });
        yield put({type: Tasks.ActionTypes.ASSIGN_PICKER_TO_ORDER_SUCCESS, response});
        yield put(change(CreateTaskBatchesForm, `tasks[${idx}].state`, 'success'));
    } catch (error) {
        yield put({type: Tasks.ActionTypes.ASSIGN_PICKER_TO_ORDER_FAILURE, error});
        yield put(change(CreateTaskBatchesForm, `tasks[${idx}].state`, 'failure'));
        yield put(change(CreateTaskBatchesForm, `tasks[${idx}].error`, Localize.t(error.message)));
    }
}

function* createAllTasks(tasks) {
    for (let i = 0; i < tasks.length; i++) {
        const task = tasks[i];
        if (task.checked) {
            yield fork(createTask, task);
        }
    }
}

function* createAllTasksSuccess(batchId) {
    toastr.success('Tạo nhiệm vụ hàng loạt thành công',
        'Hệ thống sẽ chuyển sang trang chi tiết nhóm nhiệm vụ vừa tạo trong ít giây nữa');
    yield call(clearCheckedTasks);
    yield put(stopSubmit(CreateTaskBatchesForm));
    yield put(stopSubmit(ScanOrderForm));
    yield call(delay, 3000);
    const hubId = yield select(Hubs.selectors.getCurrentHubId);
    yield put(push(`/hubs/${hubId}/task_batches/detail/${batchId}`));
}

function* createTaskBatches({payload}) {
    yield put(startSubmit(CreateTaskBatchesForm));
    yield put(startSubmit(ScanOrderForm));
    try {
        const response = yield call(fetchApi, '/taskBatches', {method: 'POST'});
        yield put({type: t.CREATE_TASK_BATCHES_SUCCESS, response});
        const {assignedTo, type} = payload;
        const batchId = response.id;
        const tasks = payload.tasks.map((t, idx) => {
            return {
                ...t, assignedTo, type, idx, batchId
            }
        });
        yield call(createAllTasks, tasks);
        yield call(createAllTasksSuccess, batchId);
    } catch (error) {
        yield put({type: t.CREATE_TASK_BATCHES_FAILURE, error});
        yield put(stopSubmit(CreateTaskBatchesForm, parseErrors(error)));
        yield put(stopSubmit(ScanOrderForm));
    }
}


function* clearTaskBatches() {
    yield put(arrayRemoveAll(CreateTaskBatchesForm, 'tasks'));
    yield put(change(CreateTaskBatchesForm, 'assignedTo', ''));
}

function* clearCheckedTasks() {
    const formValues = yield select(getFormValues(CreateTaskBatchesForm));
    const tasks = formValues.tasks.filter(t => t.checked == false);
    localStorage.setItem(TASK_BATCHES_KEY, JSON.stringify(tasks));
    /*for (let i = formValues.tasks.length - 1; i >= 0; i--) {
        const {checked} = formValues.tasks[i];
        if (checked) {
            yield put(arrayRemove(CreateTaskBatchesForm, 'tasks', i));
        }
    }*/
}

function* toggleAllTask(action) {
    const {payload} = action;
    const formValues = yield select(getFormValues(CreateTaskBatchesForm));
    for (let i = 0; i < formValues.tasks.length; i++) {
        yield put(change(CreateTaskBatchesForm, `tasks[${i}].checked`, payload))
    }
}

function* init() {
    try {
        const tasks = JSON.parse(localStorage.getItem(TASK_BATCHES_KEY)) || [];
        yield put(initialize(CreateTaskBatchesForm, {tasks, type: Tasks.constants.TYPE_TASKS.DELIVER}));
        const objectCodes = [];
        if(tasks) {
            for (let i = 0; i < tasks.length; i++) {
                const {objectCode} = tasks[i];
                objectCodes.push(objectCode);
            }
            yield call(fetchOrdersByCodes, objectCodes);
        }
    } catch (e) {
        console.error(e);
    }
}

export default function*() {
    yield takeLatest(t.INIT, init);
    yield takeLatest(t.ADD_CODE, addCode);
    yield takeLatest(t.TOGGLE_ALL_TASKS, toggleAllTask);
    yield takeLatest(t.CREATE_TASK_BATCHES_REQUEST, createTaskBatches);
    yield takeEvery(t.CLEAR_TASK_BATCHES, clearTaskBatches);
}