/**
 * Created by ltlam on 16/12/2016.
 */
import {CALL_API} from '../../ActionTypes'
import * as t from './ActionTypes'

export const fetchTaskBatches = id => {
    return {
        type: CALL_API,
        payload: {
            types: [t.TASK_BATCHES_REQUEST, t.TASK_BATCHES_SUCCESS, t.TASK_BATCHES_FAILURE],
            endpoint: `/TaskBatches/${id}`,
            method: 'GET'
        }
    }
};