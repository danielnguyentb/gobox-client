export const NAME = 'hub';

export const NUMBER_HISTORY_BARCODE = 50;
export const LIST_BARCODE = 'listBarcode';

export const STATUS_TRANSPORT = {
    NEW: 'NEW',
    IN_PROCESS :'IN_PROCESS',
    DONE : 'DONE',
    FAILED : 'FAILED',
    CANCELLED : 'CANCELLED'
};

export const STATUS_TASKS = {
    NEW: 'NEW',
    IN_PROCESS: 'IN_PROCESS',
    DONE: 'DONE',
    FAILED: 'FAILED',
    CANCELLED: 'CANCELLED'
};

export const TYPE_TASKS = {
    PICKUP: "PICKUP",
    DELIVER: "DELIVER",
    PICKUP_AND_DELIVER: "PICKUP_AND_DELIVER"
};

export const typeTasksTitle = {
    [TYPE_TASKS.PICKUP] : "Đi lấy hàng",
    [TYPE_TASKS.DELIVER] : "Đi giao hàng",
    [TYPE_TASKS.PICKUP_AND_DELIVER] : "Đi trả hàng",
};

export const statusTasksTitle = {
    [STATUS_TASKS.NEW]: "Nhiệm vụ mới",
    [STATUS_TASKS.IN_PROCESS]: "Đang thực hiện",
    [STATUS_TASKS.DONE]: "Thành công",
    [STATUS_TASKS.FAILED]: "Thất bại",
    [STATUS_TASKS.CANCELLED]: "Hủy",
};

export const statusTransportTitle = {
    [STATUS_TRANSPORT.NEW]: 'Chờ lấy',
    [STATUS_TRANSPORT.IN_PROCESS]: 'Đang lấy',
    [STATUS_TRANSPORT.DONE]: 'Thành công',
    [STATUS_TRANSPORT.FAILED]: 'Thất bại',
    [STATUS_TRANSPORT.CANCELLED]: 'Hủy',
};

export const TransportEach = {
    [STATUS_TRANSPORT.NEW]: {
        'status':'Chờ lấy',
        'sort': 1,
        'class' : 'logistic-wait'
    },
    [STATUS_TRANSPORT.IN_PROCESS]: {
        'status':'Đang lấy',
        'sort': 1,
        'class' : 'logistic-process'
    },
    [STATUS_TRANSPORT.DONE]: {
        'status': 'Thành công',
        'sort': -1,
        'class' : 'logistic-success'
    },
    [STATUS_TRANSPORT.FAILED]: {
        'status': 'Thất bại',
        'sort': -1,
        'class' : 'logistic-fail'
    },
    [STATUS_TRANSPORT.CANCELLED]: {
        'status': 'Hủy',
        'sort': -1,
        'class' : 'logistic-cancel'
    },
};

export const BARCODE_ACTION = {
    BARCODE_ACTION_IN : 'IN',
    BARCODE_ACTION_OUT : 'OUT',
};

export const HUB_PERMISSION = {
    'PERMISSION_VIEW_HUB_REQUEST' : 'PERMISSION_VIEW_HUB_REQUEST',
    'PERMISSION_VIEW_ALL_REQUEST' : 'PERMISSION_VIEW_ALL_REQUEST',
    // Quyền cho phép quản trị viên quét mã nhập kho ở tất cả các hub
    'PERMISSION_BARCODE_IN_ALL_HUB' : 'PERMISSION_BARCODE_IN_ALL_HUB',
    //Quyền cho phép nhân viên của hub quét mã nhập kho cho hub của mình phụ trách
    'PERMISSION_BARCODE_IN_HUB' : 'PERMISSION_BARCODE_IN_HUB',
    //Quyền cho phép quản trị viên quét mã xuất kho ở tất cả các hub
    'PERMISSION_BARCODE_OUT_ALL_HUB' : 'PERMISSION_BARCODE_OUT_ALL_HUB',
    //Quyền cho phép nhân viên của hub quét mã xuất kho cho hub của mình phụ trách
    'PERMISSION_BARCODE_OUT_HUB' : 'PERMISSION_BARCODE_OUT_HUB',
    //Quyền cho phép quản trị viên tạo nhiệm vụ cho đi giao cho tất cả các vận đơn
    PERMISSION_CREATE_DELIVER_TASK_ALL_HUB: 'PERMISSION_CREATE_DELIVER_TASK_ALL_HUB',
    //	Quyền cho phép quản trị viên tạo nhiệm vụ cho đi giao cho các vận đơn đang nằm trong hub của mình
    //TODO: client kiem tra lai da check quyen nay khi tao task chua
    PERMISSION_CREATE_DELIVER_TASK_HUB: 'PERMISSION_CREATE_DELIVER_TASK_HUB'
};