import { GET_HUBS, GET_MY_HUBS, BARCODE_IN, BARCODE_OUT, SET_CURRENT_HUB } from './ActionTypes'
import Localize from '../../../resources/Localize'
import {combineReducers} from 'redux'
import {LIST_BARCODE, NUMBER_HISTORY_BARCODE} from './constants'
import moment from 'moment'
moment.locale('vi');

const byId = (state = {}, action) => {
    let {response} = action;
    let copy = {...state};
    switch (action.type) {
        case GET_HUBS.SUCCESS:
            let hubs = [];
            response.map(t => {
                hubs[t.id] = t;
            });
            return {
                ...state, ...hubs
            };
    }
    return state;
};

const currentHub = (state = null, action) => {
    if(action.type == SET_CURRENT_HUB){
        return action.payload;
    }
    return state;
};

function visibleIds(state = [], action) {
    switch (action.type) {
        case GET_HUBS.SUCCESS:
            return action.response.map(hub => hub.id);

    }
    return state;
}

const myHubs = (state = [], action) => {
    switch (action.type) {
        case GET_MY_HUBS.SUCCESS:
            return action.response.map(hub => hub.id);
    }
    return state;
};

const listBarcode = (state = [], action) => {
    let {response} = action;
    switch (action.type) {
        case BARCODE_IN.SUCCESS:
        case BARCODE_OUT.SUCCESS:
            response['time'] = moment().format('H:mm:ss');

            addBarcodeFromLocalStorage(response);
            return [...state, response];
    }
    return state
};

export function getHub(state, hubId) {
    return state.hub.byId && state.hub.byId[hubId] ? state.hub.byId[hubId] : {};
}

export function getVisibleHubs(state) {
    return state.hub.visibleIds.map(id => getHub(state, id))
}

export const addBarcodeFromLocalStorage = (barcode) => {

    let listBarcodeArr = [];
    let barcodeStorage = JSON.parse(localStorage.getItem(LIST_BARCODE));
    let hubId = barcode.currentWarehouseId;

    let numberBarcode = localStorage.getItem(LIST_BARCODE+ hubId);

    if(barcodeStorage && barcodeStorage[hubId]) {

        let barcodeHub = [];
        Object.keys(barcodeStorage[hubId]).map(id =>
            barcodeHub.push(barcodeStorage[hubId][id])
        );
        if(barcode.id == barcodeHub[0].id && barcode.currentWarehouseStatus ==  barcodeHub[0].currentWarehouseStatus) {
            // don't do something
        } else {

            localStorage.setItem(LIST_BARCODE+ hubId, numberBarcode ? parseInt(numberBarcode) + 1 : 1);
            barcode['times'] =  numberBarcode ? parseInt(numberBarcode) + 1 : 1;
            barcodeHub.push(barcode);

        }
        barcodeStorage[hubId] = barcodeHub;
        if(barcodeStorage[hubId].length == NUMBER_HISTORY_BARCODE) {
            delete barcodeStorage[hubId][NUMBER_HISTORY_BARCODE];
        }
        barcodeStorage[hubId].sort((a, b) => {
            if(a.time == b.time) return 0;
            return a.time < b.time ? 1 : -1;
        });

        localStorage.setItem(LIST_BARCODE, JSON.stringify(barcodeStorage));
    } else {
        if(!barcodeStorage) {

            localStorage.setItem(LIST_BARCODE+ hubId, numberBarcode ? parseInt(numberBarcode) + 1 : 1);
            barcode['times'] =  numberBarcode ? parseInt(numberBarcode) + 1 : 1;

            listBarcodeArr[hubId] = [barcode];
            localStorage.setItem(LIST_BARCODE, JSON.stringify(listBarcodeArr));
        } else {

            localStorage.setItem(LIST_BARCODE+ hubId, numberBarcode ? parseInt(numberBarcode) + 1 : 1);
            barcode['times'] =  numberBarcode ? parseInt(numberBarcode) + 1 : 1;

            barcodeStorage[hubId] = [barcode];
            localStorage.setItem(LIST_BARCODE, JSON.stringify(barcodeStorage));
        }

    }
};

export const getListBarcode = (state, hubId) => {
    let listBarcode =  JSON.parse(localStorage.getItem(LIST_BARCODE));
    if(listBarcode && listBarcode[hubId]) {
        Object.keys(listBarcode[hubId]).map(id => {
            let hub = getHub(state,listBarcode[hubId][id].currentWarehouseId);
            listBarcode[hubId][id]['codeHub'] = hub ? hub.code : '';
        });
        return listBarcode[hubId];
    }
};

export default combineReducers({
    byId, visibleIds, myHubs, listBarcode, currentHub,
    error: (state = null, {error}, action) => error ? Localize.t(error) : null,
    errorMsg: (state = false , action) => ((action.type == BARCODE_IN.FAILURE) || (action.type == BARCODE_OUT.FAILURE)) ? true : state,
});