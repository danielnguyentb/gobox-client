import Hub from './api'
import {CALL_API} from '../../ActionTypes'
import * as types from './ActionTypes'

export const getHubs= () => dispatch => dispatch(Hub.getHubs());

export const getMyHubs = () => dispatch => dispatch(Hub.getMyHubs());

export const barcodeIn = (object)  => dispatch => dispatch(Hub.barcodeIn(object));

export const barcodeOut = (object)  => dispatch => dispatch(Hub.barcodeOut(object));

export const getPickupBelongToHub = (id) => dispatch => dispatch(Hub.getPickupBelongToHub(id));
