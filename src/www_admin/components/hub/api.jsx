import Client from '../../helpers/Client'
import * as types from './ActionTypes'

class Hub extends Client {
    getHubs() {
        return this.callApi(({
            types: [types.GET_HUBS.REQUEST, types.GET_HUBS.SUCCESS, types.GET_HUBS.FAILURE],
            endpoint: '/LogisticsHubs',
            method: 'GET'
        }), 'fetchingHub');
    }

    getMyHubs() {
        return this.callApi((({
            types: [types.GET_MY_HUBS.REQUEST, types.GET_MY_HUBS.SUCCESS, types.GET_MY_HUBS.FAILURE],
            endpoint: `/LogisticsHubs/getMyHubs`,
            method: 'GET'
        })), 'fetchingMyHubs')
    }

    barcodeIn(data) {
        return this.callApi(({
            types: [types.BARCODE_IN.REQUEST, types.BARCODE_IN.SUCCESS, types.BARCODE_IN.FAILURE],
            endpoint: `/LogisticsHubs/${data.hubId}/barcode/in/${data.barcode}`,
            method: 'PUT'
        }), 'fetchingHub');
    }
    barcodeOut(data) {
        return this.callApi(({
            types: [types.BARCODE_OUT.REQUEST, types.BARCODE_OUT.SUCCESS, types.BARCODE_OUT.FAILURE],
            endpoint: `/LogisticsHubs/${data.hubId}/barcode/out/${data.barcode}`,
            method: 'PUT'
        }), 'fetchingHub');
    }

    //lấy danh sách pickup của hub_board
    getPickupBelongToHub(id) {
        return this.callApi(({
            types: [types.GET_PICKUP_BELONG_TO_HUB.REQUEST, types.GET_PICKUP_BELONG_TO_HUB.SUCCESS, types.GET_PICKUP_BELONG_TO_HUB.FAILURE],
            endpoint: `/LogisticsHubs/${id}/user`,
            method: 'GET',
            scope: {hubId: id}
        }))
    }
}

export default new Hub();