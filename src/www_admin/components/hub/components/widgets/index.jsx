import React, {PropTypes, Component} from 'react'
import {connect} from 'react-redux'
import Select from 'react-select'
import 'react-select/dist/react-select.css'
import {hubByIdSelector, hubsSelector, hubOptionsSelector} from '../../selectors'
import {getHubs} from '../../actions'

export const HubSelector = ({placeholder, noResultText = "Không tìm thấy hub_board", options = [], onChange, value = 0}) => (
    <Select
        placeholder={placeholder}
        noResultsText={noResultText}
        value={value}
        options={options}
        onChange={onChange}
    />
);

const mapStateToProps = (state) => ({
    options: hubOptionsSelector(state)
});

class HubSelectContainerBase extends Component {
    componentDidUpdate() {
        let {getHubs, options} = this.props;
        if (!options || options.length == 0) {
            getHubs();
        }
    }

    render = () => <HubSelector {...this.props}/>;
}

class HubCodeContainerBase extends Component {
    componentWillMount() {
        let {getHubs, hubs} = this.props;
        if (!hubs || hubs.length == 0) {
            getHubs();
        }
    }

    render = () => <span>{this.props.hubCode}</span>;
}

export const HubCodeContainer = connect((state, props) => {
    const getHub = hubByIdSelector(state);
    return {
        hubs: hubsSelector(state),
        hubCode: getHub(props.hubId) ? getHub(props.hubId).code : '--'
    }
}, {getHubs})(HubCodeContainerBase);

export const HubSelectContainer = connect(mapStateToProps, {getHubs})(HubSelectContainerBase);

HubSelectContainerBase.propTypes = {
    placeholder: PropTypes.string,
    noResultText: PropTypes.string,
    value: PropTypes.number,
    options: PropTypes.array,
    onChange: PropTypes.func,
};

HubCodeContainerBase.propTypes = {
    hubId: PropTypes.number.isRequired
};