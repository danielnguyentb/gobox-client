import React from 'react';
import moment from 'moment'
moment.locale('vi');

export default ({listBarcode}) => {

        return (
            <section>
                {listBarcode && Object.keys(listBarcode).map(id =>
                    <tr key={id}>
                        <td className="fw-600">{id}</td>
                        <td className="fw-600 fs-18">
                            <i className={`ti-import mr-5 ${listBarcode[id].currentWarehouseStatus == 'IN' ? 'text-primary' : 'text-danger'}`}/>
                            {listBarcode[id].barcode}
                        </td>
                        <td>{listBarcode[id].codeHub}</td>
                        <td>{listBarcode[id].time}</td>
                    </tr>
                )}
            </section>
        );

}