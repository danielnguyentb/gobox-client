import React from 'react'
import {connect} from 'react-redux'
import DocumentTitle from '../../../../../www_components/Common/DocumentTitle'
import { Control, Form, actions, Field, combineForms, Errors } from 'react-redux-form'
import  {Button} from 'react-bootstrap'
import * as RBT from 'react-bootstrap'
import {barcodeIn, barcodeOut} from '../../actions'
import {BARCODE_ACTION, HUB_PERMISSION} from '../../constants'
import {getListBarcode} from '../../reducer'
import {toastr} from 'react-redux-toastr'

class BarcodeContent extends React.Component {
    constructor(props) {
        super(props);
    }
    componentWillMount() {
        this.state = {
            'className':'glyphicon glyphicon-volume-up',
            action: '',
            barcode: ''
        };
    }
    handleClick(e) {
        if(this.state.className == 'glyphicon glyphicon-volume-off'){
            this.setState({'className':'glyphicon glyphicon-volume-up'});
        }else{
            this.setState({'className':'glyphicon glyphicon-volume-off'});
        }
    }

    componentWillReceiveProps(nextProps) {
        if(nextProps.listBarcode != this.props.listBarcode) {
            let label = (this.state.action == BARCODE_ACTION.BARCODE_ACTION_OUT) ? 'Xuất' : 'Nhập';
            toastr.success(label + " thành công");
            this.setState({barcode: ''});
        }
        if(nextProps.errorMsg != this.props.errorMsg) {
            toastr.error("Mã vạch không hợp lệ");
        }
    }

    handleSelectAction(e) {
        this.setState({ action: e.target.value});
    }
    handleSubmit(e) {
        let barcode = e.target.value;
        let hubId = this.props.params.hubId;

        this.setState({ barcode: e.target.value });
        if(e.key == 'Enter' ) {

            if(!barcode && this.state.action == '') {
                toastr.warning("Bạn chưa nhập đầy đủ thông tin để nhập xuất");
            } else if (this.state.action == '') {
                toastr.warning("Bạn chưa chọn hành động nhập hoặc xuất");
            } else if (!barcode) {
                toastr.warning("Bạn chưa nhập mã vạch");
            } else {

                let data = {hubId: hubId, barcode: barcode};
                if(this.state.action == BARCODE_ACTION.BARCODE_ACTION_IN) {
                    this.props.barcodeIn(data);
                } else if (this.state.action == BARCODE_ACTION.BARCODE_ACTION_OUT) {
                    this.props.barcodeOut(data);
                }
            }
        }

    }
    render() {
        let {listBarcodes, isAllow} = this.props;
        return (
            <DocumentTitle title="Nhập/Xuất kho">
                <div>
                    <section className="content-header">
                        <ol className="breadcrumb mb-0">
                            <li><span className="fs-18 fw-600"><i className="fa fa-map-marker mr-5"/> CG-HN</span></li>
                            <li><span className="fs-16">Quét mã vạch</span></li>
                        </ol>
                    </section>
                    <section className="content">
                        <div className="box">
                            <div className="row">
                                <div className="col-md-offset-3 col-md-6 col-xs-12">
                                    <div className="row">
                                        <table className="table tbl-barcode">
                                            <tbody>
                                            <tr>
                                                <td colSpan="4">
                                                    <div className="col-md-3 col-xs-6 pl-0">
                                                        <Control.select model=".action" className="form-control"
                                                            onChange={this.handleSelectAction.bind(this)}
                                                            value={this.state.action}
                                                        >
                                                            <option value="select">Hành động</option>
                                                            {(isAllow(HUB_PERMISSION.PERMISSION_BARCODE_IN_ALL_HUB) || isAllow(HUB_PERMISSION.PERMISSION_BARCODE_IN_HUB)) &&
                                                            <option value={BARCODE_ACTION.BARCODE_ACTION_IN}>Nhập</option>
                                                            }
                                                            {(isAllow(HUB_PERMISSION.PERMISSION_BARCODE_OUT_ALL_HUB) || isAllow(HUB_PERMISSION.PERMISSION_BARCODE_OUT_HUB)) &&
                                                            <option value={BARCODE_ACTION.BARCODE_ACTION_OUT}>Xuất</option>
                                                            }
                                                        </Control.select>
                                                    </div>
                                                    <div className="col-md-8 col-xs-5 pr-0 pl-0">
                                                        <RBT.FormControl
                                                            type="text"
                                                            value={this.state.barcode}
                                                            placeholder="Nhập mã vạch"
                                                            onChange={e => this.setState({ barcode : e.target.value})}
                                                            onKeyPress={this.handleSubmit.bind(this)}
                                                        />
                                                    </div>
                                                    <div className="col-md-1 col-xs-1 pl-0">
                                                        <Button bsStyle="link" type="button" className="fs-20 text-base"><i className={this.state.className} onClick={this.handleClick.bind(this)}/></Button>
                                                    </div>
                                                </td>
                                            </tr>

                                            {listBarcodes && Object.keys(listBarcodes).map(id =>
                                                <tr key={id}>
                                                    <td className="fw-600">{listBarcodes[id].times}</td>
                                                    <td className="fw-600 fs-18">
                                                        <a href={`#/orders/detail/${listBarcodes[id].id}`} target="_blank">
                                                            <i className={`${listBarcodes[id].currentWarehouseStatus == 'IN' ? 'ti-import' : 'ti-export'} mr-5 ${listBarcodes[id].currentWarehouseStatus == 'IN' ? 'text-primary' : 'text-danger'}`}/>
                                                            {listBarcodes[id].barcode}
                                                        </a>
                                                    </td>
                                                    <td>{listBarcodes[id].codeHub}</td>
                                                    <td>{listBarcodes[id].time}</td>
                                                </tr>
                                            )}

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </section>
                </div>
            </DocumentTitle>
        )
    }
}

const mapStateToProps = (state, props) => {
    return {
        listBarcodes: getListBarcode(state, props.params.hubId),
        listBarcode: state.hub.listBarcode,
        errorMsg: state.hub.errorMsg
    }
};

export default connect(
    mapStateToProps, {barcodeIn, barcodeOut}
)(BarcodeContent);