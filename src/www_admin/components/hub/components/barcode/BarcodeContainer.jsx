import React from 'react'
import {connect} from 'react-redux'
import {push} from 'react-router-redux'
import {bindActionCreators} from 'redux'
import DocumentTitle from '../../../../../www_components/Common/DocumentTitle'
import { Control, Form, actions, Field, combineForms, Errors } from 'react-redux-form'
import  {Button} from 'react-bootstrap'
import BarcodeContent from './BarcodeContent'


import '../barcode_scan.scss'

class BarcodeContainer extends React.Component {
    constructor(props) {
        super(props);
    }


    render() {
        return (
            <DocumentTitle title="Nhập/Xuất kho">
                <BarcodeContent {...this.props} />
            </DocumentTitle>
    )
    }
}

const mapStateToProps = (state, props) => {
    return {
    }
};
const mapDispatchToProps = (dispatch) => ({
    ...bindActionCreators({

    }, dispatch),
    onRedirect: (path) => {
        dispatch(push(path))
    },
    dispatch
});

export default connect(
    mapStateToProps, mapDispatchToProps
)(BarcodeContainer);