import React from 'react'
import {connect} from 'react-redux'
import {getListBarcode} from '../../reducer'
import HistoryBarcodeItem from './HistoryBarcodeItem'

class HistoryBarcodeList extends React.Component {

    constructor(props) {
        super(props);
    }
    render() {
        return (
            <HistoryBarcodeItem {...this.props}/>
        )
    }
}

const mapStateToProps = (state, props) => {
    return {
        listBarcode: getListBarcode(state)
    }
};
export default connect(
    mapStateToProps,
)(HistoryBarcodeList);