import React, {Component} from 'react';
import {push, replace} from 'react-router-redux'
import {SET_CURRENT_HUB} from '../ActionTypes'
import * as Users from '../../user'

class HubComponent extends Component {
    componentDidMount(){
        const {params, dispatch} = this.props;
        const {hubId} = params || {};
        dispatch(Users.actions.fetchPickupByHubIdSaga(hubId));
        dispatch({
            type: SET_CURRENT_HUB,
            payload: hubId
        });
    }

    componentWillUnmount(){
        const {dispatch} = this.props;
        dispatch({
            type: SET_CURRENT_HUB,
            payload: null
        })
    }

    render() {
        return <div>
            {this.props.children && React.cloneElement(this.props.children, this.props)}
        </div>;
    }
}

export default HubComponent