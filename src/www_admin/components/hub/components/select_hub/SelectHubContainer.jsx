import React from 'react';
import {connect} from 'react-redux'
import {getHubs} from '../../actions'
import {getVisibleHubs} from '../../reducer'
import {bindActionCreators} from 'redux'
import {push} from 'react-router-redux'
import Select from 'react-select'

class SelectHubContainer extends React.Component {
    constructor(props) {
        super(props);
    }
    componentWillMount() {

        this.setState({
            hubId: 0,
            hubs: []
        })
    }
    componentDidMount() {
        this.props.getHubs();
    }
    componentWillReceiveProps(nextProps) {
        if(!nextProps.params.hubId) {
            this.setState({hubId: 0});
        }
        let hubsArr = [];
        if(nextProps.hubs) {
            nextProps.hubs.map(hub => {
                hubsArr.push({value: hub.id, label: hub.code})
            })
        }
        this.setState({hubs: hubsArr})
    }
    handleChangeHub(e) {
        if(e == null ) {
            this.props.onRedirect('/');
        } else {
            if(e.value != 0 ) {
                this.props.onRedirect('/hubs/'+ e.value);
            } else {
                this.props.onRedirect('/');
            }
            this.setState({hubId: e.value});
        }



    }
    render() {
        return (
            <section>
                <Select
                    placeholder="Chọn hub"
                    value={this.state.hubId}
                    options={this.state.hubs}
                    onChange={this.handleChangeHub.bind(this)}
                />
            </section>


        )
    }
}

const mapStateToProps = (state, props) => {
    return {
        hubs: getVisibleHubs(state),
    }
};

const mapDispatchToProps = (dispatch) => ({
    ...bindActionCreators({
        getHubs
    }, dispatch),
    onRedirect: (path) => {
        dispatch(push(path))
    },
    dispatch
});

export default connect(
    mapStateToProps, mapDispatchToProps
)(SelectHubContainer);