/**
 * Created by Tester-Ali on 07-11-2016.
 */
import {createSelectorCreator, defaultMemoize, createSelector} from 'reselect'
import lodash from 'lodash'
import {NAME} from './constants'

const createDeepEqualSelector = createSelectorCreator(
    defaultMemoize,
    lodash.isEqual
);

export const hubSelector = state => state[NAME];
export const hubsSelector = createDeepEqualSelector(
    hubSelector,
    hub => Object.values(hub.byId)
);
export const hubByIdSelector = createDeepEqualSelector(
    hubSelector,
    hub => lodash.memoize(
        id => hub.byId[id]
    )
);

export const hubOptionsSelector = createDeepEqualSelector(
    hubsSelector,
    hubs => {
        const defaultOption = {value: 0, label: 'Chọn Hub'};
        let v = hubs.map(t => ({
            value: t.id,
            label: t.code,
        }));
        return [defaultOption, ...v];
    }
);

export const getCurrentHubId = createSelector(
    hubSelector,
    hub => hub.currentHub
);
