import {encode} from '../../helpers'
export const generateQuery = (params) => {
    let where = {};
    let {
        warehouseId, type
    } = params;

    let or_where = [];

    where.and = [{
        or: or_where,
    }];
    if (warehouseId) where.and.push({warehouseId});
    if (type) where.and.push({type});
    let include ={"relation":"tasks", "scope": {"fields": {"id": true, "status": true}}};

    return {where, include};
};