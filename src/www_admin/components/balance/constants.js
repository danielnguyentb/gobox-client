export const NAME = 'balance';

export const PERMISSION_FINANCIAL_HISTORY_VIEW_LIST = 'PERMISSION_FINANCIAL_HISTORY_VIEW_LIST';

export const Status = {
    EXPECTED: 'EXPECTED',
    COMPLETED: 'COMPLETED'
};

export const StatusDetail = {
    [Status.EXPECTED]: {
        'label': 'Dự kiến',
        'balance': 'projectedBalance'

    },
    [Status.COMPLETED]: {
        'label': 'Đối soát',
        'balance': 'realBalance'
    }
};

export const ObjectTypes = {
    CustomerDeliveryOrder: 'CustomerDeliveryOrder'
};

export const Types = {
    SERVICE: 'SERVICE'
};

export const TypesDetail = {
    [Types.SERVICE]: {
        label: 'Vận đơn'
    }
};