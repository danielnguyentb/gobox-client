import React, {PropTypes} from 'react'
import {connect} from 'react-redux'
import Pagination from '../../../../../www_components/Pagination'
import {balanceSelector} from '../../selectors'

export class PaginationContainer extends React.Component {
    static propTypes = {
        onSearch: PropTypes.func.isRequired,
        metadata: PropTypes.object,
        query: PropTypes.object
    };

    onSubmit(data) {
        const {onSearch} = this.props;
        let selected = data.selected;
        let limit = 20;
        let skip = Math.ceil(selected * limit);
        skip = skip > 0 ? skip : 0;
        let params = {
            skip, limit, currentPage: selected + 1
        };
        onSearch(params);
    }

    render() {
        const {query, metadata} = this.props;
        const {currentPage, pageCount} = {...query, ...metadata};
        if (!pageCount || pageCount <= 1) return null;
        return (
            <Pagination
                onSubmit={::this.onSubmit}
                currentPage={currentPage || 0}
                pageCount={pageCount || 0}/>
        );
    }
}

const mapStateToProps = state => {
    return {
        metadata: balanceSelector(state).metadata
    }
};

export const PaginationContainerConnected = connect(mapStateToProps)(PaginationContainer);