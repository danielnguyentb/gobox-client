import React, {PropTypes, Component} from 'react'
import {formatCurrency} from '../../../../../helpers/format'
export const CustomerBalanceInfo = ({fullName, username, projectedBalance, realBalance, bankAccountNo, bankName, bankAccountName, bankBranch}) => {
    return (
        <section className="content-header">
            <div className="row">
                <div className="col-md-6 col-xs-12">
                    <h3 className="mt-0 mb-0">Danh sách công nợ của {username}</h3>
                    <h4 className="mb-15">
                        {(bankAccountNo && bankName) &&
                            <span>
                                Ngân hàng: {bankName} (Chi nhánh: {bankBranch}) <br/>
                                Chủ TK: {bankAccountName} - Số TK: {bankAccountNo}
                            </span>
                        }
                    </h4>
                    <span className="alert alert-warning p-10">Công nợ dương (&gt;0) là khách nợ dịch vụ. Công nợ âm (&lt;0) là dịch vụ nợ khách.</span>
                </div>
                <div className="col-md-6 col-xs-12">
                    <div className="row">
                        <div className="col-md-offset-6 col-md-3 col-xs-6">
                            <p className="fs-16">Dự kiến</p>
                            <p className="fw-600 fs-16">{formatCurrency(projectedBalance)}</p>
                        </div>
                        <div className="col-md-3 col-xs-6">
                            <p className="fs-16">Dư nợ</p>
                            <p className="fw-600 fs-16">{formatCurrency(realBalance)}</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
};