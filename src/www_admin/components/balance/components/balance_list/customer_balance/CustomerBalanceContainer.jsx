import React, {PropTypes, Component} from 'react'
import {connect} from 'react-redux'
import {CustomerBalanceInfo} from './CustomerBalanceInfo'
import {getCustomerById} from '../../../../customer/actions'
import {CustomerByIdSelector} from '../../../../customer/selectors'
import lodash from 'lodash'

export class CustomerBalanceContainer extends Component {
    static propTypes = {
        customerId: PropTypes.number.isRequired,
    };

    _updateCustomer(props, prevProps) {
        const {getCustomerById, customer, customerId} = props;
        if (prevProps) {
            if (customerId === prevProps.customerId) {
                return;
            }
            if (customer && customerId === customer.id) {
                return;
            }
            if (lodash.isEqual(customer, prevProps.customer)) {
                return;
            }
        }
        getCustomerById(customerId);
    }

    componentWillMount() {
        this._updateCustomer(this.props)
    }

    componentDidUpdate(prevProps) {
        this._updateCustomer(this.props, prevProps)
    }

    render() {
        const {customer} = this.props;
        return (
            <CustomerBalanceInfo
                {...customer}
            />
        )
    }
}
const mapStateToProps = (state, props) => {
    const getCustomer = CustomerByIdSelector(state);
    return {
        customer: getCustomer(props.customerId)
    };
};
export const CustomerBalanceContainerConnected = connect(mapStateToProps, {getCustomerById})(CustomerBalanceContainer);