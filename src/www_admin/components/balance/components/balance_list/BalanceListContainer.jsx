import React from 'react'
import BalanceList from './BalanceList'
import qs from 'qs'
import {connect} from 'react-redux'
import {push} from 'react-router-redux'
import {Status} from '../../constants'

class BalanceListContainer extends React.Component {
    constructor(props) {
        super(props);
        const {location} = props;
        this.state = {selectedIndex: this.getIndex(location)};
    }

    getIndex(location) {
        let idx = 0;
        if (location.query && Object.values(Status).indexOf(location.query.status) >= 0) {
            idx = Object.values(Status).indexOf(location.query.status);
        } else {
            idx = 2;
        }
        return idx;
    }

    search(data) {
        const {customerId, dispatch} = this.props;
        dispatch(push(`/balances/customer/${customerId}?${qs.stringify(data)}`));
    }

    selectTab(index, last) {
        this.setState({selectedIndex: index})
    }

    componentDidUpdate(prevProps) {
        const {location, customerId} = this.props;
        if (location.query.status != prevProps.location.query.status || customerId != prevProps.customerId) {
            this.setState({selectedIndex: this.getIndex(location)});
        }
    }

    render() {
        const {params, location, dispatch} = this.props;
        const {selectedIndex} = this.state;
        return (
            <BalanceList
                customerId={parseInt(params.customerId)}
                selected={selectedIndex}
                location={location}
                dispatch={dispatch}
                handleSelect={::this.selectTab}
                onSearch={::this.search}/>
        );
    }
}

const mapStateToProps = (state, props) => {
    return {
        customerId: props.params.customerId,
    }
};
export default connect(mapStateToProps)(BalanceListContainer);