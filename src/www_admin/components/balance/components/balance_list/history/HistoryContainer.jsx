import React, {PropTypes, Component} from 'react'
import {History} from './History'
import {connect} from 'react-redux'
import {balanceSelector} from '../../../selectors'
import qs from 'qs'
import {push} from 'react-router-redux'
import {fetchFinancialHistories} from '../../../actions'

export class HistoryContainer extends Component {
    componentWillMount() {
        this.search({})
    }

    onSearch(data) {
        const {location, metadata} = this.props;
        let query = {...metadata, ...location.query};
        this.search({...query, ...data});
    }

    search(data) {
        console.info('===========', data);
        const {fetchFinancialHistories, customerId, dispatch} = this.props;
        dispatch(push(`/balances/history/${customerId}?${qs.stringify(data)}`));
        fetchFinancialHistories(customerId, data);
    }

    render() {
        const {customerId, location, metadata} = this.props;
        const query = {...metadata, ...location.query};
        return (
            <History
                query={query}
                customerId={customerId}
                location={location}
                onSearch={::this.onSearch}/>
        )
    }
}
const mapStateToProps = state => {
    return {
        metadata: balanceSelector(state).historyMetadata,
    };
};
export const HistoryContainerConnected = connect(mapStateToProps, {fetchFinancialHistories})(HistoryContainer);
HistoryContainerConnected.propTypes = {
    customerId: PropTypes.number.isRequired,
};