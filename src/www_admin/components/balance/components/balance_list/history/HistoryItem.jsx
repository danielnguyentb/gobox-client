import React from 'react'
import {formatFullTime, formatCurrency} from '../../../../../helpers/format'
import Link from '../../../../../../www_components/Link'
export const HistoryItem = ({amount, objectUri, objectCode, createdTime}) => {
    return (
        <tr>
            <td>{formatFullTime(createdTime)}</td>
            <td className="fw-600">
                {objectUri ? <Link to={objectUri}>{objectCode}</Link> : objectCode}
            </td>
            <td>{formatCurrency(amount)}</td>
        </tr>
    )
};