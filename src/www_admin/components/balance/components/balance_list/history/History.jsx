import React from 'react'
import {HistoryListContainerConnected} from './HistoryListContainer'
import {SearchForm} from './SearchBox'
import {PaginationContainerConnected} from '../PaginationContainer'

export const History = ({onSearch, location, customerId, query}) => {
    const {currentPage, pageCount} = query;
    return (
        <div className="tab-pane active" id="expected">
            <div className="row">
                <div className="col-md-12">
                    <SearchForm
                        query={query}
                        onSubmit={data => onSearch({...data, skip: 0})}
                    />
                </div>
            </div>

            <div className="row">
                <HistoryListContainerConnected
                    customerId={customerId}
                    location={location}
                />
            </div>

            <div className=" row">
                <div className="col-md-12 text-right">
                    <PaginationContainerConnected
                        onSearch={onSearch}
                        currentPage={parseInt(currentPage) || 0}
                        pageCount={parseInt(pageCount) || 0}/>
                </div>
            </div>
        </div>
    )
};