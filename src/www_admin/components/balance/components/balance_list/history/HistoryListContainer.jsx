import React, {PropTypes} from 'react'
import {connect} from 'react-redux'
import {HistoryList} from './HistoryList'
import {PaginateHistoriesSelector} from '../../../selectors'

export class HistoryListContainer extends React.Component {
    static propTypes = {
        location: PropTypes.object.isRequired,
        customerId: PropTypes.number.isRequired,
    };

    render() {
        const {records} = this.props;
        return (
            <HistoryList
                records={records}
            />
        )
    }
}

const mapStateToProps = state => {
    return {
        records: PaginateHistoriesSelector(state)
    }
};
export const HistoryListContainerConnected = connect(mapStateToProps, )(HistoryListContainer);