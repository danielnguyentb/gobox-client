import React from 'react'
import {FormGroup} from 'react-bootstrap'
import SelectDateRangePicker from '../../../../../../www_components/SelectDateRangePicker'
import ChoosePriceRange from '../../../../../../www_components/ChoosePriceRange'
import {TypesDetail} from '../../../constants'


export const SearchForm = ({onSubmit, query}) => {
    const {objectType, operator, from, to, createdTimeFrom, createdTimeTo, objectCode} = query;
    return (
        <form method="POST">
            <div className="col-md-2 col-xs-12 pl-0">
                <FormGroup>
                    <label className="text-base fs-15">Loại công nợ</label>
                    <br/>
                    <select name="type" value={objectType} className="form-control mb-15"
                            onChange={e => onSubmit({objectType: e.target.value})}>
                        <option value="">Loại công nợ</option>
                        {Object
                            .keys(TypesDetail)
                            .map(key => <option key={key} value={key}>{TypesDetail[key]['label']}</option>)}
                    </select>
                </FormGroup>
            </div>
            <div className="col-md-3 col-xs-12 pl-0">
                <SelectDateRangePicker
                    onChange={(e)=> {
                        onSubmit({
                            createdTimeFrom: e.startDate ? e.startDate.toISOString() : '',
                            createdTimeTo: e.endDate ? e.endDate.toISOString() : '',
                        })
                    }}
                    startDate={createdTimeFrom}
                    endDate={createdTimeTo}
                    dateLabel="Khoảng TG phát sinh"/>
            </div>
            <div className="col-md-2 col-xs-12 pl-0">
                <FormGroup>
                    <label className="text-base fs-15">Chọn đối tượng</label>
                    <input
                        onBlur={e => onSubmit({objectCode: e.target.value})}
                        onKeyUp={e => {
                            if(e.key == 'Enter'){
                                onSubmit({objectCode: e.target.value})
                            }
                        }}
                        defaultValue={objectCode} type="text" placeholder="Đối tượng"
                        className="form-control"/>
                </FormGroup>
            </div>
            <div className="col-md-5 col-xs-12 pl-0">
                <label className="text-base fs-15">Chọn giá trị</label>
                <br/>
                <ChoosePriceRange
                    onBlur={onSubmit}
                    onEnter={onSubmit}
                    onChange={onSubmit}
                    operator={operator}
                    fromValue={parseInt(from) || 0}
                    toValue={parseInt(to) || 0}
                    object="amount"
                    placeholder="Chọn giá trị"/>
            </div>
        </form>
    )
};