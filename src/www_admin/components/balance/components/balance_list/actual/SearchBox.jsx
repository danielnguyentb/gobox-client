import React from 'react'
import {FormGroup} from 'react-bootstrap'
import SelectDateRangePicker from '../../../../../../www_components/SelectDateRangePicker'
import ChoosePriceRange from '../../../../../../www_components/ChoosePriceRange'

export const SearchForm = ({onSubmit, query}) => {
    const {createdTimeFrom, createdTimeTo, transactionTimeFrom, transactionTimeTo, operator, from, to, code} = query;
    return (
        <form onSubmit={onSubmit}>
            <div className="col-md-3 col-xs-12 pl-0">
                <SelectDateRangePicker
                    onChange={(e)=> {
                        onSubmit({
                            createdTimeFrom: e.startDate ? e.startDate.toISOString() : '',
                            createdTimeTo: e.endDate ? e.endDate.toISOString() : ''
                        })
                    }}
                    startDate={createdTimeFrom}
                    endDate={createdTimeTo}
                    dateLabel="Khoảng TG phát sinh"/>
            </div>
            <div className="col-md-3 col-xs-12 pl-0">
                <SelectDateRangePicker
                    onChange={(e)=> {
                        onSubmit({
                            transactionTimeFrom: e.startDate ? e.startDate.toISOString() : '',
                            transactionTimeTo: e.endDate ? e.endDate.toISOString() : '',
                        })
                    }}
                    startDate={transactionTimeFrom}
                    endDate={transactionTimeTo}
                    dateLabel="Khoảng TG đối soát"/>
            </div>
            <div className="col-md-2 col-xs-12 pl-0">
                <FormGroup>
                    <label className="text-base fs-15">Chọn đối tượng</label>
                    <input
                        onBlur={e => onSubmit({code: e.target.value})}
                        onKeyUp={e => {
                            if(e.key == 'Enter'){
                                onSubmit({code: e.target.value})
                            }
                        }}
                        defaultValue={code} type="text" placeholder="Đối tượng"
                        className="form-control"/>
                </FormGroup>
            </div>
            <div className="col-md-4 col-xs-12 pl-0">
                <label className="text-base fs-15">Chọn giá trị</label>
                <br/>
                <ChoosePriceRange
                    onBlur={onSubmit}
                    onEnter={onSubmit}
                    onChange={onSubmit}
                    operator={operator}
                    fromValue={parseInt(from) || 0}
                    toValue={parseInt(to) || 0}
                    object="amount"
                    placeholder="Giá trị"/>
            </div>
        </form>
    )
};