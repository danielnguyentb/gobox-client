import React, {PropTypes, Component} from 'react'
import {Actual} from './Actual'
import {connect} from 'react-redux'
import {Status} from '../../../constants'

export class ActualContainer extends Component {
    componentDidMount() {
        this.search()
    }

    search(data = {}) {
        const {onSearch, location} = this.props;
        let query = {...location.query};
        if (query.status !== Status.COMPLETED) {
            query = {};
        }
        onSearch({...query, ...data, status: Status.COMPLETED});
    }

    render() {
        const {customerId, location} = this.props;
        return (
            <Actual
                customerId={customerId}
                location={location}
                onSearch={::this.search}/>
        )
    }
}
export const ActualContainerConnected = connect()(ActualContainer);
ActualContainerConnected.propTypes = {
    customerId: PropTypes.number.isRequired,
    onSearch: PropTypes.func.isRequired,
    location: PropTypes.object.isRequired,
};