import React, {PropTypes} from 'react'
import {ActualListContainerConnected} from './ActualListContainer'
import {SearchForm} from './SearchBox'
import {PaginationContainerConnected} from '../PaginationContainer'

export const Actual = ({onSearch, customerId, location, ...others}) => {
    return (
        <div className="tab-pane" id="actual">
            <div className="row">
                <div className="col-md-12">
                    <SearchForm
                        query={location.query}
                        onSubmit={data => onSearch({...data, skip: 0})}
                    />
                </div>
            </div>

            <div className="row">
                <ActualListContainerConnected
                    customerId={customerId}
                    location={location}
                    {...others} />
            </div>

            <div className="row">
                <div className="col-md-12 text-right">
                    <PaginationContainerConnected
                        query={location.query}
                        onSearch={onSearch}/>
                </div>
            </div>
        </div>
    )
};