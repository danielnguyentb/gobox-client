import React from 'react'
import {ExpectedListContainerConnected} from './ExpectedListContainer'
import {SearchForm} from './SearchBox'
import {PaginationContainerConnected} from '../PaginationContainer'

export const Expected = ({onSearch, location, customerId}) => {
    return (
        <div className="tab-pane active" id="expected">
            <div className="row">
                <div className="col-md-12">
                    <SearchForm
                        query={location.query}
                        onSubmit={data => onSearch({...data, skip: 0})}
                    />
                </div>
            </div>

            <div className="row">
                <ExpectedListContainerConnected
                    customerId={customerId}
                    location={location}
                />
            </div>

            <div className=" row">
                <div className="col-md-12 text-right">
                    <PaginationContainerConnected
                        query={location.query}
                        onSearch={onSearch}/>
                </div>
            </div>
        </div>
    )
};