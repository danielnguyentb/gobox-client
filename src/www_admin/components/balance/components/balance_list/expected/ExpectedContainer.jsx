import React, {PropTypes, Component} from 'react'
import {Status} from '../../../constants'
import {Expected} from './Expected'
import {connect} from 'react-redux'
import lodash from 'lodash'

export class ExpectedContainer extends Component {
    componentDidMount() {
        this.search()
    }

    search(data = {}, prevProps) {
        const {onSearch, location} = this.props;
        let query = {...location};
        if (query.status !== Status.EXPECTED) {
            query = {};
        }
        onSearch({...query, ...data, status: Status.EXPECTED}, prevProps);
    }

    render() {
        const {customerId, location} = this.props;
        return (
            <Expected
                customerId={customerId}
                location={location}
                onSearch={::this.search}/>
        )
    }
}
export const ExpectedContainerConnected = connect()(ExpectedContainer);
ExpectedContainerConnected.propTypes = {
    customerId: PropTypes.number.isRequired,
    onSearch: PropTypes.func.isRequired,
};