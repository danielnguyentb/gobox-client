import React, {PropTypes} from 'react'
import {connect} from 'react-redux'
import {ExpectedList} from './ExpectedList'
import {PaginateBalancesSelector} from '../../../selectors'
import {fetchFinancialRecords} from '../../../actions'

export class ExpectedListContainer extends React.Component {

    static propTypes = {
        location: PropTypes.object.isRequired,
        customerId: PropTypes.number.isRequired,
    };

    componentDidUpdate(prevProps){
        const {isLocationChanged, fetchFinancialRecords, customerId, location} = this.props;
        if(isLocationChanged){
            fetchFinancialRecords(customerId, location.query)
        }
    }

    render() {
        const {records} = this.props;
        return (
            <ExpectedList
                records={records}
            />
        )
    }
}

const mapStateToProps = (state, props) => {
    return {
        records: PaginateBalancesSelector(state),
        isLocationChanged: state.isLocationChanged
    }
};
export const ExpectedListContainerConnected = connect(mapStateToProps, {fetchFinancialRecords})(ExpectedListContainer);