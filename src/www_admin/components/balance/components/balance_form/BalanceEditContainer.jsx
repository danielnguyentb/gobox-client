import React from 'react'

import Form from './BalanceForm'
import {connect} from 'react-redux'
import {createCustomerTransaction} from '../../../customer_transactions/actions'
import {push} from 'react-router-redux'
import {Field, reduxForm, change} from 'redux-form'

class BalanceEditContainer extends React.Component{
    constructor(props) {
        super(props);
    }
    componentWillReceiveProps(nextProps) {
        if(nextProps.createdTransactionSuccess) {
            nextProps.dispatch(push(`/balances/history/${nextProps.createdTransactionSuccess.customerId}?status=EXPECTED`));
        }
    }
    setFieldValue(nameField, value) {
        if(nameField && value) {
            this.props.change("BalanceForm", nameField, value);
        }
    }
    submit(data) {
        if(data.type == "PAYABLE") {
            data["receivableAmount"] = data.amount;
        } else if(data.type == "RECEIVABLE") {
            data["payableAmount"] = data.amount;
        }
        this.props.createCustomerTransaction(data);
    }
    render(){
        return(
            <Form {...this.props} onSubmit={::this.submit} setFieldValue={::this.setFieldValue}/>
        );
    }
}
const mapStateToProps = (state, props) => {
    return {
        createdTransactionSuccess: state.balance.createdTransactionSuccess,
    }
};
export default connect(
    mapStateToProps, { createCustomerTransaction, change }
)(BalanceEditContainer);