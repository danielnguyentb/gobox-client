import React from 'react'
import {connect} from 'react-redux'
import Typeahead from '../../../../../www_components/Typehead';

import {getCustomers} from '../../../customer/actions'
import {PaginateCustomersSelector} from '../../../customer/selectors'
import {Field, reduxForm, change} from 'redux-form'

class CustomerSearchBoxContainer extends React.Component {
    constructor(props) {
        super(props);

    }
    componentWillMount() {
        this.setState({
            data: []
        })
    }
    componentWillReceiveProps(nextProps) {
        let data = [];
        if(nextProps.suggestCustomers) {
            nextProps.suggestCustomers.map(customer => data.push({value: customer.id, label: customer.username}))
        }
        this.setState({data: data})
    }
    autoSuggestCustomer(value) {
        this.props.getCustomers({ identity: value });
    }

    handleWhenPasteValue(value) {
        let {data} = this.state;
        if(value) {
            let customerId = data.filter(t => t.label == value);
            let result = 0;
            if(customerId.length > 0) {
                result = customerId[0].value;
            }
            return result;
        }
    }
    render() {
        return (
            <div className="input-required">
                <Field component={Typeahead} name="customerId" className="pt-search-input"
                    placeholder="Khách hàng"
                    options={this.state.data}
                    onChange={::this.handleWhenPasteValue}
                    onInputChange={::this.autoSuggestCustomer}
                />
                <span className="required-symbol">*</span>
            </div>
        )
    }
}
const mapStateToProps = (state, props) => {
    return {
        suggestCustomers: PaginateCustomersSelector(state)
    }
};

export default connect(
    mapStateToProps, { getCustomers, change }
)(CustomerSearchBoxContainer);