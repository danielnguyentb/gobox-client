import React from 'react'
import * as RBT from 'react-bootstrap'
import DocumentTitle from '../../../../../www_components/Common/DocumentTitle'
import {Field, reduxForm} from 'redux-form'
import {renderField, renderFieldSelect} from '../../../../helpers/redux-form'
import CustomerSearchBoxContainer from './CustomerSearchBoxContainer'
import Alert from '../../../../../www_components/Alert'
const {SuccessToast, ErrorAlert} = Alert.components;
import {validate} from '../../helper'
import {PERMISSION_CUSTOMER_TRANSACTION_CREATE} from '../../../customer_transactions/constants'

const BalanceForm = ({handleSubmit, pristine, reset, submitting, invalid, error, setFieldValue, isAllow}) => (

    <form onSubmit={handleSubmit}>
        <DocumentTitle title="Điều chỉnh công nợ"/>
        <div className="col-md-4 col-md-offset-4 col-xs-12">
            <h2 className="text-center">Điều chỉnh công nợ</h2>

            <section className="content">
                <div className="row">
                    <div className="col-md-12">
                        <CustomerSearchBoxContainer setFieldValue={setFieldValue} />
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-6 col-xs-6">
                        <div className="input-required">
                            <Field name="type" component={renderFieldSelect} type="select" className="form-control"
                                options={[
                                    {value: "", label: "Loại điều chỉnh"},
                                    {value: "PAYABLE", label: "Gia tăng"},
                                    {value: "RECEIVABLE", label: "Giảm trừ"},
                                ]}
                            />
                            <span className="required-symbol">*</span>
                        </div>
                    </div>
                    <div className="col-md-6 col-xs-6 pl-0">
                        <div className="input-required">
                            <Field name="amount" component={renderField} type="text" placeholder="Số tiền" className="form-control"/>
                            <span className="required-symbol">*</span>
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-12">
                        <div className="input-required">
                            <Field name="detail" placeholder="Ghi chú công khai" component={renderField} className="form-control"/>
                            <span className="required-symbol">*</span>
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-12">
                        <RBT.FormGroup>
                            <Field name="privateNote" placeholder="Ghi chú riêng (Chỉ quản trị nhìn thấy)" component="textarea" className="form-control"/>
                        </RBT.FormGroup>
                    </div>
                </div>
                {/*<ErrorAlert dispatchTypes={[CUSTOMER_TRANSACTION_CREATE.FAILURE]}/>*/}
                {error && <div className="alert alert-danger">{error}</div>}
                {isAllow(PERMISSION_CUSTOMER_TRANSACTION_CREATE) &&
                    <div className="row">
                        <div className="col-md-12 text-right">
                            <button className="btn btn-primary" type="submit"
                                    disabled={pristine || submitting}
                            >
                                ĐIỀU CHỈNH
                            </button>
                        </div>
                    </div>
                }

            </section>
        </div>
    </form>
);

export default reduxForm({
    form: 'BalanceForm',
    validate
})(BalanceForm)