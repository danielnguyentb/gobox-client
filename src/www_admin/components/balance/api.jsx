import Client from '../../helpers/Client'
import * as ActionTypes from './ActionTypes'

class FinancialRecords extends Client {
    fetchFinancialRecords(filter){
        filter = typeof filter === 'string' ? filter : JSON.stringify(filter);
        return this.callApi({
            types: [ActionTypes.FINANCIAL_RECORDS_REQUEST, ActionTypes.FINANCIAL_RECORDS_SUCCESS, ActionTypes.FINANCIAL_RECORDS_FAILURE],
            endpoint: `/FinancialRecords?filter=${filter}`,
            method: 'GET'
        })
    }

    fetchFinancyHistory(filter){
        filter = typeof filter === 'string' ? filter : JSON.stringify(filter);
        return this.callApi({
            types: [ActionTypes.FINANCIAL_HISTORY_REQUEST, ActionTypes.FINANCIAL_HISTORY_SUCCESS, ActionTypes.FINANCIAL_HISTORY_FAILURE],
            endpoint: `/FinancialHistories?filter=${filter}`,
            method: 'GET'
        })
    }
}

export default new FinancialRecords();