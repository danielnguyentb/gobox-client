/**
 * Created by Tester-Ali on 11-11-2016.
 */
import {
    FINANCIAL_RECORDS_SUCCESS, FINANCIAL_HISTORY_SUCCESS, FINANCIAL_RECORD_CREATE, CUSTOMER_TRANSACTION_CREATE
} from './ActionTypes'
import {combineReducers} from 'redux'
import {LOCATION_CHANGE} from 'react-router-redux'

import Localize from '../../../resources/Localize'

const byId = (state = {}, action) => {
    let {response} = action;
    let copy = {...state};
    switch (action.type) {
        case FINANCIAL_RECORDS_SUCCESS:
            response.result.map(value => {
                copy[value.id] = value;
            });
            return copy;
    }
    return state;
};

const historyById = (state = {}, action) => {
    let {response} = action;
    let copy = {...state};
    switch (action.type) {
        case FINANCIAL_HISTORY_SUCCESS:
            response.result.map(value => {
                copy[value.id] = value;
            });
            return copy;
    }
    return state;
};

const metadata = (state = {}, action) => {
    switch (action.type) {
        case FINANCIAL_RECORDS_SUCCESS:
            return action.response.metadata;
    }
    return state;
};

const historyMetadata = (state = {}, action) => {
    switch (action.type) {
        case FINANCIAL_HISTORY_SUCCESS:
            return action.response.metadata;
    }
    return state;
};


const paginateHistoryIds = (state = [], action) => {
    switch (action.type) {
        case FINANCIAL_HISTORY_SUCCESS:
            return action.response.result.map(t => t.id);
    }
    return state;
};

const paginateIds = (state = [], action) => {
    switch (action.type) {
        case FINANCIAL_RECORDS_SUCCESS:
            return action.response.result.map(t => t.id);
    }
    return state;
};

const createdTransactionSuccess = (state = false, action) => {
    switch (action.type) {
        case CUSTOMER_TRANSACTION_CREATE.SUCCESS:
            return action.response;
        case LOCATION_CHANGE:
            return false;
    }
    return state;
};

export default combineReducers({
    byId, paginateIds, metadata, createdTransactionSuccess,
    historyById, historyMetadata, paginateHistoryIds,
    error: (state = null, {error}) => error ? Localize.t(error) : null,
})