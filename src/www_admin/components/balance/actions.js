import FinancialRecordsApi from './api'
import {generateQuery, generateHistoryQuery} from './helper'

export const fetchFinancialRecords = (customerId, filter) => dispatch => dispatch(FinancialRecordsApi.fetchFinancialRecords(generateQuery(customerId, filter)));

export const fetchFinancialHistories = (customerId, filter) => dispatch => dispatch(FinancialRecordsApi.fetchFinancyHistory(generateHistoryQuery(customerId, filter)));