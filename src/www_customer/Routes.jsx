import React from 'react';
import {Route, IndexRoute, Redirect} from 'react-router'
import {requireAuthentication} from './helpers/requireAuthentication'
import requireZopim from './helpers/requireZopim'
import AccessDenied from './components/Pages/Error/AccessDenied'
import NotFound from './components/Pages/Error/NotFound'
import {layouts, customer, order, balance} from './components'

const {
    MainLayout, LoginLayout, ErrorLayout,
    RegisterLayout, RootLayout
} = layouts;

export default (
    <Route>
        <Route name="order_print" path="/order_print"
               component={requireAuthentication(order.components.PrintBarcode)}/>
        <Route component={requireZopim(RootLayout)}>
            <Route path="/" component={requireAuthentication(MainLayout)}>
                <IndexRoute component={order.components.Management}/>
                <Route path="profile" component={customer.components.Detail}/>

                <Route path="orders">
                    <IndexRoute component={order.components.Management}/>
                    {/*<Route path="create" component={order.components.Form}/>*/}
                    <Route path="create" component={order.components.OrderForm}/>
                    <Route path=":orderId" component={order.components.Detail}/>
                </Route>

                <Route path="balances">
                    <IndexRoute name={balance.constants.Tab.EXPECTED} component={balance.components.Management}/>
                    <Route name={balance.constants.Tab.ACTUAL} path="actual" component={balance.components.Management}/>
                    <Route name={balance.constants.Tab.HISTORY} path="history"
                           component={balance.components.Management}/>
                </Route>

                <Route path="/change_pass" component={customer.components.ChangePass}/>
            </Route>

            <Route path="/login" component={LoginLayout}>
                <IndexRoute component={customer.components.Login}/>
            </Route>

            <Route path="/register" component={RegisterLayout}>
                <IndexRoute component={customer.components.Register}/>
            </Route>

            <Route path="/error" component={ErrorLayout}>
                <Route path="403" component={AccessDenied}/>
                <Route path="404" component={NotFound}/>
            </Route>

            <Route path="*" component={NotFound}/>
        </Route>
    </Route>
);
