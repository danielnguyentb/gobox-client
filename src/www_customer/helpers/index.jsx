import {SESSION_KEY} from '../constants'
import * as cookie from './cookie'
export {encode} from 'helpers'

export const saveSession = (session, expires) => {
    //localStorage.setItem(SESSION_KEY, JSON.stringify(session));
    // sessionStorage.setItem(SESSION_KEY, JSON.stringify(session));
    cookie.setCookie(SESSION_KEY, JSON.stringify(session), expires)
};

export const getSession = () => {
    //return JSON.parse(localStorage.getItem(SESSION_KEY)) || {};
    return JSON.parse(cookie.getCookie(SESSION_KEY)) || {};
};

export const clearSession = () => {
    //return localStorage.removeItem(SESSION_KEY);
    cookie.eraseCookie(SESSION_KEY);
};

export const getToken = () => {
    return getSession().id;
};

export const getUserId = () => {
    return +getSession().userId;
};

export const getPermissions = () => {
    return getSession().permissions;
};

export const getNeedChangePass = () => {
    return !!getSession().needChangePass;
};

export const serializeJSON = (data) => {
    return Object.keys(data).map(function (keyName) {
        return encodeURIComponent(keyName) + '=' + encodeURIComponent(data[keyName])
    }).join('&');
};