import React from 'react';
import {connect} from 'react-redux';
import * as customers from '../components/customer'
const Config = require('../../config.json');

export default (Component) => {

    class RootComponent extends React.Component {
        componentDidUpdate(prevProps) {
            let {profile} = this.props;
            if (JSON.stringify(profile) !== JSON.stringify(prevProps.profile)) {
                setTimeout(()=> {
                    window.requestAnimationFrame(function () {
                        if($zopim && $zopim.livechat) {
                            $zopim.livechat.setLanguage('vi');
                            if (profile && profile.id) {
                                const {username, email, mobile} = profile;
                                $zopim.livechat.setName(username || '');
                                $zopim.livechat.setEmail(email || '');
                                $zopim.livechat.setPhone(mobile || '');
                            } else {
                                $zopim.livechat.setName(`Khách hàng (${Config.serviceName})`);
                                $zopim.livechat.setEmail('');
                                $zopim.livechat.setPhone('');
                            }
                        } else if(zE && zE.identify){
                            zE.setLocale('vi');
                            if (profile && profile.id) {
                                let {username, email, mobile} = profile;
                                zE.identify({
                                    name: username || '',
                                    email: email || '',
                                    mobile: mobile || ''
                                });
                            } else {
                                zE.identify({
                                    name: `Khách hàng (${Config.serviceName})`,
                                    email: '',
                                    mobile: ''
                                })
                            }
                        }
                    });
                }, 1000);
            }
        }

        render() {
            return <Component {...this.props}/>;
        }
    }

    const mapStateToProps = (state) => ({
        profile: customers.selectors.getProfile(state),
    });

    return connect(mapStateToProps)(RootComponent);
}