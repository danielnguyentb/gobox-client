/**
 * Created by Tester-Ali on 07-09-2016.
 */
import {getToken} from './index'
import {callApi} from 'helpers'

export default async(endpoint, config, schema) => {
    const mergeConfig = Object.assign({}, {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }
    }, config);
    mergeConfig.headers.Authorization = getToken();
    return await callApi(endpoint, mergeConfig, schema)
}