import React from 'react';
import * as RBT from 'react-bootstrap';
import Select from 'react-select'
import WeightInput from '../../www_components/WeightInput'
import SimpleCurrencyInput from '../../www_components/SimpleCurrencyInput'
import FileInput from 'react-file-input'


export const renderFieldFileInput = ({input, label, type, className, onChange, accept, placeholder, meta: {touched, error, warning}}) => (
    <RBT.FormGroup validationState={touched && error ? 'error' : 'success'}>
        {label &&
        <RBT.ControlLabel>{label}</RBT.ControlLabel>
        }
        <FileInput name="file" {...input}
                   accept={accept}
                   placeholder={placeholder}
                   className={className}
                   onChange={ e => onChange(input.value)}
        />
        {touched && ((error && <span className="help-block">{error}</span>) || (warning && <span>{warning}</span>))}
    </RBT.FormGroup>
);

export const renderField = ({input, label, type, className, placeholder, meta: {touched, error, warning}}) => (
    <RBT.FormGroup validationState={touched && error ? 'error' : 'success'}>
        {label &&
        <RBT.ControlLabel>{label}</RBT.ControlLabel>
        }
        <input {...input} type={type} className={className} placeholder={placeholder}/>
        {touched && ((error && <span className="help-block">{error}</span>) || (warning && <span>{warning}</span>))}
    </RBT.FormGroup>
);

export const renderCodField = ({input, label, type, className, placeholder, meta: {touched, error, warning}}) => (
    <RBT.FormGroup validationState={touched && error ? 'error' : 'success'}>
        {label &&
        <RBT.ControlLabel>{label}</RBT.ControlLabel>
        }
        <SimpleCurrencyInput
            {...input}
            value={input.value ? parseInt(input.value) : null}
            precision={0}
            separator=''
            delimiter='.'
            unit=''
            autoFocus={false}
            onInputChange={input.onChange}
            onInputBlur={input.onBlur}
            type={type} className={className} placeholder={placeholder}/>
        {touched && ((error && <span className="help-block">{error}</span>) || (warning && <span>{warning}</span>))}
    </RBT.FormGroup>
);

export const renderWeightField = ({input, label, type, className, placeholder, meta: {touched, error, warning}}) => (
    <RBT.FormGroup validationState={touched && error ? 'error' : 'success'}>
        {label &&
        <RBT.ControlLabel>{label}</RBT.ControlLabel>
        }
        <WeightInput
            {...input}
            delimiter=","
            onBlur={value => input.onBlur(value)}
            type={type} className={className} placeholder={placeholder} onChange={value => input.onChange(value)}/>
        {touched && ((error && <span className="help-block">{error}</span>) || (warning && <span>{warning}</span>))}
    </RBT.FormGroup>
);

export const renderFieldSelect = ({input, label, type, className, options, defaultValue, placeholder, meta: {touched, error, warning}}) => (
    <RBT.FormGroup validationState={touched && error ? 'error' : 'success'}>
        <select type="select" {...input} className={className}>
            {options.map(option =>
                <option key={option.value} value={option.value}>
                    {option.label}
                </option>
            )}
        </select>
        {touched && ((error && <span className="help-block">{error}</span>) || (warning && <span>{warning}</span>))}
    </RBT.FormGroup>
);

export const renderSelectAddress = ({input, className, onChange, placeholder, options, meta: {touched, error, warning}}) => (
    <RBT.FormGroup validationState={touched && error ? 'error' : 'success'}>
        <Select options={options} {...input} onChange={onChange}
                onBlur={() => {
                    input.onBlur(input.value);
                }}
                placeholder={placeholder}/>
        {touched && ((error && <span className="help-block">{error}</span>) || (warning && <span>{warning}</span>))}
    </RBT.FormGroup>
);

export const renderSelect = ({input, className, onChangeSelect, placeholder, options, meta: {touched, error, warning}}) => (
    <RBT.FormGroup validationState={touched && error ? 'error' : 'success'}>
        <Select options={options} {...input}
                onChange={e => {
                    input.onChange(e);
                    onChangeSelect(e)
                }}
                onBlur={() => {
                    input.onBlur(input.value);
                }}
                placeholder={placeholder}/>
        {touched && ((error && <span className="help-block">{error}</span>) || (warning && <span>{warning}</span>))}
    </RBT.FormGroup>
);