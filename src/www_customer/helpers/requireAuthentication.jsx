import React from 'react';
import {connect} from 'react-redux';
import {push, replace} from 'react-router-redux';
import AccessDenied from '../components/Pages/Error/AccessDenied'
import NotFound from '../components/Pages/Error/NotFound'
import * as Customer from '../components/customer'
import {getToken} from './index'

export function requireAuthentication(Component) {

    class AuthenticatedComponent extends React.Component {

        componentWillMount() {
            this.checkAuth(this.props.isAuthenticated);
        }

        componentDidUpdate(){
            const {isAuthenticated, dispatch, token} = this.props;
            this.checkAuth(isAuthenticated);
            if(token != getToken()){
                dispatch(Customer.actions.logoutSaga());
                dispatch(Customer.actions.authClear());
            }
        }

        checkAuth(isAuthenticated) {
            if (!isAuthenticated) {
                let redirectAfterLogin = this.props.location.pathname;
                this.props.dispatch(replace(`/login?r=${redirectAfterLogin}`));
            }
        }

        render() {
            if (this.props.isAccessDenied) {
                return (<AccessDenied />);
            }
            if (this.props.isNotFound) {
                return (<NotFound />);
            }
            return (this.props.isAuthenticated
                ? <Component {...this.props}/>
                : <div/>);
        }
    }

    const mapStateToProps = (state) => ({
        token: Customer.selectors.tokenSelector(state),
        session: state.customer.session,
        isNotFound: state.isNotFound,
        isAccessDenied: state.isAccessDenied,
        isAuthenticated: state.customer.isAuthenticated
    });

    return connect(mapStateToProps)(AuthenticatedComponent);

}