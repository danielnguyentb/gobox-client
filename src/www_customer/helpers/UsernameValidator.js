"use strict";
/**
 * Created by Piggat on 11/22/2016.
 */
let bannedUsername = [
    "admin", "quantri", "moderator", "gobox", "goboxvn", "god", "shippo", "shippo.vn", "shippovn",
    "nguyenphutrong", "trandaiquang", "nguyenthikimngan", "nguyenxuanphuc",
    //một số lãnh đạo nổi tiếng
    "hochiminh", "vonguyengiap", "truongchinh", "nguyentatthanh",
    //một số tên phản cách mạng
    "viettan", "dangcongsan", "dmcs"
];
let bannedKeyword = [
    "fuck", "ditme", "ditbo", "deome", "mekiep", "bastard", "nhucut", "nigger", "dcm", "cailon", "dick", "hitler"
];
class UsernameValidator {
    static validate(username) {
        username = username.toLowerCase();
        if (bannedUsername.indexOf(username) > -1) {
            return {
                isValid: false,
                error: "Tên đăng nhập này không được phép sử dụng"
            };
        }
        for (let keyword of bannedKeyword) {
            let regex = new RegExp(keyword);
            if (username.search(regex) >= 0) {
                return {
                    isValid: false,
                    error: "Tên đăng nhập này không được phép sử dụng do chứa các ký tự có thể gây phản cảm"
                };
            }
        }
        return {
            isValid: true
        };
    }
}
exports.UsernameValidator = UsernameValidator;
//# sourceMappingURL=UsernameValidator.js.map