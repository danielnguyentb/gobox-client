export const required = (val) => val && val.length;
export const maxLength = (len) => (val) => val ? val.length <= len : undefined;
export const minLength = (len) => (val) => val ? val.length >= len : undefined;
export const isNumber = (val) => !isNaN(Number(val));
// export const rangeNumber = (min, max) => (val) => val ? (min <= val.length && val.length <= max) : undefined;
export const rangeNumber = (min, max) => (val) => (!val || val =='') ? true : ((min <= val.length && val.length <= max));