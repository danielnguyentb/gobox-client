import React, {Component} from 'react';
import 'bootstrap';
import 'admin-lte';

import './css/font/Source-Sans-Pro/_font-source-san-pro.scss';
import './css/font/themify-icon/_themify-icons.scss';
import './css/font/fontawesome/_font-awesome.scss';
import './css/general/_theme-default.scss';
import './css/general/_theme-admin.scss';
import './css/general/_react-bootstrap-switch.scss';
import './css/general/_default.scss';
import './css/general/_all-skins.scss';
import './css/general/_generals.scss';
import './css/general/_responsive.scss'
import './css/general/_pagination.scss'

import Header from './widgets/Header'
import SideBar from './widgets/SideBar'
import Footer from './widgets/Footer'
import {connect} from 'react-redux'

class MainLayout extends Component {
    render() {
        const {dispatch} = this.props;
        return (
            <div className="mainlayout">
                <Header {...this.props} />
                <SideBar {...this.props} />
                <div className="content-wrapper">
                    {this.props.children && React.cloneElement(this.props.children, {
                        dispatch
                    })}

                    <Footer {...this.props} />
                </div>
            </div>
        )
    }
}

export default connect()(MainLayout);