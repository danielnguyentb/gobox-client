export const NAME = 'balance';

export const PERMISSION_FINANCIAL_HISTORY_VIEW_LIST = 'PERMISSION_FINANCIAL_HISTORY_VIEW_LIST';

export const Status = {
    EXPECTED: 'EXPECTED',
    COMPLETED: 'COMPLETED'
};

export const Tab = {
    EXPECTED: 'EXPECTED',
    ACTUAL: 'ACTUAL',
    HISTORY: 'HISTORY'
};

export const TabIndex = {
    [Tab.EXPECTED]: 0,
    [Tab.ACTUAL]: 1,
    [Tab.HISTORY]: 2
};

export const StatusDetail = {
    [Status.EXPECTED]: {
        'label': 'Dự kiến',
        'balance': 'projectedBalance'

    },
    [Status.COMPLETED]: {
        'label': 'Đối soát',
        'balance': 'realBalance'
    }
};

export const ObjectTypes = {
    CustomerDeliveryOrder: 'CustomerDeliveryOrder'
};

export const Types = {
    SERVICE: 'SERVICE'
};

export const TypesDetail = {
    [Types.SERVICE]: {
        label: 'Vận đơn'
    }
};