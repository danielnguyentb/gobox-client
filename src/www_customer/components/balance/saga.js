/**
 * Created by Tester-Ali on 30-11-2016.
 */
import {takeLatest} from 'redux-saga'
import {call, take, put, fork, select} from 'redux-saga/effects'
import {LOCATION_CHANGE} from '../../ActionTypes'
import {fetchFinancialRecordsSaga, fetchFinancialHistoriesSaga} from './actions'
import {getProfileId} from '../customer/selectors'
import {Status} from './constants'

export function* watchLocationChange(action) {
    const {pathname, query} = action.payload;
    const customerId = yield select(getProfileId);
    if (pathname.search('balances/history') >= 0) {
        yield put(fetchFinancialHistoriesSaga(customerId, query));
    } else if (pathname.search('balances') >= 0) {
        yield put(fetchFinancialRecordsSaga(customerId, {status: Status.EXPECTED, ...query}));
    }
}

export default function*() {
    yield takeLatest(LOCATION_CHANGE, watchLocationChange);
}