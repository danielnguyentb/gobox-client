import {generateQuery, generateHistoryQuery} from './helper.jsx'
import {CALL_API} from '../../constants'
import * as ActionTypes from './ActionTypes'

export const fetchFinancialHistoriesSaga = (customerId, filter) => {
    filter = JSON.stringify(generateHistoryQuery(customerId, filter));
    return {
        type: CALL_API,
        payload: {
            types: [ActionTypes.FINANCIAL_HISTORY_REQUEST, ActionTypes.FINANCIAL_HISTORY_SUCCESS, ActionTypes.FINANCIAL_HISTORY_FAILURE],
            method: 'GET',
            endpoint: `/FinancialHistories?filter=${filter}`,
        }
    }
};
export const fetchFinancialRecordsSaga = (customerId, filter) => {
    filter = JSON.stringify(generateQuery(customerId, filter));
    return {
        type: CALL_API,
        payload: {
            types: [ActionTypes.FINANCIAL_RECORDS_REQUEST, ActionTypes.FINANCIAL_RECORDS_SUCCESS, ActionTypes.FINANCIAL_RECORDS_FAILURE],
            method: 'GET',
            endpoint: `/FinancialRecords?filter=${filter}`,
        }
    }
};