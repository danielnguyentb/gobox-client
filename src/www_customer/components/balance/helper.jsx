import {encode} from '../../helpers'
import {isNumber} from '../../helpers/validators'
import {numeral} from '../../helpers/format'

export const generateQuery = (customerId, params) => {
    let where = {};
    let or_where = [];
    let {
        status, skip = 0, limit = 20, order = ['createdTime DESC', 'id DESC'],
        type, createdTimeFrom, createdTimeTo, operator, from, to, object,
        transactionTimeFrom, transactionTimeTo, code,
    } = params;
    where.and = [{or: or_where}];
    if (customerId) where.and.push({customerId});
    if (status) where.and.push({status});
    if (type) where.and.push({type});
    if (code) where.and.push({code: {like: encode(code)}});
    if (createdTimeFrom && createdTimeTo) {
        // where.and.push({createdTime: {gt: createdTimeFrom}});
        // where.and.push({createdTime: {lt: createdTimeTo}});
        where.and.push({createdTime: {between: [createdTimeFrom, createdTimeTo]}})
    }
    if (transactionTimeFrom && transactionTimeTo) {
        where.and.push({transactionTime: {between: [transactionTimeFrom, transactionTimeTo]}})
    }
    if (operator && object && from) {
        from = numeral().unformat(from);
        to = numeral().unformat(to);
        switch (operator) {
            case 'between':
                where.and.push({
                    [object]: {[operator]: [+from, +to]}
                });
                break;
            case 'eq':
                where.and.push({
                    [object]: +from
                });
                break;
            default:
                where.and.push({
                    [object]: {[operator]: +from}
                });
                break;
        }
    }
    return {where, skip, limit, order};
};

export const generateHistoryQuery = (customerId, params) => {
    let where = {};
    let or_where = [];
    let {
        status, skip = 0, limit = 20, order = ['createdTime DESC', 'id DESC'],
        type, createdTimeFrom, createdTimeTo, operator, from, to, object,
        transactionTimeFrom, transactionTimeTo, objectCode,
    } = params;
    where.and = [{or: or_where}];
    if (customerId) where.and.push({customerId});
    if (status) where.and.push({status});
    if (type) where.and.push({type});
    if (objectCode) where.and.push({objectCode: {like: encode(objectCode)}});
    if (createdTimeFrom && createdTimeTo) {
        // where.and.push({createdTime: {gt: createdTimeFrom}});
        // where.and.push({createdTime: {lt: createdTimeTo}});
        where.and.push({createdTime: {between: [createdTimeFrom, createdTimeTo]}})
    }
    if (transactionTimeFrom && transactionTimeTo) {
        where.and.push({transactionTime: {between: [transactionTimeFrom, transactionTimeTo]}})
    }
    if (operator && object && from) {
        from = numeral().unformat(from);
        to = numeral().unformat(to);
        switch (operator) {
            case 'between':
                where.and.push({
                    [object]: {[operator]: [+from, +to]}
                });
                break;
            case 'eq':
                where.and.push({
                    [object]: +from
                });
                break;
            default:
                where.and.push({
                    [object]: {[operator]: +from}
                });
                break;
        }
    }
    return {where, skip, limit, order};
};

export const validate = values => {
    const errors = {};
    if(Object.keys(values).length === 0) return errors;
    if (!values.type || values.type == "") {
        errors.type = 'Bạn chưa chọn loại điều chỉnh công nợ'
    }
    if (!values.amount) {
        errors.amount = 'Bạn chưa nhập số tiền điều chỉnh'
    }
    if (!values.customerEmail) {
        errors.customerEmail = 'Bạn chưa nhập khách hàng cần điều chỉnh'
    }
    if (values.amount) {
        if (!isNumber(values.amount)) {
            errors.amount = 'Số tiền phải là chữ số'
        } else if (values.amount == 0) {
            errors.amount = 'Số tiền điều chỉnh phải khác 0'
        }
    }
    if (!values.detail) {
        errors.detail = 'Bạn chưa nhập ghi chú công khai'
    }
    // if (!values.privateNote) {
    //     errors.privateNote = 'Bạn chưa nhập ghi chú riêng'
    // }
    return errors
};