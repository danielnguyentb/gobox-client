/**
 * Created by Tester-Ali on 09-11-2016.
 */
import {createSelectorCreator, defaultMemoize, createSelector} from 'reselect'
import lodash from 'lodash'
import {NAME, Status, ObjectTypes} from './constants'

const createDeepEqualSelector = createSelectorCreator(
    defaultMemoize,
    lodash.isEqual
);

export const balanceSelector = state => state[NAME];

export const balancesSelector = createDeepEqualSelector(
    balanceSelector,
    balance => Object.values(balance.byId)
);

export const historiesSelector = createDeepEqualSelector(
    balanceSelector,
    balance => Object.values(balance.historyById)
);


const hydrate = (balance) => {
    const {objectType, objectId} = balance;
    balance.objectUri = ObjectTypes.CustomerDeliveryOrder == objectType ? `/orders/${objectId}` : '';
    return balance;
};

const hydrateHistory = (balance) => {
    const {objectType, objectId} = balance;
    balance.objectUri = ObjectTypes.CustomerDeliveryOrder == objectType ? `/orders/${objectId}` : '';
    return balance;
};

export const BalanceByIdSelector = createDeepEqualSelector(
    balanceSelector,
    balance => lodash.memoize(
        id => hydrate(balance.byId[id])
    )
);

export const HistoryByIdSelector = createDeepEqualSelector(
    balanceSelector,
    balance => lodash.memoize(
        id => hydrateHistory(balance.historyById[id])
    )
);

export const PaginateBalancesSelector = createSelector(
    [balanceSelector, BalanceByIdSelector],
    (balance, getBalance) => balance.paginateIds.map(id => getBalance(id))
);

export const PaginateHistoriesSelector = createSelector(
    [balanceSelector, HistoryByIdSelector],
    (balance, getHistory) => balance.paginateHistoryIds.map(id => getHistory(id))
);