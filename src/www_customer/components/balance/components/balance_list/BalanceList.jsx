import React from 'react'
import DocumentTitle from './../../../../../www_components/DocumentTitle'
import {ExpectedContainerConnected} from './expected/ExpectedContainer'
import {ActualContainerConnected} from './actual/ActualContainer'
import {HistoryContainerConnected} from './history/HistoryContainer'
import 'react-bootstrap-daterangepicker/css/daterangepicker.css'
import './../balance.scss'
import {Tab, Tabs, TabList, TabPanel} from 'react-tabs';
import {CustomerBalanceContainerConnected} from './customer_balance/CustomerBalanceContainer'

class BalanceList extends React.Component {
    render() {
        const {handleSelect, selected} = this.props;
        return (
            <div>
                <DocumentTitle title="Danh sách công nợ"/>
                <CustomerBalanceContainerConnected {...this.props} />
                <section className="content">
                    <div className="box">
                        <div className="row">
                            <div className="col-md-12">
                                <Tabs
                                    onSelect={handleSelect}
                                    selectedIndex={selected}>
                                    <TabList className="tab-balance">
                                        <Tab>DỰ KIẾN</Tab>
                                        <Tab>ĐÃ CHỐT</Tab>
                                        <Tab>LỊCH SỬ</Tab>
                                    </TabList>
                                    <TabPanel>
                                        <ExpectedContainerConnected {...this.props} />
                                    </TabPanel>
                                    <TabPanel>
                                        <ActualContainerConnected {...this.props} />
                                    </TabPanel>
                                    <TabPanel>
                                        <HistoryContainerConnected {...this.props} />
                                    </TabPanel>
                                </Tabs>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        )
    }
}
export default (BalanceList);