import React, {PropTypes, Component} from 'react'
import {Actual} from './Actual'
import {connect} from 'react-redux'
import {push} from 'react-router-redux'
import {Status} from '../../../constants'

export class ActualContainer extends Component {
    static propTypes = {
        customerId: PropTypes.number.isRequired,
        location: PropTypes.object.isRequired,
        reset: PropTypes.bool
    };

    search(data = {}) {
        const {dispatch, location} = this.props;
        const query = {...location.query, ...data, skip: 0, object: 'amount', status: Status.COMPLETED};
        dispatch(push({
            pathname: '/balances/actual',
            query
        }))
    }

    render() {
        const {customerId, location} = this.props;
        return (
            <Actual
                customerId={customerId}
                location={location}
                onSearch={::this.search}/>
        )
    }
}

export const ActualContainerConnected = connect()(ActualContainer);