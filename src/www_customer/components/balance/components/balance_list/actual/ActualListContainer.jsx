import React, {PropTypes} from 'react'
import {connect} from 'react-redux'
import {ActualList} from './ActualList'
import {PaginateBalancesSelector, balanceSelector} from '../../../selectors'

export class ActualListContainer extends React.Component {
    static propTypes = {
        location: PropTypes.object.isRequired,
        customerId: PropTypes.number.isRequired,
    };

    render() {
        const {records} = this.props;
        return (
            <ActualList
                records={records}
            />
        )
    }
}

const mapStateToProps = (state, props) => {
    return {
        records: PaginateBalancesSelector(state)
    }
};
export const ActualListContainerConnected = connect(mapStateToProps)(ActualListContainer);