import React from 'react'
import {formatFullTime, formatCurrency} from '../../../../../helpers/format'
import Link from '../../../../../../www_components/Link'

export const ActualItem = ({amount, customerEndingBalance, objectUri, code, createdTime, transactionTime}) => {
    return (
        <tr>
            <td>{formatFullTime(createdTime)}</td>
            <td>{formatFullTime(transactionTime)}</td>
            <td className="fw-600"><Link to={objectUri}>{code || '--'}</Link></td>
            <td>{amount == null ? '' : formatCurrency(amount)}</td>
            <td>{formatCurrency(customerEndingBalance)}</td>
        </tr>
    )
};