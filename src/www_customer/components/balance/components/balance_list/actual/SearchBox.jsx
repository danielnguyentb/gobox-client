import React from 'react'
import {FormGroup} from 'react-bootstrap'
import {ChoosePriceRange, CreatedTimeFields, TransactionTimeFields, CodeField} from '../search_fields'

export const SearchForm = ({handleSubmit, initialValues}) => {
    return (
        <form onSubmit={handleSubmit}>
            <div className="col-md-3 col-xs-12 pl-0">
                <CreatedTimeFields
                    createdTimeFrom={initialValues.createdTimeFrom}
                    createdTimeTo={initialValues.createdTimeTo}
                    onSubmit={handleSubmit}/>
            </div>
            <div className="col-md-3 col-xs-12 pl-0">
                <TransactionTimeFields
                    transactionTimeFrom={initialValues.transactionTimeFrom}
                    transactionTimeTo={initialValues.transactionTimeTo}
                    onSubmit={handleSubmit}/>
            </div>
            <div className="col-md-2 col-xs-12 pl-0">
                <FormGroup>
                    <label className="text-base fs-15">Đối tượng</label>

                    <CodeField
                        value={initialValues.code}
                        onSubmit={handleSubmit}/>
                </FormGroup>
            </div>
            <div className="col-md-4 col-xs-12 pl-0">
                <label className="text-base fs-15">Chọn giá trị</label>
                <br/>
                <ChoosePriceRange
                    onSubmit={handleSubmit}
                    operator={initialValues.operator}
                    from={initialValues.from}
                    to={initialValues.to}
                />
            </div>
        </form>
    )
};