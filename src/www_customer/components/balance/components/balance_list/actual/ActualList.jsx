import React from 'react'
import {ActualItem} from './ActualItem'
export const ActualList = ({records}) => {
    return (
        <table className="table tbl-headblue table-striped table-hover">
            <thead>
            <tr>
                <th>Ngày phát sinh</th>
                <th>Ngày đối soát</th>
                <th>Đối tượng</th>
                <th>Giá trị (VNĐ)</th>
                <th>Dư</th>
            </tr>
            </thead>
            <tbody>
            {records.length > 0 ? records.map(t => <ActualItem key={t.id} {...t}/>)
            : <tr><td colSpan="100%">Không tìm thấy kết quả nào</td></tr>
            }
            </tbody>
        </table>
    )
};