import React, {PropTypes} from 'react'
import {ActualListContainerConnected} from './ActualListContainer'
import {SearchForm} from './SearchBox'
import {RePagination} from '../../../../../../www_components/RePagination'

export const Actual = ({onSearch, customerId, location, ...others}) => {
    return (
        <div className="tab-pane" id="actual">
            <div className="row">
                <div className="col-md-12">
                    <SearchForm
                        initialValues={{...location.query, object: 'amount'}}
                        handleSubmit={onSearch}
                    />
                </div>
            </div>

            <div className="row">
                <ActualListContainerConnected
                    customerId={customerId}
                    location={location}
                    {...others} />
            </div>

            <div className="row">
                <div className="col-md-12 text-right">
                    <RePagination {...location}/>
                </div>
            </div>
        </div>
    )
};