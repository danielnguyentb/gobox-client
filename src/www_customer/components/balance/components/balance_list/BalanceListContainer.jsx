import React, {PropTypes} from 'react'
import BalanceList from './BalanceList'
import {connect} from 'react-redux'
import {TabIndex, Status} from '../../constants'
import {getProfileId} from '../../../customer/selectors'
import {push} from 'react-router-redux'
import './../order.scss'

class BalanceListContainer extends React.Component {
    static propTypes = {
        customerId: PropTypes.number.isRequired
    };

    constructor(props) {
        super(props);
        const {name} = props.route;
        this.state = {selectedIndex: TabIndex[name] || 0};
    }

    selectTab(index, last) {
        const {dispatch} = this.props;
        switch (index){
            case TabIndex.EXPECTED:
                dispatch(push({
                    pathname: '/balances',
                    query: {status: Status.EXPECTED}
                }));
                break;
            case TabIndex.ACTUAL:
                dispatch(push({
                    pathname: '/balances/actual',
                    query: {status: Status.COMPLETED}
                }));
                break;
            case TabIndex.HISTORY:
                dispatch(push('balances/history'));
                break;
            default:
                dispatch(push('/balances'));
        }
    }

    componentDidUpdate(prevProps) {
        const {name} = this.props['route'];
        if (name != prevProps['route']['name']) {
            this.setState({selectedIndex: TabIndex[name] || 0});
        }
    }

    render() {
        const {customerId, location, dispatch} = this.props;
        const {selectedIndex} = this.state;
        return (
            <BalanceList
                customerId={customerId}
                selected={selectedIndex}
                location={location}
                dispatch={dispatch}
                handleSelect={::this.selectTab}/>
        );
    }
}

const mapStateToProps = (state, props) => {
    return {
        customerId: +getProfileId(state),
    }
};
export default connect(mapStateToProps)(BalanceListContainer);