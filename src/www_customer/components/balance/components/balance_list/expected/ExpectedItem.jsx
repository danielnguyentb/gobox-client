import React from 'react'
import {formatFullTime, formatCurrency} from '../../../../../helpers/format'
import Link from '../../../../../../www_components/Link'
export const ExpectedItem = ({amount, objectUri, code, createdTime}) => {
    return (
        <tr>
            <td>{formatFullTime(createdTime)}</td>
            <td className="fw-600"><Link to={objectUri}>{code || '--'}</Link></td>
            <td>{amount == null ? '' : formatCurrency(amount)}</td>
        </tr>
    )
};