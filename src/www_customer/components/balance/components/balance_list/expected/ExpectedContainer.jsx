import React, {PropTypes, Component} from 'react'
import {Status} from '../../../constants'
import {Expected} from './Expected'
import {connect} from 'react-redux'
import {push} from 'react-router-redux'

export class ExpectedContainer extends Component {
    static propTypes = {
        customerId: PropTypes.number.isRequired,
        location: PropTypes.object.isRequired,
        reset: PropTypes.bool
    };

    search(data = {}) {
        const {dispatch, location} = this.props;
        const query = {...location.query, ...data, object: 'amount', skip: 0, status: Status.EXPECTED};
        dispatch(push({
            pathname: '/balances',
            query
        }));
    }

    render() {
        const {customerId, location} = this.props;
        return (
            <Expected
                customerId={customerId}
                location={location}
                onSearch={::this.search}/>
        )
    }
}
export const ExpectedContainerConnected = connect()(ExpectedContainer);