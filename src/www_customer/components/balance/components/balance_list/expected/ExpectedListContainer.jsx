import React, {PropTypes} from 'react'
import {connect} from 'react-redux'
import {ExpectedList} from './ExpectedList'
import {PaginateBalancesSelector} from '../../../selectors'

export class ExpectedListContainer extends React.Component {
    static propTypes = {
        location: PropTypes.object.isRequired,
        customerId: PropTypes.number.isRequired,
    };
    render() {
        const {records} = this.props;
        return (
            <ExpectedList
                records={records}
            />
        )
    }
}

const mapStateToProps = (state, props) => {
    return {
        records: PaginateBalancesSelector(state),
    }
};
export const ExpectedListContainerConnected = connect(mapStateToProps)(ExpectedListContainer);