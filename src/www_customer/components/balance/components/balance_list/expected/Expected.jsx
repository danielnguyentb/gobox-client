import React from 'react'
import {ExpectedListContainerConnected} from './ExpectedListContainer'
import {SearchForm} from './SearchBox'
import {RePagination} from '../../../../../../www_components/RePagination'

export const Expected = ({onSearch, location, customerId}) => {
    return (
        <div className="tab-pane active" id="expected">
            <div className="row">
                <div className="col-md-12">
                    <SearchForm
                        initialValues={{...location.query, object: 'amount'}}
                        handleSubmit={onSearch}
                    />
                </div>
            </div>

            <div className="row">
                <ExpectedListContainerConnected
                    customerId={customerId}
                    location={location}
                />
            </div>

            <div className=" row">
                <div className="col-md-12 text-right">
                    <RePagination {...location}/>
                </div>
            </div>
        </div>
    )
};