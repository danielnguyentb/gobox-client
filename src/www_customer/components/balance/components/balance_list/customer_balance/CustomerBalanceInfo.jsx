import React, {PropTypes, Component} from 'react'
import {formatCurrency} from '../../../../../helpers/format'
export const CustomerBalanceInfo = ({projectedBalance, realBalance}) => {
    return (
        <section className="content-header">
            <div className="row">
                <div className="col-md-6 col-xs-12">
                    <h3 className="mt-0 mb-0">Danh sách công nợ</h3>
                </div>
                <div className="col-md-6 col-xs-12">
                    <div className="row">
                        <div className="col-md-offset-6 col-md-3 col-xs-6">
                            <p className="fs-16">Dự kiến</p>
                            <p className="fw-600 fs-16">{formatCurrency(projectedBalance)}</p>
                        </div>
                        <div className="col-md-3 col-xs-6">
                            <p className="fs-16">Dư nợ</p>
                            <p className="fw-600 fs-16">{formatCurrency(realBalance)}</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
};