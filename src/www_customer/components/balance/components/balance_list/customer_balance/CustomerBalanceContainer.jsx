import React, {PropTypes, Component} from 'react'
import {connect} from 'react-redux'
import {CustomerBalanceInfo} from './CustomerBalanceInfo'
import {getProfile} from '../../../../customer/selectors'

export class CustomerBalanceContainer extends Component {
    static propTypes = {
        customerId: PropTypes.number.isRequired,
    };

    render() {
        const {customer} = this.props;
        return (
            <CustomerBalanceInfo
                {...customer}
            />
        )
    }
}
const mapStateToProps = (state, props) => {
    return {
        customer: getProfile(state)
    };
};
export const CustomerBalanceContainerConnected = connect(mapStateToProps)(CustomerBalanceContainer);