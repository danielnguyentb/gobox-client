import React from 'react'
import SelectDateRangePicker from '../../../../../../www_components/SelectDateRangePicker'
import {TypesDetail} from '../../../constants'

export const CreatedTimeFields = (fields) => {
    const {createdTimeFrom, createdTimeTo, onSubmit} = fields;
    return (
        <SelectDateRangePicker
            onChange={(e)=> {
                const startDate = e.startDate ? e.startDate.toISOString() : '';
                const endDate = e.endDate ? e.endDate.toISOString() : '';
                onSubmit({createdTimeFrom: startDate, createdTimeTo: endDate});
            }}
            startDate={createdTimeFrom}
            endDate={createdTimeTo}
            dateLabel="Khoảng TG phát sinh"/>
    )
};

export const TypesSelect = ({value, className, onSubmit}) => (
    <select
        value={value}
        onChange={e => {
            onSubmit({type: e.target.value});
        }}
        className="form-control">
        <option value="">Loại công nợ</option>
        {Object
            .keys(TypesDetail)
            .map(key => <option key={key} value={key}>{TypesDetail[key]['label']}</option>)}
    </select>
);

export const TransactionTimeFields = (fields) => {
    const {transactionTimeFrom, transactionTimeTo, onSubmit} = fields;
    return (
        <SelectDateRangePicker
            onChange={(e)=> {
                const startDate = e.startDate ? e.startDate.toISOString() : '';
                const endDate = e.endDate ? e.endDate.toISOString() : '';
                onSubmit({transactionTimeFrom: startDate, transactionTimeTo: endDate});
            }}
            startDate={transactionTimeFrom}
            endDate={transactionTimeTo}
            dateLabel="Khoảng TG đối soát"/>
    )
};

export const CodeField = ({value, onSubmit}) => {
    return (
        <input
            value={value}
            onBlur={e => {
                onSubmit({code: e.target.value});
            }}
            onKeyUp={e => {
                if(e.key == 'Enter') {
                    onSubmit({code: e.target.value});
                }
            }}
            type="text" placeholder="Nhập đối tượng"
            className="form-control"/>
    )
};

export const ObjectCodeField = ({value, onSubmit}) => {
    return (
        <input
            value={value}
            onBlur={e => {
                onSubmit({objectCode: e.target.value});
            }}
            onKeyUp={e => {
                if(e.key == 'Enter') {
                    onSubmit({objectCode: e.target.value});
                }
            }}
            type="text" placeholder="Nhập đối tượng"
            className="form-control"/>
    )
};


export const ChoosePriceRange = fields => {
    const {operator, from, to, onSubmit} = fields;
    return (
        <div>
            <select
                value={operator || 'eq'}
                onChange={e => {
                    onSubmit({operator: e.target.value});
                }}
                className="form-control mb-15 pull-left opt-search-time"
            >
                <option value="eq">=</option>
                <option value="lte">&lt;=</option>
                <option value="lt">&lt;</option>
                <option value="gte">&gt;=</option>
                <option value="gt">&gt;</option>
                <option value="between">Khoảng</option>
            </select>

            <div className="btn-group btn-group-toggle mb-15 mr-5">
                <input
                    defaultValue={from ? from.trim() : from}
                    type="text"
                    onBlur={e => {
                        onSubmit({from: e.target.value});
                    }}
                    onKeyUp={e => {
                        if(e.key == 'Enter') {
                            onSubmit({from: e.target.value});
                        }
                    }}
                    className="form-control input-balance"
                    placeholder={operator == 'between' ? 'Từ' : 'Giá trị'}
                />
            </div>


            {operator == 'between' &&
            <div className="btn-group btn-group-toggle mb-15 mr-5">
                <input
                    onBlur={e => {
                        onSubmit({to: e.target.value});
                    }}
                    onKeyUp={e => {
                        if(e.key == 'Enter') {
                            onSubmit({to: e.target.value});
                        }
                    }}
                    defaultValue={to ? to.trim() : to}
                    type="text"
                    className="form-control input-balance"
                    placeholder="Tới"
                />
            </div>
            }
        </div>
    )
};
