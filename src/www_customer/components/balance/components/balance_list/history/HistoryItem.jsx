import React from 'react'
import {formatFullTime, formatCurrency} from '../../../../../helpers/format'
import Link from '../../../../../../www_components/Link'
export const HistoryItem = ({id, amount, objectUri, objectCode, createdTime}) => {
    return (
        <tr id={id}>
            <td>{formatFullTime(createdTime)}</td>
            <td className="fw-600"><Link to={objectUri}>{objectCode || '--'}</Link></td>
            <td>{amount == null ? '' : formatCurrency(amount)}</td>
        </tr>
    )
};