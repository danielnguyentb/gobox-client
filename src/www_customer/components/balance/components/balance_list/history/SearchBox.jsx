import React from 'react'
import {FormGroup} from 'react-bootstrap'
import {ChoosePriceRange, CreatedTimeFields, TypesSelect, ObjectCodeField} from '../search_fields'


export const SearchForm = ({handleSubmit, initialValues}) => {
    return (
        <form method="POST" onSubmit={handleSubmit}>
            <div className="col-md-2 col-xs-12 pl-0">
                <FormGroup>
                    <label className="text-base fs-15">Loại công nợ</label>
                    <br/>
                    <TypesSelect onSubmit={handleSubmit} value={initialValues.type}/>
                </FormGroup>
            </div>
            <div className="col-md-3 col-xs-12 pl-0">
                <CreatedTimeFields
                    createdTimeFrom={initialValues.createdTimeFrom}
                    createdTimeTo={initialValues.createdTimeTo}
                    onSubmit={handleSubmit}/>
            </div>
            <div className="col-md-2 col-xs-12 pl-0">
                <FormGroup>
                    <label className="text-base fs-15">Đối tượng</label>
                    <ObjectCodeField
                        value={initialValues.objectCode}
                        onSubmit={handleSubmit}/>
                </FormGroup>
            </div>
            <div className="col-md-5 col-xs-12 pl-0">
                <label className="text-base fs-15">Chọn giá trị</label>
                <br/>
                <ChoosePriceRange
                    onSubmit={handleSubmit}
                    operator={initialValues.operator}
                    object={initialValues.object}
                    from={initialValues.from}
                    to={initialValues.to} />
            </div>
        </form>
    )
};