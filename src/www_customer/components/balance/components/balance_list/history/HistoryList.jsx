import React from 'react'
import {HistoryItem} from './HistoryItem'
export const HistoryList = ({records}) => {
    return (
        <table className="table tbl-headblue table-striped table-hover">
            <thead>
            <tr>
                <th>Ngày phát sinh</th>
                <th>Đối tượng</th>
                <th>Giá trị (VNĐ)</th>
            </tr>
            </thead>
            <tbody>
            {records.length > 0 ? records.map(t => <HistoryItem key={t.id} {...t}/>)
                : <tr><td colSpan="100%">Không tìm thấy kết quả nào</td></tr>
            }
            </tbody>
        </table>
    )
};