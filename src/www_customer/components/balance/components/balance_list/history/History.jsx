import React from 'react'
import {HistoryListContainerConnected} from './HistoryListContainer'
import {SearchForm} from './SearchBox'
import {RePagination} from '../../../../../../www_components/RePagination'

export const History = ({onSearch, location, customerId, query}) => {
    return (
        <div className="tab-pane active" id="expected">
            <div className="row">
                <div className="col-md-12">
                    <SearchForm
                        initialValues={{...location.query, object: 'amount'}}
                        handleSubmit={onSearch}
                    />
                </div>
            </div>

            <div className="row">
                <HistoryListContainerConnected
                    customerId={customerId}
                    location={location}
                />
            </div>

            <div className=" row">
                <div className="col-md-12 text-right">
                    <RePagination {...location}/>
                </div>
            </div>
        </div>
    )
};