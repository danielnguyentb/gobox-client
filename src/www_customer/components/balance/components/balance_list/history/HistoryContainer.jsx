import React, {PropTypes, Component} from 'react'
import {History} from './History'
import {connect} from 'react-redux'
import {push} from 'react-router-redux'

export class HistoryContainer extends Component {
    static propTypes = {
        customerId: PropTypes.number.isRequired,
    };

    search(data = {}) {
        const {dispatch, location} = this.props;
        const query = {...location.query, ...data, skip: 0, object: 'amount'};
        dispatch(push({
            pathname: '/balances/history',
            query
        }));
    }

    render() {
        const {customerId, location, metadata} = this.props;
        const query = {...metadata, ...location.query};
        return (
            <History
                query={query}
                customerId={customerId}
                location={location}
                onSearch={::this.search}/>
        )
    }
}
export const HistoryContainerConnected = connect()(HistoryContainer);