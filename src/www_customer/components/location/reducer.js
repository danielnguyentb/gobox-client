import {
    LOCATION_PROVINCES_SUCCESS,
    LOCATION_DISTRICTS_SUCCESS
} from './ActionTypes'
import {combineReducers} from 'redux'
import Localize from '../../../resources/Localize'
import {TYPE_DISTRICT, TYPE_PROVINCE} from './constants'

const byId = (state = {}, action) => {
    let copy = {...state};
    switch (action.type) {
        case LOCATION_PROVINCES_SUCCESS:
            action.response.map(t => {
                t['type'] = TYPE_PROVINCE;
                copy[t.id] = t;
            });
            return copy;
        case LOCATION_DISTRICTS_SUCCESS:
            action.response.map(t => {
                t['type'] = TYPE_DISTRICT;
                copy[t.id] = t;
            });
            return copy;
    }
    return state;
};


export default combineReducers({
    byId,
    error: (state = null, {error}) => error ? Localize.t(error) : null,
    requestingProvinces: (state = false, {requestingProvinces}) => typeof requestingProvinces === 'boolean' ? requestingProvinces : state,
})