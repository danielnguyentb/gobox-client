export const NAME = 'location';

export const TYPE_PROVINCE = 'PROVINCE';
export const TYPE_DISTRICT = 'DISTRICT';

export const DISABLED = 'DISABLED';
export const ENABLED = 'ENABLED';