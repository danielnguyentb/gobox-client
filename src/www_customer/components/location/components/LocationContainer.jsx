import React, {Component, PropTypes} from 'react'
import {connect} from 'react-redux';
import {getProvinces, getDistrictsById} from '../actions'
import {ProvinceOptionsSelector, DistrictOptionsSelector} from '../selectors'
import LocationBox from './LocationBox'

class SelectAddress extends React.Component {
    static propTypes = {
        provinceErrorComponent: PropTypes.node.isRequired,
        districtErrorComponent: PropTypes.node.isRequired,
        defaultProvinceId: PropTypes.number.isRequired,
        defaultDistrictId: PropTypes.number.isRequired,
        handleChange: PropTypes.func.isRequired,
        options: PropTypes.object,
    };

    constructor(props) {
        super(props);
        this.state = {
            provinceId: props.defaultProvinceId || 0,
            districtId: props.defaultDistrictId || 0,
        }
    }

    componentWillMount() {
        let {getProvinces, getDistrictsById, defaultProvinceId} = this.props;
        getProvinces();
        if(defaultProvinceId){
            getDistrictsById(defaultProvinceId);
        }
    }
    
    onChangeProvince(newValue) {
        let provinceId = parseInt(newValue, 10)
        this.setState({provinceId});
        this.props.getDistrictsByProvinceId(provinceId);
        this.props.handleChange({provinceId});
    }
    
    onChangeDistrict(newValue) {
        let districtId = parseInt(newValue, 10);
        this.setState({districtId});
        this.props.handleChange({districtId});
    }

    render() {
        let {provinceErrorComponent, districtErrorComponent, provinces, getDistricts} = this.props;
        let {options, provinceId, districtId} = this.state;
        let districts = getDistricts(provinceId);
        return (
            <LocationBox
                districtErrorComponent={districtErrorComponent}
                provinceErrorComponent={provinceErrorComponent}
                provinces={provinces}
                districts={districts}
                options={options}
                provinceId={provinceId}
                districtId={districtId}
                onChangeProvince={::this.onChangeProvince}
                onChangeDistrict={::this.onChangeDistrict}
             />
        )
    }
}
const mapStateToProps = (state, props) => {
    const getDistricts = DistrictOptionsSelector(state);
    return {
        provinces: ProvinceOptionsSelector(state),
        getDistricts,
    }
};

export default connect(
    mapStateToProps, {getProvinces, getDistrictsById}
)(SelectAddress);