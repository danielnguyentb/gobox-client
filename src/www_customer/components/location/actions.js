import Location from './api'

export const getDistrictsById = (province_id) => dispatch => dispatch(Location.getDistrictsById(province_id));

export const getProvinces = () => dispatch => dispatch(Location.getProvinces());