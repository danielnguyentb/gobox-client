/**
 * Created by Tester-Ali on 07-11-2016.
 */

import {createSelectorCreator, defaultMemoize, createSelector} from 'reselect'
import lodash from 'lodash'
import {NAME, TYPE_DISTRICT, TYPE_PROVINCE, ENABLED, DISABLED} from './constants'

export const locationSelector = state => state[NAME];

const createDeepEqualSelector = createSelectorCreator(
    defaultMemoize,
    lodash.isEqual
);

export const locationsSelector = createDeepEqualSelector(
    locationSelector,
    location => Object.values(location.byId)
);

export const LocationByIdSelector = createDeepEqualSelector(
    locationSelector,
    location => lodash.memoize(
        id => location.byId[id]
    )
);

export const ProvincesSelector = createDeepEqualSelector(
    locationsSelector,
    locations => locations.filter(t => t['type'] === TYPE_PROVINCE)
);

export const DistrictsByProvinceIdSelector = createDeepEqualSelector(
    locationsSelector,
    locations => lodash.memoize(
        provinceId => locations.filter(t => t['type'] === TYPE_DISTRICT && t['parentId'] == provinceId)
    )
);

export const ProvinceOptionsSelector = createDeepEqualSelector(
    ProvincesSelector,
    (provinces) => lodash.memoize(
        (checkDisabled = false) => provinces.map(province => {
            const t = {value: province.id, label: province.name};
            if (checkDisabled) {
                const notYet = province.state == DISABLED ? ' (Chưa hỗ trợ)' : '';
                t['disabled'] = province.state == DISABLED;
                t['label'] += notYet;
            }
            return t;
        })
    )
);

export const DistrictOptionsSelector = createDeepEqualSelector(
    DistrictsByProvinceIdSelector,
    getDistricts => lodash.memoize(
        (id, checkDisabled = false) => getDistricts(id).map(district => {
            let t = {value: district.id, label: district.name};
            if (checkDisabled) {
                const notYet = district.state == DISABLED ? ' (Chưa hỗ trợ)' : '';
                t['disabled'] = district.state == DISABLED;
                t['label'] += notYet;
            }
            return t;
        })
    )
);