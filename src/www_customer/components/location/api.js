import Client from '../../helpers/Client'
import * as types from './ActionTypes'

class DeliveryOrder extends Client {

    getDistrictsById(province_id) {
        return this.callApi({
            types: [types.LOCATION_DISTRICTS_REQUEST, types.LOCATION_DISTRICTS_SUCCESS, types.LOCATION_DISTRICTS_FAILURE],
            endpoint: `/Locations/${province_id}/children`,
            method: 'get',
        }, 'fetchingLocation')
    }

    getProvinces() {
        return this.callApi({
            types: [types.LOCATION_PROVINCES_REQUEST, types.LOCATION_PROVINCES_SUCCESS, types.LOCATION_PROVINCES_FAILURE],
            endpoint: `/Locations/provinces`,
            method: 'get',
        }, 'requestingProvinces')
    }
}

export default new DeliveryOrder();