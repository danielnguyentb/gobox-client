import React from 'react';
import {connect} from 'react-redux';
import {push} from 'react-router-redux'
import {bindActionCreators} from 'redux'
import OrderReceiveForm from './OrderReceiveForm'

class OrderReceiveFormContainer extends React.Component {
    constructor(props) {
        super(props);
    }

    changeSetField(field, value) {
        this.props.onChangeFiledForm("OrderForm", field, value);
    }

    render() {
        return (
            <OrderReceiveForm {...this.props} changeSetField={::this.changeSetField}/>
        )
    }
}

const mapStateToProps = (state, props) => {
    return {}
};

export default connect(
    mapStateToProps
)(OrderReceiveFormContainer);