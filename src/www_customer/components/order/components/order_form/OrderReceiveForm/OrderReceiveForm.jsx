import React from 'react'
import * as RBT from 'react-bootstrap'
import {renderField} from '../../../../../helpers/renderField'
import {Field, reduxForm} from 'redux-form'
import {renderSelectAddress} from '../../../helper'
import SelectAddress from '../select_address/SelectAddress'

export default (props) => (
    <div className="col-md-4 col-xs-12">
        <h5 className="fw-600">NGƯỜI NHẬN</h5>

        <div className="input-required">
            <Field name="deliverToContact" component={renderField} type="text" className="form-control"
                   placeholder="Họ và tên"/>
            <span className="required-symbol">*</span>
        </div>

        <div className="input-required">
            <Field name="deliverToPhone" component={renderField} type="phone" className="form-control"
                   placeholder="Số điện thoại"/>
            <span className="required-symbol">*</span>
        </div>

        <SelectAddress
            modelParent="deliverToProvinceId"
            modelChildren="deliverToDistrictId"
            {...props}
            onChange={props.changeSetField}
        />

        <div className="input-required">

            <Field name="deliverToDetail" component={renderField} type="phone" className="form-control"
                   placeholder="Địa chỉ"/>
            <span className="required-symbol">*</span>
        </div>

    </div>
)