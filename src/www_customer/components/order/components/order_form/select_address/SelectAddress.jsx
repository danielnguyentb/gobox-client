import React, {Component, PropTypes} from 'react'
import {connect} from 'react-redux';
import * as RBT from 'react-bootstrap'
import {getProvinces, getDistrictsById} from '../../../../location/actions'
import {ProvinceOptionsSelector, DistrictOptionsSelector} from '../../../../location/selectors'
import Select from 'react-select';
import 'react-select/dist/react-select.css';

import {Field, reduxForm, change, focus} from 'redux-form'

class SelectAddress extends React.Component {
    static propTypes = {
        modelParent: PropTypes.string,
        modelChildren: PropTypes.string,
        searchable: React.PropTypes.bool,
        searchableDistrict: React.PropTypes.bool,
        autoSelected: PropTypes.string,
        stateEnable: PropTypes.string,
        onChange: PropTypes.func.isRequired,
        defaultProvinceId: React.PropTypes.number // if defaultProvinceId then call API get list district of that province
    };
    constructor(props) {
        super(props);
    }
    componentWillMount() {
        this.setState({
            disabled: false,
            searchable: this.props.searchable,
            searchableDistrict: this.props.searchableDistrict,
            selectProvinceValue: null,
            selectDistrictValue: '',
            clearable: true,
            dataDistricts: []
        });
        if(this.props.defaultProvinceId) {
            this.getDistrictByProvinceId(this.props.defaultProvinceId);
        }
    }

    getDistrictByProvinceId(newValue) {
        if (newValue != this.props.selectProvinceValue) {
            this.setState({selectProvinceValue: newValue});
            if (newValue >= 1) {
                this.props.onChange( this.props.modelParent, newValue);
                this.props.getDistrictsById(newValue);
            }
        }
    }
    updateProvinceValue(newValue) {
        if (newValue.value != this.props.selectProvinceValue) {

            this.setState({selectProvinceValue: newValue.value});
            if (newValue.value >= 1) {
                this.props.onChange( this.props.modelParent, newValue.value);
                this.props.getDistrictsById(newValue.value);
                this.props.onChange( this.props.modelChildren, 0);
            }
        }
    }
    updateDistrictValue(newValue) {
        if (newValue.value != this.props.selectProvinceValue) {
            this.setState({selectDistrictValue: newValue.value});
            this.props.onChange( this.props.modelChildren, newValue.value);
        }
    }
    render() {
        const {provinces, getDistricts} = this.props;
        const districts = getDistricts(this.state.selectProvinceValue);

        return (
            <section>
                <div className="row">
                    <div className="col-md-6 col-xs-6">
                        <div className="input-required">
                            <Field component={renderSelectAddress} name={this.props.modelParent}
                                   options={provinces} simpleValue
                                   clearable={this.state.clearable}
                                   disabled={this.state.disabled}
                                   onChangeSelect={this.updateProvinceValue.bind(this)}
                                   placeholder="Tỉnh/TP"
                                   searchable={this.state.searchable}/>

                            <span className="required-symbol">*</span>
                        </div>

                    </div>
                    <div className="col-md-6 col-xs-6">
                        <div className="input-required">
                            <Field component={renderSelectAddress}
                                   options={districts} name={this.props.modelChildren}
                                   clearable={this.state.clearable} simpleValue
                                   disabled={this.state.disabled}
                                   placeholder="Quận/Huyện"
                                   onChangeSelect={this.updateDistrictValue.bind(this)}
                                   searchableDistrict={this.state.searchableDistrict}/>
                            <span className="required-symbol">*</span>
                        </div>
                    </div>

                </div>
            </section>
        )
    }
}

const renderSelectAddress = ({input, className, onChangeSelect, placeholder, options, meta: {touched, error, warning}}) => (
    <RBT.FormGroup validationState={touched && error ? 'error' : ''}>
        <Select options={options} {...input}
                onChange={e => {
                    input.onChange(e);
                    onChangeSelect(e)
                }}
                onBlur={() => {
                    input.onBlur(input.value)
                }}
                placeholder={placeholder}/>
        {touched && ((error && <span className="help-block">{error}</span>) || (warning && <span>{warning}</span>))}
    </RBT.FormGroup>
);

const mapStateToProps = (state, props) => {
    const getDistricts = DistrictOptionsSelector(state);
    const getProvinces = ProvinceOptionsSelector(state);
    return {
        provinces: getProvinces(props.stateEnable),
        getDistricts,
    }
};

export default connect(
    mapStateToProps, {getProvinces, getDistrictsById, change}
)(SelectAddress);