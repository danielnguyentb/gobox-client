import React from 'react';
import {connect} from 'react-redux';
import DocumentTitle from '../../../../../www_components/DocumentTitle'
import {push} from 'react-router-redux'
import {bindActionCreators} from 'redux'
import './../order.scss';
import OrderContent from './OrderContent'
import {getListAddress, createAddress, updateAddress} from '../../../customer/actions'

import {getProfile} from '../../../customer/selectors'
import {createDeliveryOrder, getLastPickupAddress} from '../../actions'
import {getProvinces} from '../../../location/actions'
import AddressModalContainer from './OrderPickupAddress/AddressModalContainer'
import {change, reset} from 'redux-form'
import {toastr} from '../../../../../www_components/Toastr2'
import {services, Status} from '../../constants'
const Config = require('../../../../../config.json');

class OrderFormContainer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showModal: false,
            showCreateModal: false,
            addressSelectedId: null
        }
    }

    componentWillMount() {
        this.props.getLastPickupAddress();
        this.props.getProvinces();
    }

    componentWillReceiveProps(nextProps) {
        // auto redirect list order
        if(nextProps.created_success) {
            nextProps.onRedirect(`/orders`);
        }
        // Set auto selected address when created address success
        if(nextProps.createdAddressSuccess || nextProps.updatedAddressSuccess
         || JSON.stringify(nextProps.createdAddressSuccessResponse) != JSON.stringify(this.props.createdAddressSuccessResponse)
         || JSON.stringify(nextProps.updatedAddressSuccessResponse) != JSON.stringify(this.props.updatedAddressSuccessResponse)
        )
        {
            this.closeModal();
            this.closeCreateModal();
            if(nextProps.createdAddressSuccess || nextProps.createdAddressSuccessResponse) {
                toastr.success("Tạo địa chỉ lấy hàng thành công");
                this.props.reset("CustomerCreateAddressForm");
            } else if(nextProps.updatedAddressSuccessResponse || nextProps.updatedAddressSuccess){
                toastr.success("Cập nhật địa chỉ lấy hàng thành công");
            }
            this.setAddressSelected(nextProps.createdAddressSuccess.id);
        }

    }
    // Set default address when selected or created success
    setAddressSelected(id) {
        if(id) {
            this.setState({addressSelectedId: id})
        }
    }
    closeModal() {
        this.setState({showModal: false});
    }
    closeCreateModal() {
        this.setState({showCreateModal: false});
    }
    openModal() {
        this.setState({showModal: true});
    }
    openCreateModal() {
        this.setState({showCreateModal: true});
    }

    onChangeFiledForm(form, field, val) {
        this.props.change(form, field, val);
    }
    // submit deliveryOrder
    submit(deliveryOrder) {
        const {featureFee, feature, cod, weightKg} = deliveryOrder;
        if(feature == services.INSURANCE && featureFee > Config.insuranceLimit){
            toastr.confirm(`Trong trường hợp giá trị hàng hóa của Quý Khách lớn hơn ${Config.insuranceLimit} VNĐ xin liên hệ trực tiếp với Dịch vụ để được tư vấn và có mức phí hợp lý hơn`);
            return;
        }
        if(cod > Config.CodLimit){
            toastr.confirm(`Trong trường hợp số tiền COD của vận đơn lớn hơn ${Config.CodLimit} VNĐ xin liên hệ trực tiếp với Dịch vụ để được tư vấn và có mức phí hợp lý hơn`);
            return;
        }
        if(weightKg > Config.weightLimit){
            toastr.confirm(`Trong trường hợp cân nặng của vận đơn lớn hơn ${Config.weightLimit} Kg xin liên hệ trực tiếp với Dịch vụ để được tư vấn và có mức phí hợp lý hơn`);
            return;
        }
        deliveryOrder.weight = 1000 * weightKg;
        this.props.createDeliveryOrder(deliveryOrder);
    }

    // submit pickup address of customer
    submitCreateAddress(data) {
        let {profile} = this.props;
        if(profile) {
            data["objectType"] =  'Customers';
            data["objectId"] = profile.id;

            // If selected address then update that address

            this.props.createAddress(data);

        }
    }
    submitUpdateCreateAddress(data) {
        let {profile} = this.props;
        if(profile) {
            data["objectType"] =  'Customers';
            data["objectId"] = profile.id;

            // If selected address then update that address
            this.props.updateAddress(data);
        }
    }


    // set field for the CustomerAddressForm
    changeSetField(field, value) {
        this.onChangeFiledForm("CustomerAddressForm", field, value);
    }
    changeSetFieldOrderFrom(field, value) {
        this.onChangeFiledForm("OrderForm", field, value);
    }
    changeSetFieldCreate(field, value) {
        this.onChangeFiledForm("CustomerCreateAddressForm", field, value);
    }

    openModalEditPickupAddress(id) {
        this.openModal();
    }
    render() {
        return (
            <DocumentTitle title="Tạo vận đơn">
                <section>
                    <OrderContent{...this.props}
                              onSubmit={::this.submit}
                              openModal={::this.openModal}
                              openCreateModal={::this.openCreateModal}
                              closeModal={::this.closeModal} stateModal={this.state.showModal}
                              onChangeFiledForm={::this.onChangeFiledForm}
                              openModalEditPickupAddress={::this.openModalEditPickupAddress}
                              setAddressSelected={::this.setAddressSelected}
                              changeSetField={::this.changeSetField}
                              changeSetFieldOrderFrom={::this.changeSetFieldOrderFrom}
                              addressSelectedId={this.state.addressSelectedId}
                    />

                    <AddressModalContainer {...this.props}
                                  onSubmitCreateAddress={::this.submitCreateAddress}
                                  onSumitUpdateAddress={::this.submitUpdateCreateAddress}
                                  openModal={::this.openModal}
                                  openCreateModal={::this.openCreateModal}
                                  closeModal={::this.closeModal}
                                  closeCreateModal={::this.closeCreateModal}
                                  stateModal={this.state.showModal}
                                  stateCreateModal={this.state.showCreateModal}
                                  changeSetField={::this.changeSetField}
                                  changeSetFieldCreate={::this.changeSetFieldCreate}
                                  addressSelectedId={this.state.addressSelectedId}
                    />
                </section>
            </DocumentTitle>
        )
    }
}
const mapDispatchToProps = (dispatch) => ({
    ...bindActionCreators({
        createDeliveryOrder, getLastPickupAddress, getProvinces, change, createAddress, updateAddress, reset
    }, dispatch),
    onRedirect: (path) => {
        dispatch(push(path))
    },
    dispatch
});

const mapStateToProps = (state, props) => {
    return {
        error: state.order.error,
        create_error: state.order.create_error,
        created_success: state.order.created_success,
        submittedFailed: state.order.submittedFailed,
        profile: getProfile(state),
        updatedAddressSuccess: state.customer.updatedAddressSuccess,
        createdAddressSuccess: state.customer.createdAddressSuccess,
        createdAddressSuccessResponse: state.customer.createdAddressSuccessResponse,
        updatedAddressSuccessResponse: state.customer.updatedAddressSuccessResponse,
    }
};

OrderFormContainer.propTypes = {
    dispatch: React.PropTypes.func.isRequired
};

export default connect(
    mapStateToProps, mapDispatchToProps
)(OrderFormContainer);