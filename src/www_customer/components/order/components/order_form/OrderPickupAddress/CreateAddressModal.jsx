import React from 'react'
import * as RBT from 'react-bootstrap'
import {renderField} from '../../../../../helpers/renderField'
import {Field, reduxForm} from 'redux-form'
import SelectAddress from '../select_address/SelectAddress'
import {validateAddressForm} from '../../../helper'
import {ErrorAlert} from '../../../../../../www_components/Alert/components'
import {DELIVERY_ORDER_CREATE_ADDRESS} from '../../../../customer/ActionTypes'

const AddressModal = (props) => {
    let {handleSubmit, pristine, submitting, changeSetFieldCreate, closeCreateModal, stateCreateModal} = props;
    return(
        <RBT.Modal show={stateCreateModal} onHide={closeCreateModal}>

            <form onSubmit={handleSubmit}>

                <RBT.Modal.Header closeButton>
                    <RBT.Modal.Title>Tạo mới địa chỉ lấy hàng</RBT.Modal.Title>
                </RBT.Modal.Header>
                <RBT.Modal.Body>
                    <div className="row">
                        <div className="col-md-6 col-xs-6 fw-600">
                            <p className="fw-600">Tên người liên hệ</p>
                            <div className="input-required">
                                <Field name="contactName" component={renderField} type="text" className="form-control"
                                       placeholder="Tên người liên hệ"/>
                                <span className="required-symbol">*</span>
                            </div>
                        </div>
                        <div className="col-md-6 col-xs-6 fw-600">
                            <p className="fw-600">Điện thoại liên hệ</p>
                            <div className="input-required">
                                <Field name="contactPhone" component={renderField} type="text" className="form-control"
                                       placeholder="Điện thoại liên hệ"/>
                                <span className="required-symbol">*</span>
                            </div>
                        </div>
                    </div>

                    <SelectAddress modelParent="provinceId"
                                   autoSelected="true" stateEnable="true"
                                   modelChildren="districtId"
                                   {...props}
                                   onChange={changeSetFieldCreate}
                    />

                    <div className="input-required">
                        <Field name="detail"
                               component={renderField} type="text"
                               className="form-control" placeholder="Địa chỉ"/>
                        <span className="required-symbol">*</span>
                    </div>

                    <ErrorAlert dispatchTypes={[DELIVERY_ORDER_CREATE_ADDRESS.FAILURE]} />

                </RBT.Modal.Body>
                <RBT.Modal.Footer>
                    <RBT.Button onClick={closeCreateModal}>HỦY</RBT.Button>
                    <button className="btn btn-primary" type="submit"  disabled={pristine || submitting}>
                        TẠO MỚI
                    </button>
                </RBT.Modal.Footer>
            </form>

        </RBT.Modal>
    )

};
export default reduxForm({
    form: "CustomerCreateAddressForm",
    validate: validateAddressForm
})(AddressModal);