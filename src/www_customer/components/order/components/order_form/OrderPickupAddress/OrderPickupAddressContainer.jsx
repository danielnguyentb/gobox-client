import React from 'react';
import {connect} from 'react-redux';
import OrderPickup from './OrderPickup'
import {getListAddress} from '../../../../customer/actions'
import {getListCustomerAddress, getById} from '../../../../customer/reducer'

class OrderPickupAddressContainer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            dataListAddress: [],
            showAddressDetail: false,
            addressSelected: null
        }
    }
    componentWillMount() {
        let {profile} = this.props;

        if( profile ) {
            this.props.getListAddress(profile.id);
        }
    }
    componentWillReceiveProps(nextProps) {
        if(nextProps.createdAddressSuccess ||
            JSON.stringify(nextProps.createdAddressSuccessResponse) != JSON.stringify(this.props.createdAddressSuccessResponse)) {
            if(nextProps.createdAddressSuccessResponse) {
               this.changeSetDeliverFromId({value: nextProps.createdAddressSuccessResponse.id});
            }
        }
    }
    changeSetDeliverFromId(data) {
        if(data.value) {
            this.props.onChangeFiledForm("OrderForm", "deliverFromId", data.value);
            this.setState({showAddressDetail: true});
            this.props.setAddressSelected(data.value);
        }
    }
    componentDidMount() {
        if(this.props.listAddress) {
            this.buildListAddress(this.props.listAddress);
        }
    }

    componentWillUpdate(nextProps) {
        if((JSON.stringify(nextProps.listAddress) != JSON.stringify(this.props.listAddress) && nextProps.listAddress)) {
            this.buildListAddress(nextProps.listAddress);
        }
    }

    buildListAddress(data) {
        let listAddress = [];
        if(data.length > 0) {
            data.map(obj =>
                listAddress.push({value: obj.id, label: "#"+ obj.id + ", "+ obj.contactName+ ", " + obj.districtName + ", "+ obj.provinceName})
            )
        }
        this.setState({dataListAddress: listAddress});
    }

    render() {
        return (
            <OrderPickup
                {...this.props}
                dataListAddress={this.state.dataListAddress}
                changeSetDeliverFromId={::this.changeSetDeliverFromId}
                showAddressDetail={this.state.showAddressDetail}
                addressSelected={this.props.addressSelected}

            />
        )
    }
}
const mapStateToProps = (state, props) => {
    return {
        listAddress: getListCustomerAddress(state),
        addressSelected: getById(state, props.addressSelectedId)
    }
};

export default connect(
    mapStateToProps, {getListAddress}
)(OrderPickupAddressContainer);