import React from 'react'
import Link from './../../../../../../www_components/Link'
import {renderField, renderSelect} from '../../../../../helpers/renderField'
import ReactTooltip from 'react-tooltip';
import * as RBT from 'react-bootstrap'
import  {Button} from 'react-bootstrap'
import {Field, reduxForm} from 'redux-form'
import SelectAddress from '../select_address/SelectAddress'
import AddressModal from './AddressModal'
import Select from 'react-select'

export default (props) => (
    <div className="col-md-4 col-xs-12">
        <h5 className="fw-600">ĐỊA CHỈ LẤY HÀNG</h5>
        <div className="input-required">
            {props.dataListAddress.length > 0 &&
            <RBT.FormGroup>

                <Field
                    className="form-control"
                    component={renderSelect}
                    name="deliverFromId"
                    options={props.dataListAddress}
                    onChangeSelect={props.changeSetDeliverFromId}
                    placeholder="Vui lòng chọn địa chỉ lấy hàng"
                />
                <span className="required-symbol">*</span>
            </RBT.FormGroup>
            }
        </div>

        {props.showAddressDetail &&

            <div className="mb-20">
                {props.addressSelected &&
                <span>
                        <b>#{props.addressSelected.id}, {props.addressSelected.contactName} / {props.addressSelected.contactPhone}
                            <span className="info-pickup-address" data-tip="Sửa địa chỉ lấy hàng" onClick={props.openModalEditPickupAddress}><i className="ml-10 ti-pencil"/></span>
                        </b>
                        <br/>
                        {props.addressSelected.fullAddress }
                </span>
                }
                <ReactTooltip />

            </div>

        }
        {/*{!props.showAddressDetail &&*/}

            <div className="option-display">
            {props.dataListAddress.length > 0 &&
            <p className="text-center">Hoặc</p>
            }
            {props.dataListAddress.length == 0 ? <p>Bạn chưa có địa chỉ lấy hàng, vui lòng tạo địa chỉ đầu tiên!</p> : ''}
            <RBT.FormGroup className="text-center fw-600">

            <Button bsStyle="primary" onClick={props.openCreateModal}><i className="fa fa-plus fw-400 mr-5"/>
            {props.dataListAddress.length > 0 ? "TẠO MỚI" : "TẠO MỚI ĐỊA CHỈ"}
            </Button>

            </RBT.FormGroup>

            </div>
        {/*}*/}
    </div>
);