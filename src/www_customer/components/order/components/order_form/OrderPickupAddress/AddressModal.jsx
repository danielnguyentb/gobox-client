import React from 'react'
import * as RBT from 'react-bootstrap'
import {renderField, renderSelectAddress} from '../../../../../helpers/renderField'
import {Field, reduxForm} from 'redux-form'
import SelectAddress from '../select_address/SelectAddress'
import {validateAddressForm} from '../../../helper'
import {ErrorAlert} from '../../../../../../www_components/Alert/components'
import {DELIVERY_ORDER_CREATE_ADDRESS} from '../../../../customer/ActionTypes'
import Touchable from '../../../../../../www_components/Touchable'

const AddressModal = (props) => {
    let {handleSubmit, pristine, reset, submitting, invalid, addressSelectedId, changeSetField, closeModal, stateModal, initialValues} = props;
    return(
        <RBT.Modal show={stateModal} onHide={closeModal}>

            <form onSubmit={handleSubmit}>

                <RBT.Modal.Header closeButton>
                    <RBT.Modal.Title>Sửa địa chỉ lấy hàng</RBT.Modal.Title>
                </RBT.Modal.Header>
                <RBT.Modal.Body>
                    <div className="row">
                        <div className="col-md-6 col-xs-6 fw-600">
                            <p className="fw-600">Tên người liên hệ</p>
                            <div className="input-required">
                                <Field name="contactName" component={renderField} type="text" className="form-control"
                                       placeholder="Tên người liên hệ"/>
                                <span className="required-symbol">*</span>
                            </div>
                        </div>
                        <div className="col-md-6 col-xs-6 fw-600">
                            <p className="fw-600">Điện thoại liên hệ</p>
                            <div className="input-required">
                                <Field name="contactPhone" component={renderField} type="text" className="form-control"
                                       placeholder="Điện thoại liên hệ"/>
                                <span className="required-symbol">*</span>
                            </div>
                        </div>
                    </div>

                    <SelectAddress modelParent="provinceId"
                                   defaultProvinceId={initialValues ? initialValues.provinceId : null}
                                   autoSelected="true" stateEnable="true"
                                   modelChildren="districtId"
                                   {...props}
                                   onChange={changeSetField}
                    />

                    <div className="input-required">
                        <Field name="detail"
                               component={renderField} type="text"
                               className="form-control" placeholder="Địa chỉ"/>
                        <span className="required-symbol">*</span>
                    </div>

                    <ErrorAlert dispatchTypes={[DELIVERY_ORDER_CREATE_ADDRESS.FAILURE]} />

                </RBT.Modal.Body>
                <RBT.Modal.Footer>
                    <RBT.Button onClick={closeModal}>HỦY</RBT.Button>
                    <Touchable actionTypes={[DELIVERY_ORDER_CREATE_ADDRESS.SUCCESS, DELIVERY_ORDER_CREATE_ADDRESS.FAILURE]}
                               className="btn btn-primary" type="submit"
                               disabled={pristine}
                    >
                        THAY ĐỔI
                    </Touchable>
                </RBT.Modal.Footer>
            </form>

        </RBT.Modal>
    )

};
export default reduxForm({
    form: "CustomerAddressForm",
    validate: validateAddressForm
})(AddressModal);