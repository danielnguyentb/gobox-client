import React from 'react'
import {connect} from 'react-redux'
import OrderByExcel from './OrderByExcel'
import {uploadOrders} from '../../../actions'
import * as RBT from 'react-bootstrap'

class OrderByExcelContainer extends React.Component{

    constructor(props) {
        super(props);
        this.state = {
            showModal: false,
            stateLoading: false,
            message: '',
            error: false
        }
    }

    componentWillReceiveProps(nextProps) {
        if(JSON.stringify(nextProps.uploadedOrderExcel) != JSON.stringify(this.props.uploadedOrderExcel)) {
            this.setState({
                showModal: true,
                error: false,
                stateLoading: false,
                message: 'Upload file thành công. Hệ thống sẽ gửi thông báo kết quả phân tích vào email của bạn'
            });
        } else if(JSON.stringify(nextProps.uploadOrderExcelErrorMsg) != JSON.stringify(this.props.uploadOrderExcelErrorMsg)) {
            this.setState({
                showModal: true,
                error: true,
                stateLoading: false,
                message: nextProps.uploadOrderExcelErrorMsg
            });
        }
    }
    handleSubmit(event) {
        if(event.target.files[0]) {
            this.setState({stateLoading: true});
            let formData = new FormData();
            formData.append("file", event.target.files[0]);
            this.props.uploadOrders(formData);
        }
    }

    closeModal() {
        this.setState({showModal: false})
    }
    render(){
        let {error, message, showModal, stateLoading} = this.state;
        return(
            <section>
                <OrderByExcel
                    {...this.props}
                    onSubmit={::this.handleSubmit}
                    stateShowModal={showModal}
                    stateLoading={stateLoading}
                />

                <RBT.Modal show={showModal} onHide={this.closeModal.bind(this)}>
                    <RBT.Modal.Header closeButton className={`${error ? 'bg-danger' : 'bg-success'} no-border`}>
                        <RBT.Modal.Title>
                            {message}
                        </RBT.Modal.Title>
                    </RBT.Modal.Header>
                </RBT.Modal>
            </section>
        )
    }
}
const mapStateToProps = (state, props) => {
    return {
        uploadedOrderExcel: state.order.uploadedOrderExcel,
        uploadOrderExcelErrorMsg: state.order.uploadOrderExcelErrorMsg
    }
};
export default connect(
    mapStateToProps, {uploadOrders}
)
(OrderByExcelContainer);