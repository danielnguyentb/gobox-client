import React from 'react'
import * as RBT from 'react-bootstrap'
import AppConfig from '../../../../../../config.json'
import FileInput from 'react-file-input'
import imageLoading from '../../../../../../resources/loading.gif'

const OrderByExcel = ({onSubmit, stateLoading}) => (
    <div className="row">
        <div className="col-md-offset-4 col-md-4 col-xs-12">
            <h3>
                Tạo vận đơn hàng loạt bằng file excel
            </h3>

            <RBT.FormGroup>
                <div className="myinput">
                    {!stateLoading ?
                        <FileInput name="file" accept=".xlsx"
                            placeholder="Chọn file upload"
                            className="form-control pt-upload-file"
                            onChange={onSubmit}
                        />
                        :
                        <div className="text-center">
                            <img src={imageLoading} width={40} alt=""/> Tập tin đang được tải lên...
                        </div>
                    }

                </div>
            </RBT.FormGroup>

            <h4>
                Xem hướng dẫn tải file excel mẫu
                <a href={AppConfig.linkDownloadOrderSample} target="_blank" className="cursor-poiter"> tại đây</a>
            </h4>

            <div className="alert alert-warning fs-15">
                Cảm phiền Quý Khách mỗi lần tải lên file excel chỉ chứa tối đa thông tin của 100 vận đơn để giảm rủi ro gặp lỗi trong quá trình phân tích nội dung file.
                Chúng tôi hiện tại cũng đang chỉ xử lý 100 dòng dữ liệu vận đơn đầu tiên của file để giảm rủi ro này.<br/>
                Cám ơn Quý Khách!
            </div>
        </div>
    </div>
);
export default (OrderByExcel)
