import React from 'react'
import Link from '../../../../../www_components/Link';
import {Tab, Tabs, TabList, TabPanel} from 'react-tabs';
import OrderForm from './OrderForm/OrderForm'
import OrderByExcelContainer from './OrderByExcel/OrderByExcelContainer'

const OrderContent =  (props) => {
    const {profile, handleSelect, selected} = props;
    if(!profile) return null;
    return  (
        <div>
            <section className="content-header">
                <h1 >Tạo vận đơn</h1>
                <ol className="breadcrumb mb-0">
                    <li><Link to="/orders">Danh sách vận đơn</Link></li>
                    <li className="active">Tạo vận đơn</li>
                </ol>
            </section>
            <section className="content">
                <div className="box">
                    <div className="row">
                        <div className="col-md-12">
                            <Tabs
                                onSelect={handleSelect}
                                selectedIndex={selected}>
                                <TabList className="tab-balance">
                                    <Tab>TẠO VẬN ĐƠN</Tab>
                                    <Tab>TẠO VẬN ĐƠN BẰNG EXCEL</Tab>
                                </TabList>
                                <TabPanel>
                                    <OrderForm {...props} />
                                </TabPanel>
                                <TabPanel>
                                    <OrderByExcelContainer/>
                                </TabPanel>
                            </Tabs>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    )
};

export default (OrderContent)
