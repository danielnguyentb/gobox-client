import React from 'react'
import {Field, reduxForm} from 'redux-form'
import {validate} from '../../../helper'

import OrderInfoFormContainer from '../OrderInfoForm/OrderInfoFormContainer'
import OrderPickupAddress from '../OrderPickupAddress/OrderPickupAddressContainer'
import OrderReceiveForm from '../OrderReceiveForm/OrderReceiveFormContainer'

const OrderForm2 =  (props) => {
    const {handleSubmit, pristine, error, submitting, invalid } = props;
    return  (
        <form onSubmit={handleSubmit}>
            <div className="box">
                <div className="row">
                    <div className="col-md-12">
                        <div className="widget widget-grey mb-15">
                            <div className="widget-body">
                                <div className="row">
                                    <OrderPickupAddress {...props} />
                                    <OrderReceiveForm {...props} />
                                    <OrderInfoFormContainer {...props} />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-12 text-right">
                        <p className="mb-10"><small>(Các thông tin có đánh dấu <span className="text-danger">*</span> không được bỏ trống!)</small></p>
                    </div>
                </div>
                {error && <div className="alert alert-danger">{error}</div>}
                <div className="row">
                    <div className="col-md-12 text-right mb-30 pb-30">
                        <button
                            disabled={pristine || submitting}
                            className="btn btn-primary" type="submit">
                            TẠO VẬN ĐƠN
                        </button>
                    </div>
                </div>
            </div>
        </form>
    )
};
export default reduxForm({
    form: 'OrderForm',
    validate
})(OrderForm2)
