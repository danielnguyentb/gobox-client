import React from 'react'
import {renderField, renderWeightField, renderCodField} from '../../../../../helpers/renderField'
import NumericInput from 'react-number-format'
import ReactTooltip from 'react-tooltip';
import * as RBT from 'react-bootstrap'
import {Field, Fields, reduxForm} from 'redux-form'
import SimpleCurrencyInput from '../../../../../../www_components/SimpleCurrencyInput'
import InsurranService from '../../services_component/InsurranceService';

export default (props) => (
    <div className="col-md-4 col-xs-12">
        <h5 className="fw-600">THÔNG TIN VẬN ĐƠN</h5>
        <div className="row">
            <div className="col-md-6 col-xs-6">
                <div className="pt-input-notes">
                    <Field name="cod" component={renderCodField} type="text" className="form-control"
                           placeholder="Tiền COD"/>
                    <span className="currency-symbol">VNĐ</span>
                </div>
                <i data-tip="Tiền thu khi giao hàng cho khách (không bao gồm phí dịch vụ)" data-place="top" className="ti-help-alt pull-right fs-18 lh-34" />
            </div>
            <div className="col-md-6 col-xs-6 pl-0">
                <div className="pt-input-notes width-100">
                    <Field name="weightKg" component={renderWeightField} type="text" className="form-control"
                           placeholder="Cân nặng"/>
                    <span className="currency-symbol">kg</span>
                </div>
            </div>
        </div>
        {/*<Fields component={InsurranService} {...props} names={[ 'feature', 'featureFee' ]}/>*/}
        <Fields
            names={[ 'feature', 'featureFee' ]}
            component={InsurranService}
            onChange={props.changeSetFieldOrderFrom}
        />
        {/*<div className="row">*/}
            {/*<div className="col-md-1 col-xs-1 ">*/}
                {/*<Field name="feature" className="mt-10" component={renderField} type="checkbox"/>*/}
            {/*</div>*/}
            {/*<div className="col-md-3 col-xs-5 pl-0 lh-34">Bảo hiểm?</div>*/}
            {/*<div className="col-md-8 col-xs-6">*/}
                {/*<div className="pt-input-notes">*/}
                    {/*<Field name="featureFee" component={renderField} type="text" className="form-control"*/}
                           {/*placeholder="Giá trị hàng"/>*/}
                    {/*<span className="currency-symbol">VNĐ</span>*/}
                {/*</div>*/}
                {/*<i data-tip="Tiền thu khi giao hàng cho khách" data-place="top" className="ti-help-alt pull-right fs-18 lh-34"></i>*/}
            {/*</div>*/}
        {/*</div>*/}

        <div className="row">
            <div className="col-md-12">
                <div className="pt-input-notes">
                    <Field name="privateNote" component={renderField} type="text" className="form-control"
                           placeholder="Mã đơn riêng / ghi chú riêng"/>
                </div>
                <i data-tip="Thông tin này chỉ Quý khách đọc được. Dùng cho Quý khách quản lý riêng mã đơn hàng hoặc ghi chú." data-place="top" className="ti-help-alt pull-right fs-18 lh-34"></i>
                <ReactTooltip/>
            </div>
        </div>

        <RBT.FormGroup>
            <Field name="note" component="textarea" className="form-control"
                   placeholder="Ghi chú để chúng tôi phục vụ tốt hơn"/>
        </RBT.FormGroup>
    </div>
);

const renderNumericInput = ({input, format, onBlur, onChange}) => (
    <div className="input-required">
        <NumericInput {...input}
                      className="form-control code-notes" placeholder="Tiền thu hộ (COD)"/>
        <span className="currency-symbol">VNĐ</span>
    </div>
);

const renderSimpleInput = ({input}) => (
    <SimpleCurrencyInput
        precision={0}
        separator=''
        {...input}
        delimiter='.'
        className="form-control code-notes"

    />
);
