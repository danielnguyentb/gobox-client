import React from 'react';
import {connect} from 'react-redux';
import {push} from 'react-router-redux'
import {bindActionCreators} from 'redux'
import OrderInfo from './OrderInfo'
import parseDecimalNumber from 'parse-decimal-number';
import {Field, reduxForm, change} from 'redux-form'

class OrderInfoFormContainer extends React.Component {
    constructor(props) {
        super(props);
    }
    handleSetCod(val) {
        this.props.change("OrderForm", "cod", val);
    }
    render() {
        return (
            <OrderInfo {...this.props} handleSetCod={::this.handleSetCod} />
        )
    }
}

const mapStateToProps = (state, props) => {
    return {}
};

export default connect(
    mapStateToProps, {change}
)(OrderInfoFormContainer);