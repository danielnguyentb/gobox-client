import React from 'react'
import * as RBT from 'react-bootstrap'
import Link from './../../../../../../www_components/Link'
import {services} from '../../../constants'
class InsurranceService extends React.Component {
    static propTypes = {
        onChange: React.PropTypes.func.isRequired
    };

    constructor(props) {
        super(props);
        this.state = {
            disabled: false
        }
    }
    handleChecked(e) {
        this.setState({disabled: e.target.checked});
    }

    render() {
        let fields = this.props;
        return (
            <div className="row">
                <div className="col-md-1 col-xs-1 ">
                    <RBT.Checkbox
                        {...fields.feature.input}
                        className="mt-10 pt-checkbox"
                        onChange={ (e) => this.handleChecked(e)}
                        onBlur={ (e) => {
                            let value = e.target.checked ? services.INSURANCE : false;
                            fields.feature.input.onChange(value);
                        }}
                    />
                    {fields.feature.meta.touched && fields.feature.meta.error &&
                    <span className="error">{fields.feature.meta.error}</span>}
                </div>
                <div className="col-md-5 col-xs-5 pl-0 lh-34">Bảo hiểm?</div>
                <div className="col-md-6 col-xs-6 pl-0">

                    <div className="pt-input-notes width-100">
                        <RBT.FormGroup validationState={fields.featureFee.meta.touched && fields.featureFee.meta.error ? 'error' : ''}>
                            <input
                                {...fields.featureFee.input}
                                type="text" className="form-control"
                                placeholder="Giá trị hàng"
                                disabled={!this.state.disabled}
                            />
                            {fields.featureFee.meta.touched && ((fields.featureFee.meta.error &&
                                <span className="help-block">{fields.featureFee.meta.error}</span>) || (fields.featureFee.meta.warning && <span>{fields.featureFee.meta.warning}</span>))}
                        </RBT.FormGroup>
                        <span className="currency-symbol">VNĐ</span>
                    </div>
                </div>
                <div className="col-md-12 col-xs-12">
                    <p className="fs-13">(Khai báo giá trị hàng hóa để được bồi hoàn khi xảy ra mất mát. Xem thêm chính sách <Link to="" className="fw-600">tại đây</Link>)</p>
                </div>
            </div>
        )
    }
}
export default (InsurranceService);