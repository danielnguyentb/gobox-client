import React from 'react'
import {Fields, reduxForm} from 'redux-form'
import * as RBT from 'react-bootstrap'
import Link from './../../../../../../www_components/Link'
import {services} from '../../../constants'
class InsurranceService extends React.Component {
    static propTypes = {
        onChange: React.PropTypes.func.isRequired
    };

    constructor(props) {
        super(props);
        this.state = {
            disabled: false
        }
    }
    handleChecked(e) {
        this.setState({disabled: e.target.checked});
        if(e.target.checked) {
            this.props.onChange("feature", services.INSURANCE);
        } else {
            this.props.onChange("feature", null);
        }

    }

    render() {
        let fields = this.props;
        return (
            <div className="row">
                <div className="col-md-1 col-xs-1 ">
                    <RBT.Checkbox
                        {...fields.features.input}
                        className="mt-10"
                        onChange={e => this.handleChecked(e)}
                    />
                    {fields.features.meta.touched && fields.features.meta.error &&
                    <span className="error">{fields.features.meta.error}</span>}
                </div>
                <div className="col-md-3 col-xs-5 pl-0 lh-34">Bảo hiểm?</div>
                <div className="col-md-8 col-xs-6">

                    <div className="pt-input-notes">
                        <RBT.FormGroup validationState={fields.featureFee.meta.touched && fields.featureFee.meta.error ? 'error' : ''}>
                            <input
                                {...fields.featureFee.input}
                                type="text" className="form-control"
                                placeholder="Giá trị hàng"
                                disabled={!this.state.disabled}
                            />
                            {fields.featureFee.meta.touched && ((fields.featureFee.meta.error &&
                                <span className="help-block">{fields.featureFee.meta.error}</span>) || (fields.featureFee.meta.warning && <span>{fields.featureFee.meta.warning}</span>))}
                        </RBT.FormGroup>
                        <span className="currency-symbol">VNĐ</span>
                    </div>
                    <i data-tip="Khai báo giá trị hàng hóa để được bồi hoàn khi xảy ra mất mát. Xem thêm chính sách tại đây" data-place="top" className="ti-help-alt pull-right fs-18 lh-34"></i>
                </div>
            </div>
        )
    }
}
export default (InsurranceService);