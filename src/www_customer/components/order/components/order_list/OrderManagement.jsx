import React from 'react';
import {connect} from 'react-redux';
import {push} from 'react-router-redux'
import ReactTooltip from 'react-tooltip'
import './../order.scss';
import DocumentTitle from '../../../../../www_components/DocumentTitle'
import OrderListContainer from './order_table/OrderListContainer'
import SearchContainer from './header/SearchContainer'
import RePagination from '../../../../../www_components/RePagination'
import Counter from './counter'

class OrderManagement extends React.Component {
    search(data) {
        const {location, dispatch} = this.props;
        const query = {
            ...location.query,
            ...data,
        };
        dispatch(push({
            pathname: '/orders',
            query
        }));
    }

    render() {
        const {location} = this.props;
        return (
            <div>
                <DocumentTitle title="Danh sách vận đơn"/>
                <ReactTooltip/>
                <section className="content-header">
                    <div className="row">
                        <div className="col-md-12 col-xs-12">
                            <h2 className="mt-0">Danh sách vận đơn</h2>
                        </div>
                    </div>
                </section>
                <section className="content">
                    <div className="box">
                        <SearchContainer {...this.props} onSearch={::this.search}/>

                        <div className="row">
                            <Counter/>
                        </div>
                        <div className="row">
                            <OrderListContainer {...this.props} onSearch={::this.search}/>
                        </div>
                        <div className="row">
                            <div className="col-md-12 text-right">
                                <RePagination {...location}/>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        )
    }
}

export default connect()(OrderManagement);