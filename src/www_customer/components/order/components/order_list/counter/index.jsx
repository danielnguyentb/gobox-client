import React from 'react'
import {connect} from 'react-redux'
import * as selectors from '../../../selectors'
import Link from '../../../../../../www_components/Link'

export const CounterContainer = ({count, printableOrderIds, disabled}) => {
    const href = `/order_print?ids=${printableOrderIds.join(',')}`;
    return (
        <div>
            <div className="col-md-6 col-xs-6 mb-5">
                <button
                    disabled={disabled}
                    className=" btn btn-success"
                    type="button"
                    data-tip="In các vận đơn đã chọn">
                    <Link className={`text-white multi-print ${disabled ? 'not-allowed' : ''}`} to={href} target="_blank" onClick={e => {
                        if(disabled){
                            e.preventDefault();
                        }
                    }}><i className="ti-printer mr-5"></i>
                        IN NHIỀU
                    </Link>
                </button>
            </div>
            <div className="col-md-6 col-xs-6 text-right mb-5">
                <p>Tìm thấy <b>{count}</b> kết quả</p>
            </div>
        </div>
    )
};

const mapStateToProps = (state) => ({
    count: selectors.orderSelector(state).metadata.count || 0,
    disabled: selectors.PrintableNotSelector(state),
    printableOrderIds: selectors.PrintableOrderIdsSelector(state)
});

export default connect(mapStateToProps)(CounterContainer)