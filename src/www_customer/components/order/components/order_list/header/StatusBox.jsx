import React, {PropTypes} from 'react'
import StatusButton from './StatusButton'

const StatusBox = ({status, query, ...args}) => {
    return (
        <ul className="orders-status">
            {status.map(obj => <li key={obj.code}><StatusButton
                isActivated={query.status == obj.code} {...args} {...obj}/></li>)}
        </ul>
    )
};
StatusBox.propTypes = {
    status: PropTypes.array.isRequired,
    isLoading: PropTypes.bool,
    onSearch: PropTypes.func.isRequired,
};
export default StatusBox;