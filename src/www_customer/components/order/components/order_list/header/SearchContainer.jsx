import React, {PropTypes, Component} from 'react'
import SearchBox from './SearchBox'
import {connect} from 'react-redux'
import {getOrderStates} from '../../../actions'


class SearchContainer extends Component {
    componentWillMount() {
        this.props.getOrderStates();
    }

    search(data) {
        this.props.onSearch({...data, skip: 0});
    }

    render() {
        let {metadata, location, ...rest} = this.props;
        let query = {...metadata, ...location.query};
        return <SearchBox {...rest} query={query} onSearch={::this.search}/>
    }
}

const mapStateToProps = (state) => ({
    metadata: state.order.metadata,
    status: state.order.status,
    isLoading: state.order.fetchingDeliveryOrder || state.order.fetchingOrderStates,
});
SearchContainer.propTypes = {
    onSearch: PropTypes.func.isRequired,
    status: PropTypes.array.isRequired,
};
export default connect(
    mapStateToProps, {getOrderStates})
(SearchContainer)