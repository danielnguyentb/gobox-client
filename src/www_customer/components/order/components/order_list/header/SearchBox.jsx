import React from 'react'
import StatusBox from './StatusBox'

export default (props) => {
    return (

        <form method="GET">
            <div className="row">
                <div className="col-md-12">
                    <StatusBox {...props} />
                </div>
            </div>

            <div className="row box-search-order hide">
                <div className="col-md-3 col-xs-12">
                    <div className="form-group has-feedback">
                        <input type="text" defaultValue="" className="form-control" placeholder="Mã đơn"/>
                        <span className="ti-search form-control-feedback text-hint"/>
                    </div>
                </div>
            </div>
        </form>
    )
}