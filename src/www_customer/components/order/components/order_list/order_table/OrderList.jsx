import React from 'react'
import OrderItem from './OrderItem'
import SearchTable from './SearchTable'
import '../../order.scss';
export default ({orders, ...args, actionDeliveryOrderChangeStatus, onPrintableClick, onPrintableAllClick, printableAll}) => (
    <table className="table tbl-headblue table-striped table-hover tbl-orders">
        <thead>
        <tr>
            <th>
                <input
                    checked={printableAll}
                    onChange={onPrintableAllClick}
                    className="checkbox" type="checkbox"/></th>
            <th><i className="fa fa-barcode"></i> Mã đơn</th>
            <th className="hidden-xs" width={110}>Loại VĐ</th>
            <th>Trạng thái</th>
            <th><i className="glyphicon glyphicon-user"></i> Người nhận</th>
            <th className="hidden-xs">Mã đơn/ghi chú riêng</th>
            <th className="hidden-xs">COD (VNĐ)</th>
            <th>Phí (VNĐ)</th>
            <th className="hidden-xs">Tiền BH (VNĐ)</th>
            <th className="hidden-xs">TG giao</th>
            <th className="hidden-xs text-center">Thao tác</th>
        </tr>
        </thead>
        <tbody>
        <SearchTable {...args}/>
        {orders.length > 0 ? orders.map(order => {
            return (
                <OrderItem
                    key={order.id}
                    actionDeliveryOrderChangeStatus={actionDeliveryOrderChangeStatus}
                    order={order}
                    onPrintableClick={onPrintableClick}
                    {...args} />
            )
        })
            : (
            <tr>
                <td colSpan="100%"><h4 style={{'textAlign': 'center'}}>Không có đơn nào thỏa mãn điều kiện tìm kiếm
                    !</h4>
                </td>
            </tr>)}
        </tbody>
    </table>
)