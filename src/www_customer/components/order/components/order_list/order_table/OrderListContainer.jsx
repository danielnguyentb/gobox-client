import React, {PropTypes} from 'react';
import {connect} from 'react-redux';
import OrderList from './OrderList'
import {getDeliveryOrders, changeStatus, getOrderStates} from '../../../actions'
import * as selectors from '../../../selectors'
import {toastr} from 'react-redux-toastr'
import {DELIVERY_ORDERS_REQUEST, TOGGLE_PRINTABLE_ALL} from '../../../ActionTypes'

class OrderListContainer extends React.Component {
    componentWillMount() {
        this.props.getDeliveryOrders(this.props.location.query);
    }

    componentWillReceiveProps({location, getDeliveryOrders, changeStatusSuccess, error, getOrderStates}) {
        if (location.search != this.props.location.search) {
            getDeliveryOrders(location.query);
        }

        if (changeStatusSuccess) {
            toastr.success(changeStatusSuccess.message);
            getOrderStates();
        }
        if (error) {
            toastr.error(error);
        }
    }

    actionDeliveryOrderChangeStatus(orderId, status) {
        const {changeStatus} = this.props;
        if (orderId && status != '') {
            changeStatus(orderId, status);
        }
    }

    handlePrintableClick(orderId, printable) {
        const {dispatch} = this.props;
        dispatch({
            type: 'TOGGLE_PRINTABLE',
            payload: {orderId, printable}
        });
    }

    handlePrintableAllClick(e) {
        const {dispatch} = this.props;
        dispatch({
            type: TOGGLE_PRINTABLE_ALL,
            payload: e.target.checked
        })
    }

    render() {
        let {metadata, location, orders, loadingOrders, printableAll, ...rest} = this.props;
        let params = {...metadata, ...location.query};
        if (loadingOrders) return null;
        return orders ? (
            <OrderList
                orders={orders}
                printableAll={printableAll}
                onPrintableAllClick={::this.handlePrintableAllClick}
                onPrintableClick={::this.handlePrintableClick}
                actionDeliveryOrderChangeStatus={this.actionDeliveryOrderChangeStatus.bind(this) }
                query={params} {...rest} /> ) : null;
    }
}

const mapStateToProps = (state) => ({
    orders: selectors.PaginateDeliveryOrdersSelector(state),
    printableAll: selectors.PrintableAllSelector(state),
    changeStatusSuccess: state.order.changeStatusSuccess,
    error: state.order.error,
    loadingOrders: state.ajaxType == DELIVERY_ORDERS_REQUEST
});
OrderListContainer.propTypes = {
    onSearch: PropTypes.func.isRequired,
    orders: PropTypes.array.isRequired,
};
export default connect(mapStateToProps, {getDeliveryOrders, changeStatus, getOrderStates})(OrderListContainer);