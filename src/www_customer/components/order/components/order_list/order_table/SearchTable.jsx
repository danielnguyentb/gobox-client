import React, {PropTypes} from 'react'
import Select from 'react-select'
import {typesTitle, types} from '../../../constants'

class SearchTable extends React.Component {
    constructor(props) {
        super(props);
        this.state = props.query;
    }

    onBlur = (name, callback) => event => callback({[name]: event.target.value, skip: 0});

    onEnter = (name, callback) => event => {
        event.preventDefault();
        if (event.key == 'Enter') {
            callback({[name]: event.target.value, skip: 0})
        }
    };

    onSelect(data) {
        this.props.onSearch({...data, skip: 0});
    }
    render() {
        let {barcode, deliverTo, privateNote, type} = this.state;
        let {onSearch} = this.props;
        return (
            <tr>
                <td className="hidden-xs">&nbsp;</td>
                <td>
                    <div className="form-group has-feedback order-indentity">
                        <input
                            defaultValue={barcode}
                            onBlur={this.onBlur('barcode', onSearch)}
                            onKeyUp={this.onEnter('barcode', onSearch)}
                            className="form-control" placeholder="Mã đơn" type="text"/>
                        <span className="ti-search form-control-feedback text-hint"/>
                    </div>
                </td>
                <td className="hidden-xs">
                    <Select
                        placeholder="Loại VĐ"
                        value={type ? type : ''}
                        options={[
                            {label: 'Tất cả', value: ''},
                            {label: typesTitle[types.DELIVERY], value: types.DELIVERY},
                            {label: typesTitle[types.RETURN], value: types.RETURN}
                        ]}
                        onChange={t => this.onSelect({type: t ? t.value : ''})}
                    />
                </td>
                <td>
                </td>
                <td>
                    <div className="form-group">
                        <input
                            defaultValue={deliverTo}
                            onBlur={this.onBlur('deliverTo', onSearch)}
                            onKeyUp={this.onEnter('deliverTo', onSearch)}
                            className="form-control" placeholder="Người nhận / Số điện thoại" type="text"/>
                        {/*Chú ý cho phép search cả tên hoặc số điện thoại*/}
                    </div>
                </td>
                <td className="hidden-xs">
                    <div className="form-group">
                        <input
                            defaultValue={privateNote}
                            onBlur={this.onBlur('privateNote', onSearch)}
                            onKeyUp={this.onEnter('privateNote', onSearch)}
                            className="form-control" placeholder="Mã đơn / ghi chú riêng" type="text"/>
                    </div>
                </td>
                <td className="hidden-xs">&nbsp;</td>
                <td>&nbsp;</td>
                <td className="hidden-xs">&nbsp;</td>
                <td className="hidden-xs">&nbsp;</td>
                <td className="hidden-xs">&nbsp;</td>
            </tr>
        )
    }
}

SearchTable.propTypes = {
    onSearch: PropTypes.func.isRequired,
};

export default SearchTable;