import React from 'react'
import ReactTooltip from 'react-tooltip'
import * as RBT from 'react-bootstrap'
import Link from '../../../../../../www_components/Link'
import {formatCurrency} from '../../../../../helpers/format'
import {typesTitle, Status, allowCancelOrder} from '../../../constants'
import {toastr} from "../../../../../../www_components/Toastr2"

export default ({order, onSearch, actionDeliveryOrderChangeStatus, onPrintableClick}) => {
    const {
        statusDetail, barcode, cod, deliveredLocalize, orderStatus, totalFee, realCod,
        deliverToContact, tip, deliverToDetail, privateNote, id, insuranceService,
        isEndingStatus, type, printable,
    } = order || {};
    const {title, label} = statusDetail;
    return (
        <tr>
            <td><input className="checkbox" type="checkbox" checked={printable} onChange={e => {
                onPrintableClick(id, e.target.checked);
            }}/></td>
            <td><Link to={`/orders/${id}`} target="_blank" className="fw-600 fs-15">{barcode}</Link></td>
            <td className="hidden-xs">{typesTitle[type]}</td>
            <td><span className={`label label-${label}`}>{title}</span></td>
            <td>
                <span data-tip={tip} data-html={true}>{deliverToContact}</span>
                <br/>
                <span className="text-grey">{deliverToDetail}</span>
            </td>
            <td className="hidden-xs">{privateNote}</td>
            <td className="hidden-xs" width={100}>
                {(realCod || realCod === 0 ) ?
                    <span
                        data-tip="COD thực thu"
                        className='text-success'>{formatCurrency(realCod)}</span>
                    :
                    <span>{formatCurrency(cod)}</span>
                }
            </td>
            <td width={100}>{formatCurrency(totalFee)}</td>
            <td className="hidden-xs" width={100}>{insuranceService ? formatCurrency(+insuranceService['customField1']) : '...'}</td>
            <td className="hidden-xs" width={100}>{deliveredLocalize}</td>
            <td className="hidden-xs" width={100}>
                <Link target='_blank' to={`/order_print?ids=${id}`} className="btn btn-default btn-xs mr-5"
                      data-tip="In vận đơn">
                    <i className="ti-printer"/>
                </Link>
                <ReactTooltip />

                {allowCancelOrder.includes(orderStatus) &&
                <RBT.Button
                    data-tip="Hủy vận đơn" className="btn btn-xs btn-outline btn-danger"
                    onClick={e => {
                        toastr.confirm("Bạn có muốn hủy vận đơn " + barcode, {
                            onOk: () => actionDeliveryOrderChangeStatus(id, Status.CUSTOMER_CANCELED)
                        })
                    }}
                >
                    <i className="ti-close"></i>
                </RBT.Button>
                }
            </td>
        </tr>
    )
}