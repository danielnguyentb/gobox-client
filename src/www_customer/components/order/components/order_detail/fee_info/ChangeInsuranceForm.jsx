import React, {PropTypes} from 'react'
import {Fields, reduxForm} from 'redux-form'
import * as RBT from 'react-bootstrap'
import {ReInsuranceService} from '../../services_component/ReInsuranceService'
import {formatCurrency} from '../../../../../helpers/format'
import {toastr} from '../../../../../../www_components/Toastr2'

export const InsuranceForm = ({handleSubmit, error, submitting, invalid, pristine, close}) => {
    return (
        <form onSubmit={handleSubmit}>
            <Fields names={['insuranceService', 'insuranceValue']} component={ReInsuranceService}/>
            {error && <div className="alert alert-danger">{error}</div>}
            <div className="row">
                <div className="col-md-12 text-right">
                    <button
                        className="btn btn-default mr-5" type="button" onClick={close}
                    >HỦY
                    </button>
                    <button
                        disabled={pristine || submitting || invalid}
                        className="btn btn-primary" type="submit"
                    >LƯU
                    </button>
                </div>
            </div>
        </form>
    )
};

InsuranceForm.propTypes = {
    handleSubmit: PropTypes.func,
    insuranceService: PropTypes.bool,
    insuranceValue: PropTypes.number
};
export const validate = values => {
    const errors = {};
    if(Object.keys(values).length === 0) return errors;
    if (values.insuranceService) {
        if (!values.insuranceValue) {
            errors.insuranceValue = 'Giá trị hàng không được để trống'
        } else if (typeof (+values.insuranceValue) !== 'number') {
            errors.insuranceValue = 'Giá trị hàng phải là số'
        } else if (values.insuranceValue < 0) {
            errors.insuranceValue = 'Giá trị hàng không được nhỏ hơn 0'
        }
    }
    return errors;
};
export const ReInsuranceForm = reduxForm({
    form: 'ChangeInsuranceForm',
    validate
})(InsuranceForm);


export class InsuranceModal extends React.Component {
    constructor(props) {
        super(props);
        this.state = {showModal: false}
    }

    closeModal() {
        this.setState({showModal: false});
    }

    componentDidUpdate(prevProps) {
        const {changeSuccess} = this.props;
        if (changeSuccess && changeSuccess != prevProps.changeSuccess) {
            this.setState({showModal: false});
            toastr.success('Cập nhật thành công');
        }
    }

    render() {
        const {onSubmit, insuranceService, insuranceValue, disabled} = this.props;
        const {showModal} = this.state;
        const insuranceFee = insuranceService ? formatCurrency(+insuranceValue) : '--';
        return (
            <div>
                {disabled ? <span>{insuranceFee}</span>
                    :
                    <div className="input-edit" onClick={e => this.setState({showModal: true})}>
                        <span>{insuranceFee}</span>
                    </div>
                }
                <RBT.Modal show={showModal} bsSize="small"  onHide={e => this.setState({showModal: false})}>
                    <RBT.Modal.Header closeButton>
                        <RBT.Modal.Title>Sửa thông tin bảo hiểm</RBT.Modal.Title>
                    </RBT.Modal.Header>
                    <RBT.Modal.Body>
                        <ReInsuranceForm
                            close={::this.closeModal}
                            onSubmit={onSubmit}
                            initialValues={{
                                insuranceValue, insuranceService
                            }}/>
                    </RBT.Modal.Body>
                </RBT.Modal>
            </div>
        )
    }
}