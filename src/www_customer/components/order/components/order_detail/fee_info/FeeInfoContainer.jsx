import React from 'react'
import FeeInfo from './FeeInfo'
import {connect} from 'react-redux'
import {CHANGE_INSURANCE_SUCCESS} from '../../../ActionTypes'
import {InsuranceByOrderIdSelector} from '../../../selectors'
import {toastr} from '../../../../../../www_components/Toastr2'
const Config = require('../../../../../../config.json');

class FeeInfoContainer extends React.Component {
    onSubmit(data){
        const {onSubmit} = this.props;
        const {weightKg} = data;
        if(typeof weightKg !== 'undefined'){
            if(weightKg > Config.weightLimit){
                toastr.confirm(`Trong trường hợp cân nặng của vận đơn lớn hơn ${Config.weightLimit} Kg xin liên hệ trực tiếp với Dịch vụ để được tư vấn và có mức phí hợp lý hơn`);
                return;
            }
            data.weight = weightKg * 1000;
        }
        onSubmit(data);
    }

    render() {
        return (
            <FeeInfo
                {...this.props}
                onSubmit={::this.onSubmit}
            />
        );
    }
}

const mapStateToProps = (state, props) => {
    const getInsurance = InsuranceByOrderIdSelector(state);
    return {
        changeSuccess: state.actionType !== CHANGE_INSURANCE_SUCCESS,
        insurance: getInsurance(props.orderId)
    }
};
export default connect(mapStateToProps)(FeeInfoContainer);