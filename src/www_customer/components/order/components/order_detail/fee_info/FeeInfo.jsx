import React from 'react'
import {InlineWeightInput, InlineNumericEdit} from '../../../../../../www_components'
import {formatCurrency} from '../../../../../helpers/format'
import {InsuranceModal} from './ChangeInsuranceForm'

export default ({order, onSubmit, insurance, changeSuccess}) => {
    const {weight, weightKg, cod, realCod, totalFee, status, isEndingStatus, isAfterWaitingPickup} = order;
    const insuranceService = !!(insurance);
    const insuranceValue = insuranceService ? insurance['customField1'] : null;
    return (
        <div className="panel panel-default no-shadow">
            <div className="panel-body tbl-info-order">
                <table>
                    <thead>
                    <tr>
                        <th colSpan="100%">THÔNG TIN PHÍ</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td className="fw-600 pt-nowrap">Cân nặng (Kg):</td>
                        <td width={150}>
                            <InlineWeightInput
                                onSubmit={weightKg => onSubmit({weightKg})}
                                disabled={isEndingStatus || isAfterWaitingPickup}
                                value={weightKg}
                            />
                        </td>
                        <td className="fw-600 pt-nowrap hidden">Kích thước:</td>
                        <td className="hidden" width={150}>--</td>
                        <td className="fw-600 pt-nowrap">Tiền bảo hiểm (VNĐ):</td>
                        <td width={150}>
                            <InsuranceModal
                                onSubmit={onSubmit}
                                disabled={isEndingStatus}
                                changeSuccess={changeSuccess}
                                insuranceValue={insuranceValue}
                                insuranceService={insuranceService}/></td>
                        <td className="fw-600 pt-nowrap">COD (VNĐ):&nbsp;
                            <InlineNumericEdit
                                disabled={isEndingStatus || (realCod || realCod === 0 )}
                                onSubmit={value => onSubmit({cod: value})}
                                value={cod}/>
                        </td>
                    </tr>
                    {(realCod ||  realCod === 0) &&
                    <tr>
                        <td className="fw-600 pt-nowrap">COD thực thu (VNĐ):</td>
                        <td width={150}><span className="text-success fw-600">{formatCurrency(realCod)}</span></td>
                    </tr>
                    }
                    <tr>
                        <td className="fw-600 pt-nowrap">Tổng phí:</td>
                        <td colSpan="7">{formatCurrency(totalFee)} VNĐ</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    )
}
