import React from 'react'
import TextAreaInputEdit from '../../../../../../www_components/TextAreaInputEdit'

export default ({order, handlePrivateNoteEdit}) => (
    <div className="panel panel-default no-shadow">
        <div className="panel-body tbl-info-order">
            <div className="hub-address">{/*<i className="fa fa-map-marker mr-5"/> CG - HN*/}</div>
            <table>
                <thead>
                <tr>
                    <th colSpan="100%">NGƯỜI NHẬN</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td className="fw-600 pt-nowrap">Họ tên:</td>
                    <td>{order.deliverToContact} / {order.deliverToPhone}</td>
                </tr>
                <tr>
                    <td className="fw-600 pt-nowrap">Địa chỉ giao:</td>
                    <td>
                        {order.deliverToFullAddress}
                    </td>
                </tr>
                <tr className="hidden">
                    <td className="fw-600 pt-nowrap">Giao trước:</td>
                    <td>12h</td>
                </tr>

                <tr>
                    <td className="fw-600 pt-nowrap">Ghi chú:</td>
                    <td>{order.note}</td>
                </tr>

                <tr>
                    <td className="fw-600 pt-nowrap">Ghi chú riêng:</td>
                    <td>

                        <TextAreaInputEdit
                            value={order.privateNote}
                            name="privateNote"
                            onKeyUpFunc={handlePrivateNoteEdit}
                        />
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
)