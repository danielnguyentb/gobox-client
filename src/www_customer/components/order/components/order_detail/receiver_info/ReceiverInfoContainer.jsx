import React, {PropTypes, Component} from 'react'
import ReceiverInfo from './ReceiverInfo'
import {connect} from 'react-redux'
import {update} from '../../../actions'
class ReceiverInfoContainer extends Component {
    constructor(props) {
        super(props);
    }
    handlePrivateNoteEdit(val) {
        let {order, update} = this.props;
        if(order && val) {
            update(order.id, {"privateNote": val});
        }
    }
    render() {
        return <ReceiverInfo {...this.props} handlePrivateNoteEdit={::this.handlePrivateNoteEdit} />
    }
}
const mapStateToProps = (state, props) => ({});
export default connect(
    mapStateToProps, {update}
)(ReceiverInfoContainer);