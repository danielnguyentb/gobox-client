import React from 'react'
import DocumentTitle from '../../../../../www_components/DocumentTitle'
import CustomerInfoContainer from './customer_info/CustomerInfoContainer'
import ReceiverInfoContainer from './receiver_info/ReceiverInfoContainer'
import FeeInfoContainer from './fee_info/FeeInfoContainer'
import BoxchatContainer from './boxchat/BoxchatContainer'
import TimeLineContainer from './time_line/TimeLineContainer'
import OtherOrderContainer from './other_order/OtherOrderContainer'
import Link from '../../../../../www_components/Link';
import {statusTitle, statusLabel, typesTitle} from '../../constants'
import {DELIVERY_ORDER_UPDATE_FAILURE} from '../../ActionTypes'
import {ErrorToast} from '../../../../../www_components/Alert/components'
import * as RBT from 'react-bootstrap'

export default (props) => {
    const {barcode, orderStatus, type, id, historyTimesCustomer} = props.order || {};
    return (
        <DocumentTitle title={`Chi tiết vận đơn ${barcode}`}>
            <div>
                <ErrorToast dispatchTypes={[DELIVERY_ORDER_UPDATE_FAILURE]}/>
                <section className="content-header">
                    <div className="row">
                        <div className="col-md-9 col-xs-12">
                            <h3 className="mt-0 mb-0">
                                <span>Chi tiết vận đơn</span>
                                <span className="text-primary ml-5 mr-15"> {barcode} </span>

                                <span className={`lbl-style label label-${statusLabel[orderStatus]} ml-15 fs-14`}>
                            {statusTitle[orderStatus]}
                        </span>

                                <span className="fs-16 ml-20">
                            ({typesTitle[type]})
                        </span>

                            </h3>

                            <ol className="breadcrumb mb-0 pt-0">
                                <li><Link to="/orders">Danh sách vận đơn</Link></li>
                                <li className="active">Chi tiết vận đơn</li>
                            </ol>
                        </div>
                        <div className="col-md-3 col-xs-12 text-right">
                            <RBT.Button bsStyle="success">
                                <Link className="text-white multi-print" target="_blank" to={`/order_print?ids=${id}`}>
                                    <i className="ti-printer mr-5"/>IN VẬN ĐƠN
                                </Link>
                            </RBT.Button>
                        </div>
                    </div>

                </section>

                <section className="content">
                    <div className="box">
                        <div className="row">
                            <div className="col-md-9 col-xs-12">
                                <div className="row">
                                    <div className="col-md-6 col-xs-12">
                                        <CustomerInfoContainer {...props} />
                                    </div>
                                    <div className="col-md-6 col-xs-12">
                                        <ReceiverInfoContainer {...props}/>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-md-12 col-xs-12">
                                        <FeeInfoContainer {...props}/>
                                    </div>
                                </div>
                                <div className="row">
                                    {id &&
                                    <OtherOrderContainer {...props}/>
                                    }
                                </div>
                                <div className="row">
                                    <div className="col-md-12 col-xs-12">
                                        {id &&
                                        <BoxchatContainer {...props} />
                                        }
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-3 col-xs-12">
                                <div className="row">
                                    {id && historyTimesCustomer &&
                                    <TimeLineContainer timelines={historyTimesCustomer}/>
                                    }
                                </div>
                            </div>
                        </div>

                    </div>
                </section>
            </div>
        </DocumentTitle>
    )
}