import React from 'react'
import {connect} from 'react-redux'
import {LogsByIdSelector} from '../../../selectors'
import {getChatHistories} from '../../../actions'
import {isEqual} from 'lodash'

import ChatBoxCustomer from '../../../../../../www_components/ChatBoxCustomer'

class BoxchatContainer extends React.Component{
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        const {logs, getChatHistories, order} = this.props;
        if (!logs || logs.length == 0) {
            getChatHistories(order.id)
        }
    }
    componentDidUpdate(prevProps) {
        const {getChatHistories, order} = this.props;
        if (!isEqual(order, prevProps.order)) {
            getChatHistories(order.id)
        }
    }
    render(){
        let {order, logs} = this.props;
        if(!order) return null;
        return(
            <ChatBoxCustomer objectId={order.id} histories={logs} />
        )
    }
}
const mapStateToProps = (state, props) => {
    const getLogs = LogsByIdSelector(state);
    return {
        logs: getLogs(props.order.id),
    }
};
export default connect(
    mapStateToProps, {LogsByIdSelector, getChatHistories}
)(BoxchatContainer);