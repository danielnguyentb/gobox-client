import React from 'react'

import avatar from './../img/avatar.png'

class Boxchat extends React.Component{
    render(){
        return(
                <div className="row">
                    <div className="col-md-12 col-xs-12">
                        <h4 className="fw-600 mt-0">Bình luận</h4>
                        <div className="log-activity">
                            <p className="mb-0 text-grey fs-12">
                                <span className="fw-600 mr-5">Nguyễn Thị Thanh Phương / @thanhphuong</span>
                                <span className="mr-10">tạo nhiệm vụ đi giao</span>
                                <span>10:18 ngày 22/12</span>
                            </p>
                        </div>
                        <div className="log-activity">
                            <p className="mb-0 text-gre fs-12y">
                                <span className="fw-600 mr-5">Trần Thị Thảo Thảo / @thaothao</span>
                                <span className="mr-10">lấy hàng thành công</span>
                                <span>18:18 ngày 22/12</span>
                            </p>
                        </div>
                        <div className="chat-items">
                            <div className="avatar-chat">
                                <img src={avatar} alt="" />
                            </div>
                            <div className="content-chat">
                                <p className="mb-0">
                                    <span className="fw-600 mr-10 text-primary">Nguyễn Xuân Kiên / @kiennx</span>
                                    <span className="text-grey fs-12">10:18 ngày 22/12</span>
                                </p>
                                <span>Mong dịch vụ ship hàng đến người nhận giúp tôi càng sớm càng tốt. Cảm ơn dịch vụ Shippo!</span>
                            </div>
                            <div className="clearfix"></div>
                        </div>
                        <div className="chat-items">
                            <div className="avatar-chat">
                                <img src={avatar} alt="" />
                            </div>
                            <div className="content-chat">
                                <p className="mb-0">
                                    <span className="fw-600 mr-10">Nguyễn Tri Thương Thương / @thuong</span>
                                    <span className="text-grey fs-12">10:18 ngày 22/12</span>
                                </p>
                                <span>Mong dịch vụ ship hàng đến người nhận giúp tôi càng sớm càng tốt. Cảm ơn dịch vụ Shippo!</span>
                            </div>
                            <div className="clearfix"></div>
                        </div>
                        <div className="log-activity">
                            <p className="mb-0 text-grey fs-12">
                                <span className="fw-600 mr-5">Nguyễn Thị Thanh Phương / @thanhphuong</span>
                                <span className="mr-10">tạo nhiệm vụ đi giao</span>
                                <span>10:18 ngày 22/12</span>
                            </p>
                        </div>
                        <div className="chat-items">
                            <div className="avatar-chat">
                                <img src={avatar} alt="" />
                            </div>
                            <div className="content-chat">
                                <p className="mb-0">
                                    <span className="fw-600 mr-10">Lương Thanh Lâm Rừng / @lanlan</span>
                                    <span className="text-grey fs-12">10:18 ngày 22/12</span>
                                </p>
                                <span>Gió hỡi gió hãy mang quà lại đây. Mong dịch vụ ship hàng đến người nhận giúp tôi càng sớm càng tốt. Cảm ơn dịch vụ Shippo!</span>
                            </div>
                            <div className="clearfix"></div>
                        </div>
                        <div className="chat-items">
                            <div className="avatar-chat">
                                <img src={avatar} alt="" />
                            </div>
                            <div className="content-chat">
                                <p className="mb-0">
                                    <span className="fw-600 mr-10 text-primary">Hà Minh Phong / @minhphong</span>
                                    <span className="text-grey fs-12">10:18 ngày 22/12</span>
                                </p>
                                <span>Mong dịch vụ ship hàng đến người nhận giúp tôi càng sớm càng tốt. Cảm ơn dịch vụ Shippo!</span>
                            </div>
                            <div className="clearfix"></div>
                        </div>
                    </div>
                </div>
        )
    }
}
export default (Boxchat)