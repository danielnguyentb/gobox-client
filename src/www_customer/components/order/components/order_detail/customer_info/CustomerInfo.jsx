import React from 'react'


export default ({order}) => (
    <div className="panel panel-default no-shadow">
        <div className="panel-body tbl-info-order">
            <div className="hub-address">{/*<i className="fa fa-map-marker mr-5"/> CG - HN*/}</div>
            <table>
                <thead>
                <tr>
                    <th colSpan="100%">KHÁCH HÀNG</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td className="fw-600 pt-nowrap">Liên hệ:</td>
                    <td>{order.deliverFromContact } / {order.deliverFromPhone}</td>
                </tr>
                <tr>
                    <td className="fw-600 pt-nowrap">Địa chỉ lấy:</td>
                    <td>
                        {order.deliverFromFullAddress}
                    </td>
                </tr>
                <tr className="hidden">
                    <td className="fw-600 pt-nowrap">Lấy trước:</td>
                    <td>12h</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
)