import React, {PropTypes, Component} from 'react'
import CustomerInfo from './CustomerInfo'
class ReceiverInfoContainer extends Component {
    constructor(props) {
        super(props)
    }
    render() {
        return <CustomerInfo {...this.props} />
    }
}
export default (ReceiverInfoContainer);