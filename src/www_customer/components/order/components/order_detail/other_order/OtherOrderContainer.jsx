import React from 'react'
import {connect} from 'react-redux';
import OtherOrder from './OtherOrder'
import {getRelatedOrders} from '../../../actions'
import {RelatedOrdersSelector} from '../../../selectors'

let isOrigin = false;
class OtherOrderContainer extends React.Component{
    static propTypes = {
        order: React.PropTypes.object,
    };
    componentDidMount() {
        let {order} = this.props;
        if(order && order.originOrder) {
            this.props.getRelatedOrders(order.originOrder.id);
            isOrigin = true;
        } else if(order) {
            this.props.getRelatedOrders(order.id);
        }
    }
    render(){
        return (
            <div className="col-md-offset-3 col-md-6 col-xs-12">
                <h4 className="fw-600">Vận đơn liên quan</h4>

                <ul className="other-orders">
                    <OtherOrder {...this.props} isOrigin={isOrigin} />
                </ul>
            </div>
        )
    }
}
const mapStateToProps = (state, props) => ({
    relatedOrders: RelatedOrdersSelector(state)
});


export default connect(
    mapStateToProps, {getRelatedOrders}
)(OtherOrderContainer);