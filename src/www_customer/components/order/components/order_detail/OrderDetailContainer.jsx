import React from 'react';
import {connect} from 'react-redux';
import DocumentTitle from '../../../../../www_components/DocumentTitle'
import {getDeliveryOrder, getRelatedOrders, update, changeInsurance} from '../../actions'
import './../order.scss';
import OrderDetail from './OrderDetail'
import {RelatedOrdersSelector, DeliveryOrderByIdSelector} from '../../selectors'
import {toastr} from '../../../../../www_components/Toastr2'
import {DELIVERY_ORDER_UPDATE_FAILURE} from '../../ActionTypes'
import {ErrorToast} from '../../../../../www_components/Alert'
const Config = require('../../../../../config.json');

class OrderDetailContainer extends React.Component {
    componentWillMount() {
        const {orderId, getDeliveryOrder, getRelatedOrders} = this.props;
        if (orderId) {
            getDeliveryOrder(orderId);
            getRelatedOrders(orderId);
        }
    }

    componentDidUpdate(prevProps) {
        const {updatedOrder, orderId, getDeliveryOrder, getRelatedOrders} = this.props;
        if (orderId && orderId != prevProps.orderId) {
            getDeliveryOrder(orderId);
            getRelatedOrders(orderId);
        }
        if (updatedOrder) {
            toastr.success("Cập nhật vận đơn thành công");
        }
    }

    handleSubmit(data) {
        const {status, cod, totalFee, weightKg, insuranceValue, insuranceService} = data;
        const {orderId, changeInsurance, update} = this.props;
        if(insuranceValue > Config.insuranceLimit){
            toastr.confirm(`Trong trường hợp giá trị hàng hóa của Quý Khách lớn hơn ${Config.insuranceLimit} VNĐ xin liên hệ trực tiếp với Dịch vụ để được tư vấn và có mức phí hợp lý hơn`);
            return;
        }
        if(cod > Config.CodLimit){
            toastr.confirm(`Trong trường hợp số tiền COD của vận đơn lớn hơn ${Config.CodLimit} VNĐ xin liên hệ trực tiếp với Dịch vụ để được tư vấn và có mức phí hợp lý hơn`);
            return;
        }
        if(weightKg > Config.weightLimit){
            toastr.confirm(`Trong trường hợp cân nặng của vận đơn lớn hơn ${Config.weightLimit} Kg xin liên hệ trực tiếp với Dịch vụ để được tư vấn và có mức phí hợp lý hơn`);
            return;
        }
        if (typeof insuranceService !== 'undefined') {
            changeInsurance(orderId, {
                action: insuranceService ? 'CHECKED' : 'UNCHECKED',
                newInsurance: parseFloat(insuranceValue) || null
            })
        } else {
            update(orderId, data);
        }
    }

    render() {
        const {order} = this.props;
        return (
            <div>
                <DocumentTitle title="Chi tiết vận đơn"/>
                <ErrorToast dispatchTypes={[DELIVERY_ORDER_UPDATE_FAILURE]}/>
                <OrderDetail
                    {...this.props}
                    order={order || {}}
                    onSubmit={::this.handleSubmit}/>
            </div>
        )
    }
}

const mapStateToProps = (state, props) => {
    const orderId = parseInt(props.params.orderId) || 0;
    const getOrder = DeliveryOrderByIdSelector(state);
    return {
        orderId,
        order: getOrder(orderId),
        getRelatedOrders: RelatedOrdersSelector(state),
        error: state.order.error,
        updatedOrder: state.order.updatedOrder
    }
};

export default connect(
    mapStateToProps, {getDeliveryOrder, getRelatedOrders, update, changeInsurance}
)(OrderDetailContainer);
