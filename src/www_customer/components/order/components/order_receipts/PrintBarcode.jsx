import React from 'react'
const Config = require('../../../../../config.json');
import {formatTime, formatCurrency} from '../../../../helpers/format'
import BarcodeRender from 'react-barcode'
import './print.scss'

import logo from './img/logo.png'

export const PrintBarcode = ({order, profile}) => {
    const {
        barcode, deliverFromId, deliverFromFullAddress, deliverFromContact, deliverFromPhone,
        deliverToFullAddress, deliverToContact, deliverToPhone, cod, realCod, note, createdTime
    } = order || {};
    const {username} = profile || {};
    return (
        <div className="container order-print break-page">
            <div className="row">
                {/*TODO: update config.json.example in product*/}
                <div className="text-uppercase pull-left">
                    <img src={logo} alt="" />
                    <p className="mb-0 text-center">{Config.serviceDomain}</p>
                </div>
                <div className="pull-left ml-30 text-center mt-30">
                    <h3 className="mt-0 mb-0">VẬN ĐƠN {barcode}</h3>
                    <p>Ngày {formatTime(createdTime)} @{username}</p>
                </div>
                <div className="pull-right">
                    <BarcodeRender
                        displayValue={false}
                        value={barcode || 'loading'}/>
                </div>
            </div>


            <div className="row mt-5">
                <table className="table table-bordered">
                    <tr>
                        <th colSpan="100%">Lấy hàng (ID: {deliverFromId})</th>
                    </tr>
                    <tr>
                        <td className="pt-nowrap">Họ tên</td>
                        <td className="fw-600">{deliverFromContact} / {deliverFromPhone}</td>
                    </tr>
                    <tr>
                        <td className="pt-nowrap">Địa chỉ lấy</td>
                        <td>{deliverFromFullAddress}</td>
                    </tr>
                    <tr>
                        <th colSpan="100%">Địa chỉ giao hàng</th>
                    </tr>
                    <tr>
                        <td className="pt-nowrap">Họ tên</td>
                        <td className="fw-600">{deliverToContact} / {deliverToPhone}</td>
                    </tr>
                    <tr>
                        <td className="pt-nowrap">Địa chỉ giao</td>
                        <td>{deliverToFullAddress}</td>
                    </tr>
                    <tr>
                        <td>Ghi chú</td>
                        <td>{note}</td>
                    </tr>
                    <tr>
                        <td className="pt-nowrap">Thu hộ (COD)</td>
                        <td>{formatCurrency((realCod != null ? realCod : cod), true)}</td>
                    </tr>
                </table>

            </div>
        </div>
    );
};