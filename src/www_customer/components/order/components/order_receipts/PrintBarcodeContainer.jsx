import React from 'react'
import {PrintBarcode} from './PrintBarcode'
import {fetchDeliveryOrderByIdsSaga} from '../../actions'
import * as selectors from '../../selectors'
import * as customers from '../../../customer'
import {connect} from 'react-redux'
import {isEmpty} from 'lodash'

class PrintBarcodeContainer extends React.Component {
    componentDidMount() {
        const {dispatch, orderIds, profileId} = this.props;
        if (orderIds && orderIds.length > 0) {
            dispatch(fetchDeliveryOrderByIdsSaga(orderIds));
        }
        if (profileId > 0) {
            dispatch(customers.actions.fetchCustomerByIdSaga(profileId));
        }
    }

    componentDidUpdate(prevProps) {
        const {orders, profile} = this.props;
        if (!isEmpty(orders) && !isEmpty(profile)) {
            setTimeout(() => {
                window.requestAnimationFrame(function () {
                    window.print();
                });
            }, 2000);
        }
    }

    render() {
        const {orders, profile} = this.props;
        console.warn(orders);
        return (
            <div>
                {orders.map(order => {
                    return <PrintBarcode key={order.id} order={order} profile={profile}/>
                })}
            </div>
        );
    }
}

const mapStateToProps = (state, props) => {
    const getOrders = selectors.DeliveryOrdersByIdsSelector(state);
    const {query} = props.location;
    const orderIds = typeof query['ids'] == 'string' ? query['ids'].split(',') : null;
    const orders = orderIds && orderIds.length > 0 ? getOrders(orderIds).filter(t => !isEmpty(t)) : [];
    console.warn(orderIds, getOrders(orderIds), orders);
    return {
        orderIds, orders,
        profileId: customers.selectors.getProfileId(state),
        profile: customers.selectors.getProfile(state),
    }
};
export default connect(mapStateToProps)(PrintBarcodeContainer);