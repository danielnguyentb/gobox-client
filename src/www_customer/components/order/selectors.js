/**
 * Created by Tester-Ali on 07-11-2016.
 */
import {createSelectorCreator, defaultMemoize, createSelector} from 'reselect'
import lodash from 'lodash'
import {NAME, statusLabel, statusTitle, endingStatus, afterWaitingPickupStatus} from './constants'
import {formatFullTime} from '../../helpers/format'

const createDeepEqualSelector = createSelectorCreator(
    defaultMemoize,
    lodash.isEqual
);

export const orderSelector = state => state[NAME];
export const ordersSelector = createDeepEqualSelector(
    orderSelector,
    order => Object.values(order.byId)
);

export const featuresSelector = createSelector(
    orderSelector,
    order => Object.values(order.featureById)
);

export const insurancesSelector = createSelector(
    featuresSelector,
    features => features.filter(t => t['featureId'] == 1)
);

export const InsuranceByOrderIdSelector = createDeepEqualSelector(
    insurancesSelector,
    insurances => lodash.memoize(
        orderId => insurances.filter(t => t['orderId'] == orderId && !t['isDeleted']).pop()
    )
);
export const DeliveryOrderByIdSelector = createDeepEqualSelector(
    [orderSelector, InsuranceByOrderIdSelector],
    (order, getInsurance) => lodash.memoize(
        id => hydrate(order.byId[id], getInsurance(id))
    )
);
export const DeliveryOrdersByIdsSelector = createDeepEqualSelector(
    [orderSelector, InsuranceByOrderIdSelector],
    (order, getInsurance) => lodash.memoize(
        ids => ids.map(id => hydrate(order.byId[id], getInsurance(id)))
    )
);

export const hydrate = (order, insurance) => {
    if (!order) return order;
    order['insuranceService'] = insurance;
    order['isEndingStatus'] = endingStatus.includes(order['orderStatus']);
    order['isAfterWaitingPickup'] = afterWaitingPickupStatus.includes(order['orderStatus']);
    order['tip'] = `${order.deliverToContact} - ${order.deliverToPhone}<br/>${order.deliverToFullAddress || ''}`;
    order['deliveredLocalize'] = order.delivered ? formatFullTime(order.delivered) : '--/--/--';
    order['weightKg'] = order['weight'] != null ? parseInt(order['weight']) / 1000 : null;
    return order;
};

export const PaginateDeliveryOrdersSelector = createSelector(
    [orderSelector, DeliveryOrderByIdSelector],
    (order, getOrder) => order.paginateIds.map(id => getOrder(id))
);

//all printable checkbox checked
export const PrintableAllSelector = createSelector(
    PaginateDeliveryOrdersSelector,
    orders => orders.every(t => t.printable === true)
);

//all printable checkbox not checked
export const PrintableNotSelector = createSelector(
    PaginateDeliveryOrdersSelector,
    orders => orders.every(t => t.printable !== true)
);

export const PrintableOrderIdsSelector = createSelector(
    PaginateDeliveryOrdersSelector,
    orders => orders.filter(t => t.printable === true).map(t => t.id)
);

export const RelatedOrdersSelector = createSelector(
    [orderSelector, DeliveryOrderByIdSelector],
    (order, getOrder) => order.relatedOrderIds.map(id => getOrder(id))
);

export const logsSelector = createSelector(
    orderSelector,
    order => Object.values(order.logById)
);

export const LogsByIdSelector = createSelector(
    logsSelector,
    logs => lodash.memoize(
        orderId => logs.filter(log => log.objectId == orderId)
    )
);