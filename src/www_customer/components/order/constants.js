export const NAME = 'order';

export const services = {
    INSURANCE: 'INSURANCE'    // bảo hiểm
};

export const types = {
    DELIVERY: 'DELIVERY',
    RETURN: 'RETURN'
};

export const typesTitle = {
    [types.DELIVERY]: 'Giao đi',
    [types.RETURN]: 'Trả lại'
};

export const Status = {
    CUSTOMER_WAITING_FOR_PICKUP : 'CUSTOMER_WAITING_FOR_PICKUP',
    CUSTOMER_DELIVERING : 'CUSTOMER_DELIVERING',
    CUSTOMER_REDELIVERING : 'CUSTOMER_REDELIVERING',
    CUSTOMER_DELIVERED : 'CUSTOMER_DELIVERED',
    CUSTOMER_COMPLETED : 'CUSTOMER_COMPLETED',
    CUSTOMER_RETURN : 'CUSTOMER_RETURN',
    CUSTOMER_RETURN_COMPLETED : 'CUSTOMER_RETURN_COMPLETED',
    CUSTOMER_CANCELED : 'CUSTOMER_CANCELED',
    CUSTOMER_REJECT : 'CUSTOMER_REJECT'
};


export const statusLabel = {
    [Status.CUSTOMER_WAITING_FOR_PICKUP]: 'default',
    [Status.CUSTOMER_DELIVERING]: 'warning',
    [Status.CUSTOMER_REDELIVERING]: 'warning',
    [Status.CUSTOMER_COMPLETED]: 'success',
    [Status.CUSTOMER_RETURN]: 'warning',
    [Status.CUSTOMER_DELIVERED]: 'success',
    [Status.CUSTOMER_RETURN_COMPLETED]: 'success',
    [Status.CUSTOMER_CANCELED]: 'danger',
    [Status.CUSTOMER_REJECT]: 'danger',
};

export const statusTitle = {
    [Status.CUSTOMER_WAITING_FOR_PICKUP] : 'Chờ lấy hàng',
    [Status.CUSTOMER_DELIVERING] : 'Đang giao',
    [Status.CUSTOMER_REDELIVERING] : 'Đang giao lại',
    [Status.CUSTOMER_DELIVERED] : 'Đã giao',
    [Status.CUSTOMER_COMPLETED] : 'Thành công',
    [Status.CUSTOMER_RETURN] : 'Trả lại',
    [Status.CUSTOMER_RETURN_COMPLETED] : 'Trả lại thành công',
    [Status.CUSTOMER_CANCELED] : 'Hủy',
    [Status.CUSTOMER_REJECT] : 'Từ chối'
};

export const afterWaitingPickupStatus = [
    Status.CUSTOMER_DELIVERING,
    Status.CUSTOMER_REDELIVERING,
    Status.CUSTOMER_DELIVERED,
    Status.CUSTOMER_COMPLETED,
    Status.CUSTOMER_RETURN,
    Status.CUSTOMER_RETURN_COMPLETED,
    Status.CUSTOMER_CANCELED,
    Status.CUSTOMER_REJECT
];

export const endingStatus = [
    Status.CUSTOMER_COMPLETED,
    Status.CUSTOMER_CANCELED,
    Status.CUSTOMER_REJECT
];

export const allowCancelOrder = [
    Status.CUSTOMER_WAITING_FOR_PICKUP
];