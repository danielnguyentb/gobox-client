import Client from '../../helpers/Client'
import * as types from './ActionTypes'

class DeliveryOrder extends Client {

    create(data) {
        return this.callApi({
            types: [types.DELIVERY_ORDER_CREATE_REQUEST, types.DELIVERY_ORDER_CREATE_SUCCESS, types.DELIVERY_ORDER_CREATE_FAILURE],
            endpoint: '/DeliveryOrders',
            method: 'POST',
            body: data,
            form: "OrderForm"
        })
    }

    update(id, data) {
        return this.callApi({
            types: [types.DELIVERY_ORDER_UPDATE_REQUEST, types.DELIVERY_ORDER_UPDATE_SUCCESS, types.DELIVERY_ORDER_UPDATE_FAILURE],
            endpoint: `/DeliveryOrders/${id}`,
            method: 'PUT',
            body: data,
        })
    }

    getDeliveryOrder(id, filter) {
        filter = typeof filter == 'string' ? filter : JSON.stringify(filter);
        return this.callApi({
            types: [types.DELIVERY_ORDER_REQUEST, types.DELIVERY_ORDER_SUCCESS, types.DELIVERY_ORDER_FAILURE],
            endpoint: `/DeliveryOrders/${id}?filter=${filter}`,
            method: 'GET',
        }, 'fetchingDeliveryOrder')
    }

    getDeliveryOrders(filter = '') {
        filter = typeof filter == 'string' ? filter : JSON.stringify(filter);
        return this.callApi({
            types: [types.DELIVERY_ORDERS_REQUEST, types.DELIVERY_ORDERS_SUCCESS, types.DELIVERY_ORDERS_FAILURE],
            endpoint: `/DeliveryOrders?filter=${filter}`,
            method: 'GET',
        }, 'fetchingDeliveryOrders')
    }

    getCountByStatus() {
        return this.callApi(({
            types: [types.ORDER_STATUS_REQUEST, types.ORDER_STATUS_SUCCESS, types.ORDER_STATUS_FAILURE],
            endpoint: '/DeliveryOrders/getCountByStatus',
            method: 'GET'
        }), 'fetchingOrderStates')
    }

    getLastPickupAddress() {
        return this.callApi(({
            types: [types.DELIVERY_ORDER_LAST_PICKUP_ADDRESS.REQUEST, types.DELIVERY_ORDER_LAST_PICKUP_ADDRESS.SUCCESS, types.DELIVERY_ORDER_LAST_PICKUP_ADDRESS.FAILURE],
            endpoint: '/DeliveryOrders/lastPickupAddress',
            method: 'GET'
        }), 'loading')
    }

    changeStatus(orderId, status) {
        return this.callApi(({
            types: [types.DELIVERY_ORDER_CHANGE_STATUS.REQUEST, types.DELIVERY_ORDER_CHANGE_STATUS.SUCCESS, types.DELIVERY_ORDER_CHANGE_STATUS.FAILURE],
            endpoint: `/DeliveryOrders/${orderId}/changeStatus/${status}`,
            method: 'PUT'
        }), 'loading')
    }

    getRelatedOrders(id) {
        return this.callApi(({
            types: [types.DELIVERY_ORDER_RELATES.REQUEST, types.DELIVERY_ORDER_RELATES.SUCCESS, types.DELIVERY_ORDER_RELATES.FAILURE],
            endpoint: `/DeliveryOrders/${id}/relatedOrders`,
            method: 'GET',
        }))
    }

    changeInsurance(orderId, data) {
        return this.callApi({
            types: [types.CHANGE_INSURANCE_REQUEST, types.CHANGE_INSURANCE_SUCCESS, types.CHANGE_INSURANCE_FAILURE],
            endpoint: `/DeliveryOrders/${orderId}/changeInsurance`,
            method: 'PATCH',
            form: 'ChangeInsuranceForm',
            body: data
        })
    }

    uploadOrders(data) {
        return this.callApi({
            types: [types.DELIVERY_ORDER_UPLOAD_ORDERS.REQUEST, types.DELIVERY_ORDER_UPLOAD_ORDERS.SUCCESS, types.DELIVERY_ORDER_UPLOAD_ORDERS.FAILURE],
            endpoint: `/DeliveryOrders/uploadOrders`,
            method: 'POST',
            form: 'OrderUploadExcelForm',
            body: data,
            headers: {
                'Content-Type': null
            }
        })
    }

    getChatHistories(id, filter) {
        filter = typeof filter == 'string' ? filter : JSON.stringify(filter);
        return this.callApi({
            types: [types.DELIVERY_ORDER_CHAT.REQUEST, types.DELIVERY_ORDER_CHAT.SUCCESS, types.DELIVERY_ORDER_CHAT.FAILURE],
            endpoint: `/DeliveryOrders/${id}/histories?filter=${filter}`,
            method: 'GET',
        })
    }
}
export default new DeliveryOrder();