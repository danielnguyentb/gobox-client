import {
    DELIVERY_ORDER_SUCCESS,
    DELIVERY_ORDERS_SUCCESS,
    DELIVERY_ORDER_CREATE_SUCCESS,
    ORDER_STATUS_SUCCESS,
    DELIVERY_ORDER_CREATE_FAILURE,
    DELIVERY_ORDER_LAST_PICKUP_ADDRESS,
    DELIVERY_ORDER_CHANGE_STATUS,
    DELIVERY_ORDER_RELATES,
    DELIVERY_ORDER_UPDATE_SUCCESS,
    CHANGE_INSURANCE_SUCCESS, DELIVERY_ORDER_UPLOAD_ORDERS,
    TOGGLE_PRINTABLE, TOGGLE_PRINTABLE_ALL,
    DELIVERY_ORDER_CHAT
} from './ActionTypes'

import {combineReducers} from 'redux'
import {statusLabel, statusTitle} from './constants'
import {LOCATION_CHANGE} from 'react-router-redux'
import Localize from '../../../resources/Localize'
import moment from 'moment'
moment.locale('vi');

const orderStatus = (status = '') => {
    let obj = {};
    obj.code = status;
    obj.label = statusLabel.hasOwnProperty(status) ? statusLabel[status] : '';
    obj.title = statusTitle.hasOwnProperty(status) ? statusTitle[status] : '';
    return obj;
};
const status = (state = [], action) => {
    let copy = {...state};
    let {response} = action;
    switch (action.type) {
        case  ORDER_STATUS_SUCCESS :
            let countStatus = action.response;
            let statusArr = Object.keys(countStatus).map(key => {
                let obj = orderStatus(key);
                obj.count = countStatus[key];
                return obj;
            });
            let defaultStatus = {
                code: '',
                label: 'primary',
                title: 'Tất cả',
                count: statusArr.reduce((p, s) => p + s.count, 0)
            };
            return [defaultStatus, ...statusArr];
        case  DELIVERY_ORDER_CHANGE_STATUS.SUCCESS:
            let status_arr = [];
            Object.keys(copy).map(t => {
                if (copy[t].code == response.status) {
                    ++copy[t].count;
                }
                status_arr.push(copy[t]);
            });
            return status_arr;
    }
    return state;
};

const order = (state = {}) => {
    state.statusDetail = orderStatus(state.orderStatus);
    return state;
};

const byId = (state = {}, action) => {
    let {response} = action;
    let copy = {...state};
    switch (action.type) {
        case DELIVERY_ORDERS_SUCCESS:
            response.result.map((value) => {
                copy[value.id] = order(value);
                copy[value.id]['printable'] = true;
            });
            return copy;
        case DELIVERY_ORDER_SUCCESS:
        case DELIVERY_ORDER_CREATE_SUCCESS:
            if (!copy.hasOwnProperty(response.id)) {
                copy[response.id] = order(response);
            }
            return copy;
        case DELIVERY_ORDER_CHANGE_STATUS.SUCCESS:
            response['orderStatus'] = response.status;
            if (copy.hasOwnProperty(response.id)) order(Object.assign(copy[response.id], response));
            return copy;

        case DELIVERY_ORDER_LAST_PICKUP_ADDRESS.SUCCESS:
            if (response) {
                delete response.deliverFromStreetId;
                delete response.deliverFromPlaceId;
                delete response.deliverFromWardId;
                delete response.deliverFromCountryId;
                delete response.deliverFromId;
                copy[0] = order(response);
            }
            return copy;
        case DELIVERY_ORDER_UPDATE_SUCCESS:
            copy[response.id] = {
                ...copy[response.id],
                ...response
            };
            return copy;
        case DELIVERY_ORDER_RELATES.SUCCESS:
            response.map(t => {
                copy[t.id] = {
                    ...copy[t.id],
                    ...t
                };
            });
            break;
        case TOGGLE_PRINTABLE:
            const {orderId, printable} = action.payload;
            copy[orderId]['printable'] = printable;
            break;
        case TOGGLE_PRINTABLE_ALL:
            Object.keys(copy).map(id => {
                copy[id]['printable'] = action.payload;
            });
            break;
    }
    return copy;
};

const logById = (state = {}, action) => {
    const {response} = action;
    const copy = {...state};
    switch (action.type) {
        case DELIVERY_ORDER_CHAT.SUCCESS:
            response.map(t => {
                copy[t.id] = t;
            });
            return copy;
    }
    return state;
};

const featureById = (state = {}, action) => {
    const {response} = action;
    const copy = {...state};
    switch (action.type) {
        case DELIVERY_ORDERS_SUCCESS:
            response.result.map(t => {
                if (t['features']) {
                    t['features'].map(v => {
                        copy[v.id] = v;
                    });
                }
            });
            break;
        case DELIVERY_ORDER_SUCCESS:
            if (response['features']) {
                response['features'].map(v => {
                    copy[v.id] = v;
                });
            }
            break;
        case CHANGE_INSURANCE_SUCCESS:
            copy[response.id] = response;
            break;
    }
    return copy;
};


const metadata = (state = {}, action) => {
    let copy = {...state};
    switch (action.type) {
        case DELIVERY_ORDERS_SUCCESS:
            return {...state, ...action.response.metadata};
    }
    return copy;
};

const visibleIds = (state = [], action) => {
    let {response} = action;
    switch (action.type) {
        case DELIVERY_ORDERS_SUCCESS:
            return response.result.map(t => t.id);
        case DELIVERY_ORDER_SUCCESS:
        case DELIVERY_ORDER_CREATE_SUCCESS:
            if (!state.hasOwnProperty(response.id)) {
                return [...state, response.id];
            }
            return state;
    }
    return state;
};

const paginateIds = (state = [], action) => {
    let {response} = action;
    switch (action.type) {
        case DELIVERY_ORDERS_SUCCESS:
            return response.result.map(t => t.id);
    }
    return state;
};

const relatedOrderIds = (state = [], action) => {
    const {response} = action;
    if (action.type === DELIVERY_ORDER_RELATES.SUCCESS) {
        return response.map(t => t.id);
    }
    return state;
};

const uploadOrderExcelErrorMsg = (state = false, action) => {
    switch (action.type) {
        case DELIVERY_ORDER_UPLOAD_ORDERS.FAILURE:
            return action.error;
        case DELIVERY_ORDER_UPLOAD_ORDERS.SUCCESS:
            return action.response;
        case LOCATION_CHANGE:
            return false
    }
    return state;
};

export default combineReducers({
    byId, visibleIds, metadata, status, paginateIds, relatedOrderIds, featureById, logById, uploadOrderExcelErrorMsg,
    error: (state = null, {error}) => error ? Localize.t(error) : null,
    create_error: (state = false, action) => action.type == DELIVERY_ORDER_CREATE_FAILURE ? Localize.t(action.error) : null,
    created_success: (state, action) => action.type == DELIVERY_ORDER_CREATE_SUCCESS ? action.response : null,
    changeStatusSuccess: (state, action) => action.type == DELIVERY_ORDER_CHANGE_STATUS.SUCCESS ? action.response : null,
    submittedFailed: (state = false, action) => action.type == "rrf/setSubmitFailed" ? true : state,
    updatedOrder: (state, {type}) => type == DELIVERY_ORDER_UPDATE_SUCCESS,
    uploadedOrderExcel: ( state, {type}) => type == DELIVERY_ORDER_UPLOAD_ORDERS.SUCCESS
})
