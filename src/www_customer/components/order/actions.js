import DeliveryOrder from './api'
import {CALL_API} from '../../ActionTypes'
import * as types from './ActionTypes'
import {generateQuery, queryIncludeOriginOrder} from './helper'

export const createDeliveryOrder = (data) => dispatch => dispatch(DeliveryOrder.create(data));

export const getDeliveryOrders = (filter) => dispatch => dispatch(DeliveryOrder.getDeliveryOrders(generateQuery(filter)));

export const getDeliveryOrder = (id) => dispatch => dispatch(DeliveryOrder.getDeliveryOrder(id, queryIncludeOriginOrder()));

export const fetchDeliveryOrderByIdsSaga = (ids) => {
    const filter = {
        where: {
            id: {
                inq: ids
            }
        }
    };
    return {
        type: CALL_API,
        payload: {
            types: [types.DELIVERY_ORDERS_REQUEST, types.DELIVERY_ORDERS_SUCCESS, types.DELIVERY_ORDERS_FAILURE],
            endpoint: `/DeliveryOrders?filter=${JSON.stringify(filter)}`,
            method: 'GET',
        }
    }
};

export const getOrderStates = () => dispatch => dispatch(DeliveryOrder.getCountByStatus());

export const getLastPickupAddress = () => dispatch => dispatch(DeliveryOrder.getLastPickupAddress());

export const changeStatus = (orderId, status) => dispatch => dispatch(DeliveryOrder.changeStatus(orderId, status));

export const getRelatedOrders = (id) => dispatch => dispatch(DeliveryOrder.getRelatedOrders(id));

export const update = (id, data) => dispatch => dispatch(DeliveryOrder.update(id, data));

export const changeInsurance = (id, data) => dispatch => dispatch(DeliveryOrder.changeInsurance(id, data));

export const uploadOrders = (data) => dispatch => dispatch(DeliveryOrder.uploadOrders(data));

export const getChatHistories = (id) => dispatch => dispatch(DeliveryOrder.getChatHistories(id, {
    include: {relation: 'actionBy'}
}));
