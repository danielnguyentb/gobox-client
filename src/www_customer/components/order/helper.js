/**
 * Created by Tester-Ali on 14-10-2016.
 */
import {encode} from '../../helpers'
import SelectAddress from '../location/components/LocationContainer'
import * as RBT from 'react-bootstrap';
import {required, isNumber, rangeNumber, minLength, maxLength} from '../../helpers/validators'
const Config = require('../../../config.json');

export const generateQuery = (params) => {
    let {barcode = '', type = '', deliverTo = '', privateNote = '', status, limit, skip, where = {}} = params;
    limit = limit ? limit : 20;
    skip = skip ? skip : 0;
    let or_where = [];
    if (deliverTo) {
        let t = encode(deliverTo);
        or_where.push({'deliverToContact': {like: t}});
        or_where.push({'deliverToPhone': {like: t}});
    }
    where.and = [{
        or: or_where,
    }];
    if (barcode) where.and.push({barcode: {like: encode(barcode)}});
    if (privateNote) where.and.push({privateNote: {like: encode(privateNote)}});
    if (type) where.and.push({type: {like: encode(type)}});
    if (status) where.and.push({orderStatus: status});
    const include = {relation: 'features'};
    return {where, limit, skip, order: 'createdTime DESC', include};
};

export const include = [{"relation": "originOrder"}, {relation: 'features'}];

export const queryIncludeOriginOrder = () => ({include});

export const renderSelectAddress = ({
    input, label, type, className, actionChange,
    defaultProvinceId, defaultDistrictId,
    meta: {touched, error, warning}
}) => (
    <RBT.FormGroup validationState={touched && error ? 'error' : ''}>
        {label &&
        <RBT.ControlLabel>{label}</RBT.ControlLabel>
        }

        <SelectAddress provinceErrorComponent={error} districtErrorComponent={error}
                       defaultProvinceId={defaultProvinceId}
                       defaultDistrictId={defaultDistrictId} handleChange={actionChange}/>
        {touched && ((error && <span className="help-block">{error}</span>) || (warning && <span>{warning}</span>))}
    </RBT.FormGroup>
);

export const validate = values => {
    const errors = {};
    if(Object.keys(values).length === 0) return errors;
    if (!values.deliverToContact) {
        errors.deliverToContact = 'Tên người nhận không được để trống.'
    }
    if (!values.deliverToPhone) {
        errors.deliverToPhone = 'Số điện thoại người nhận không được để trống.'
    }

    if (values.deliverToPhone) {
        if (!isNumber(values.deliverToPhone)) {
            errors.deliverToPhone = 'Số điện thoại phải là chữ số.'
        }
        if ((values.deliverToPhone.length < 6) || (values.deliverToPhone.length > 12)) {
            errors.deliverToPhone = 'Không phải là định dạng của số điện thoại.'
        }
    }
    if (!values.deliverToDetail) {
        errors.deliverToDetail = 'Địa chỉ người nhận không được để trống.'
    }

    if (!values.deliverToProvinceId) {
        errors.deliverToProvinceId = 'Tỉnh/TP người nhận không được để trống.'
    }
    if (!values.deliverToDistrictId) {
        errors.deliverToDistrictId = 'Quận/Huyện người nhận không được để trống.'
    }

    if (!values.deliverFromId) {
        errors.deliverFromId = 'Địa chỉ lấy hàng chưa được chọn'
    }
    if (values.cod) {
        if (isNumber(values.cod) == false) {
            errors.cod = 'COD phải là số.'
        } else if(values.cod < 0){
            errors.cod = 'COD không được nhỏ hơn 0'
        }
    }
    if (values.weightKg) {
        if (isNumber(values.weightKg) == false) {
            errors.weightKg = 'Cân nặng phải là số.'
        } else if(values.weightKg < 0){
            errors.weightKg = 'Cân nặng không được nhỏ hơn 0'
        }
    }

    if (values.feature) {
        if (!values.featureFee) {
            errors.featureFee = 'Bạn đã chọn gói bảo hiểm nhưng chưa nhập giá trị hàng'
        } else {
            if (!isNumber(values.featureFee)) {
                errors.featureFee = 'Giá trị hàng phải là chữ số.'
            }
        }

    }
    return errors
};

export const validateAddressForm = values => {
    const errors = {};

    if (!values.contactName) {
        errors.contactName = 'Tên người liên hệ không được để trống.'
    }
    if (!values.contactPhone) {
        errors.contactPhone = 'Số điện thoại lấy hàng không được để trống.'
    }
    if (values.contactPhone) {
        if (!isNumber(values.contactPhone)) {
            errors.contactPhone = 'Số điện thoại phải là chữ số.'
        }
        if ((values.contactPhone.length < 6) || (values.contactPhone.length > 12)) {
            errors.contactPhone = 'Không phải là định dạng của số điện thoại.'
        }
    }
    if (!values.detail) {
        errors.detail = 'Địa chỉ lấy hàng không được để trống.'
    }
    if (!values.provinceId) {
        errors.provinceId = 'Tỉnh/TP lấy hàng không được để trống.'
    }
    if (!values.districtId) {
        errors.districtId = 'Quận/Huyện lấy hàng không được để trống.'
    }
    return errors
};
