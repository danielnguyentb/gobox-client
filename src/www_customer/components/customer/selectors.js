/**
 * Created by Tester-Ali on 07-11-2016.
 */

import {createSelectorCreator, defaultMemoize, createSelector} from 'reselect'
import lodash from 'lodash'
import {NAME} from './constants'

export const customerSelector = state => state[NAME];

const createDeepEqualSelector = createSelectorCreator(
    defaultMemoize,
    lodash.isEqual
);

export const CustomerByIdSelector = createDeepEqualSelector(
    customerSelector,
    customer => lodash.memoize(
        id => customer.byId[id]
    )
);

export const sessionSelector = createDeepEqualSelector(
    customerSelector,
    customer => customer.session
);
export const tokenSelector = createSelector(
    sessionSelector,
    session => session ? session.id : null
);
export const isAuthenticated = createSelector(
    customerSelector,
    customer => customer.isAuthenticated
);

export const getProfileId = createSelector(
    customerSelector,
    customer => {
        if (!customer.isAuthenticated) {
            return null;
        }
        if (!customer.session || !customer.session.userId) {
            return null;
        }
        return customer.session.userId;
    }
);

export const getProfile = createDeepEqualSelector(
    [getProfileId, CustomerByIdSelector],
    (profileId, getCustomer) => getCustomer(profileId)
);