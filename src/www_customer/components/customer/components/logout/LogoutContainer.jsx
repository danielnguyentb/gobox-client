import React, {PropTypes} from 'react'
import {connect} from 'react-redux'
import {logoutSaga} from '../../actions'

class LogoutContainer extends React.Component {
    logout() {
        let {dispatch} = this.props;
        dispatch(logoutSaga());
    }

    render() {
        return (
            <div onClick={::this.logout}>
                <span><i className="fa fa-power-off mr-10"></i>&nbsp;Đăng xuất</span>
            </div>
        )
    }
}

export default connect()(LogoutContainer)