import React from 'react'
import RegisterContainer from './RegisterContainer'
import img_dark from './logo.png';
import Link from '../../../../../www_components/Link';

export default (props) => (
    <div className="container page-container hidden-register">
        <div className="page-content">
            <div className="v2  hidden">
                <div className="logo mb-10 hidden"><img src={img_dark} alt=""/></div>
                <RegisterContainer {...props} />
                 <hr></hr>
                <div className="clearfix">
                    <p className="text-muted mb-0 pull-left">Bạn có tài khoản F1?</p>
                    <Link to="/login" className="inline-block pull-right">Đăng nhập</Link>
                </div>
            </div>
            <div className="info-view">
                <h3> Cảm ơn bạn đã quan tâm tới dịch vụ Shippo.vn <br/>
                    Hiện Shippo.vn đang trong giai đoạn chuẩn bị và sẽ ra mắt trong thời gian sắp tới <br/>
                    Nếu bạn muốn trải nghiệm và góp ý cho dịch vụ,<br/>
                    vui lòng liên hệ qua công cụ "Chat với chúng tôi" ở phía dưới bên phải trang web.<br/>
                    Trân trọng !
                </h3>
            </div>


        </div>
    </div>
)