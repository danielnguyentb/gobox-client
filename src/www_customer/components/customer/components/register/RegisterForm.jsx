import React from 'react';
import * as RBT from 'react-bootstrap';
import {Field, reduxForm} from 'redux-form'
import {renderField, renderPasswordStrengthField, registerValidate} from '../../helper'
import {CUSTOMER_CREATE_FAILURE, CUSTOMER_CREATE_SUCCESS} from '../../ActionTypes'
import {ErrorAlert, SuccessToast} from '../../../../../www_components/Alert'


const Form = ({handleSubmit, error, pristine, submitting}) => (
    <form onSubmit={handleSubmit} method="POST" autoComplete="off" className="frm-register hidden">
        <ErrorAlert dispatchTypes={[CUSTOMER_CREATE_FAILURE]} />
        <SuccessToast dispatchTypes={{[CUSTOMER_CREATE_SUCCESS]: 'Đăng kí thành công'}} />
        <Field name="username" component={renderField} autoFocus={true} className="form-control" label="Tên đăng nhập" type="text"/>
        <Field
            name="email" component={renderField} className="form-control" label="Email" type="email"
            note="(Dịch vụ sẽ gửi các thông báo quan trọng vào email. Quý khách vui lòng sử dụng email thường dùng!)"
        />
        <Field name="password" component={renderPasswordStrengthField} className="form-control" label="Mật khẩu"
               type="password"/>
        <Field
            name="mobile" component={renderField} className="form-control" label="Số điện thoại"
            type="tel"
            note="(Số điện thoại để chúng tôi liên hệ với Quý khách)"
        />

        <RBT.FormGroup>
            <p>Với việc click nút đăng ký phía dưới, tôi đồng ý với các điều khoản của dịch vụ</p>
        </RBT.FormGroup>
        {error && <div className="alert alert-danger">{error}</div>}
        <button
            type="submit"
            disabled={pristine || submitting}
            className="btn-lg btn btn-primary btn-rounded btn-block">Đăng ký
        </button>
    </form>

);

export default reduxForm({
    form: 'registerForm',
    validate: registerValidate
})(Form)