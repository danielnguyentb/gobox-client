import React, {Component} from 'react';
import {connect} from 'react-redux'
import {push} from 'react-router-redux'
import RegisterForm from './RegisterForm'
import {bindActionCreators} from 'redux'
import {createCustomer, login} from '../../actions'
import {CUSTOMER_CREATE_SUCCESS} from '../../ActionTypes'

class RegisterContainer extends React.Component {
    constructor(props) {
        super(props);
        this.username = '';
        this.password = '';
    }

    registerCustomer(customer) {
        let {username, password, mobile, email} = customer;
        this.props.createCustomer({username, password, mobile, email});
        this.username = username;
        this.password = password;
    }

    componentWillReceiveProps({createdCustomer, login, isAuthenticated, onRedirect}) {
        if (createdCustomer && createdCustomer !== this.props.createdCustomer) {
            let {username, password} = this;
            login({username, password});
        } else if (isAuthenticated) {
            onRedirect('/')
        }
    }

    render() {
        return <RegisterForm onSubmit={::this.registerCustomer}/>;
    }
}

const mapStateToProps = (state) => {
    return {
        createdCustomer: state.ajaxType === CUSTOMER_CREATE_SUCCESS,
        isAuthenticated: state.customer.isAuthenticated,
    }
};

const mapDispatchToProps = (dispatch) => ({
    login: bindActionCreators(login, dispatch),
    createCustomer: bindActionCreators(createCustomer, dispatch),
    onRedirect: (path) => {
        dispatch(push(path))
    },
    dispatch
});

export default connect(
    mapStateToProps, mapDispatchToProps
)(RegisterContainer);