import React from 'react';
import Link from '../../../../../www_components/Link';
import './login.scss'
import ReCAPTCHA from 'react-google-recaptcha'
import img_dark from './img/logo.png'
import Config from '../../../../../config.json'

import {FormGroup} from 'react-bootstrap'
import {Field, reduxForm} from 'redux-form'
import {renderField, loginValidate} from '../../helper'

export class UserLoginForm extends React.Component {
    render() {
        const {requireCaptcha, onChangeCaptcha, handleSubmit, error, submitting, pristine} = this.props;
        return (
            <div className="container page-container">
                <div className="page-content">
                    <div className="v2">
                        <div className="logo"><img src={img_dark} alt="image"/></div>

                        <form method="POST" onSubmit={handleSubmit}>
                            <Field component={renderField} type="text" name="email" className="form-control"
                                   placeholder="Tên đăng nhập"/>
                            <Field component={renderField} type="password" name="password" className="form-control"
                                   placeholder="Mật khẩu"/>
                            {requireCaptcha &&
                            <FormGroup>
                                <div className="row">
                                    <div className="col-xs-12 ">
                                        <ReCAPTCHA
                                            ref="recaptcha"
                                            sitekey={Config.reCaptchaKey}
                                            onChange={onChangeCaptcha}
                                        />
                                    </div>
                                </div>
                            </FormGroup>
                            }
                            <FormGroup>
                                <div className="row">
                                    <div className="col-xs-12 text-left">
                                        <Field
                                            id="remember_me"
                                            className="checkbox checkbox-inline pull-left pt-5"
                                            name="ttl" component="input" type="checkbox"/>
                                        <label htmlFor="remember_me" className="pl-5">Duy trì đăng nhập</label>

                                        <div className="pull-right hidden">
                                            <a href="#"
                                               className="inline-block form-control-static">Quên
                                                mật khẩu?</a></div>
                                    </div>
                                </div>
                            </FormGroup>
                            {error && <div className="alert alert-danger">{error}</div>}
                            <button
                                disabled={pristine || submitting}
                                type="submit"
                                className="btn-lg btn btn-primary btn-rounded btn-block"
                            >Đăng nhập</button>
                        </form>

                        <hr />
                        <div className="clearfix">
                            <p className="text-muted mb-0 pull-left">Bạn chưa có tài khoản?</p>
                            <Link to="/register" className="inline-block pull-right">Đăng ký</Link>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export const UserLoginReduxForm = reduxForm({
    form: 'UserLoginForm',
    validate: loginValidate
})(UserLoginForm);