import React, {Component} from 'react';
import {connect} from 'react-redux'
import {UserLoginReduxForm} from './UserLoginForm'
import {login, fetchApplicationValidation} from '../../actions'
import {replace} from 'react-router-redux'
import {TTL_VALUE} from '../../constants'
import {customerSelector} from '../../selectors'

class LoginContainer extends Component {
    captcha: '';

    componentWillMount() {
        let {isAuthenticated, dispatch} = this.props;
        if (isAuthenticated) {
            dispatch(replace('/'));
        } else {
            dispatch(fetchApplicationValidation());
        }
    }

    handleSubmit(credentials) {
        const {email, password, ttl} = credentials;
        const {dispatch, validation} = this.props;
        const {captcha} = this;
        dispatch(login({
            ...validation,
            ttl: ttl ? TTL_VALUE : 0,
            captcha, email, password
        }));
    }

    handleChangeCaptcha(value) {
        this.captcha = value;
    }

    render() {
        const {requireCaptcha} = this.props;
        return (
            <UserLoginReduxForm
                onChangeCaptcha={::this.handleChangeCaptcha}
                requireCaptcha={requireCaptcha}
                initialValues={{ttl: true}}
                onSubmit={::this.handleSubmit}/>
        );
    }
}

const mapStateToProps = (state) => {
    const customer = customerSelector(state);
    return {
        requireCaptcha: customer.validation && customer.validation.captcha,
        validation: customer.validation,
        loginFailed: customer.loginFailed,
        isAuthenticated: customer.isAuthenticated,
        session: customer.session,
        login_error: customer.login_error,
        needChangePass: customer.session.needChangePass,
    }
};

export default connect(
    mapStateToProps,
)(LoginContainer);