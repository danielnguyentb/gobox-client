import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import {getCustomerById, changePassword} from '../../actions'
import DocumentTitle from '../../../../../www_components/DocumentTitle'
import ChangePassForm from './ChangePassForm';
import {replace} from 'react-router-redux'
import {bindActionCreators} from 'redux'

class CustomerChangePassContainer extends Component {

    componentWillReceiveProps(nextProps) {

    }
    handleChangePassword(old_password, new_password) {
        this.props.changePassword(old_password, new_password);
    }
    render() {
        return (
            <DocumentTitle title="Yêu cầu thay đổi mật khẩu">
                <ChangePassForm {...this.props}
                      changePassword={this.handleChangePassword.bind(this)}
                />
            </DocumentTitle>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        changedPassword: state.customer.changedPassword,
        error: state.customer.error,
        message: state.customer.changedPasswordMessage
    }
};

const mapDispatchToProps = (dispatch) => ({
    ...bindActionCreators({
        changePassword
    }, dispatch),
    onRedirect: (path) => {
        dispatch(replace(path))
    },
    dispatch
});

export default connect(
    mapStateToProps, mapDispatchToProps
)(CustomerChangePassContainer);