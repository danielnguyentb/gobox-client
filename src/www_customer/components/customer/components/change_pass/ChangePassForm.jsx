import React from 'react';
import Validation from 'react-validation';
import * as RBT from 'react-bootstrap';
import ObjectForm from '../../../../helpers/ValidationForm'
import PasswordStrength from '../../../../../www_components/Forms/PasswordStrength'
import ChangeUserPassword from '../../../../../www_components/ChangeUserPassword/index'

Validation.rules = ObjectForm;

export default class ChangePassForm extends React.Component {
    constructor(props) {
        super(props);
    }

    componentWillMount() {
        this.setState({
            showModal: false,
            showMessageChangePassSuccess: false,
            currentPassword: '',
            newPassword: '',
            isValid: false
        });
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.error) {
            this.showModal(nextProps.error);
        }
        if (this.props.updatingUser && !nextProps.updatingUser && !nextProps.error) {
            if (nextProps.message) {
                this.showModal(nextProps.message);
            }
        }

        if (nextProps.changedPassword) {
            let self = this;
            this.setState({showMessageChangePassSuccess: true});
            setTimeout(function () {
                self.setState({showMessageChangePassSuccess: false});
                nextProps.onRedirect('/login');
            }, 3000);
        }

    }

    showModal(msg) {
        this.setState({messagePopup: msg});
        this.setState({showModal: true});
    }

    closeModal() {
        this.setState({showModal: false});
    }

    render() {
        let {changePassword} = this.props;
        let disabled = this.state.isValid ? '' : "false";

        return (
            <div>
                <div className="row">
                    <div className="col-md-12 text-center">
                        <h4 className="fw-600 mt-30 pt-30">Hệ thống yêu cầu bạn thay đổi mật khẩu để sử dụng trong những
                            lần đăng nhập tiếp theo.</h4>
                        <h4 className="fw-600 mb-15">Vui lòng nhập thông tin cần thiết để đổi mật khẩu!</h4>
                    </div>
                </div>
                <div className="col-md-4 col-md-offset-4 col-xs-12">
                    <section className="content">
                        <Validation.components.Form>
                            <ChangeUserPassword
                                onChange={data => this.setState(data)}
                                requireCurrentPassword={true}
                                validate={true}
                            />

                            {this.state.showMessageChangePassSuccess &&
                            <p className="fw-600 mb-15 text-success">Đổi mật khẩu thành công. Sau 3s, hệ thống sẽ tự
                                động chuyển về trang đăng nhập!</p>}

                            <RBT.FormGroup controlId='' className="text-right">
                                <Validation.components.Button
                                    disabled={disabled}
                                    className="btn btn-primary"
                                    onClick={(e) => {
                                        e.preventDefault();
                                        changePassword(this.state.currentPassword, this.state.newPassword)
                                    }}
                                >
                                    Thay đổi mật khẩu
                                </Validation.components.Button>
                            </RBT.FormGroup>
                        </Validation.components.Form>
                        {/* Popup thông báo */}
                    </section>
                    <RBT.Modal show={this.state.showModal} onHide={this.closeModal.bind(this)}>
                        <RBT.Modal.Header closeButton>
                            <RBT.Modal.Title>Thông báo</RBT.Modal.Title>
                        </RBT.Modal.Header>
                        <RBT.Modal.Body>
                            {this.state.messagePopup}
                        </RBT.Modal.Body>
                        <RBT.Modal.Footer>
                            <RBT.Button onClick={this.closeModal.bind(this)}>ĐÓNG</RBT.Button>
                        </RBT.Modal.Footer>
                    </RBT.Modal>
                </div>
            </div>

        );
    }
}