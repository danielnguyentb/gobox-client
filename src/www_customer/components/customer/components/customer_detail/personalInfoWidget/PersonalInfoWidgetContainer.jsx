import React from 'react'
import {connect} from 'react-redux'
import PersonalInfoWidget from './PersonalInfoWidget'

class PersonalInfoWidgetContainer extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <PersonalInfoWidget
                {...this.props}
                initialValues={this.props.profile}
            />
        )
    }
}
const mapStateToProps = (state) => {
    return {
    }
};


export default connect(
    mapStateToProps,
)(PersonalInfoWidgetContainer);