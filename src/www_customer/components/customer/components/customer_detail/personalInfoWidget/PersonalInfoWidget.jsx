import React from 'react'
import {renderField, renderFieldSelect, renderSelectAddress} from '../../../../../helpers/renderField'
import {Field, reduxForm} from 'redux-form'
import {validate} from '../../../helper'

class PersonalInfoWidget extends React.Component {
    constructor(props) {
        super(props);
    }

    handleSetBankName(e) {
        if (e.value) {
            this.props.setFieldForm("bankName", e.value);
        }
    }

    render() {
        let {handleSubmit, customer, banks, submitting, pristine, error} = this.props;
        if (!customer || !customer.id) return null;
        return (
            <form onSubmit={handleSubmit}>
                <table className="table tbl-info-detail tbl-customer-detail">
                    <thead>
                    <tr>
                        <th colSpan="4">THÔNG TIN CÁ NHÂN</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td className="fw-600 mb-15">Họ tên</td>
                        <td>
                            <Field component={renderField} name="lastName" className="form-control" placeholder="Họ"/>
                        </td>
                        <td colSpan="2">
                            <Field component={renderField} name="firstName" className="form-control" placeholder="Tên"/>
                        </td>
                    </tr>
                    <tr>
                        <td className="fw-600">Ngày sinh</td>
                        <td>
                            <Field component={renderField} name="date_birthday" className="form-control"
                                   placeholder="Ngày"/>
                        </td>
                        <td>
                            <Field component={renderFieldSelect} name="month_birthday" className="form-control"
                                   options={[
                                       {value: "", label: "Tháng"},
                                       {value: 1, label: "Tháng 1"},
                                       {value: 2, label: "Tháng 2"},
                                       {value: 3, label: "Tháng 3"},
                                       {value: 4, label: "Tháng 4"},
                                       {value: 5, label: "Tháng 5"},
                                       {value: 6, label: "Tháng 6"},
                                       {value: 7, label: "Tháng 7"},
                                       {value: 8, label: "Tháng 8"},
                                       {value: 9, label: "Tháng 9"},
                                       {value: 10, label: "Tháng 10"},
                                       {value: 11, label: "Tháng 11"},
                                       {value: 12, label: "Tháng 12"},
                                   ]}
                            />
                        </td>
                        <td>
                            <Field component={renderField} name="year_birthday" className="form-control"
                                   placeholder="Năm"/>
                        </td>
                    </tr>
                    <tr>
                        <td className="fw-600">Giới tính</td>
                        <td>
                            <Field component="select" name="gender" className="form-control">
                                <option value="1">Nam</option>
                                <option value="0">Nữ</option>
                                <option value="">Khác</option>
                            </Field>
                        </td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td className="fw-600">Số ĐT</td>
                        <td colSpan="3">
                            <Field component={renderField} name="mobile" className="form-control"/>
                        </td>
                    </tr>
                    <tr className="fw-600">
                        <td colSpan="100%" className="pt-30">
                            THÔNG TIN TÀI KHOẢN NGÂN HÀNG
                            <p className="mb-0 fw-400">(Thông tin ngân hàng dùng để dịch vụ chuyển tiền thu hộ cho Quý
                                khách)</p>
                        </td>
                    </tr>
                    <tr>
                        <td className="fw-600">Ngân hàng</td>
                        <td colSpan="3">
                            <Field component={renderSelectAddress} name="bankName"
                                   options={banks}
                                   onChange={::this.handleSetBankName}
                                   placeholder="Chọn ngân hàng"/>
                        </td>
                    </tr>
                    <tr>
                        <td className="fw-600">Chi nhánh</td>
                        <td colSpan="3">
                            <Field component={renderField} name="bankBranch" className="form-control"
                                   placeholder="Nhập chi nhánh ngân hàng"/>
                        </td>
                    </tr>
                    <tr>
                        <td className="fw-600">Chủ TK</td>
                        <td colSpan="3">

                            <Field component={renderField} name="bankAccountName" className="form-control"
                                   placeholder="Nhập tên chủ tài khoản"/>
                        </td>
                    </tr>
                    <tr>
                        <td className="fw-600">Số TK</td>
                        <td colSpan="3">

                            <Field component={renderField} name="bankAccountNo" className="form-control"
                                   placeholder="Nhập số tài khoản"/>
                        </td>
                    </tr>
                    {error &&
                    <tr>
                        <td className="fw-600"> </td>
                        <td colSpan="3">
                            <div className="alert alert-danger">
                                {error}
                            </div>
                        </td>
                    </tr>
                    }
                    <tr>
                        <td colSpan="4" className="text-right">
                            <button
                                disabled={pristine || submitting}
                                className="btn btn-primary text-right" type="submit">
                                THAY ĐỔI
                            </button>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        )
    }
}

export default reduxForm({
    form: 'CustomerDetailForm',
    validate
})(PersonalInfoWidget)