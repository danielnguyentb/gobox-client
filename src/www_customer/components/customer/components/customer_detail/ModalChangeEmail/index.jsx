import React from 'react'
import {connect} from 'react-redux'
import ModalChangeEmail from './ModalChangeEmail'
import * as RBT from 'react-bootstrap'
import {changeEmailCustomer} from '../../../actions'
import {reset} from 'redux-form'

class ModalChangeEmailContainer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showModal: false
        }
    }
    componentWillReceiveProps(nextProps) {
        if (nextProps.updatedCustomer) {
            this.closeModal();
        }
    }
    handleSubmitEmail(data) {
        let {customer, changeEmailCustomer} = this.props;
        if(customer) {
            data['id'] = customer.id;
            changeEmailCustomer(data);
        }
    }
    closeModal() {
        this.setState({showModal: false});
        this.resetForm();
    }
    resetForm() {
        this.props.reset("ModalChangeEmail");
    }
    render() {
        let {customer} = this.props;
        if (!customer) return null;
        return (
            <tr>
                <td className="fw-600">Email</td>
                <td>{customer.email}</td>
                <td>
                    <RBT.Button bsStyle="link" onClick={(e) => this.setState({showModal: true})}>Thay đổi</RBT.Button>

                    <ModalChangeEmail
                        onSubmit={::this.handleSubmitEmail}
                        showModal={this.state.showModal}
                        closeModal={(e) => this.closeModal(e)}
                    />
                </td>
            </tr>

        )
    }

}
const mapStateToProps = (state, props) => ({});

export default connect(
    mapStateToProps, {changeEmailCustomer, reset}
)(ModalChangeEmailContainer);