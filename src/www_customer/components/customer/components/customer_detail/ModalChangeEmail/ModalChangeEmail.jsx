import React from 'react'
import {Field, reduxForm} from 'redux-form'
import {renderField} from '../../../../../helpers/renderField'
import * as RBT from 'react-bootstrap'

const ModalChangeEmail = ({handleSubmit, error, pristine, submitting, showModal, closeModal}) => (
    <form>
        <RBT.Modal show={showModal} onHide={closeModal}>
            <RBT.Modal.Header closeButton>
                <RBT.Modal.Title>Thay đổi email</RBT.Modal.Title>
            </RBT.Modal.Header>
            <RBT.Modal.Body>
                <RBT.FormGroup controlId=''>
                    <RBT.ControlLabel>Email mới</RBT.ControlLabel>
                    <div className="input-required">
                        <Field component={renderField} name="email" className="form-control"/>
                        <span className="required-symbol">*</span>
                    </div>
                </RBT.FormGroup>
                {error &&
                    <div className="alert alert-danger">
                        {error}
                    </div>
                }
            </RBT.Modal.Body>
            <RBT.Modal.Footer>
                <RBT.Button onClick={closeModal}>ĐÓNG</RBT.Button>
                <button disabled={pristine || submitting} className="btn btn-primary text-right" type="submit" onClick={handleSubmit}>
                    LƯU
                </button>
            </RBT.Modal.Footer>
        </RBT.Modal>
    </form>
);
const validate = values => {
    const errors = {};
    if (!values.email) {
        errors.email = 'Email không được để trống'
    }
    if(values.email) {
        if (!(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(values.email))) {
            errors.email = values.email + " không phải là định dạng email"
        }
    }
    return errors;
};

export default reduxForm({
    form: 'ModalChangeEmail',
    validate
})(ModalChangeEmail);


