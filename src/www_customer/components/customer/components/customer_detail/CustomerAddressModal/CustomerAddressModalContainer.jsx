import React from 'react';
import {connect} from 'react-redux';
import {push} from 'react-router-redux'
import {bindActionCreators} from 'redux'
import AddressModal from './AddressModal'
import CreateAddressModal from './CreateAddressModal'

import {getListCustomerAddress, getById} from '../../../../customer/reducer'
import {getProvinces} from '../../../../location/actions'

class AddressModalContainer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            initialValues: {}
        }
    }
    componentWillMount() {
        this.props.getProvinces();
    }
    componentDidMount() {
        if(this.props.addressSelected) {
            this.handleInitialValues(this.props.addressSelected);
        }
    }
    componentWillUpdate(nextProps) {
        if(JSON.stringify(nextProps.addressSelected) != JSON.stringify(this.props.addressSelected)) {
            this.handleInitialValues(nextProps.addressSelected);
        }
    }
    handleInitialValues(dataObj) {
        Object.keys(dataObj).map(key => {
            this.props.setFieldFormCustomerAddress( key, dataObj[key])
        });
        this.setState({initialValues: dataObj});
    }
    render() {
        return (
            <section>
                <AddressModal
                    {...this.props}
                    initialValues={this.state.initialValues}
                    onSubmit={this.props.onSubmitUpdateCustomerAddress}
                />

                <CreateAddressModal
                    {...this.props}
                    onSubmit={this.props.onSubmitCustomerAddress}
                />
            </section>
        )
    }
}


const mapStateToProps = (state, props) => {
    return {
        addressSelected: getById(state, props.addressSelectedId)
    }
};


export default connect(
    mapStateToProps, {getProvinces}
)(AddressModalContainer);