import React from 'react'
import * as RBT from 'react-bootstrap'
import  {Button} from 'react-bootstrap'
import {toastr} from "../../../../../../www_components/Toastr2"
import ReactTooltip from 'react-tooltip'

export default ({id, contactName, contactPhone, fullAddress, openModal, handleDeleteAddress, openModalEdit}) => {
    return (
        <tr className="bd-b">
            <td>{id}</td>
            <td className="fw-600">{contactName}</td>
            <td>{contactPhone}</td>
            <td>{fullAddress}</td>
            <td width={100} className="text-center">
                <RBT.Button bsStyle="link" onClick={e => openModalEdit(id)} data-tip="Chỉnh sửa">
                    <i className="ti-pencil text-black"> </i>
                </RBT.Button>
                <ReactTooltip />

                <Button type="button" className="btn btn-xs btn-outline btn-danger"
                        data-tip="Xóa"
                        onClick={e => {
                            toastr.confirm("Bạn có muốn xóa địa chỉ lấy hàng "+ contactName, {
                                onOk: () => handleDeleteAddress(id)
                            })
                        }}
                >
                    <i className="ti-close"> </i>
                </Button>
                <ReactTooltip />
            </td>
        </tr>
    )
}