import React from 'react'
import {connect} from 'react-redux'
import {getListAddress, createAddress, deleteAddress} from '../../../actions'
import {getListCustomerAddress} from '../../../reducer'

import CustomerListAddress from './CustomerListAddress'
import CustomerAddressModalContainer from '../CustomerAddressModal/CustomerAddressModalContainer'
import {toastr} from '../../../../../../www_components/Toastr2'

class CustomerAddressesContainer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showModal: false,
            showCreateModal: false,
        }
    }
    componentWillReceiveProps(nextProps) {
        if(nextProps.createdAddressSuccess || nextProps.updatedAddressSuccess) {
            this.closeModal();
            this.closeCreateModal();
        }
    }
    componentWillMount() {
        let {profile} = this.props;
        if(profile) {
            this.props.getListAddress(profile.id);
        }
    }

    closeModal() {
        this.setState({showModal: false});
    }
    closeCreateModal() {
        this.setState({showCreateModal: false});
    }
    openModal() {
        this.setState({showModal: true});
    }

    openCreateModal() {
        this.setState({showCreateModal: true});
    }

    handleDeleteAddress(objId) {
        let {profile} = this.props;
        if(profile) {
            let data = {id: profile.id, objectId: objId};
            this.props.deleteAddress(data);
        }
    }
    openModalNew() {
        this.openCreateModal();
        this.props.setOpenStateModal(false);
    }
    openModalEdit(id) {
        this.openModal();
        if(id) {
            this.props.setSelectedAddressId(id);
            this.props.setOpenStateModal(true);
        }
    }

    render() {
        return (
            <section>
                <CustomerListAddress
                    {...this.props}
                    closeModal={::this.closeModal}
                    openModal={::this.openModal}
                    handleDeleteAddress={::this.handleDeleteAddress}
                    openModalEdit={::this.openModalEdit}
                    openModalNew={::this.openModalNew}
                />

                <CustomerAddressModalContainer
                    {...this.props}
                    closeModal={::this.closeModal}
                    openModal={::this.openModal}
                    closeCreateModal={::this.closeCreateModal}
                    openCreateModal={::this.openCreateModal}
                    stateModal={this.state.showModal}
                    stateCreateModal={this.state.showCreateModal}
                />
            </section>
        )
    }
}
const mapStateToProps = (state, props) => {
    return {
        listAddress: getListCustomerAddress(state),
    }
};

export default connect(
    mapStateToProps, {getListAddress, createAddress, deleteAddress}
)(CustomerAddressesContainer);