import React from 'react'
import * as RBT from 'react-bootstrap'
import {renderField, renderSelectAddress} from '../../../../../helpers/renderField'
import ReactTooltip from 'react-tooltip'
import  {Button} from 'react-bootstrap'
import {toastr} from "../../../../../../www_components/Toastr2"
import CustomerListAddressItem from './CustomerListAddressItem'

export default (props) => (
    <table className="table table-responsive tbl-info-detail">
        <thead>
        <tr>
            <th colSpan="3">ĐỊA CHỈ LẤY HÀNG</th>
            <th colSpan="2" className="text-right">
                <Button type="button" className=" btn btn-default" onClick={props.openModalNew}>
                    <i className="fa fa-plus mr-5"> </i>
                    TẠO MỚI
                </Button>
            </th>
        </tr>
        </thead>
        <tbody>
        <tr className="bd-b">
            <th width={20}>ID</th>
            <th width={100}>Người liên hệ</th>
            <th>SĐT</th>
            <th width={200}>Địa chỉ</th>
            <th className="text-center" width={100}>Thao tác</th>
        </tr>
        {props.listAddress && props.listAddress.map(obj =>
            <CustomerListAddressItem key={obj.id} {...obj} {...props}/>
        )}
        </tbody>
    </table>
);