import React from 'react';
import Validation from 'react-validation';
import Link from './../../../../../www_components/Link'
import img_avatar from './avatar.jpg';
import ObjectForm from '../../../../helpers/ValidationForm';
import {connect} from 'react-redux'
import {toastr} from '../../../../../www_components/Toastr2'
import CustomerAddressesContainer from './CustomerAddresses/CustomerAddressesContainer'
import PersonalInfoWidgeContainer from './personalInfoWidget/PersonalInfoWidgetContainer'
import ModalChangeEmailContainer from './ModalChangeEmail'
import {reset} from 'redux-form'
import ChangePassModal from './ChangePassModal'
import moment from 'moment';
moment.locale('vi');

import Scroll from 'react-scroll'

Validation.rules = ObjectForm;

class CustomerDetail extends React.Component {
    constructor(props) {
        super(props);
    }

    componentWillMount() {
        this.setState({
            avatar: '',
            showModalPW: false,
            disabled: false,
            isChecked: false,
            currentPassword: '',
            newPassword: '',
            isChangedPass: false,
            errorMsg: null,
            isValid : false,
        });
    }

    componentWillReceiveProps(nextProps) {
        if(nextProps.createdAddressSuccess || nextProps.updatedAddressSuccess || nextProps.deleteAddressSuccess) {
            if(nextProps.createdAddressSuccess) {
                this.props.reset("CustomerAddressCreate");
                toastr.success("Tạo địa chỉ lấy hàng thành công");
            } else if(nextProps.updatedAddressSuccess){
                toastr.success("Cập nhật địa chỉ lấy hàng thành công");
            } else if(nextProps.deleteAddressSuccess) {
                toastr.success("Xóa địa chỉ lấy hàng thành công");
            }
        }

        if (nextProps.updatedCustomer) {
            toastr.success("Lưu thông tin thành công");
        }

        this.handleUnDisabled();
    }

    handleUnDisabled() {
        this.setState({disabled: false});
    }

    render() {
        let {customer, changePassword, profile} = this.props;
        let disabled = this.state.isValid ? '' : "disabled";
        let Link       = Scroll.Link;
        let Element    = Scroll.Element;

        return (
            <div>
                <section className="content-header">
                    <h1>Chi tiết khách hàng</h1>
                </section>
                <section className="content">
                    <div className="box">
                        <div className="row">
                            <div className="col-md-offset-1 col-md-3 col-xs-12 text-center">
                                <div className="row">
                                    <div className="col-md-12 block-avatar hidden">
                                        <img src={customer ? customer.avatar : img_avatar}/>
                                    </div>
                                    <div className="col-md-12 user-infomation">
                                        <h4>
                                            { customer ? customer.fullName : ' -- '}
                                        </h4>
                                        <p>{customer ? customer.username : ''}</p>
                                    </div>
                                </div>

                                <div className="row menu-detail-info">
                                    <div className="col-md-12">
                                        <hr/>
                                        <ul>
                                            <li><Link to="PickupAddress" spy={true} smooth={true} offset={50} duration={500} >Danh sách địa chỉ lấy hàng</Link></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-6 col-xs-12">
                                <table className="table tbl-info-detail">
                                    <thead>
                                    <tr>
                                        <th colSpan="3">BẢO MẬT</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td className="fw-600">Mật khẩu</td>
                                        <td>******* {customer && customer.lastChangedPass ? "(từ "+ moment(customer.lastChangedPass).format('D-M-Y H:mm:s') + ")" : '(Chưa đổi mật khẩu bao giờ)'}</td>
                                        {}
                                        <td><ChangePassModal/></td>
                                    </tr>

                                    <ModalChangeEmailContainer {...this.props} />

                                    </tbody>
                                </table>

                                <Element name="PersonalInfo" className="element">
                                    <PersonalInfoWidgeContainer {...this.props} />
                                </Element>

                                <Element name="PickupAddress" className="element">
                                    {profile &&
                                        <CustomerAddressesContainer {...this.props} />
                                    }
                                </Element>

                            </div>
                        </div>
                    </div>
                </section>
            </div>
        );
    }
}
const mapStateToProps = (state) => {
    return {}
};

export default connect(
    mapStateToProps, {reset}
)(CustomerDetail);