import React from 'react'
import {connectModal} from 'redux-modal'
import {Modal} from 'react-bootstrap'
import Form from './Form'

export const ChangePassModal = ({show, handleHide, ...rest}) => {
    return (
        <Modal show={show} onHide={handleHide}>
            <Form handleHide={handleHide} {...rest}/>
        </Modal>
    )
};

export default connectModal({name: 'ChangePassModal'})(ChangePassModal)