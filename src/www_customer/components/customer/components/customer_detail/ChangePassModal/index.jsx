import React, {PropTypes} from 'react'
import {connect} from 'react-redux'
import {Button} from 'react-bootstrap'
import {show} from 'redux-modal'
import Modal from './Modal'
import {CUSTOMER_CHANGE_PASSWORD_REQUEST} from '../../../ActionTypes'

export class ChangePassContainer extends React.Component {
    handleClick(){
        const {dispatch} = this.props;
        dispatch(show('ChangePassModal', {
            onSubmit(data){
                dispatch({
                    type: CUSTOMER_CHANGE_PASSWORD_REQUEST,
                    payload: data
                })
            }
        }))
    }

    render() {
        return (
            <div>
                <Button bsStyle="link" onClick={::this.handleClick}>Thay đổi</Button>
                <Modal/>
            </div>
        )
    }
}

export default connect()(ChangePassContainer)