import React from 'react'
import {reduxForm, Field} from 'redux-form'
import * as RBT from 'react-bootstrap'
import {scoreFn} from '../../../helper'
import Localize from '../../../../../../resources/Localize'

export const renderPasswordField = ({input, meta : {error, touched}, label, autoFocus}) => {
    return (
        <RBT.FormGroup validationState={touched && error ? 'error' : 'success'}>
            <label className="control-label">{label}</label>
            <input {...input} autoFocus={autoFocus} type="password" className="form-control"/>
            {error && touched ? <span className="help-block">{error}</span> : null}
        </RBT.FormGroup>
    )
};

export const Form = ({handleHide, error, handleSubmit, submitting, pristine, invalid}) => {
    return (
        <form onSubmit={handleSubmit}>
            <RBT.Modal.Header closeButton>
                <RBT.Modal.Title>Thay đổi mật khẩu</RBT.Modal.Title>
            </RBT.Modal.Header>
            <RBT.Modal.Body>
                {error && <div className="alert alert-danger">{error}</div>}
                <Field component={renderPasswordField} autoFocus={true} name="oldPassword" label="Mật khẩu hiện tại"/>
                <Field component={renderPasswordField} autoFocus={false} name="newPassword" label="Mật khẩu mới"/>
                <Field component={renderPasswordField} autoFocus={false} name="passwordConfirm"
                       label="Xác nhận mật khẩu mới"/>
            </RBT.Modal.Body>

            <RBT.Modal.Footer>
                <RBT.Button type="button" onClick={handleHide}>ĐÓNG</RBT.Button>
                <RBT.Button
                    disabled={submitting || pristine || invalid}
                    bsStyle="primary"
                    type="submit"
                >LƯU</RBT.Button>
            </RBT.Modal.Footer>
        </form>
    )
};

export const validate = values => {
    const errors = {};
    if(Object.keys(values).length === 0) return errors;
    if (!values.oldPassword) {
        errors.oldPassword = 'Mật khẩu hiện tại không được để trống'
    }
    if (!values.newPassword) {
        errors.newPassword = 'Mật khẩu mới không được để trống'
    } else if (values.newPassword == values.oldPassword) {
        errors.newPassword = 'Mật khẩu mới không được trùng với mật khẩu hiện tại'
    } else if (scoreFn(values.newPassword).score <= 2) {
        errors.newPassword = Localize.t(scoreFn(values.newPassword).feedback.warning);
    }
    if (!values.passwordConfirm) {
        errors.passwordConfirm = 'Xác nhận mật khẩu mới không được để trống'
    } else if (values.newPassword != values.passwordConfirm) {
        errors.passwordConfirm = 'Mật khẩu xác nhận không khớp'
    }
    return errors;
};

export default reduxForm({
    form: 'ChangePassForm',
    validate
})(Form);