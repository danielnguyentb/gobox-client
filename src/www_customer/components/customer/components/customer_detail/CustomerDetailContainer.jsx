import  React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux'
import CustomerDetail from './CustomerDetail'
import {updateCustomer, getCustomerById, changePassword, createAddress, updateAddress} from '../../actions'
import DocumentTitle from '../../../../../www_components/DocumentTitle'
import {getProfile, getProfileId} from '../../selectors'
import Banks from '../../../../../resources/Banks'
import {Field, reduxForm, change} from 'redux-form'
import moment from 'moment'
moment.locale('vi');

import './../customer_detail.scss'

class CustomerDetailContainer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            addressSelectedId: null,
            openStateModal: false
        }
    }
    componentWillMount() {
        this.setState({banks : []});
        let {profileId, profile, getCustomerById} = this.props;
        if (profileId > 0 && (!profile || !profile.id)) {
            getCustomerById(profileId);
        }

        let banks_arr = [];
        Banks.map(bank => {
            banks_arr.push({value: bank.code + " - " + bank.name, label: bank.name + " ("+ bank.code +")"});
        });
        this.setState({banks: banks_arr});

    }

    submit(customer) {
        let obj = Object.assign({}, customer);
        if (obj.birthday) {
            let birthday = new Date(obj.birthday);
            birthday.setDate(customer['date_birthday']);
            birthday.setMonth(customer['month_birthday'] - 1);
            birthday.setFullYear(customer['year_birthday']);
            obj['birthday'] = moment(birthday.toISOString()).format("Y-M-D");
        } else {
            if(customer['date_birthday'] || customer['year_birthday'] ) {
                let birthday = new Date();
                birthday.setDate(customer['date_birthday']);
                birthday.setMonth(customer['month_birthday'] - 1);
                birthday.setFullYear(customer['year_birthday']);
                obj['birthday'] = moment(birthday.toISOString()).format("Y-M-D");
            } else {
                obj['birthday'] = null;
            }
        }
        delete obj['date_birthday'];
        delete obj['month_birthday'];
        delete obj['year_birthday'];
        this.props.updateCustomer(obj);
    }
    submitCustomerAddress(data) {
        let {profile} = this.props;
        if(profile) {
            data["objectType"] =  'Customers';
            data["objectId"] = profile.id;
            // If selected address then update that address
            this.props.createAddress(data);

        }
    }
    submitUpdateCustomerAddress(data) {
        let {profile} = this.props;
        if(profile) {
            data["objectType"] =  'Customers';
            data["objectId"] = profile.id;
            // If selected address then update that address
            this.props.updateAddress(data);
        }
    }
    setFieldForm(field, val) {
        this.props.change("CustomerDetailForm", field, val);
    }
    setFieldFormCustomerAddress(field, val) {
        this.props.change("CustomerAddressFormInDetail", field, val);
    }
    setFieldFormCreateCustomerAddress(field, val) {
        this.props.change("CustomerAddressCreate", field, val);
    }
    setSelectedAddressId(id) {
        if(id) {
            this.setState({addressSelectedId: id})
        }
    }
    setOpenStateModal(state) {
        if(state) {
            this.setState({openStateModal: true});
        } else {
            this.setState({openStateModal: false});
        }
    }
    render() {
        return (
            <DocumentTitle title="Chi tiết khách hàng">
                <CustomerDetail
                    {...this.props} onSubmit={this.submit.bind(this)}
                    onSubmitCustomerAddress={::this.submitCustomerAddress}
                    onSubmitUpdateCustomerAddress={::this.submitUpdateCustomerAddress}
                    customer={this.props.profile}
                    customerId={this.props.profileId}
                    banks={this.state.banks}
                    setFieldForm={::this.setFieldForm}
                    setFieldFormCustomerAddress={::this.setFieldFormCustomerAddress}
                    setFieldFormCreateCustomerAddress={::this.setFieldFormCreateCustomerAddress}
                    setSelectedAddressId={::this.setSelectedAddressId}
                    addressSelectedId={this.state.addressSelectedId}
                    setOpenStateModal={::this.setOpenStateModal}
                    openStateModal={this.state.openStateModal}
                />
            </DocumentTitle>
        );
    }


}

const mapStateToProps = (state) => {
    return {
        profileId: getProfileId(state),
        profile: getProfile(state),
        updatedCustomer: state.customer.updatedCustomer,
        error: state.customer.error,
        createdAddressSuccess: state.customer.createdAddressSuccess,
        updatedAddressSuccess: state.customer.updatedAddressSuccess,
        deleteAddressSuccess: state.customer.deleteAddressSuccess,
    }
};


export default connect(
    mapStateToProps, {getCustomerById, updateCustomer, changePassword, change, createAddress, updateAddress}
)(CustomerDetailContainer);
