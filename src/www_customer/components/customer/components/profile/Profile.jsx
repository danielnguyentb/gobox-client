import React from 'react';
import Link from '../../../../../www_components/Link'
import {MenuItem} from 'react-bootstrap'
import {Logout} from '../../components'
import avatar from './user1.png'

export default ({id, fullName, dispatch}) => {
    return (
        <div className="navbar-custom-menu">
            <ul className="nav navbar-nav">
                <li>
                    <Link to="/orders/create" className="btn btn-primary create-order"><i className="fa fa-plus mr-5"/>TẠO VẬN ĐƠN</Link>
                </li>
                <li className="dropdown user user-menu">
                    <Link to="/" className="dropdown-toggle" data-toggle="dropdown">
                        <img src={avatar} className="user-image" alt="User Image"/>
                        <span className="hidden-xs">{fullName}</span>
                    </Link>
                    <ul className="dropdown-menu profile-dropdown" role="menu">
                        <li><Link to={`/profile`}><i className="fa fa-info-circle"/>Trang
                            cá nhân</Link></li>
                        <MenuItem><Logout dispatch={dispatch} /></MenuItem>
                    </ul>
                </li>
            </ul>
        </div>
    )
}
