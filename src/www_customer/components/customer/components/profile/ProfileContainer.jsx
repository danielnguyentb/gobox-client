import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux'
import Profile from './Profile'
import {getProfile, getProfileId} from '../../selectors'
import {getCustomerById} from '../../actions'

class ProfileContainer extends Component {
    componentWillMount(){
        let {profileId, profile, getCustomerById} = this.props;
        if (profileId > 0 && (!profile || !profile.id)) {
            getCustomerById(profileId);
        }
    }

    render() {
        return (
            <Profile {...this.props.profile} dispatch={this.props.dispatch} />
        );
    }
}


const mapStateToProps = (state) => {
    return {
        profile: getProfile(state),
        profileId: getProfileId(state)
    }
};


export default connect(
    mapStateToProps, {getCustomerById}
)(ProfileContainer)