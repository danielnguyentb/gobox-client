import Customer from './api'
import {CALL_API} from '../../ActionTypes'
import * as types from './ActionTypes'
import {clearSession} from '../../helpers'

export const loginCustomerSuccess = (session) => ({
    type: types.AUTH_LOAD_SUCCESS,
    session
});

export const authClear = () => {
    clearSession();
    return {
        type: types.AUTH_CLEAR
    }
};

export const login = (credentials) => {
    return {
        type: CALL_API,
        payload: {
            types: [types.AUTH_LOGIN_REQUEST, types.AUTH_LOGIN_SUCCESS, types.AUTH_LOGIN_FAILURE],
            endpoint: '/customers/login',
            method: 'POST',
            body: JSON.stringify(credentials),
            form: 'UserLoginForm',
        }
    }
};

export const fetchApplicationValidation = () => {
    return {
        type: CALL_API,
        payload: {
            types: [types.APPLICATION_VALIDATION_REQUEST, types.APPLICATION_VALIDATION_SUCCESS, types.APPLICATION_VALIDATION_FAILURE],
            endpoint: '/applicationValidations',
            method: 'GET',
        }
    }
};

export const logoutSaga = () => {
    return {
        type: CALL_API,
        payload: {
            types: [types.AUTH_LOGOUT_REQUEST, types.AUTH_LOGOUT_SUCCESS, types.AUTH_LOGOUT_FAILURE],
            endpoint: '/customers/logout',
            method: 'POST'
        }
    }
};


export const changePasswordSaga = (data) => {
    return {
        type: CALL_API,
        payload: {
            types: [types.CUSTOMER_CHANGE_PASSWORD_REQUEST, types.CUSTOMER_CHANGE_PASSWORD_SUCCESS, types.CUSTOMER_CHANGE_PASSWORD_FAILURE],
            endpoint: `/customers/changePassword`,
            body: JSON.stringify({data}),
            method: 'PUT',
        }
    }
};

export const getListAddress = (id) => dispatch => dispatch(Customer.getListAddress(id));

export const createAddress = (data) => dispatch => dispatch(Customer.createAddress(data));

export const updateAddress = (data) => dispatch => dispatch(Customer.updateAddress(data));

export const deleteAddress = (data) => dispatch => dispatch(Customer.deleteAddress(data));

export const getAllCustomers = (filter) => {
    return dispatch => {
        dispatch(Customer.getCustomers(filter))
    }
};

export const getCustomerById = (customer_id) => {
    return dispatch => {
        dispatch(Customer.getCustomerById(customer_id))
    }
};

export const fetchCustomerByIdSaga = (id) => {
    return {
        type: CALL_API,
        payload: {
            types: [types.CUSTOMER_REQUEST, types.CUSTOMER_SUCCESS, types.CUSTOMER_FAILURE],
            endpoint: `/customers/${id}`,
            method: 'GET'
        }
    }
};

export const createCustomer = (customer) => {
    return dispatch => {
        dispatch(Customer.create(customer))
    }
};

export const updateCustomer = (customer) => {
    return dispatch => {
        dispatch(Customer.update(customer))
    }
};

export const changeEmailCustomer = (customer) => {
    return dispatch => {
        dispatch(Customer.changeEmail(customer))
    }
};

export const changePassword = (old_password, new_password) => {
    return (dispatch, getState) => {
        let data = {
            oldPassword: old_password,
            newPassword: new_password
        };

        dispatch(Customer.changePassword(data))
    }
};