/**
 * Created by Tester-Ali on 30-11-2016.
 */
import {takeEvery, takeLatest, delay} from 'redux-saga'
import {call, put, fork, select} from 'redux-saga/effects'
import * as ActionTypes from './ActionTypes'
import {replace, push} from 'react-router-redux'
import {saveSession} from '../../helpers'
import {TTL_VALUE} from './constants'
import {fetchApplicationValidation, authClear} from './actions'
import {tokenSelector} from './selectors'
import fetchApi from '../../helpers/callApi'
import {actions as tActions} from '../../../www_components/Toastr2'
import {startSubmit, stopSubmit} from 'redux-form'
import {parseErrors} from '../../helpers/reduxForm'
import {hide} from 'redux-modal'
import {getLocation} from '../../selectors'

function* loggedIn(action) {
    const session = action.response;
    if (session) {
        if (session.needChangePass == 1) {
            yield put(replace(`/change_pass`));
        } else {
            const expires = new Date();
            expires.setMonth(expires.getMonth() + 6);
            saveSession(session, session.ttl == TTL_VALUE ? expires : null);
            yield call(redirectToBack);
        }
    } else {
        yield put(replace('/login'))
    }
}

function* redirectToBack() {
    const location = yield select(getLocation);
    const r = location && location['query'] && location['query']['r'];
    yield put(replace(r || '/'));
}

function* loadAuth(action) {
    const {session} = action;
    if (session) {
        if (session.needChangePass == 1) {
            yield put(replace(`/change_pass`));
        } else {
            const expires = new Date();
            expires.setMonth(expires.getMonth() + 6);
            saveSession(session, session.ttl == TTL_VALUE ? expires : null);
        }
    } else {
        yield put(replace('/login'))
    }
}

function* loggedInFail(action) {
    yield put(fetchApplicationValidation());
    if (typeof window.grecaptcha !== "undefined") {
        window.grecaptcha.reset();
    }
}

function* changePass(action) {
    const {payload} = action;
    const form = 'ChangePassForm';
    const token = yield select(tokenSelector);
    const config = {
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': token,
        },
        method: 'PUT',
        body: JSON.stringify(payload),
    };
    try {
        yield put(startSubmit(form));
        const response = yield call(fetchApi, '/customers/changePassword', config);
        yield put({type: ActionTypes.CUSTOMER_CHANGE_PASSWORD_SUCCESS, response});
        yield put(stopSubmit(form));
        yield put(hide('ChangePassModal'));
        yield put(tActions.success(
            'Mật khẩu đã được đổi thành công',
            'Toàn bộ các phiên đăng nhập hiện tại sẽ không còn hiệu lực, Quý khách vui lòng đăng nhập lại trong ít giây nữa'
        ));
        yield put(authClear());
        yield delay(5000);
        yield put(push('/login'));
    } catch (error) {
        yield put({type: ActionTypes.CUSTOMER_CHANGE_PASSWORD_FAILURE, error});
        yield put(stopSubmit(form, parseErrors(error)))
    }
}

function* logout() {
    yield put(authClear());
    const location = yield select(getLocation);
    yield put(replace({
        pathname: '/login',
        query: {r: location && location.pathname}
    }));
}

export default function* startApp() {
    yield takeLatest(ActionTypes.AUTH_LOGIN_SUCCESS, loggedIn);
    yield takeLatest(ActionTypes.AUTH_LOAD_SUCCESS, loadAuth);
    yield takeEvery(ActionTypes.AUTH_LOGIN_FAILURE, loggedInFail);
    yield takeLatest(ActionTypes.CUSTOMER_CHANGE_PASSWORD_REQUEST, changePass);
    yield takeEvery(ActionTypes.AUTH_LOGOUT_REQUEST, logout)
}