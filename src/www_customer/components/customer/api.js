import Client from '../../helpers/Client'
import * as types from './ActionTypes'

class Customer extends Client {

    getProfile(){
        return this.callApi({
            types: [types.AUTH_PROFILE_REQUEST, types.AUTH_PROFILE_SUCCESS, types.AUTH_PROFILE_FAILURE],
            endpoint: `/customers/me`,
            method: 'GET',
        });
    }

    getCustomerById(customer_id) {
        return this.callApi({
            types: [types.CUSTOMER_REQUEST, types.CUSTOMER_SUCCESS, types.CUSTOMER_FAILURE],
            endpoint: `/customers/${customer_id}`,
        })
    }

    create(customer) {
        return this.callApi({
            types: [types.CUSTOMER_CREATE_REQUEST, types.CUSTOMER_CREATE_SUCCESS, types.CUSTOMER_CREATE_FAILURE],
            endpoint: '/customers',
            method: 'POST',
            form: 'registerForm',
            body: customer,
        })
    }

    update(customer) {
        return this.callApi({
            types: [types.CUSTOMER_UPDATE_REQUEST, types.CUSTOMER_UPDATE_SUCCESS, types.CUSTOMER_UPDATE_FAILURE],
            endpoint: `/customers/${customer.id}`,
            method: 'PUT',
            body: customer,
            form: "CustomerDetailForm"
        }, 'updatingCustomer')
    }

    changeEmail(customer) {
        return this.callApi({
            types: [types.CUSTOMER_UPDATE_REQUEST, types.CUSTOMER_UPDATE_SUCCESS, types.CUSTOMER_UPDATE_FAILURE],
            endpoint: `/customers/${customer.id}`,
            method: 'PUT',
            body: customer,
            form: "ModalChangeEmail"
        }, 'updatingCustomer')
    }

    changePassword(data) {
        return this.callApi({
            types: [types.CUSTOMER_CHANGE_PASSWORD_REQUEST, types.CUSTOMER_CHANGE_PASSWORD_SUCCESS, types.CUSTOMER_CHANGE_PASSWORD_FAILURE],
            endpoint: `/customers/changePassword`,
            body: data,
            method: 'PUT',
        })
    }

    getApplicationValidation(){
        return this.callApi({
            types: [types.APPLICATION_VALIDATION_REQUEST, types.APPLICATION_VALIDATION_SUCCESS, types.APPLICATION_VALIDATION_FAILURE],
            endpoint: '/applicationValidations',
            method: 'GET',
            authenticate: false,
        })
    }

    login(credentials) {
        return this.callApi({
            types: [types.AUTH_LOGIN_REQUEST, types.AUTH_LOGIN_SUCCESS, types.AUTH_LOGIN_FAILURE],
            endpoint: '/customers/login',
            method: 'POST',
            body: credentials,
            form: 'UserLoginForm',
            authenticate: false,
        })
    }

    logout() {
        return this.callApi({
            types: [types.AUTH_LOGOUT_REQUEST, types.AUTH_LOGOUT_SUCCESS, types.AUTH_LOGOUT_FAILURE],
            endpoint: '/customers/logout',
            method: 'POST'
        })
    }
    getListAddress(id) {
        return this.callApi(({
            types: [types.DELIVERY_ORDER_LIST_ADDRESS.REQUEST, types.DELIVERY_ORDER_LIST_ADDRESS.SUCCESS, types.DELIVERY_ORDER_LIST_ADDRESS.FAILURE],
            endpoint: `/customers/${id}/addresses`,
            method: 'GET',
        }))
    }
    createAddress(data) {
        return this.callApi(({
            types: [types.DELIVERY_ORDER_CREATE_ADDRESS.REQUEST, types.DELIVERY_ORDER_CREATE_ADDRESS.SUCCESS, types.DELIVERY_ORDER_CREATE_ADDRESS.FAILURE],
            endpoint: `/customers/${data.objectId}/addresses`,
            method: 'POST',
            body: data,
            form: "CustomerCreateAddressForm"
        }))
    }
    updateAddress(data) {
        return this.callApi(({
            types: [types.DELIVERY_ORDER_UPDATE_ADDRESS.REQUEST, types.DELIVERY_ORDER_UPDATE_ADDRESS.SUCCESS, types.DELIVERY_ORDER_UPDATE_ADDRESS.FAILURE],
            endpoint: `/customers/${data.objectId}/addresses/${data.id}`,
            method: 'PUT',
            body: data
        }))
    }
    deleteAddress(data) {
        return this.callApi(({
            types: [types.DELIVERY_ORDER_DELETE_ADDRESS.REQUEST, types.DELIVERY_ORDER_DELETE_ADDRESS.SUCCESS, types.DELIVERY_ORDER_DELETE_ADDRESS.FAILURE],
            endpoint: `/customers/${data.id}/addresses/${data.objectId}`,
            method: 'DELETE',
            body: data
        }))
    }
}

export default new Customer();