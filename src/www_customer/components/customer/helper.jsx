/**
 * Created by Tester-Ali on 31-10-2016.
 */
import React from 'react';
import * as RBT from 'react-bootstrap';
import zxcvbn from 'zxcvbn'
import ReactPasswordStrength from '../../../www_components/PasswordStrength'
import Localize from '../../../resources/Localize'
import {isNumber} from '../../helpers/validators'
import {UsernameValidator} from '../../helpers/UsernameValidator'

export const scoreFn = (value) => {
    let result = zxcvbn(value);
    if (value.length == 0) {
        result.feedback.warning = 'Mật khẩu không được để trống';
    } else if (value.length < 6 || /[A-Z]/.test(value) == false || /\d/.test(value) == false) {
        result.score = result.score > 0 ? 1 : 0;
        result.feedback.warning = 'Tối thiểu 6 ký tự, bao gồm chữ hoa và số';
    }
    return result;
};

export const loginValidate = values => {
    const errors = {};
    if(Object.keys(values).length === 0) return errors;
    if (!values.email) {
        errors.email = 'Tên đăng nhập không được để trống'
    }
    if (!values.password) {
        errors.password = 'Mật khẩu không được để trống';
    }
    return errors;
};

export const registerValidate = values => {
    const errors = {};
    if(Object.keys(values).length === 0) return errors;
    if (!values.username) {
        errors.username = 'Tên đăng nhập không được để trống'
    } else if (values.username.length < 3 && values.username.length >= 16) {
        errors.username = 'Tên đăng nhập cần ít nhất 3 đến 16 kí tự'
    } else if (!UsernameValidator.validate(values.username).isValid) {
        errors.username = UsernameValidator.validate(values.username).error
    }
    if (!values.email) {
        errors.email = 'Email không được để trống'
    } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
        errors.email = 'Email không hợp lệ'
    }
    if (!values.password) {
        errors.password = 'Mật khẩu không được để trống';
    } else if (scoreFn(values.password).score <= 2) {
        errors.password = Localize.t(scoreFn(values.password).feedback.warning);
    }

    if (values.password != values.confirmPassword) {
        errors.confirmPassword = 'Mật khẩu không khớp';
    }

    return errors
};

export const validate = values => {
    const errors = {};
    if(Object.keys(values).length === 0) return errors;
    if (values.date_birthday || values.month_birthday || values.year_birthday) {
        if (!values.date_birthday) {
            errors.date_birthday = 'Bạn chưa nhập ngày sinh.'
        } else if (!isNumber(values.date_birthday)) {
            errors.date_birthday = 'Ngày sinh phải là chữ số'
        }
        if (!values.month_birthday || values.month_birthday == "") {
            errors.month_birthday = 'Bạn chưa chọn tháng sinh.'
        }
        if (!values.year_birthday) {
            errors.year_birthday = 'Bạn chưa nhập năm sinh.'
        } else if (!isNumber(values.year_birthday)) {
            errors.year_birthday = 'Năm sinh phải là chữ số'
        }
    }
    if (values.year_birthday) {
        let date = new Date();
        if(values.year_birthday >= date.getFullYear() || values.year_birthday <= 0) {
            errors.year_birthday = 'Năm sinh không hợp lệ'
        }
    }
    if(values.month_birthday) {
        if(values.month_birthday <= 0) {
            errors.month_birthday = 'Tháng sinh không hợp lệ'
        }
    }
    if(values.date_birthday) {
        if(values.date_birthday <= 0 || values.date_birthday > 31) {
            errors.date_birthday = 'Ngày sinh không hợp lệ'
        }
    }
    if(values.bankName || values.bankBranch || values.bankAccountName || values.bankAccountNo) {
        if(!values.bankName) {
            errors.bankName = 'Bạn chưa chọn ngân hàng'
        }
        if(!values.bankAccountName) {
            errors.bankAccountName = 'Bạn chưa nhập tên chủ tài khoản'
        }
        if(!values.bankAccountNo) {
            errors.bankAccountNo = 'Bạn chưa nhập số tài khoản ngân hàng'
        }
    }
    // if(!values.mobile) {
    //     errors.mobile = 'Bạn chưa cung cấp số điện thoại liên hệ'
    // }
    if(values.mobile) {
        if(!isNumber(values.mobile)) {
            errors.mobile = 'Số diện thoại phải là chữ số'
        }
        if((values.mobile.length < 6) || (values.mobile.length > 12) ) {
            errors.mobile = 'Không phải là định dạng số điện thoại.'
        }
    }
    return errors
};

export const renderField = ({input, placeholder, label, autoFocus = false, type, className, note, meta: {touched, error, warning}}) => (
    <RBT.FormGroup validationState={touched && error ? 'error' : 'success'}>
        <input {...input} autoFocus={autoFocus} type={type} className={className} placeholder={placeholder || label}/>
        {error && touched ? <span className="help-block">{error}</span> :
            <small className="text-left text-note">{note}</small>}
    </RBT.FormGroup>
);

export const renderPasswordStrengthField = ({input, placeholder, label, type, className, meta: {touched, error, warning}}) => (
    <RBT.FormGroup className="style-strength-pass" validationState={touched && error ? 'error' : 'success'}>
        <ReactPasswordStrength
            minLength={6}
            minScore={2}
            scoreFn={scoreFn}
            scoreWords={['Yếu', 'Đơn giản quá', 'Tạm được', 'Tốt', 'Rất tốt']}
            inputProps={{...input, className, type, placeholder: placeholder || label, autoComplete: 'off'}}
        />
        {touched && ((error && <span className="help-block">{error}</span>) || (warning && <span>{warning}</span>))}
    </RBT.FormGroup>
);