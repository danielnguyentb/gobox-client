import React from 'react';
import {render} from 'react-dom';
import Root from './Root'

const rootElement = document.getElementById('app');
render(<Root />, rootElement);

if (module.hot) {
    module.hot.accept('./Root', () => {
        const NextApp = require('./Root').default;
        render(<NextApp />, rootElement);
    });
}