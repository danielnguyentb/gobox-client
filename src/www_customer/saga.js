/**
 * Created by Tester-Ali on 30-11-2016.
 */
import {takeEvery, takeLatest} from 'redux-saga'
import {call, put, fork, select} from 'redux-saga/effects'
import {LOCATION_CHANGE} from './ActionTypes'
import {actions as ReduxToastrActions} from 'react-redux-toastr'
import {actions as ReToastrActions} from 'cores/Toastr2'
import {CALL_API, UNAUTHORIZED_CODE} from './constants'
import fetchApi from './helpers/callApi'
import {parseErrors} from './helpers/reduxForm'
import {startSubmit, stopSubmit} from 'redux-form'
import {getToken, getSession} from './helpers'
import {push} from 'react-router-redux'
import {customer, balance} from './components'

function* resetToast(action) {
    yield put(ReduxToastrActions.clean());
    yield put(ReToastrActions.clean());
}

export function* callApi(action) {
    const {types, endpoint, schema, body, method = 'GET', headers, form, ...rest} = action.payload;
    const [ requestType, successType, failureType ] = types;
    const config = {
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            ...headers,
        },
        method, body: typeof body === 'string' ? body : JSON.stringify(body),
    };
    try {
        const token = yield select(customer.selectors.tokenSelector);
        config.headers.Authorization = token || getToken();
        yield put({type: requestType});
        if (form) {
            yield put(startSubmit(form))
        }
        const response = yield call(fetchApi, endpoint, config, schema);
        yield put({type: successType, response, ...rest});
        if (form) {
            yield put(stopSubmit(form))
        }
    } catch (error) {
        const {code, statusCode} = error;
        yield put({type: failureType, error});
        if (form) {
            yield put(stopSubmit(form, parseErrors(error)))
        }
        if(code == UNAUTHORIZED_CODE || statusCode == UNAUTHORIZED_CODE){
            yield put(customer.actions.authClear());
            yield put(push('/login'));
        }
    }
}

export default function* startApp() {
    const session = getSession();
    if(session && session.id){
        yield put(customer.actions.loginCustomerSuccess(session));
    }
    yield takeLatest(LOCATION_CHANGE, resetToast);
    yield takeEvery(CALL_API, callApi);
    yield fork(balance.saga);
    yield fork(customer.saga);
}