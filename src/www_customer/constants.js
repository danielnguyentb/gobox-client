export {LOCATION_CHANGE} from 'react-router-redux'
export const SESSION_KEY = 'SESSION_FRONTEND';
export const CALL_API = 'CALL_API';
export const RESOURCE_NOT_FOUND_CODE = 404;
export const UNAUTHORIZED_CODE = 401;
export const ACCESS_DENIED_CODE = 403;