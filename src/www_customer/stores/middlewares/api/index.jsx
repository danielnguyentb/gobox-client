import callApi from '../../../helpers/callApi'
import Localize from '../../../../resources/Localize'
import {CALL_API} from '../../../constants'
import {startSubmit, stopSubmit, reset} from 'redux-form'

// A Redux middleware that interprets actions with CALL_API info specified.
// Performs the call and promises when such actions are dispatched.
export default store => next => action => {
    const callAPI = action[CALL_API];
    if (typeof callAPI === 'undefined') {
        return next(action)
    }

    let {endpoint} = callAPI;
    let [formName = null, formReset = false] = [];
    const {schema, types, config, loading = 'loading', scope, form} = callAPI;
    if (Array.isArray(form)) {
        [formName, formReset] = form;
    } else {
        formName = form;
    }
    const request = config;

    if (typeof endpoint === 'function') {
        endpoint = endpoint(store.getState())
    }

    if (config && typeof config !== 'object') {
        throw new Error('Specify a object config.')
    }

    if (typeof endpoint !== 'string') {
        throw new Error('Specify a string endpoint URL.')
    }
    if (!schema) {
        //throw new Error('Specify one of the exported Schemas.')
    }
    if (!Array.isArray(types) || types.length !== 3) {
        throw new Error('History an array of three action ActionTypes.')
    }
    if (!types.every(type => typeof type === 'string')) {
        throw new Error('History action ActionTypes to be strings.')
    }

    function actionWith(data) {
        const finalAction = Object.assign({ajax: true}, action, data);
        delete finalAction[CALL_API];
        return finalAction;
    }

    let failureMessages = [];
    const [ requestType, successType, failureType ] = types;
    failureMessages[failureType] = null;
    next(actionWith({
        failureMessages,
        type: requestType, request, [loading]: true
    }));
    if (formName) {
        store.dispatch(startSubmit(formName))
    }

    return callApi(endpoint, config, schema).then(
        response => {
            if (formName) {
                setTimeout(() => {
                    store.dispatch(stopSubmit(formName));
                    formReset && store.dispatch(reset(formName));
                }, 100);
            }
            return next(actionWith({
                request, response, scope,
                type: successType,
                failureMessages,
                [loading]: false,
            }))
        },
        error => {
            if (formName) {
                setTimeout(() => {
                    store.dispatch(stopSubmit(formName, parseErrors(error)));
                }, 100);
            }
            let {message, statusCode, code, details} = error;
            failureMessages[failureType] = Localize.t(message);
            next(actionWith({
                [loading]: false,
                code: statusCode || code,
                request, scope,
                type: failureType,
                errors: error,
                error: message,
                failureMessages,
                details
            }));
        }
    )
}


export const parseErrors = errors => {
    const submitErrors = {};
    if (errors.details) {
        const {messages} = errors.details;
        Object.keys(messages).map(field => {
            submitErrors[field] = (Localize.t(messages[field][0]))
        });
    }
    if (Object.keys(submitErrors).length === 0) {
        submitErrors['_error'] = Localize.t(errors.message)
    }
    return submitErrors;
};
