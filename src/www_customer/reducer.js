import {routerReducer as routing} from 'react-router-redux'
import {loadingBarReducer as loadingBar} from 'react-redux-loading-bar'
import {reducer as toastr} from 'react-redux-toastr'
import {reducer as toastr2} from 'cores/Toastr2'
import {combineForms} from 'react-redux-form';
import {combineReducers} from 'redux'
import {RESOURCE_NOT_FOUND_CODE, ACCESS_DENIED_CODE} from './constants'
import {LOCATION_CHANGE} from './ActionTypes'
import {reducer as formReducer} from 'redux-form'
import * as alert from 'cores/Alert'
import {constants as touchableConstants, reducer as touchableReducer} from 'cores/Touchable'
import * as paginate from 'cores/RePagination'
import { reducer as modalReducer } from 'redux-modal'
import {order, location, balance, customer} from './components'

const appReducer = {
    [customer.constants.NAME]: customer.reducer,
    [order.constants.NAME]: order.reducer,
    [location.constants.NAME]: location.reducer,
};


const isAccessDenied = (state = false, {code, type}) => {
    if (code === ACCESS_DENIED_CODE) {
        return true;
    } else if (type === LOCATION_CHANGE) {
        return false;
    }
    return state;
};

const isNotFound = (state = false, {code, type, request}) => {
    if (code === RESOURCE_NOT_FOUND_CODE && request.method === 'GET') {
        return true;
    } else if (type === LOCATION_CHANGE) {
        return false;
    }
    return state;
};

const actionType = (state = null, {type, ajax = false}) => type;
const ajaxType = (state = null, action) => {
    const {type, ajax = false} = action;
    return ajax ? type : state;
};
const ajax = (state, {ajax = false}) => {
    if (typeof ajax === 'boolean') return ajax;
    return state;
};

const isLocationChanged = (state = null, action) => {
    return action.type === LOCATION_CHANGE;
};

export default combineReducers({
    ...appReducer,
    [balance.constants.NAME]: balance.reducer,
    [touchableConstants.NAME]: touchableReducer,
    routing, loadingBar, toastr, toastr2,
    [paginate.NAME]: paginate.reducer,
    [alert.NAME]: alert.reducer,
    form: formReducer,
    modal: modalReducer,
    deep: combineForms(appReducer, 'deep'),
    isAccessDenied, isNotFound, ajaxType, isLocationChanged, actionType, ajax,
});